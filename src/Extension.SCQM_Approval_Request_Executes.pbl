﻿@display "No"


@execute "Stage2_Org_Submission_Custom_Executes"
@comment
Custom Executes to be run on SUBMISSION of the Org Stage 2 Worksheet.
@end-comment

@end-execute


@comment
Custom Executes to be run on COMPLETION and APPROVAL of the Org Stage 2 Worksheet.
Copy the details about Facilities to the product's expected Loc_List location
Set the flag indicating that it is now time to create the necessary Stage2 Org Loc Worksheets (if necessary).
@end-comment
@execute "Stage2_Org_Finish_Approve_Custom_Executes"

@end-execute

@comment
Custom Executes to be run on REJECTION of the Org Stage 2 Worksheet.
Copy the details about Facilities to the product's expected Loc_List location
Set the flag indicating that it is now time to create the necessary Stage2 Org Loc Worksheets (if necessary).
@end-comment
@execute "Stage2_Org_Finish_Reject_Custom_Executes"

@end-execute




@execute "Stage2_OrgLoc_Finish_Custom_Executes"
@comment
Custom Executes to be run on COMPLETION of the Org Loc Stage 2 Worksheet.
@end-comment

@end-execute


@execute "Stage3_OrgLoc_Finish_Custom_Executes"
@comment
Custom Executes to be run on COMPLETION of the Org Loc Stage 3 Worksheet.
@end-comment

@end-execute


@execute "Final_Approval_Custom_Executes"
@comment
Custom Executes to be run on FINAL APPROVAL of the entire Approval Request.
REMEMBER: This will be in the context of the SCQM_Approval_Request store
====================================
NOTE: This is where you will call all entity constructors in the customer-specific workbooks.
This includes the construction of the Organization node, all Organization Location nodes,
as well as all Contact, User, and Data Firewall nodes.
These nodes will be used to create the entities approved in the Approval Request (when required).
This is NOT used for the creation of the APL record(s) when needed.
====================================
@end-comment

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Workbook_Stage2.Create_New_Org_Node"
        @invoke-store-id "call('SCQM.AR.GetOrgRequestStoreId', store-id())"
        @invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage2_Org) and /store/SCQM_Approval_Request/Org_Status/Status_Code = 'AR_Org_Legally_Approved'"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Workbook_Stage2.Create_New_User_Nodes"
        @invoke-store-id "call('SCQM.AR.GetOrgRequestStoreId', store-id())"
        @invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage2_Org) and /store/SCQM_Approval_Request/Org_Status/Status_Code = 'AR_Org_Legally_Approved'"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Workbook_Stage2.Create_New_Contact_Nodes"
        @invoke-store-id "call('SCQM.AR.GetOrgRequestStoreId', store-id())"
        @invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage2_Org) and /store/SCQM_Approval_Request/Org_Status/Status_Code = 'AR_Org_Legally_Approved'"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Workbook_Stage2.Create_New_Document_Nodes"
        @invoke-store-id "call('SCQM.AR.GetOrgRequestStoreId', store-id())"
        @invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage2_Org) and /store/SCQM_Approval_Request/Org_Status/Status_Code = 'AR_Org_Legally_Approved'"

    @@invoke
        @@invoke-execute "SCQM_Approval_Request_Organization_Workbook_Stage3.Create_New_Document_Nodes"
        @@invoke-store-id "call('SCQM.AR.GetOrgRequestStoreId', store-id())"
        @@invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage3_Org) and /store/SCQM_Approval_Request/Org_Status/Status_Code = 'AR_Org_Legally_Approved'"

    @invoke
        @invoke-execute "Org_Loc_Entity_Creation_Loop_Stage_2"
        @invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage2_Org_Loc) and /store/SCQM_Approval_Request/OrgLoc_Status/Status_Code = 'AR_OrgLoc_Facility_Certified'"

    @@invoke
        @@invoke-execute "Org_Loc_Entity_Creation_Loop_Stage_3"
        @@invoke-when "is-not-blank(/store/SCQM_Approval_Request/Request_Type/Include_Stage3_Org_Loc) and /store/SCQM_Approval_Request/OrgLoc_Status/Status_Code = 'AR_OrgLoc_Facility_Certified'"

@end-execute

@execute "Org_Loc_Entity_Creation_Loop_Stage_2"
    @execute-select "call('SCQM.AR.GetApprovedOrgLocs', store-id())/item"
    @execute-select-key "StoreId"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Location_Workbook_Stage2.Create_New_Org_Loc_Node"
        @invoke-store-id "execute-select-context()/StoreId"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Location_Workbook_Stage2.Create_New_User_Nodes"
        @invoke-store-id "execute-select-context()/StoreId"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Location_Workbook_Stage2.Create_New_Contact_Nodes"
        @invoke-store-id "execute-select-context()/StoreId"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Location_Workbook_Stage2.Create_New_Document_Nodes"
        @invoke-store-id "execute-select-context()/StoreId"

@end-execute

@comment

@execute "Org_Loc_Entity_Creation_Loop_Stage_3"
    @execute-select "call('SCQM.AR.GetApprovedOrgLocs', store-id())/item"
    @execute-select-key "StoreId"

    @invoke
        @invoke-execute "SCQM_Approval_Request_Organization_Location_Workbook_Stage3.Create_New_Document_Nodes"
        @invoke-store-id "execute-select-context()/StoreId"

@end-execute


[[Extension.SCQM_Approval_Request_Executes.Contacts]]
[[Extension.SCQM_Approval_Request_Executes.Custom_Fields]]
[[Extension.SCQM_Approval_Request_Executes.Org_and_OrgLoc_Creation]]

@end-comment
