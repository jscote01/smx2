﻿@title "^^Packaging|Packaging^^"
    @title-ui-style "primary large margin-tb-mini"

@mode "Other"

@block
    @block-ui-style "well well-mini well-info-light inverse"
    @icon "icon-info-sign info medium"
    {label(margin-lr-mini medium): '^^Spec_Packaging_Note|Specification Note: When you are inputting the packaging, add them in order from closest (directly in contact) to the food to furthest from the food^^.'}
@end-block

@newline "Medium"

@block
    @block-ui-style "form-horizontal"
    
    @choice "System_Of_Measurement"
        @choice-style "RadioButtonList"
        @choice-header "^^System of Measurement|System of Measurement^^:"
        @choice-default-expression "'English'"
        @choice-columns "2"

        @option
            @option-text "^^English|English^^"
            @option-value "English"
        @option
            @option-text "^^Metric|Metric^^"
            @option-value "Metric"
        
    @end-choice

@end-block

@list "Packaging"
    @list-header "^^Layers of Packaging|Layers of Packaging^^:"
    @list-header-ui-style "medium info block"
    @list-allow-order "No"
    
    @block
        @block-ui-style "clear span6"
        
        @choice "Package_Type"
            @choice-style "DropDownLookup"
            @choice-header "^^Type of Package|Type of Package^^:"
            @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
            @choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
            @choice-field "Item_Name"
            @choice-filter "Dropdown_Group_ID = 'Package Type'"
            @choice-ui-style "width-auto"
            @choice-translate "Yes"
        
    @end-block
    
    @block
        @block-ui-style "span6"
        @block-visible-when "Package_Type/Item_Name = 'Other'"
        
        @text "Package_Type_Other"
            @text-header "^^Please Explain|Please Explain^^:"
            @text-rows "5"
    
    @end-block
    
    @block
        @block-ui-style "clear span6"
        
        @choice "Package_Material"
            @choice-style "DropDownLookup"
            @choice-header "^^Material|Material^^:"
            @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
            @choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
            @choice-field "Item_Name"
            @choice-filter "Dropdown_Group_ID = 'Package Material'"
            @choice-ui-style "width-auto"
            @choice-translate "Yes"
    
    @end-block
    
    @block
        @block-ui-style "span6"
        @block-visible-when "Package_Material/Item_Name = 'Other'"
        
        @text "Package_Material_Other"
            @text-header "^^Please Explain|Please Explain^^:"
            @text-rows "5"
    
    @end-block
    
    @block
        @block-ui-style "clear span6"
        
        @choice "Package_Closure"
            @choice-style "DropDownLookup"
            @choice-header "^^Enclosure|Enclosure^^:"
            @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
            @choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
            @choice-field "Item_Name"
            @choice-filter "Dropdown_Group_ID = 'Package Closure'"
            @choice-ui-style "width-auto"
            @choice-translate "Yes"
    
    @end-block
    
    @block
        @block-ui-style "span6"
        @block-visible-when "Package_Closure/Item_Name = 'Other'"
        
        @text "Package_Closure_Other"
            @text-header "^^Please Explain|Please Explain^^:"
            @text-rows "5"
    
    @end-block
    
    @block
        @block-ui-style "clear span6"
        
        @choice "Tamper_Evident"
            @choice-style "RadioButtonList"
            @choice-columns "2"
            @choice-header "^^Is Tamper Evident?|Is Tamper Evident?^^"
            @option
                @option-text "^^Yes|Yes^^"
                @option-value "Yes"
            @option
                @option-text "^^No|No^^"
                @option-value "No"
        @end-choice

    @end-block
    
    @newline "Small"
    
    @list "Photos_List"
        @list-optional "Yes"
        @list-allow-order "No"        
        @list-sort "Ascending"
        @list-style "Grid"
        @list-header "^^Photos|Photos^^:"
        @list-options "HeaderActions[ShowAdd:false, ShowDelete:false]"
       
        @file "Photo_File"
        @file-optional "Yes"
   
        @text "Photo_Description"
            @text-header "^^Photo Description|Photo Description^^:"
            @text-rows "5"
            @text-optional "Yes"   
    @end-list

@newline "Small"

@end-list

@block
    @block-ui-style "form-horizontal"
    
    @newline "Medium"
    
    {label(medium info block): '^^Weight Information Per Each Item|Weight Information Per Each Item^^'}
    
    @newline "Small"
    
    @block
        @block-visible-when "System_Of_Measurement = 'English' or is-blank(System_Of_Measurement)"
        @block-ui-style "clear span6"
        
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}{label(required-label): '*'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|-
        {label(small bold block): '^^Ounces|Ounces^^'}
|
        @text "Serving_Target_Weight"
            @text-header "^^Target Weight|Target Weight^^:"
            @text-type "Number"
            @text-columns "20"
            @text-header-ui-style "hide"
            @text-validation-test "(is-blank(Serving_Min_Weight) or Serving_Target_Weight >= Serving_Min_Weight) and (is-blank(Serving_Max_Weight) or Serving_Target_Weight <= Serving_Max_Weight)"
            @text-validation-error "^^Target_Validation_Message|Target must be between Min and Max^^"  
|
        @text "Serving_Min_Weight"
            @text-header "^^Min Weight|Min Weight^^:"
            @text-type "Number"
            @text-optional "Yes"
            @text-columns "20"
            @text-header-ui-style "hide"
            @text-validation-test "is-blank(Serving_Min_Weight) or (Serving_Min_Weight <= Serving_Target_Weight and Serving_Min_Weight <= Serving_Max_Weight)"
            @text-validation-error "^^Min_Validation_Message|Min must be less than or equal to Target^^"
|
        @text "Serving_Max_Weight"
            @text-header "^^Max Weight|Max Weight^^:"
            @text-type "Number"
            @text-columns "20"
            @text-optional "Yes"
            @text-header-ui-style "hide"
            @text-validation-test "Serving_Max_Weight >= Serving_Target_Weight and (is-blank(Serving_Min_Weight) or Serving_Max_Weight >= Serving_Min_Weight)"
            @text-validation-error "^^Max_Validation_Message|Max must be greater than or equal to Target^^"
|
|-
        {label(small bold block): '^^Grams|Grams^^'}
|
        @text "Serving_Target_Weight_G"
            @text-header "^^Target Weight|Target Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-expression "if(is-not-blank(Serving_Target_Weight),call('Core.ConvertFrom', 'oz','g', coalesce(Serving_Target_Weight, 0)),0)"
            @text-expression-when "is-changed(Serving_Target_Weight)"
            @text-columns "20"
            @text-read-only-when "true()"
|
        @text "Serving_Min_Weight_G"
            @text-header "^^Min Weight|Min Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-optional "Yes"
            @text-expression "if(is-not-blank(Serving_Min_Weight), call('Core.ConvertFrom', 'oz', 'g', coalesce(Serving_Min_Weight, 0)), 0)"
            @text-expression-when "is-changed(Serving_Min_Weight)"
            @text-columns "20"
            @text-read-only-when "true()"
|
        @text "Serving_Max_Weight_G"
            @text-header "^^Max Weight|Max Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-optional "Yes"
            @text-expression "if(is-not-blank(Serving_Max_Weight), call('Core.ConvertFrom', 'oz', 'g', coalesce(Serving_Max_Weight, 0)), 0)"
            @text-expression-when "is-changed(Serving_Max_Weight)"
            @text-columns "20"
            @text-read-only-when "true()"
|}
    @end-block
    
    @block
        @block-visible-when "System_Of_Measurement = 'Metric'"
        @block-ui-style "clear span6"
        
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}{label(required-label): '*'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|- 
        {label(small bold block): '^^Grams|Grams^^'}
|
        @text "Serving_Target_Weight_G"
            @text-header "^^Target Weight|Target Weight^^:"
            @text-type "Number"
            @text-columns "20"
            @text-header-ui-style "hide"
            @text-validation-test "(is-blank(Serving_Min_Weight_G) or Serving_Target_Weight_G >= Serving_Min_Weight_G) and (is-blank(Serving_Max_Weight_G) or Serving_Target_Weight_G <= Serving_Max_Weight_G)"
            @text-validation-error "^^Target_Validation_Message|Target must be between Min and Max^^"  
|
        @text "Serving_Min_Weight_G"
            @text-header "^^Min Weight|Min Weight^^:"
            @text-type "Number"
            @text-optional "Yes"
            @text-columns "20"
            @text-header-ui-style "hide"
            @text-validation-test "is-blank(Serving_Min_Weight_G) or (Serving_Min_Weight_G <= Serving_Target_Weight_G and Serving_Min_Weight_G <= Serving_Max_Weight_G)"
            @text-validation-error "^^Min_Validation_Message|Min must be less than or equal to Target^^"
|
        @text "Serving_Max_Weight_G"
            @text-header "^^Max Weight|Max Weight^^:"
            @text-type "Number"
            @text-columns "20"
            @text-optional "Yes"
            @text-header-ui-style "hide"
            @text-validation-test "Serving_Max_Weight_G >= Serving_Target_Weight_G and (is-blank(Serving_Min_Weight_G) or Serving_Max_Weight_G >= Serving_Min_Weight_G)"
            @text-validation-error "^^Max_Validation_Message|Max must be greater than or equal to Target^^"
|
|-
        {label(small bold block): '^^Ounces|Ounces^^'}
|
        @text "Serving_Target_Weight"
            @text-header "^^Target Weight|Target Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-expression "if(is-not-blank(Serving_Target_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Target_Weight_G, 0)), 0)"
            @text-expression-when "is-changed(Serving_Target_Weight_G)"
            @text-columns "20"
            @text-read-only-when "true()"
|
        @text "Serving_Min_Weight"
            @text-header "^^Min Weight|Min Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-optional "Yes"
            @text-expression "if(is-not-blank(Serving_Min_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Min_Weight_G, 0)), 0)"
            @text-expression-when "is-changed(Serving_Min_Weight_G)"
            @text-columns "20"
            @text-read-only-when "true()"
|
        @text "Serving_Max_Weight"
            @text-header "^^Max Weight|Max Weight^^:"
            @text-header-ui-style "hide"
            @text-type "Number"
            @text-expression "if(is-not-blank(Serving_Max_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Max_Weight_G, 0)), 0)"
            @text-expression-when "is-changed(Serving_Max_Weight_G)"
            @text-columns "20"
            @text-read-only-when "true()"
|}
    @end-block
    
    @newline "Medium"
    
    {label(medium info block): '^^General Packaging Information|General Packaging Information^^'}
    
    @newline "Small"
    
    @block
        @block-ui-style "clear span6"
        
        @text "Serving_Portions_Inner"
            @text-header "^^Each Items per Inner Package|Each Items per Inner Package^^:"
            @text-type "Integer"
        
        @text "Inner_Pkg_Per_Box"
            @text-header "^^Inner Packages per Box|Inner Packages per Box^^:"
            @text-type "Integer"
            @text-footer "^^If the Inner Package is the Box, enter '1'|If the Inner Package is the Box, enter '1'^^"
        
        @block
            @block-visible-when "is-not-blank(Serving_Portions_Inner) and is-not-blank(Inner_Pkg_Per_Box)"
        
            @label "Serving_Portions_Per_Box"
                @label-header "^^Each Items per Box|Each Items per Box^^:"
        
            @block
                @block-visible-when "false()"
                
                @text "Serving_Portions_Per_Box"
                    @text-header "^^Each Items per Box|Each Items per Box^^ (^^CALC|CALC^^):"
                    @text-expression "if(is-not-blank(Serving_Portions_Inner) and is-not-blank(Inner_Pkg_Per_Box), Serving_Portions_Inner * Inner_Pkg_Per_Box, Serving_Portions_Per_Box)"
                    @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box)"
            
            @end-block
    
            @block
                @block-visible-when "is-not-blank(Serving_Target_Weight) and (System_Of_Measurement = 'English' or is-blank(System_Of_Measurement))"
        
                @newline "Medium"
                
                {label(medium info block): '^^Net Weight Per Box|Net Weight Per Box^^'}
                
                @newline "Small"
    
{|class="table table-naked"
|- 
|
|               {label(small bold): '^^Target Weight|Target Weight^^'}
|               {label(small bold): '^^Min Weight|Min Weight^^'}
|               {label(small bold): '^^Max Weight|Max Weight^^'}
|-
                {label(small bold): '^^Pounds|Pounds^^'}
|
                @label "Target_Weight_Per_Box"
                    @label-header "^^Target Weight|Target Weight^^:"
                    @label-header-ui-style "hide"
    
                @block
                    @block-visible-when "is-not-blank(Serving_Min_Weight)"
|
                    @label "Min_Weight_Per_Box"
                        @label-header "^^Min Weight|Min Weight^^:"
                        @label-header-ui-style "hide"
                    
                @end-block
    
                @block
                    @block-visible-when "is-not-blank(Serving_Max_Weight)"
|
                    @label "Max_Weight_Per_Box"
                        @label-header "^^Max Weight|Max Weight^^:"
                        @label-header-ui-style "hide"
            
                @end-block
|
|-
                {label(small bold): '^^Kilograms|Kilograms^^'}
|
                @label "Target_Weight_Per_Box_Kg"
                    @label-header "^^Target Weight|Target Weight^^:"
                    @label-header-ui-style "hide"
    
                @block
                    @block-visible-when "is-not-blank(Serving_Min_Weight)"
|
                    @label "Min_Weight_Per_Box_Kg"
                        @label-header "^^Min Weight|Min Weight^^:"
                        @label-header-ui-style "hide"
        
                @end-block
            
                @block
                    @block-visible-when "is-not-blank(Serving_Max_Weight)"
|
                    @label "Max_Weight_Per_Box_Kg"
                        @label-header "^^Max Weight|Max Weight^^:"
                        @label-header-ui-style "hide"
    
                @end-block
|}
            @end-block
    
        @end-block
    
        @block
            @block-visible-when "is-not-blank(Serving_Target_Weight_G) and System_Of_Measurement = 'Metric'"
        
            @newline "Medium"
            
            {label(medium info block): '^^Net Weight Per Box|Net Weight Per Box^^'}
            
            @newline "Small"
    
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|- 
        {label(small bold  block): '^^Kilograms|Kilograms^^'}
|
            @label "Target_Weight_Per_Box_Kg"
                @label-header "^^Target Weight|Target Weight^^:"
                @label-header-ui-style "hide"
            
            @block
                @block-visible-when "is-not-blank(Serving_Min_Weight_G)"
|
                @label "Min_Weight_Per_Box_Kg"
                    @label-header "^^Min Weight|Min Weight^^:"
                    @label-header-ui-style "hide"
        
            @end-block
|
            @label "Max_Weight_Per_Box_Kg"
                @label-header "^^Max Weight|Max Weight^^:"
                @label-header-ui-style "hide"
|
|-
        {label(small bold): '^^Pounds|Pounds^^'}
|
            @label "format(Target_Weight_Per_Box,'Number', '{0:N2}')"
                @label-header "^^Target Weight|Target Weight^^:"
                @label-header-ui-style "hide"
    
            @block
                @block-visible-when "is-not-blank(Serving_Min_Weight)"
|
                @label "format(Min_Weight_Per_Box,'Number', '{0:N2}')"
                    @label-header "^^Min Weight|Min Weight^^:"
                    @label-header-ui-style "hide"
    
            @end-block
|
            @label "format(Max_Weight_Per_Box,'Number', '{0:N2}')"
                @label-header "^^Max Weight|Max Weight^^:"
                @label-header-ui-style "hide"
|}

        @end-block
    
    @block
        @block-visible-when "false()"
        
        @text "Target_Weight_Per_Box"
            @text-header "^^Net Target Weight per Box|Net Target Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Target_Weight), Serving_Portions_Per_Box * Serving_Target_Weight div 16, Target_Weight_Per_Box)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight) or is-changed(Serving_Target_Weight_G)"
            @text-type "Number"
        
        @text "Min_Weight_Per_Box"
            @text-header "^^Net Min Weight per Box|Net Min Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Min_Weight),Serving_Portions_Per_Box * Serving_Min_Weight div 16, 0)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Min_Weight) or is-changed(Serving_Min_Weight_G)"
            @text-type "Number"
            @text-optional "Yes"
        
        @text "Max_Weight_Per_Box"
            @text-header "^^Net Max Weight per Box|Net Max Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Max_Weight),Serving_Portions_Per_Box * Serving_Max_Weight div 16, 0)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Max_Weight)or is-changed(Serving_Max_Weight_G)"
            @text-type "Number"
            @text-optional "Yes"
        
        @text "Target_Weight_Per_Box_Kg"
            @text-header "^^Net Target Weight per Box|Net Target Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Target_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Target_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight) or is-changed(Serving_Target_Weight_G)"
            @text-type "Number"
        
        @text "Min_Weight_Per_Box_Kg"
            @text-header "^^Net Min Weight per Box|Net Min Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Min_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Min_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Min_Weight) or  is-changed(Serving_Min_Weight_G)"
            @text-type "Number"
            @text-optional "Yes"
        
        @text "Max_Weight_Per_Box_Kg"
            @text-header "^^Net Max Weight per Box|Net Max Weight per Box^^ (^^CALC|CALC^^):"
            @text-expression "if(is-not-blank(Max_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Max_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
            @text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Max_Weight) or is-changed(Serving_Max_Weight_G)"
            @text-type "Number"
            @text-optional "Yes"
        
    @end-block
    
@end-block
    
@block
    @block-ui-style "span6"
    
    @text "Boxes_Per_Case"
        @text-header "^^Boxes Per Case|Boxes Per Case^^:"
        @text-type "Integer"
    
    @block
        @block-visible-when "is-not-blank(Boxes_Per_Case) and is-not-blank(Target_Weight_Per_Box)"
        
        @label "format(Case_Weight, 'Number', '{0:N2}')"
            @label-header "^^Case Weight|Case Weight^^ (^^lbs|lbs^^):"
        
        @label "Case_Weight_Kg"
            @label-header "^^Case Weight|Case Weight^^ (^^kg|kg^^):"
        
        @block
            @block-visible-when "false()"
        
            @text "Case_Weight"
                @text-header "^^Case Weight|Case Weight^^ (^^CALC|CALC^^):"
                @text-expression "if(is-not-blank(Boxes_Per_Case) and is-not-blank(Target_Weight_Per_Box), Boxes_Per_Case * Target_Weight_Per_Box, Case_Weight)"
                @text-expression-when "is-changed(Boxes_Per_Case) or is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight)"
                @text-type "Number"
            
            @text "Case_Weight_Kg"
                @text-header "^^Case Weight|Case Weight^^ (^^CALC|CALC^^):"
                @text-expression "if(is-not-blank(Case_Weight), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Case_Weight, 0)), 'Number', '{0:N2}'), 0)"
                @text-expression-when "is-changed(Boxes_Per_Case) or is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight)"
                @text-type "Number"
            
        @end-block
        
        @end-block
        
    @end-block
    
@end-block
