﻿@comment
==============================================================================
APPROVED PRODUCT LIST FUNCTIONS
==============================================================================
@end-comment

@comment
Return a comma-delimited list of the Subprocess Ids (integers) that are attached to the given APL record.
    #param StoreId $APL_StoreId of the APL record
    #return String Comma-delimited list of the Subprocess Ids
    EX: call('SCQM.APL.SubprocessIdList', store-id())
    RETURN EX: 1, 2, 3, 4
    NOTE: Starting with an empty string '' to prevent SQL error if no Subprocess_Ids are returned.
@end-comment
@function "SCQM.APL.SubprocessIdList"
@parameter "$APL_StoreId"
join(concat(char(39), char(39)), queue('SCQM_Approved_Product_List_Subprocess_Pairing.Master_Q', concat('APL = ', char(39), $APL_StoreId, char(39)), 'Subprocess_Id', 0, 0, 'Subprocess_Id', false())/item/Subprocess_Id, ', ')
@end-function

@comment
Check if any Subprocess has been started
  #param StoreId $APL_StoreId StoreId of the APL from SCQM_Approved_Product_List
  #return Boolean True if any subprocess has a Status
@end-comment
@function "SCQM.APL.AnySubProcessStarted"
@parameter "$APL_StoreId"
queue-count('SCQM_Approved_Product_List_Subprocesses.Master_Q', concat('Subprocess_Id IN (', call('SCQM.APL.SubprocessIdList', $APL_StoreId),') and Status is not null')) > 0
@end-function

@comment
The following returns true if the given APL record has all its subprocesses in a state where Conditional Approval is allowed.
All subprocesses for the APL type must be CA Ready
APL must not yet be approved (conditionally or finally)
There must be exactly one active APL
Corresponding Specification must be Finalized
    #param StoreId $APL_StoreId of the APL record
    #return Boolean True if all Subprocesses found are ready for Conditional Approval
    EX: call('SCQM.APL.APLRecord_ReadyforCondApproval', store-id())
@end-comment
@function "SCQM.APL.APLRecord_ReadyforCondApproval"
@parameter "$APL_StoreId"
queue-count('SCQM_Approved_Product_List_Subprocesses.Master_Q', concat('Subprocess_Id IN (', call('SCQM.APL.SubprocessIdList', $APL_StoreId), ') and Subprocess_CA_Ready is null')) = 0 
and 
(
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_Draft' or 
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_In_Process'
) 
and store($APL_StoreId)/*[1]/Spec_Status_Finalized = 1
and call('SCQM.APLGroup.GetNonDiscontinuedCountriesCount', store($APL_StoreId)/*[1]/APL_Group/@refId) = 1
@end-function

@comment
The following returns true if the given APL record has all its subprocesses in a state where Approval is allowed.
All subprocesses for the APL type must be ready for final approval (marked completed)
APL must not yet be finally approved
Corresponding Specification must be Finalized
    #param StoreId $APL_StoreId of the APL record
    #return Boolean True if all Subprocesses found are ready for Approval
    EX: call('SCQM.APL.APLRecord_ReadyforApproval', store-id())
@end-comment
@function "SCQM.APL.APLRecord_ReadyforApproval"
@parameter "$APL_StoreId"
queue-count('SCQM_Approved_Product_List_Subprocesses.Master_Q', concat('Subprocess_Id IN (', call('SCQM.APL.SubprocessIdList', $APL_StoreId),') and Subprocess_Completed is null')) = 0 and 
(
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_Draft' or 
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_In_Process' or 
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_Conditionally_Approved'
) and store($APL_StoreId)/*[1]/Spec_Status_Finalized = 1
@end-function

@comment
The following returns true if the given APL record has all its subprocesses in a state where Conditional Approval or Approval is allowed BUT the Specification is still not Finalized
    #param StoreId $APL_StoreId of the APL record
    #return Boolean True if all Subprocesses found are ready for Conditional Approval or Approval but Specification is not Finalized
    EX: call('SCQM.APL.APLRecord_ReadyforApproval', store-id())
@end-comment
@function "SCQM.APL.APLRecord_ReadyforApprovalsExceptSpecification"
@parameter "$APL_StoreId"
queue-count('SCQM_Approved_Product_List_Subprocesses.Master_Q', concat('Subprocess_Id IN (', call('SCQM.APL.SubprocessIdList', $APL_StoreId),') and Subprocess_Completed is null')) = 0 and 
(
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_Draft' or 
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_In_Process' or 
    store($APL_StoreId)/*[1]/Status/Status_Code = 'APL_Conditionally_Approved'
) and store($APL_StoreId)/*[1]/Spec_Status_Finalized != 1
@end-function

@comment
Get the current APL_Group for the given Location and Specification
  #param StoreId $Location Store ID of the Location
  #param StoreId $Specification Store ID of the Specification
  #return boolean True if a group already exists
@end-comment
@function "SCQM.APL.HasActiveGroup"
@parameter "$Location"
@parameter "$Specification"
queue-count('SCQM_Approved_Product_List_Group.Master_Q', concat('Org_Location = ', char(39), $Location, char(39), ' and Specification = ', char(39), $Specification, char(39), ' and Status_Code not in (', char(39), 'APL_Discarded', char(39), ', ', char(39), 'APL_Discontinued', char(39), ')')) > 0
@end-function

@comment
Get the StoreId of the current APL_Group for the given Location and Specification
  #param StoreId $Location Store ID of the Location
  #param StoreId $Specification Store ID of the Specification
  #return boolean True if a group already exists
@end-comment
@function "SCQM.APL.ActiveGroupStoreId"
@parameter "$Location"
@parameter "$Specification"
find-store-id('SCQM_Approved_Product_List_Group.Master_Q', concat('Org_Location = ', char(39), $Location, char(39), ' and Specification = ', char(39), $Specification, char(39), ' and Status_Code not in (', char(39), 'APL_Discarded', char(39), ', ', char(39), 'APL_Discontinued', char(39), ')'), 'APL_Group_Id')
@end-function

@comment
Check if the given APL Group and Country already have an APL record created
  #param StoreId $APLGroup Store ID of the APL Group
  #param StoreId $Country Store ID of the Country
  #return boolean True if the given APL Group and Country are already contained in an APL record
@end-comment 
@function "SCQM.APL.ExistingGroupCountry"
@parameter "$APLGroup"
@parameter "$Country"
queue-count('SCQM_Approved_Product_List.Master_Q', concat('APL_Group = ', char(39), $APLGroup, char(39), ' and Country = ', char(39), $Country, char(39), ' and Status_Code not in (', char(39), 'APL_Discarded', char(39), ', ', char(39), 'APL_Discontinued', char(39), ')')) > 0
@end-function

@comment
Is the current user the APL Relationship Approver.
  EX: call('SCQM.APL.IsRelationshipApprover', store-id())
@end-comment
@function "SCQM.APL.IsRelationshipApprover"
@parameter "$APL_Id"
boolean(call('Core.IsITUser')) or boolean(user()/User_Name = call('SCQM.GetReviewerUserName', store($APL_Id)/*[1]/APL_Group/@refId, 'SCQM_APL_Relationship_Approver'))
@end-function

@comment
Is the current user the APL Relationship Approver given the APL Group StoreId.
  EX: call('SCQM.APL.IsRelationshipApprover_byAPLGroup', store-id())
@end-comment
@function "SCQM.APL.IsRelationshipApprover_byAPLGroup"
@parameter "$APLGroup_Id"
boolean(call('Core.IsITUser')) or boolean(user()/User_Name = call('SCQM.GetReviewerUserName', $APLGroup_Id, 'SCQM_APL_Relationship_Approver'))
@end-function

@comment
== USED FOR NOTIFICATIONS FROM SCQM_Approved_Product_List ==
Get the contact email address for the Organization Location affiliated with a given Approved Product List store
  #param StoreId $APLStoreId
  #return String Comma-separated list of email addresses
@end-comment
@function "SCQM.APL.GetOrgLocContactEmail"
@parameter "$APLStoreId"
join(queue('Core_Contact_Locations.All', concat('Core_Organization_Location = ', char(39), store($APLStoreId)/*[1]/Org_Location/@refId, char(39)))/item/Email, ',')
@end-function


@comment
Return the number of days until the Conditional Approval expires.
  #param Date $ExpireDate
  #return String Number of days
  EX: call('SCQM.APL.DaysUntilConditionalApprovalExpires', CondApprove_Date)
@end-comment
@function "SCQM.APL.DaysUntilConditionalApprovalExpires"
@parameter "$ExpireDate"
date-difference(localize-date(now(0)), localize-date($ExpireDate), 'Days')
@end-function

@comment
Return the number of days until the Relationship Approver should be notified about the pending Conditional Approval expiration.
  #param Date $ExpireDate
  #return String Number of days
  EX: call('SCQM.APL.DaysUntilNoticeAboutPendingConditionalApprovalExpiration', CondApprove_Date)
@end-comment
@function "SCQM.APL.DaysUntilNoticeAboutPendingConditionalApprovalExpiration"
@parameter "$ExpireDate"
call('SCQM.APL.DaysUntilConditionalApprovalExpires', $ExpireDate) - call('SCQM.APL.DaysBeforeConditionalExpiration')
@end-function

@comment
Get a count of all APLs related to the same Approval Request as the current APL
  #return Integer Count of APLs related to the same AR
@end-comment
@function "SCQM.APL.GetCountAPLsByAR"
queue-count(
    'SCQM_Approved_Product_List.Master_Q', 
    concat(
        'SCQM_Approval_Request = ', char(39), /store/*[1]/SCQM_Approval_Request/@refId, char(39)
    )
)
@end-function

@comment
Determine if all APLs related to the same Approval Request as the current APL have been discarded
@end-comment
@function "SCQM.APL.AreAllAPLsForThisARDiscarded"
queue-count(
    'SCQM_Approved_Product_List.Master_Q', 
    concat(
        'SCQM_Approval_Request = ', char(39), /store/*[1]/SCQM_Approval_Request/@refId, char(39),
        ' and Status_Code in (', char(39), 'APL_Discontinued', char(39), ',', char(39), 'APL_Discarded', char(39), ')'
    )
) = call('SCQM.APL.GetCountAPLsByAR')
@end-function

@comment
Determine if all APLs related to the same Approval Request as the current APL have been completed (discarded or approved)
@end-comment
@function "SCQM.APL.AreAllAPLsForThisARComplete"
queue-count(
    'SCQM_Approved_Product_List.Master_Q', 
    concat(
        'SCQM_Approval_Request = ', char(39), /store/*[1]/SCQM_Approval_Request/@refId, char(39),
        ' and Status_Code in (', char(39), 'APL_Discontinued', char(39), ',', char(39), 'APL_Discarded', char(39), ',', char(39), 'APL_Approved', char(39), ',', char(39), 'APL_Conditionally_Approved', char(39), ',', char(39), 'APL_Archived', char(39), ')'
    )
) = call('SCQM.APL.GetCountAPLsByAR')
@end-function

@comment
Determine if any APLs related to the same Approval Request as the current APL have been approved
@end-comment
@function "SCQM.APL.AreAnyAPLsForThisARApproved"
queue-count(
    'SCQM_Approved_Product_List.Master_Q', 
    concat(
        'SCQM_Approval_Request = ', char(39), /store/*[1]/SCQM_Approval_Request/@refId, char(39),
        ' and Status_Code in (', char(39), 'APL_Approved', char(39), ',', char(39), 'APL_Conditionally_Approved', char(39), ',', char(39), 'APL_Archived', char(39), ')'
    )
) > 0
@end-function