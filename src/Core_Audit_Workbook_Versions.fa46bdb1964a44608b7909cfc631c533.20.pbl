﻿@topic
@topic-title "Identification of persons  responsible for conducting the internal HACCP review  (S)"
@topic-number "27"

@block "_Controls_Block"
@block-container "Controls"
@block-ui-style "form-horizontal"

	@choice "_27_Choice"
	@choice-style "RadioButtonList"
	@choice-columns "3"
	@choice-direction "Horizontal"
	@choice-postback "Yes"

		@option
		@option-text "Yes"
		@option-ui-style-checked "btn btn-medium white bcolor-success"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "No"
		@option-ui-style-checked "btn btn-medium white bcolor-danger"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "N/A"
		@option-ui-style-checked "btn btn-medium white bcolor-inverse"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

	@end-choice

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_27_Choice) and  _27_Choice = 'No'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_27_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_27_Choice) or _27_Choice [.= 'N/A' or .='Yes']"
				@text-expression "'Identification of persons responsible does not conduct the internal HACCP review  (S)'"
				@text-expression-when "is-changed(_27_Choice) and is-blank(_27_Comments) and _27_Choice = 'No'"
				@text-ui-style "block"

				@block

					@text "_27_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block

					@text "_27_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_27_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "27"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "1"
	@finding-select "_27_Comments"
	@finding-score-when "is-not-blank(_27_Choice) and _27_Choice != 'N/A'"
	@finding-meta-Question_Number "27"

		@case
		@case-type "Positive"
		@case-points "1"
		@case-test "not(_27_Choice = 'No')"

		@case
		@case-type "Secondary"
		@case-points "_27_Selected_Points + 0"
		@case-test "_27_Choice = 'No'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_27_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-blank(_27_Selected_Points), 0, _27_Selected_Points)"
		@text-expression-when "is-blank(_27_Selected_Points)"

	@end-block

@end-block
