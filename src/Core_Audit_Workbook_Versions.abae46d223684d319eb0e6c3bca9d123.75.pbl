﻿@topic
@topic-title "Second Tier suppliers specifications/quality requirements for all ingredients and packaging materials are maintained."
@topic-number "630"

@block "_Controls_Block"
@block-container "Controls"
@block-container-options "Options[Style:width:300px; line-height:50px;]"
@block-ui-style "form-horizontal pos-rel"

	@block "StyleBlock1"
	@block-ui-style "form-inline"

		@link "_630_Tooltip"
		@link-icon "icon-list-ul"
		@link-href "Yes"
		@link-target "Inline"
		@link-ui-style "btn btn-medium btn-inverse margin-lr-sm margin-tb-sm"
		@link-text-ui-style "hide"
		@link-toggle-ui-style "hide"

		@block "_630_Tooltip"
		@block-ui-style "clear well well-small bcolor-white padding-lr-mini form-horizontal hide medium bold pos-abs zindex-modal align-left"

Second Tier suppliers specifications/quality requirements for all ingredients and packaging materials are maintained.

		@end-block

		@block "ActiveControlsBlock"
		@block-ui-style "control-group pull-right"

			@choice "_630_Choice"
			@choice-style "RadioButtonList"
			@choice-columns "5"
			@choice-direction "Horizontal"
			@choice-postback "Yes"
			@choice-ui-style "btn"

				@option "PT1"
				@option-value "10"
				@option-text "10"
				@option-ui-style-checked "btn-medium white bcolor-success"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT2"
				@option-value "7"
				@option-text "7"
				@option-ui-style-checked "btn-medium white bcolor-warning"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT3"
				@option-value "3"
				@option-text "3"
				@option-ui-style-checked "btn-medium white bcolor-danger-light"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT4"
				@option-value "0"
				@option-text "0"
				@option-ui-style-checked "btn-medium white bcolor-danger"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "NA"
				@option-value "N/A"
				@option-text "N/A"
				@option-ui-style-checked "btn-medium white bcolor-inverse"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

			@end-choice

		@end-block

	@end-block

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_630_Choice) and _630_Choice != 'N/A' and _630_Choice != '10'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_630_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_630_Choice) or _630_Choice [. = 'N/A' or . = '10']"
				@text-expression "case(_630_Choice = '0', 'Second Tier suppliers specifications/quality requirements for all ingredients and packaging materials were not maintained.','Second Tier suppliers specifications/quality requirements for all ingredients and packaging materials are maintained.')"
				@text-expression-when "is-changed(_630_Choice) and not(is-changed(_630_Comments)) and _630_Choice != 'N/A'"
				@text-ui-style "block"

				@block
				@block-visible-when "is-not-blank(_630_Choice) and (_630_Choice = '0')"

					@text "_630_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block
				@block-visible-when "is-not-blank(_630_Choice) and not(_630_Choice = 'N/A' or _630_Choice = '10') and not(_630_Choice = '0')"

					@text "_630_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_630_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "630"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "10"
	@finding-select "_630_Comments"
	@finding-score-when "is-not-blank(_630_Choice) and _630_Choice != 'N/A'"
	@finding-meta-Question_Number "630"

		@case
		@case-type "Positive"
		@case-points "_630_Choice"
		@case-test "not(_630_Choice = '0') and _630_Choice != 'N/A'"

		@case
		@case-type "Secondary"
		@case-points "_630_Choice"
		@case-test "_630_Choice = '0' and _630_Choice != 'N/A'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_630_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "_630_Choice"
		@text-expression-when "is-changed(_630_Choice)"

	@end-block

@end-block
