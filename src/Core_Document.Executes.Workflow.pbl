﻿@display "No"
@print "No"

[[Core_Document.Executes.Workflow.Tasks]]


@execute
    @execute-when "is-blank(Executed_On) and Status/Status_Code = call('CoreDoc.GetActiveStatus')"
    
    @update
        @update-target-select "Should_Mark_Executed"
        @update-source-select "'Yes'"
@end-execute

@execute
    @execute-when "is-not-blank(Execute_Doc)"
    
    @update
        @update-target-select "Execute_Doc"
        @update-source-select "''"

    @invoke
        @invoke-execute "Core_Document.ExecuteDoc"
@end-execute

@execute "ExecuteDoc"
@comment
Record when the document becomes "executed"
Execute_Doc flag is set by propagator
A document is executed when the document reaches an "active" state; one of the following occurs:
- the document type requires no approval and the document status becomes Active
- the document type requires acknowledgment and the document becomes acknowledged
- the document type requires approval and the document becomes approved
Update-run-workbook will result in calculating the inactive/expire dates
@end-comment
    
    @update
        @update-target-select "Should_Mark_Executed"
        @update-source-select "''"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Executed'"
        
    @invoke
        @invoke-execute "Core_Document.SetAutoInactiveDate"
        @invoke-when "is-blank(Auto_Inactive_Date) and is-not-blank(Document_Type) and Document_Type/Auto_Inactive_Type = '%INACTIVE_TYPE_DAYS%days%INACTIVE_TYPE_DAYS%' and is-not-blank(Document_Type/Auto_Inactive_Days)"

    @invoke
        @invoke-execute "Core_Document.SetAutoInactiveAdvanceDate"
        @invoke-when "is-blank(Auto_Inactive_Advance_Date) and is-not-blank(Document_Type) and is-not-blank(Document_Type/Inactive_Notification_Advance_Days)"

    @invoke
        @invoke-execute "Core_Document.SetAutoExpireDate"    
        @invoke-when "is-blank(Auto_Expire_Date) and is-not-blank(Document_Type) and Document_Type/Auto_Expire_Type = '%EXPIRE_TYPE_DAYS%days%EXPIRE_TYPE_DAYS%' and is-not-blank(Document_Type/Auto_Expire_Days)"

    @invoke
        @invoke-execute "Core_Document.SetAutoExpireAdvanceDate"
        @invoke-when "is-blank(Auto_Expire_Advance_Date) and is-not-blank(Document_Type) and is-not-blank(Document_Type/Expire_Notification_Advance_Days)"

    @invoke
        @invoke-execute "Core_Document.SetAutoExpireAdvanceTaskAssignmentDate"
        @invoke-when "is-blank(Auto_Expire_Advance_Task_Assignment_Date) and (Document_Type/Auto_Expire_Type = '%EXPIRE_TYPE_DAYS%days%EXPIRE_TYPE_DAYS%' or Document_Type/Auto_Expire_Type = '%EXPIRE_TYPE_DATE%date%EXPIRE_TYPE_DATE%')"

    @invoke
        @invoke-execute "Core_Document.SetAutoExpirePostDate"
        @invoke-when "is-blank(Auto_Expire_Post_Date) and is-not-blank(Document_Type) and is-not-blank(Document_Type/Expire_Notification_Post_Days)"

    @invoke
        @invoke-execute "Core_Document.Archive"
        @invoke-when "boolean(call('CoreDoc.HasCurrentVersion', 'Active'))"
        @invoke-store-id "call('CoreDoc.GetCurrentVersionStoreId', 'Active')"
        @invoke-run-workbook "Yes"        
        
    @invoke
        @invoke-execute "Core_Document.Archive"
        @invoke-when "boolean(call('CoreDoc.HasCurrentVersion', 'Expired'))"
        @invoke-store-id "call('CoreDoc.GetCurrentVersionStoreId', 'Expired')"
        @invoke-run-workbook "Yes"        
@end-execute