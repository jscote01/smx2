﻿@title " "
    @title-ui-style "hide"    

@mode "Summary"

@block
    @block-ui-style "clear span6"

    [[Shared_Core_Task_Assignments.Content.User_Assignments]]
    
    @newline "Small"
    
    @block
        @block-visible-when "is-not-blank(call('OMX.ManagerLog.PreviousLogStoreId'))"

        @link "{{store-url(call('OMX.ManagerLog.PreviousLogStoreId'))}}"
            @link-text "^^Previous Management Log|Previous Management Log^^"
            @link-ui-style "btn btn-info"
            @link-href "Yes"
            @link-target "NewWindow"        
        
        @block
            @block-visible-when "is-blank(store(call('OMX.ManagerLog.PreviousLogStoreId'))/*[1]/Manager_Reviews)"
            @newline "Small"
            {label(medium italic margin-tb-mini):'^^The previous Management Log has not yet been reviewed|The previous Management Log has not yet been reviewed^^.'}
        @end-block        

        @newline "Large"

    @end-block    
    
    {label(medium info block):'^^AM Shift Status|AM Shift Status^^'}
    
    @newline "Small"
    
    @label "localize-token(AM_Checklist_Status/Status_Name)"
        @label-header "^^Checklist Status|Checklist Status^^:"
    
    @label "localize-token(AM_Walkthrough_Status/Status_Name)"
        @label-header "^^Walk-through Status|Walk-through Status^^:"
        
    @newline "Large"

    {label(medium info block):'^^PM Shift Status|PM Shift Status^^'}

    @newline "Small"

    @label "localize-token(PM_Checklist_Status/Status_Name)"
        @label-header "^^Checklist Status|Checklist Status^^:"
    
    @label "localize-token(PM_Walkthrough_Status/Status_Name)"
        @label-header "^^Walk-through Status|Walk-through Status^^:"
    
    @newline "Large"

    @list "Manager_Reviews"
        @list-header "^^Manager Reviews|Manager Reviews^^"
        @list-header-ui-style "medium info block"
        @list-sort "Descending"
        @list-style "Discussion"
        @list-track "Yes"        
        @list-optional "Yes"

        @text "Manager_Reviewer"
            @text-header "^^Name|Name^^:"
            @text-read-only-when "is-not-blank(Manager_Review) and is-not-blank(Manager_Reviewer)" 

        @block
            @block-visible-when "is-not-blank(Manager_Review_DateTime)"
            @label "localize-timestamp(Manager_Review_DateTime)"
                @label-header "^^Reviewed On|Reviewed On^^:"
        @end-block        

        @block
            @block-visible-when "is-blank(Manager_Review_DateTime)"
            @button "Manager_Review"
                @button-value "Reviewed"
                @button-text "^^Reviewed|Reviewed^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success"
                @button-action "Save,Reload"                
        @end-block        

    @end-list
    
@end-block

@block
    @block-ui-style "span4 form-horizontal"

    {label(medium info block):'^^Communication|Communication^^'}

    @newline "Small"
    
    @text "Daily_Communication"
        @text-header "^^Daily Communication|Daily Communication^^:"
        @text-ui-style "block"
        @text-rows "5"
        @text-optional "Yes"

    @newline "Small"

    @text "Huddle_Focus_AM"
        @text-header "^^AM Pre-Shift Huddle Focus|AM Pre-Shift Huddle Focus^^:"
        @text-ui-style "block"
        @text-rows "5"
        @text-optional "Yes"

    @newline "Small"

    @text "Huddle_Focus_PM"
        @text-header "^^PM Pre-Shift Huddle Focus|PM Pre-Shift Huddle Focus^^:"
        @text-ui-style "block"
        @text-rows "5"
        @text-optional "Yes"

    @newline "Small"

    @link "OMX_Management_Log.Shift_Notes_View"
        @link-ui-style "well well-small well-warning-light block"
        @link-text-ui-style "badge pull-right label label-primary large margin-lr-mini"
        @link-text-expression "count(Shift_Notes_Discussion/item)"
        @link-description "^^Shift Notes|Shift Notes^^"
        @link-description-ui-style "medium inverse"
        @link-align "Inline" 

@end-block
