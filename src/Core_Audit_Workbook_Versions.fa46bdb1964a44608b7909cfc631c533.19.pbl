﻿@topic
@topic-title "Record keeping and documentation is sufficient to demonstrate the HACCP controls are in place and effective  (P)"
@topic-number "26"

@block "_Controls_Block"
@block-container "Controls"
@block-ui-style "form-horizontal"

	@choice "_26_Choice"
	@choice-style "RadioButtonList"
	@choice-columns "3"
	@choice-direction "Horizontal"
	@choice-postback "Yes"

		@option
		@option-text "Yes"
		@option-ui-style-checked "btn btn-medium white bcolor-success"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "No"
		@option-ui-style-checked "btn btn-medium white bcolor-danger"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "N/A"
		@option-ui-style-checked "btn btn-medium white bcolor-inverse"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

	@end-choice

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_26_Choice) and  _26_Choice = 'No'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_26_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_26_Choice) or _26_Choice [.= 'N/A' or .='Yes']"
				@text-expression "'Record keeping and documentation does not sufficiently demonstrate the HACCP controls are in place and effective  (P)'"
				@text-expression-when "is-changed(_26_Choice) and is-blank(_26_Comments) and _26_Choice = 'No'"
				@text-ui-style "block"

				@block

					@text "_26_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block

					@text "_26_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_26_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "26"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "2"
	@finding-select "_26_Comments"
	@finding-score-when "is-not-blank(_26_Choice) and _26_Choice != 'N/A'"
	@finding-meta-Question_Number "26"

		@case
		@case-type "Positive"
		@case-points "2"
		@case-test "not(_26_Choice = 'No')"

		@case
		@case-type "Primary"
		@case-points "_26_Selected_Points + 0"
		@case-test "_26_Choice = 'No'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_26_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-blank(_26_Selected_Points), 0, _26_Selected_Points)"
		@text-expression-when "is-blank(_26_Selected_Points)"

	@end-block

@end-block
