﻿@display "No"

@comment
This is called from SCQM_Approved_Product_List

Parameter:SCQM_Specification - StoreId of the Specification to track

Example Invoke:
    invoke
    invoke-execute "SCQM_APL_Specification.Create"
    invoke-parameter "SCQM_Specification"
    invoke-parameter-expression "$SomeStoreId"
    invoke-parameter-direction "In"
@end-comment

@execute "Create"

    @comment
    Create new node of values from the passed in parameter $Parameter:SCQM_Specification
    @end-comment    
    @update
    @update-target-select "$SCQM_APL_Specification_CreateNodes"
    @update-source-type "Node"    
    @update
    @update-target-select "$SCQM_APL_Specification_CreateNodes/SCQM_Specification"
    @update-source-select "$Parameter:SCQM_Specification"

    @comment
    Create the new store if a specification is given and it does not already exist in the app.
    @end-comment    
    @invoke
    @invoke-execute "Core_Executes.CreateAppStore"
    @invoke-when "is-not-blank($SCQM_APL_Specification_CreateNodes/SCQM_Specification) and is-blank(find-store-id('SCQM_APL_Specification.Master_Q', concat('SCQM_Specification = ', char(39), $Parameter:SCQM_Specification, char(39)), '_id'))"
    @invoke-parameter "AppName"
    @invoke-parameter-expression "'%app%SCQM_APL_Specification%app%'"
    @invoke-parameter "Nodes"
    @invoke-parameter-expression "$SCQM_APL_Specification_CreateNodes"
    @invoke-parameter "StoreId"
    @invoke-parameter-expression "$SCQM_APL_Specification_NewStoreId"
    @invoke-parameter-direction "Out"

    @comment
    Update the new store if a specification is given and it does already exist in the app.
    @end-comment    
    @invoke
    @invoke-execute "Core_Executes.UpdateAppStore"
    @invoke-when "is-not-blank($SCQM_APL_Specification_CreateNodes/SCQM_Specification)"
    @invoke-parameter "AppName"
    @invoke-parameter-expression "'%app%SCQM_APL_Specification%app%'"
    @invoke-parameter "Nodes"
    @invoke-parameter-expression "$SCQM_APL_Specification_CreateNodes"
    @invoke-parameter "StoreId"
    @invoke-parameter-expression "find-store-id('SCQM_APL_Specification.Master_Q', concat('SCQM_Specification = ', char(39), $Parameter:SCQM_Specification, char(39)), '_id')"
    @invoke-parameter-direction "In"

    @update
        @update-target-select "Touch"
        @update-source-type "Node"
        @update-run-workbook "Yes"    
        
@end-execute