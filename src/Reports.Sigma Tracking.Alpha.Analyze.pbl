﻿@title " "
@display "No"

@analyze

@query "My_Filters"
    @query-queue "Reports_Center.Master_Q"
    @query-filter "StoreId = {{store-id()}}"
    @query-sort "RID ASC"
    @query-limit "1"    

    @column "RID"    
        @column-source "StoreId"
        
    @column "Start_Date"
    @column "End_Date"
    @column "Region"
    @column "Supplier"
    @column "Specification_Name"
        @column-source "'{{Chart_1_Configuration/item[1]/Specification}}'"        
    @column "Year"
        @column-source "'{{/store/*/Year}}'"        
    
@end-query

@query "Calendar"

    @column "Seq"
    @column "Month"
    
    @row "1 | Jan"
    @row "2 | Feb"
    @row "3 | Mar"
    @row "4 | Apr"
    @row "5 | May"
    @row "6 | Jun"
    @row "7 | Jul"
    @row "8 | Aug"
    @row "9 | Sep"
    @row "10 | Oct"
    @row "11 | Nov"
    @row "12 | Dec"

@end-query

@query "Filters"
    @subquery "Calendar"
    @subquery "My_Filters"
    @subquery-join "1=1"
    @subquery-join-type "Left"    

    @column "Seq"
        @column-source "CAST(Calendar.Seq AS decimal(15,0))"

    @column "RID"    
        @column-source "My_Filters.RID"
        
    @column "Start_Date"
        @column-source "My_Filters.Start_Date"
        
    @column "End_Date"
        @column-source "My_Filters.End_Date"
    @column "Region"
        @column-source "My_Filters.Region"
    @column "Supplier"
        @column-source "My_Filters.Supplier"
    @column "Specification_Name"
        @column-source "My_Filters.Specification_Name"        
    @column "Year"
        @column-source "My_Filters.Year" 
    @column "Month"
        @column-source "Calendar.Month" 

@end-query

@query "ALL_KPIs"
    @query-queue "Yum_KPI_Holding_App.Master_Q"   
    @query-filter "Frequency = 'Weekly' AND Per_Out_Of_Spec IS NOT NULL AND Audit_Completion_Date IS NOT NULL AND Audit_Completion_Year = {{/store/*/Year}} AND Specification_Name = {{Chart_1_Configuration/item[1]/Specification}}"
    @query-queue-datafirewall "No"
    
    @column "KPI_ID"
        @column-source "StoreId"
     
    @column "template_id"
    @column "Score"
    @column "Measure"
    @column "Comments"
    @column "Core_Organization_Location"
    @column "Core_Audit"
    @column "Audit_Completion_Date"
    @column "Audit_Completion_Month"
        @column-source "CAST(Audit_Completion_Month AS decimal(15,2))"
    @column "Month"
        @column-source "LEFT(DATENAME(MM,Audit_Completion_Date),3)"
    @column "Audit_Completion_Year"
    @column "Core_Account"
    @column "Core_Organization"
    @column "Specification"
    @column "Specification_Name"
    @column "Range"
    @column "Min"
        @column-source "CAST(Min AS decimal(15,2))"
    @column "Target"
    @column "Max"
        @column-source "CAST(Max AS decimal(15,2))"
    @column "Unit_Name"
    @column "Units"
    @column "Appear_On_KPI"
    @column "Test_Methods"
    @column "Standard"
    @column "Standard_Item_Name"
    @column "Number_Tests"
       @column-source "CAST(Number_Tests AS decimal(15,2))" 
    @column "Is_Historical"
    @column "Frequency"
    @column "Average"
        @column-source "CAST(Average AS decimal(15,2))"
    @column "StdDev"
        @column-source "CAST(StdDev AS decimal(15,2))"
    @column "Per_Out_Of_Spec"
        @column-source "CAST(Per_Out_Of_Spec AS decimal(15,2))"
 
@end-query

@query "Series_1_Aggregates"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[1]/Standard}}"
    @subquery "ALL_KPIs"
    
    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_1"
    @subquery "Filters"
    @subquery "Series_1_Aggregates"
    @subquery-join "Series_1_Aggregates.Audit_Completion_Month = Filters.Seq AND Series_1_Aggregates.Specification_Name = Filters.Specification_Name"
        @subquery-join-type "Left" 
    
    @column "Standard_Item_Name"
        @column-source "ISNULL(Series_1_Aggregates.Standard_Item_Name, '{{Chart_1_Configuration/item[1]/Standards_List/item[1]/Standard}}')"
        
    @column "Specification_Name"
        @column-source "ISNULL(Series_1_Aggregates.Specification_Name, Filters.Specification_Name)"
        
    @column "Audit_Completion_Month"
        @column-source "ISNULL(Series_1_Aggregates.Audit_Completion_Month,Filters.Seq)"
        
    @column "Audit_Completion_Year"
        @column-source "ISNULL(Series_1_Aggregates.Audit_Completion_Year,Filters.Year)"
    
    @column "Month"
        @column-source "ISNULL(Series_1_Aggregates.Month,Filters.Month)"

    @column "Number_Tests"
        @column-source "ISNULL(Series_1_Aggregates.Number_Tests, 0)"
        
    @column "Per_Out_Of_Spec"
        @column-source "ISNULL(Series_1_Aggregates.Per_Out_Of_Spec, 0)"    
        
    @column "Min"
        @column-source "ISNULL(Series_1_Aggregates.Min, 0)"    
        
    @column "Max"    
        @column-source "ISNULL(Series_1_Aggregates.Max, 0)"    
        
    @column "Average" 
        @column-source "ISNULL(Series_1_Aggregates.Average, 0)"    
        
    @column "StdDev"
        @column-source "ISNULL(Series_1_Aggregates.StdDev, 0)"    
        
@end-query

@query "Series_2"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[2]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_3"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[3]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_4"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[4]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_5"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[5]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_6"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[6]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_7"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[7]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_8"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[8]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_9"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[9]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Series_10"
    @query-filter "Standard_Item_Name = {{Chart_1_Configuration/item[1]/Standards_List/item[10]/Standard}}"
    @subquery "ALL_KPIs"

    @column "Standard_Item_Name"
    @column "Specification_Name"
    @column "Audit_Completion_Month"
    @column "Audit_Completion_Year"
    @column "Month"

    @column "Number_Tests"
        @column-aggregate "Sum"

    @column "Per_Out_Of_Spec"
        @column-aggregate "Average"        
    
    @column "Min"
        @column-aggregate "Minimum"        
    
    @column "Max"    
        @column-aggregate "Maximum"        
        
    @column "Average" 
        @column-aggregate "Average"        
    
    @column "StdDev"
        @column-aggregate "Average"
        
@end-query

@query "Combined"
    @subquery "Series_1"
    @subquery "Series_2"
        @subquery-join "Series_2.Specification_Name = Series_1.Specification_Name AND Series_2.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_2.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_3"
        @subquery-join "Series_3.Specification_Name = Series_1.Specification_Name AND Series_3.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_3.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_4"
        @subquery-join "Series_4.Specification_Name = Series_1.Specification_Name AND Series_4.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_4.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_5"
        @subquery-join "Series_5.Specification_Name = Series_1.Specification_Name AND Series_5.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_5.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_6"
        @subquery-join "Series_6.Specification_Name = Series_1.Specification_Name AND Series_6.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_6.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left"
    @subquery "Series_7"
        @subquery-join "Series_7.Specification_Name = Series_1.Specification_Name AND Series_7.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_7.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_8"
        @subquery-join "Series_8.Specification_Name = Series_1.Specification_Name AND Series_8.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_8.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_9"
        @subquery-join "Series_9.Specification_Name = Series_1.Specification_Name AND Series_9.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_9.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 
    @subquery "Series_10"
        @subquery-join "Series_10.Specification_Name = Series_1.Specification_Name AND Series_10.Audit_Completion_Year = Series_1.Audit_Completion_Year AND Series_10.Audit_Completion_Month = Series_1.Audit_Completion_Month"
        @subquery-join-type "Left" 

    @column "Specification_Name"
        @column-source "Series_1.Specification_Name"  
        
    @column "Supplier"
        @column-source "'{{replace(Supplier/Organization_Name,char(39),concat(char(39),char(39)))}}'"
        
    @column "Region"
        @column-source "'{{Region/@text}}'"
    
    @column "Audit_Completion_Month"
        @column-source "Series_1.Audit_Completion_Month"        
        
    @column "Audit_Completion_Year"
        @column-source "Series_1.Audit_Completion_Year"        
        
    @column "Month"
        @column-source "Series_1.Month"        
        
    @column "Series_1_Name"
        @column-source "Series_1.Standard_Item_Name"

    @column "Series_1_Per_Out_Of_Spec"
        @column-source "Series_1.Per_Out_Of_Spec"
        
    @column "Series_1_Number_Tests"
        @column-source "Series_1.Number_Tests"
    
    @column "Series_1_Min"
        @column-source "Series_1.Min"

    @column "Series_1_Max"    
        @column-source "Series_1.Max"

    @column "Series_1_Average" 
        @column-source "Series_1.Average"

    @column "Series_1_StdDev"
        @column-source "Series_1.StdDev"
        
    @column "Series_2_Name"
        @column-source "ISNULL(Series_2.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[2]/Standard}}')"

    @column "Series_2_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_2.Per_Out_Of_Spec, 0)"

    @column "Series_2_Number_Tests"
        @column-source "ISNULL(Series_2.Number_Tests, 0)"
    
    @column "Series_2_Min"
        @column-source "ISNULL(Series_2.Min, 0)"

    @column "Series_2_Max"    
        @column-source "ISNULL(Series_2.Max, 0)"

    @column "Series_2_Average" 
        @column-source "ISNULL(Series_2.Average, 0)"

    @column "Series_2_StdDev"
        @column-source "ISNULL(Series_2.StdDev, 0)"
        
    @column "Series_3_Name"
        @column-source "ISNULL(Series_3.Standard_Item_Name, '{{Chart_1_Configuration/item[1]/Standards_List/item[3]/Standard}}')"

    @column "Series_3_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_3.Per_Out_Of_Spec, 0)"
    
    @column "Series_3_Number_Tests"
        @column-source "ISNULL(Series_3.Number_Tests, 0)"
    
    @column "Series_3_Min"
        @column-source "ISNULL(Series_3.Min, 0)"

    @column "Series_3_Max"    
        @column-source "ISNULL(Series_3.Max, 0)"

    @column "Series_3_Average" 
        @column-source "ISNULL(Series_3.Average, 0)"

    @column "Series_3_StdDev"
        @column-source "ISNULL(Series_3.StdDev, 0)"
    
    @column "Series_4_Name"
        @column-source "ISNULL(Series_4.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[4]/Standard}}')"

    @column "Series_4_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_4.Per_Out_Of_Spec, 0)"
    
    @column "Series_4_Number_Tests"
        @column-source "ISNULL(Series_4.Number_Tests, 0)"
    
    @column "Series_4_Min"
        @column-source "ISNULL(Series_4.Min, 0)"

    @column "Series_4_Max"    
        @column-source "ISNULL(Series_4.Max, 0)"

    @column "Series_4_Average" 
        @column-source "ISNULL(Series_4.Average, 0)"

    @column "Series_4_StdDev"
        @column-source "ISNULL(Series_4.StdDev, 0)"
    
    @column "Series_5_Name"
        @column-source "ISNULL(Series_5.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[5]/Standard}}')"

    @column "Series_5_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_5.Per_Out_Of_Spec, 0)"
 
     @column "Series_5_Number_Tests"
        @column-source "ISNULL(Series_5.Number_Tests, 0)"
    
    @column "Series_5_Min"
        @column-source "ISNULL(Series_5.Min, 0)"

    @column "Series_5_Max"    
        @column-source "ISNULL(Series_5.Max, 0)"

    @column "Series_5_Average" 
        @column-source "ISNULL(Series_5.Average, 0)"

    @column "Series_5_StdDev"
        @column-source "ISNULL(Series_5.StdDev, 0)"
    
    @column "Series_6_Name"
        @column-source "ISNULL(Series_6.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[6]/Standard}}')"

    @column "Series_6_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_6.Per_Out_Of_Spec, 0)"

    @column "Series_6_Number_Tests"
        @column-source "ISNULL(Series_6.Number_Tests, 0)"
    
    @column "Series_6_Min"
        @column-source "ISNULL(Series_6.Min, 0)"

    @column "Series_6_Max"    
        @column-source "ISNULL(Series_6.Max, 0)"

    @column "Series_6_Average" 
        @column-source "ISNULL(Series_6.Average, 0)"

    @column "Series_6_StdDev"
        @column-source "ISNULL(Series_6.StdDev, 0)"

    @column "Series_7_Name"
        @column-source "ISNULL(Series_7.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[7]/Standard}}')"

    @column "Series_7_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_7.Per_Out_Of_Spec, 0)"

    @column "Series_7_Number_Tests"
        @column-source "ISNULL(Series_7.Number_Tests, 0)"
    
    @column "Series_7_Min"
        @column-source "ISNULL(Series_7.Min, 0)"

    @column "Series_7_Max"    
        @column-source "ISNULL(Series_7.Max, 0)"

    @column "Series_7_Average" 
        @column-source "ISNULL(Series_7.Average, 0)"

    @column "Series_7_StdDev"
        @column-source "ISNULL(Series_7.StdDev, 0)"
   
    @column "Series_8_Name"
        @column-source "ISNULL(Series_8.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[8]/Standard}}')"

    @column "Series_8_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_8.Per_Out_Of_Spec, 0)"

    @column "Series_8_Number_Tests"
        @column-source "ISNULL(Series_8.Number_Tests, 0)"
    
    @column "Series_8_Min"
        @column-source "ISNULL(Series_8.Min, 0)"

    @column "Series_8_Max"    
        @column-source "ISNULL(Series_8.Max, 0)"

    @column "Series_8_Average" 
        @column-source "ISNULL(Series_8.Average, 0)"

    @column "Series_8_StdDev"
        @column-source "ISNULL(Series_8.StdDev, 0)"
        
    @column "Series_9_Name"
        @column-source "ISNULL(Series_9.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[9]/Standard}}')"

    @column "Series_9_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_9.Per_Out_Of_Spec, 0)"
   
    @column "Series_9_Number_Tests"
        @column-source "ISNULL(Series_9.Number_Tests, 0)"
    
    @column "Series_9_Min"
        @column-source "ISNULL(Series_9.Min, 0)"

    @column "Series_9_Max"    
        @column-source "ISNULL(Series_9.Max, 0)"

    @column "Series_9_Average" 
        @column-source "ISNULL(Series_9.Average, 0)"

    @column "Series_9_StdDev"
        @column-source "ISNULL(Series_9.StdDev, 0)"
        
    @column "Series_10_Name"
        @column-source "ISNULL(Series_10.Standard_Item_Name,'{{Chart_1_Configuration/item[1]/Standards_List/item[10]/Standard}}')"

    @column "Series_10_Per_Out_Of_Spec"
        @column-source "ISNULL(Series_10.Per_Out_Of_Spec, 0)"

    @column "Series_10_Number_Tests"
        @column-source "ISNULL(Series_10.Number_Tests, 0)"
    
    @column "Series_10_Min"
        @column-source "ISNULL(Series_10.Min, 0)"

    @column "Series_10_Max"    
        @column-source "ISNULL(Series_10.Max, 0)"

    @column "Series_10_Average" 
        @column-source "ISNULL(Series_10.Average, 0)"

    @column "Series_10_StdDev"
        @column-source "ISNULL(Series_10.StdDev, 0)"
@end-query

@end-analyze