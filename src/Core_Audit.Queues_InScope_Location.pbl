﻿@title "Service Location Queues"

@comment
'''This queue displays a list of all scheduled audits this week and in the past.'''

@end-comment

@queue "Location_Scheduled_This_Week_Q"
@queue-header "Scheduled and Open Audits - This Week"
@queue-sort "Audit_Scheduled_Date"
@queue-filter "Status in ( 'Scheduled', 'In-Progress', 'Completed')  and (ThisWeek(Audit_Scheduled_Date) or Past(Audit_Scheduled_Date)) "
@queue-test "call('Core.CheckPermission', 'Core_Audit.Location_Scheduled_This_Week_Q') or call('Core.IsITUser')"
@queue-scope "Account_Location"
@queue-sequence "40"
@queue-activator "Audits"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Zip_Code"

@column "Round_Title"

@column "Audit_Scheduled_Date"

@column "Auditor_Full_Name"

@column "Workorder_Number"

@column "Status"

@end-queue
----

@comment
'''This queue displays a list of all scheduled audits in the future.'''

@end-comment

@queue "Location_Scheduled_Future_Q"
@queue-header "Scheduled and Open Audits - Future"
@queue-sort "Auditor_Full_Name, Audit_Scheduled_Date"
@queue-filter "Status in ( 'Scheduled', 'In-Progress', 'Completed') and not(ThisWeek(Audit_Scheduled_Date)) and not(Past(Audit_Scheduled_Date)) "
@queue-test "call('Core.CheckPermission', 'Core_Audit.Location_Scheduled_Future_Q') or call('Core.IsITUser')"
@queue-sequence "41"
@queue-scope "Account_Location"
@queue-allow-new "No"
@queue-activator "Audits"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Zip_Code"

@column "Round_Title"

@column "Audit_Scheduled_Date"

@column "Auditor_Full_Name"

@column "Workorder_Number"

@column "Status"

@end-queue


----
@comment
'''This queue displays a list of all closed audits.'''

@end-comment

@queue "Location_Closed_Q"
@queue-header "Closed Audits"
@queue-sort "Audit_Completion_Date desc"
@queue-test "call('Core.CheckPermission', 'Core_Audit.Location_Closed_Q') or call('Core.IsITUser')"
@queue-filter "Status = 'Closed' "
@queue-sequence "42"
@queue-scope "Service_Location"
@queue-activator "Audits"
@queue-allow-new "No"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Zip_Code"

@column "Round_Title"

@column "Audit_Scheduled_Date"

@column "Audit_Completion_Date"

@column "Audit_Score_Calc"

@column "Auditor_Full_Name"

@column "Workorder_Number"

@column "Status"

@column "Sent_To_Client"

@column "Audit_Report"

@end-queue

@comment
This queue displays a list of ALL audits.

@end-comment

@queue "Location_All_Q"
@queue-header "All Audits"
@queue-sort "Audit_Completion_Date desc"
@queue-test "call('Core.CheckPermission', 'Core_Audit.Location_All_Q') or call('Core.IsITUser')"
@queue-sequence "43"
@queue-scope "Account_Location"
@queue-activator "Audits | All "
@queue-allow-new "No"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Zip_Code"

@column "Round_Title"

@column "Audit_Scheduled_Date"

@column "Auditor_Full_Name"

@column "Workorder_Number"

@column "Status"

@column "Audit_Completion_Date"

@column "Audit_Score_Calc"

@column "Sent_To_Client"

@column "Audit_Report"

@end-queue