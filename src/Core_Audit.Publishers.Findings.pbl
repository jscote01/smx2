﻿@display "No"

'''This tells the system when and how to publish findings'''

@publish "Questionnaire_Published_Findings"
@publish-select "/store/Core_Audit/Questionnaire//findings/finding[@triggered = 'true' and @type != 'Series' and not(parent::findings/parent::instance[contains(@tag, 'Deleted')])]"
@publish-when "Status = 'Closed' and is-blank(Import_Source) and not(is-offline()) and (Service_Type = 'Facility Audit' or is-blank(Service_Type)) and is-blank(Assignments)"
@publish-app "Core_Audit_Findings"

    @property "Id"
    @property-select "@id"
    @property-is-key "Yes"
    
    @property "Core_Account"
    @property-select "/store/Core_Audit/Core_Account/@refId"
    
    @property "Core_Organization"
    @property-select "/store/Core_Audit/Core_Organization/@refId"
    
    @property "Core_Organization_Location"
    @property-select "/store/Core_Audit/Core_Organization_Location/@refId"
    
    @property "Core_Audit_Service"
    @property-select "/store/Core_Audit/Core_Audit_Service/@refId"
    
    @property "Core_Audit_Round"
    @property-select "/store/Core_Audit/Core_Audit_Round/@refId"
    
    @property "Core_Audit_Service_Version"
    @property-select "/store/Core_Audit/Core_Audit_Service_Version/@refId"
    
    @property "Core_Audit"
    @property-select "/store/@id"
    
    @property "Audit_Type"
    @property-select "/store/Core_Audit/Audit_Type"
    
    @property "Service_Type"
    @property-select "/store/Core_Audit/Service_Type/Service_Type"
    
    @property "Service_Type_Desc"
    @property-select "/store/Core_Audit/Service_Type/Service_Type_Description"
    
    @property "Status"
    @property-select "/store/Core_Audit/Status"
    
    @property "Audit_Scheduled_Date"
    @property-type "Date"
    @property-select "/store/Core_Audit/Audit_Scheduled_Date"
    
    @property "Audit_Completion_Date"
    @property-type "Date"
    @property-select "/store/Core_Audit/Audit_Completion_Date"
    
    @property "Round_Title"
    @property-select "/store/Core_Audit/Core_Audit_Round/Round_Title"
    
    @property "Round_Start_Date"
    @property-type "Date"
    @property-select "/store/Core_Audit/Core_Audit_Round/Round_Start_Date"
    
    @property "Round_End_Date"
    @property-type "Date"
    @property-select "/store/Core_Audit/Core_Audit_Round/Round_End_Date"
    
    @property "Score_Golf"
    @property-type "Integer"
    @property-select "/store/Core_Audit/Audit_Score/@points"
    
    @property "Audit_Rating"
    @property-select "/store/Core_Audit/Audit_Rating"
    
    @property "Audit_Score"
    @property-type "Number"
    @property-select "/store/Core_Audit/Audit_Score/@value"
    
    @property "Audit_Score_Calc"
    @property-type "Number"
    @property-select "/store/Core_Audit/Audit_Score_Calc"
    
    @property "Street_Address"
    @property-select "/store/Core_Audit/Core_Organization_Location/Physical_Address_Line_1"
    
    @property "City"
    @property-select "/store/Core_Audit/Core_Organization_Location/Physical_City"
    
    @property "State"
    @property-select "/store/Core_Audit/Core_Organization_Location/Physical_State"
    
    @property "Zip_Code"
    @property-select "/store/Core_Audit/Core_Organization_Location/Physical_Zip"
    
    @property "Location_Phone_Number"
    @property-select "/store/Core_Audit/Core_Organization_Location/Phone_Number"
    
    @property "Auditor_Full_Name"
    @property-select "/store/Core_Audit/Auditor_Full_Name"
    
    @property "Code"
    @property-select "/store/Core_Audit/Code"
    
    @property "Questionnaire_Lookup_Workbook"
    @property-select "/store/Core_Audit/Active_Parameters/Questionnaire_Lookup/name"
    
    @property "Domain_Name"
    @property-select "/store/Core_Audit/Core_Account/Account_Domain"
    
    @property "Tag"
    @property-select "coalesce(parent::findings/parent::instance/@token, parent::findings/parent::instance/@meta-token, parent::findings/parent::instance/@tag)"
    
    @property "Tag_Original"
    @property-select "parent::findings/parent::instance/@tag"
    
    @property "Topic1"
    @property-select "coalesce(topic1/@token, topic1/@text)"
    
    @property "Topic1_Original"
    @property-select "topic1/@text"
    
    @property "Topic1_Number"
    @property-select "topic1/@number"
    
    @property "Topic2_Number"
    @property-select "if(topic3, topic3/@number, topic2/@number)"
    
    @property "Topic2"
    @property-select "coalesce(topic3/@token, topic3/@text, topic2/@token, topic2/@text)"
    
    @property "Topic2_Original"
    @property-select "if(topic3, topic3/@text, topic2/@text)"
    
    @property "Topic3"
    @property-select "if(topic3, coalesce(topic2/@token, topic2/@text), '')"
    
    @property "Topic3_Original"
    @property-select "if(topic3, topic2/@text, '')"
    
    @property "Type"
    @property-select "@type"
    
    @property "Base_Type"
    @property-select "eval(replace(/store/Core_Audit/Active_Parameters/Finding_Base_Type_Expression, '__Type__', @type))"
    
    @property "Points"
    @property-type "Number"
    @property-select "@points"
    
    @property "Maximum_Points"
    @property-type "Number"
    @property-select "@maximumPoints"
    
    @property "Flag"
    @property-select "coalesce(flagText/@token, flagText)"
    
    @property "Flag_Original"
    @property-select "flagText"
    
    @property "Rule"
    @property-select "coalesce(ruleText/@token,ruleText)"
    
    @property "Rule_Original"
    @property-select "ruleText"
    
    @property "Repeat"
    @property-type "Boolean"
    @property-select "@repeat"
    
    @property "Repeat_Count"
    @property-type "Integer"
    @property-select "@repeatCount"
    
    @property "Repeat_Text"
    @property-select "repeatText"
    
    @comment
    Going away from extracting variables from text fields.
    
        @property "Issue_Count"
        @property-type "Integer"
        @property-select "count-variable('Issue', noteText)"
        
        @property "Photo_Count"
        @property-type "Integer"
        @property-select "count-variable('Photo included', noteText)"
        
        @property "Photo_Name"
        @property-select "extract-variable('Photo included', noteText)"
        
        @property "Photo_Time"
        @property-select "extract-variable('Photo Time', noteText)"
    
        @property "Note"
        @property-select "If(@type = 'Information', extract-variable('Information', noteText, concat(char(8226), ' ')),
        If(@type = 'Compliant', extract-variable('Compliant', noteText, concat(char(8226), ' ')),
        extract-variable('Issue', noteText, concat(char(8226), ' '))))"
        
        @property "Corrective_Actions_From_Audit"
        @property-select "extract-variable('Corrective Action', noteText, concat(char(8226), ' '))"
    
    @end-comment    
    
    @property "Note"
    @property-select "noteText"
    
    @property "Full_Note"
    @property-select "noteText"
    
    @property "Audit_Contact_First_Name"
    @property-select "/store/Core_Audit/Manager_First_Name"
    
    @property "Audit_Contact_Last_Name"
    @property-select "/store/Core_Audit/Manager_Last_Name"
    
    @property "Corrective_Actions"
    @property-select "/store/Core_Audit/Active_Parameters/Corrective_Actions"
    
    @property "Corrective_Actions_Recommended"
    @property-select "/store/Core_Audit/Active_Parameters/Corrective_Actions_Recommended"
    
    @property "CAPs_Conditional"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Conditional"
    
    @property "CAPs_Optional"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Optional"
    
    @property "CAPs_Conditional_Ratings"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Conditional_Ratings"
    
    @property "CAPs_Optional_Ratings"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Optional_Ratings"
    
    @property "CAPs_Conditional_Max_Criticals"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Conditional_Max_Criticals"
    
    @property "CAPs_Conditional_Line_Items"
    @property-select "/store/Core_Audit/Active_Parameters/CAPs_Conditional_Line_Items"
    
    @property "Days_To_Respond"
    @property-type "Integer"
    @property-select "/store/Core_Audit/Active_Parameters/Days_To_Respond"
    
    @property "Response_Date_Holidays"
    @property-select "/store/Core_Audit/Active_Parameters/Response_Date_Holidays"
    
    @property "Responses_Reviewed"
    @property-select "/store/Core_Audit/Active_Parameters/Responses_Reviewed"
    
    @property "Responses_Reviewed_By"
    @property-select "/store/Core_Audit/Active_Parameters/Responses_Reviewed_By"
    
    @property "Corrective_Actions_Module"
    @property-select "/store/Core_Audit/Active_Parameters/Corrective_Actions_Module"
    
    @property "Pest_Issue"
    @property-select "extract-variable('Pest', noteText, concat(char(8226), ' '))"

@end-publish


