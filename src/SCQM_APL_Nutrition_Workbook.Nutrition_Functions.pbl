﻿@display "No"

^^Nutrition & Allergens|Nutrition & Allergens^^

@comment
This function returns if True if Nutrition is required for the commodity type, False if it is optional, and NA if it is not applicable. Default is optional.
@end-comment
@function "SCQM.APLNutrition.IsNutritionRequired"
case(
call('SCQM.APLNutrition.GetCategoryCode') = '1', 'True', 
call('SCQM.APLNutrition.GetCategoryCode') = '2', 'NA', 
'False')
@end-function

@comment
This function will return the Category Code of the specification for this APL. This function will only work in the scope of an APL Subprocess
@end-comment
@function "SCQM.APLNutrition.GetCategoryCode"
store(store(call('SCQM.APL.Subprocess.SpecificationStoreId', store-id()))/SCQM_Specification/Specification_Workbook/Product_Hierarchy_1/@refId)/SCQM_Admin_Product_Categories/Code
@end-function

@comment
Is the current user the given Approver
@end-comment
@function "SCQM.APLNutrition.IsApprover"
@parameter "$StoreId"
@parameter "$RoleId"
@parameter "$UserId"
boolean-from-string($UserId = call('Core.GetTaskAssignment', $StoreId, call('Core.GetRoleIdByID', $RoleId))/Assignee)
@end-function

@comment
This function will return the Sub Category Code of the specification for this APL. This function will only work in the scope of an APL Subprocess
@end-comment
@function "SCQM.APLNutrition.GetLocationID"
store(/store/SCQM_Approved_Product_List_Subprocesses/APL_Group/@refId)/SCQM_Approved_Product_List_Group/Org_Location/@refId
@end-function

@comment
This function will determine what topics should be read-only and when
@end-comment
@function "SCQM.APLNutrition.ReadOnlyTopics"
not(
    (call('SCQM.APLNutrition.IsApprover', store-id(), SCQM_APL_Local_User, user-id()) or call('Core.IsITUser')) 
    and 
    (Status/Status_Code = 'Requested' or Status/Status_Code = 'Returned' or Status/Status_Code = 'Submitted')
    and (contains(topic-id(), 'SCQM_APL_Nutrition_Workbook.Buttons') or contains(topic-id(), 'SCQM_APL_Nutrition_Workbook.IT'))
) 
or (is-not-blank(Verify_Status) or is-not-blank(Submit) or is-blank(Initial_Monitor) or is-not-blank(Request_Monitor))
@end-function
