﻿@title " "
    @title-ui-style "hide"

@mode "Configuration"

@choice "Template_Type"
    @choice-style "DropDownLookup"
    @choice-header "^^Template Type|Template Type^^:"
    @choice-queue "EULA_Templates_Configuration.Active_Lookup_Q"
    @choice-select "EULA_Templates_Configuration/Template_Code | EULA_Templates_Configuration/Template_Name | EULA_Templates_Configuration/Template_Header | EULA_Templates_Configuration/Template_Sequence"
    @choice-field "Template_Name"
    @choice-optional-when "queue-count('EULA_Templates_Configuration.Active_Lookup_Q') = 0"
@end-choice

@choice "Language"
    @choice-style "DropDownList"
    @choice-header "^^Language|Language^^:"
    @choice-postback "Yes"
    @choice-ui-style "width-auto"
    @options
        @options-select "get-supported-languages()/language"
        @options-value "code"
        @options-text "name"
@end-choice

@checkbox "Force_All_User_Reread"
    @checkbox-text "^^Force all users to re-read this Template Type?|Force all users to re-read this Template Type?^^"

@block
    @block-visible-when "is-not-blank(Force_All_User_Reread_Last_Date)"

    @label "localize-timestamp(Force_All_User_Reread_Last_Date)"
        @label-header "^^Last Re-read Forced On|Last Re-read Forced On^^:"

@end-block

@block
    @block-visible-when "is-not-blank(Force_All_User_Reread)"
    @block-ui-style "clear span6 well well-mini well-danger-light inverse"
    @icon "icon-warning-sign danger medium"
    {label(medium margin-lr-mini):'NOTE: This will force ALL users to re-read this template type regardless of language.  Make sure you have all template languages updated before checking this.'}
    
    @block
        @block-visible-when "is-not-blank(EULA_Committed_Date)"

        @newline "Small"
        
        @button "Force_Reread"
            @button-value "Reread"
            @button-text "^^Force Re-read of Type|Force Re-read of Type^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-warning"
            @button-confirm "^^EULA_Force_Reread_Warning|This will cause all users to be forced to re-read and agree to this template type.  Are you sure?^^"    

    @end-block    

@end-block

@newline

@label "localize-timestamp(EULA_Created_Date)"
    @label-header "^^Created On|Created On^^:"

@label "localize-timestamp(EULA_Committed_Date)"
    @label-header "^^Committed On|Committed On^^:"

@label "Version"
    @label-header "^^Version|Version^^:"

@block
    @block-ui-style "form-horizontal"

    @text "Description"
        @text-header "^^Changes in this Version|Changes in this Version^^:"
        @text-optional "Yes"
        @text-columns "80"
        @text-rows "5"

@end-block

@@ TEMPLATE
@block
    @block-ui-style "clear span6 form-horizontal"

    @text "Template_Verbiage"
        @text-header "^^Template|Template^^:"
        @text-header-ui-style "queue-title"        
        @text-columns "160"
        @text-rows "40"

@end-block

@@ TEMPLATE PREVIEW
@block
    @block-ui-style "span6"

    {label(queue-title):'^^Preview of Template|Preview of Template^^:'}

    @newline "Small"

    {{html:eval-expressions(Template_Verbiage)}}

@end-block


@@ BUTTONS
@block
    @block-visible-when "is-blank(Commit_Version)"
    
    @button "Commit_Version"
        @button-value "Commit"
        @button-text "^^Commit Version|Commit Version^^"
        @button-confirm "^^Commit_Version_Button_Confirm|Are you sure you want to commit this version of the EULA? Once committed, changes can no longer be made.^^"
        @button-optional "Yes"
        @button-ui-style "btn-success"
        @button-action "Save,Reload"        

@end-block
