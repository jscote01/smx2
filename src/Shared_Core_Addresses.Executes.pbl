﻿@display "No"
@print "No"


@execute
    @execute-when "is-changed(Physical_Country)"
    
    @update
        @update-target-select "Physical_State"
        @update-source-type "Node"
    
    @update
        @update-target-select "Physical_State"
        @update-source-select "''"
@end-execute

@execute
    @execute-when "is-changed(Mailing_Country)"
    
    @update
        @update-target-select "Mailing_State"
        @update-source-type "Node"
    
    @update
        @update-target-select "Mailing_State"
        @update-source-select "''"
@end-execute

@execute
    @execute-when "is-changed(Mail_Add) and not(boolean-from-string(Mail_Add))"

    @update
        @update-target-select "Mailing_Address_Line_1"
        @update-source-type "Node"
    @update
        @update-target-select "Mailing_Address_Line_2"
        @update-source-type "Node"
    @update
        @update-target-select "Mailing_City"
        @update-source-type "Node"
    @update
        @update-target-select "Mailing_State"
        @update-source-type "Node"
    @update
        @update-target-select "Mailing_Zip"
        @update-source-type "Node"
    @update
        @update-target-select "Mailing_Country"
        @update-source-type "Node"

@end-execute


@comment
Clean up lookups on shared fields
@end-comment

@execute
    @execute-when "is-not-blank(Physical_Country) and is-blank(Physical_Country/@refId)"
    
    @update
        @update-target-select "Physical_Country"
        @update-source-select "Physical_Country"
        @update-source-type "Lookup"
@end-execute

@execute
    @execute-when "is-not-blank(Physical_State) and is-blank(Physical_State/@refId) and call('Core.CountryHasStates',Physical_Country)"
    
    @update
        @update-target-select "Physical_State"
        @update-source-select "Physical_State"
        @update-source-type "Lookup"
@end-execute

@execute
    @execute-when "is-not-blank(Mailing_Country) and is-blank(Mailing_Country/@refId)"
    
    @update
        @update-target-select "Mailing_Country"
        @update-source-select "Mailing_Country"
        @update-source-type "Lookup"
@end-execute

@execute
    @execute-when "is-not-blank(Mailing_State) and is-blank(Mailing_State/@refId) and call('Core.CountryHasStates',Mailing_Country)"
    
    @update
        @update-target-select "Mailing_State"
        @update-source-select "Mailing_State"
        @update-source-type "Lookup"
@end-execute



@comment
PUBLIC INVOKABLE NAMED EXECUTES - BELOW THIS COMMENT
@end-comment

@execute "BuildAddressesFromNodeset"
@comment
:AppName
:StoreId
:Nodeset
@end-comment
    @invoke
        @invoke-execute "Shared_Wikis.CreatePhysicalAddress"
        @invoke-parameter "PhysicalAppName"
            @invoke-parameter-expression "$Parameter:AppName"
        @invoke-parameter "PhysicalStoreId"
            @invoke-parameter-expression "$Parameter:StoreId"
        @invoke-parameter "PhysicalNodes"
            @invoke-parameter-expression "$Parameter:Nodeset"
    
    @invoke
        @invoke-execute "Shared_Wikis.CreateMailingAddress"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "$Parameter:AppName"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "$Parameter:StoreId"
        @invoke-parameter "Nodes"
            @invoke-parameter-expression "$Parameter:Nodeset"

    @invoke
        @invoke-execute "Shared_Wikis.SetFlagIfAddressesDifferent"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "$Parameter:AppName"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "$Parameter:StoreId"
@end-execute


@execute "SetFlagIfAddressesDifferent"
@comment
:AppName
:StoreId
@end-comment
    @update
        @update-target-select "$Store"
        @update-source-select "store($Parameter:StoreId, $Parameter:AppName)"
        @update-source-type "Node"
    
    @update
        @update-target-select "$physicalAddress"
        @update-source-select "Join($Store/Physical_Address_Line_1,$Store/Physical_Address_Line_2,$Store/Physical_City,$Store/Physical_State,$Store/Physical_Zip,$Store/Physical_Country/@refId,'|')"
    
    @update
        @update-target-select "$mailingAddress"
        @update-source-select "Join($Store/Mailing_Address_Line_1,$Store/Mailing_Address_Line_2,$Store/Mailing_City,$Store/Mailing_State,$Store/Mailing_Zip,$Store/Mailing_Country/@refId,'|')"

    @update
        @update-store-id "concat($Parameter:AppName, '.', $Parameter:StoreId)"
        @update-target-select "{{concat('/store/', $Parameter:AppName, '/Mail_Add')}}"
        @update-source-select "if(string-equal($physicalAddress, $mailingAddress) or is-blank($mailingAddress), 'false', 'true')"
    
    @invoke
        @invoke-execute "Shared_Wikis.ClearMailingAddress"
        @invoke-when "string-equal($physicalAddress, $mailingAddress)"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "$Parameter:AppName"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "$Parameter:StoreId"
@end-execute


@execute "CreatePhysicalAddress"
@comment
:PhysicalAppName
:PhysicalStoreId
:PhysicalNodes
@end-comment
    @update
        @update-target-select "$CountryStoreId"
        @update-source-select "coalesce($Parameter:PhysicalNodes/Physical_Country/@refId, if(starts-with($Parameter:PhysicalNodes/Physical_Country, 'str-'), $Parameter:PhysicalNodes/Physical_Country, ''))"
    @update
        @update-target-select "$StateStoreId"
        @update-source-select "coalesce($Parameter:PhysicalNodes/Physical_State/@refId, if(starts-with($Parameter:PhysicalNodes/Physical_State, 'str-'), $Parameter:PhysicalNodes/Physical_State, ''))"

    @update
        @update-target-select "$PhysicalAddressNodes"
        @update-source-select "''"
    @update
        @update-target-select "$PhysicalAddressNodes"
        @update-source-select "''"
    @update
        @update-target-select "$PhysicalAddressNodes/Physical_Address_Line_1"
        @update-source-select "$Parameter:PhysicalNodes/Physical_Address_Line_1"
    @update
        @update-target-select "$PhysicalAddressNodes/Physical_Address_Line_2"
        @update-source-select "$Parameter:PhysicalNodes/Physical_Address_Line_2"
    @update
        @update-target-select "$PhysicalAddressNodes/Physical_City"
        @update-source-select "$Parameter:PhysicalNodes/Physical_City"
    @update
        @update-target-select "$PhysicalAddressNodes/Physical_Zip"
        @update-source-select "$Parameter:PhysicalNodes/Physical_Zip"
    @update
        @update-target-select "$PhysicalAddressNodes/Physical_Country"
        @update-source-select "$CountryStoreId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Admin_Country/Country_Code | Core_Admin_Country/Country_Name"

    @invoke
        @invoke-execute "Shared_Wikis.PhysicalStateLookup"
        @invoke-when "boolean(call('Core.CountryHasStates', $CountryStoreId))"
        @invoke-parameter "StateStoreId"
            @invoke-parameter-expression "$StateStoreId"
        @invoke-parameter "OutNode"
            @invoke-parameter-expression "$PhysicalAddressNodes/Physical_State"
            @invoke-parameter-direction "Out"
    
    @invoke
        @invoke-execute "Shared_Wikis.PhysicalStateText"
        @invoke-when "not(boolean(call('Core.CountryHasStates', $CountryStoreId)))"
        @invoke-parameter "StateText"
            @invoke-parameter-expression "$Parameter:PhysicalNodes/Physical_State"
        @invoke-parameter "OutString"
            @invoke-parameter-expression "$PhysicalAddressNodes/Physical_State"
            @invoke-parameter-direction "Out"

    @update
        @update-store-id "concat($Parameter:PhysicalAppName, '.', $Parameter:PhysicalStoreId)"
        @update-target-select "{{concat('/store/', $Parameter:PhysicalAppName)}}"
        @update-source-select "$PhysicalAddressNodes/*"
        @update-source-type "Subnodes"
@end-execute


@execute "CreateMailingAddress"
@comment
:AppName
:StoreId
:Nodes
@end-comment
    @update
        @update-target-select "$CountryStoreId"
        @update-source-select "coalesce($Parameter:Nodes/Mailing_Country/@refId, if(starts-with($Parameter:Nodes/Mailing_Country, 'str-'), $Parameter:Nodes/Mailing_Country, ''))"
    @update
        @update-target-select "$StateStoreId"
        @update-source-select "coalesce($Parameter:Nodes/Mailing_State/@refId, if(starts-with($Parameter:Nodes/Mailing_State, 'str-'), $Parameter:Nodes/Mailing_State, ''))"
  
    @update
        @update-target-select "$MailingAddressNodes"
        @update-source-select "''"
    @update
        @update-target-select "$MailingAddressNodes/Mailing_Address_Line_1"
        @update-source-select "$Parameter:Nodes/Mailing_Address_Line_1"
    @update
        @update-target-select "$MailingAddressNodes/Mailing_Address_Line_2"
        @update-source-select "$Parameter:Nodes/Mailing_Address_Line_2"
    @update
        @update-target-select "$MailingAddressNodes/Mailing_City"
        @update-source-select "$Parameter:Nodes/Mailing_City"
    @update
        @update-target-select "$MailingAddressNodes/Mailing_Zip"
        @update-source-select "$Parameter:Nodes/Mailing_Zip"
    @update
        @update-target-select "$MailingAddressNodes/Mailing_Country"
        @update-source-select "$CountryStoreId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Admin_Country/Country_Code | Core_Admin_Country/Country_Name"
    
    @invoke
        @invoke-execute "Shared_Wikis.MailingStateLookup"
        @invoke-when "boolean(call('Core.CountryHasStates', $CountryStoreId))"
        @invoke-parameter "StateStoreId"
            @invoke-parameter-expression "$StateStoreId"
        @invoke-parameter "OutNode"
            @invoke-parameter-expression "$MailingAddressNodes/Mailing_State"
            @invoke-parameter-direction "Out"
    
    @invoke
        @invoke-execute "Shared_Wikis.MailingStateText"
        @invoke-when "not(boolean(call('Core.CountryHasStates', $CountryStoreId)))"
        @invoke-parameter "StateText"
            @invoke-parameter-expression "$Parameter:Nodes/Mailing_State"
        @invoke-parameter "OutString"
            @invoke-parameter-expression "$MailingAddressNodes/Mailing_State"
            @invoke-parameter-direction "Out"

    @update
        @update-store-id "concat($Parameter:AppName, '.', $Parameter:StoreId)"
        @update-target-select "{{concat('/store/', $Parameter:AppName)}}"
        @update-source-select "$MailingAddressNodes/*"
        @update-source-type "Subnodes"
@end-execute


@execute "ClearMailingAddress"
@comment
:AppName
:StoreId
@end-comment
    @update
        @update-target-select "$AddressNodes"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_Address_Line_1"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_Address_Line_2"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_City"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_State"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_Zip"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mailing_Country"
        @update-source-select "''"
    @update
        @update-target-select "$AddressNodes/Mail_Add"
        @update-source-select "'false'"
    @update
        @update-target-select "$AddressNodes/Mail_Add/@text"
        @update-source-select "''"
    
    @update
        @update-store-id "concat($Parameter:AppName, '.', $Parameter:StoreId)"
        @update-target-select "{{concat('/store/', $Parameter:AppName)}}"
        @update-source-select "$AddressNodes/*"
        @update-source-type "Subnodes"
@end-execute



@comment
INTERNAL HELPER METHODS BELOW THIS LINE
@end-comment

@execute "MailingStateLookup"
    @update
        @update-target-select "$Lookup_Node"
        @update-source-select "''"

    @update
        @update-target-select "$Lookup_Node/Mailing_State"
        @update-source-select "$Parameter:StateStoreId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Admin_State_Province/State_Id | Core_Admin_State_Province/State_Code | Core_Admin_State_Province/State_Name"

    @update
        @update-target-select "$Parameter:OutNode"
        @update-source-select "$Lookup_Node/Mailing_State"
        @update-source-type "Node"
@end-execute

@execute "MailingStateText"
    @update
        @update-target-select "$Parameter:OutText"
        @update-source-select "$Parameter:StateText"
@end-execute

@execute "PhysicalStateLookup"
    @update
        @update-target-select "$Lookup_Node"
        @update-source-select "''"

    @update
        @update-target-select "$Lookup_Node/Physical_State"
        @update-source-select "$Parameter:StateStoreId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Admin_State_Province/State_Id | Core_Admin_State_Province/State_Code | Core_Admin_State_Province/State_Name"

    @update
        @update-target-select "$Parameter:OutNode"
        @update-source-select "$Lookup_Node/Physical_State"
        @update-source-type "Node"    
@end-execute

@execute "PhysicalStateText"
    @update
        @update-target-select "$Parameter:OutText"
        @update-source-select "$Parameter:StateText"
@end-execute