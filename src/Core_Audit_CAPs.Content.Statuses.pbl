﻿@title " "
@mode "AuthorEdit, ReviewerEdit, HybridEdit"

@comment
Desktop Addressed by Author Buttons
not(find-store-id()) will perform save and next
find-store-id() will perform save and refresh to display submit cap for review banner.
@end-comment

@block
    @block-visible-when "is-blank(Action)"
    
@comment
    Inline HTML is a hack because we don't have a legit cancel button
@end-comment
    
    <a class="btn btn-warning pointer pull-right" rql-action="cancel">^^Cancel|Cancel^^</a>
@end-block

@block
    @block-visible-when "not(%Item_Read_Only_When%true()%Item_Read_Only_When%) and not(call('Core.IsUserCAPHybrid', /store/Core_Audit_CAPs/Shared_Audit/@refId)) and call('CoreCAP.GetOverallCAPStatus') != 'Closed'"
    
    @block
        @block-visible-when "not(call('CoreCAP.IsActionNeededOnThisAudit'))"
        
        @button "Shared_Corrective_Action_Status_Button"
            @button-value "Addressed by Author"
            @button-show-value "No"
            @button-text "^^Addressed|Addressed^^"
            @button-action "Validate,Save,Next"
            @button-optional "Yes"
            @button-ui-style "btn-success pull-right margin-lr-mini"
            @button-align "Right"
            @button-inline "Yes"
    @end-block
    
    @block
        @block-visible-when "call('CoreCAP.IsActionNeededOnThisAudit')"
        
        @button "Shared_Corrective_Action_Status_Button"
            @button-value "Addressed by Author"
            @button-show-value "No"
            @button-text "^^Addressed|Addressed^^"
            @button-action "Validate,Save,Refresh"
            @button-optional "Yes"
            @button-ui-style "btn-success pull-right margin-lr-mini"
            @button-align "Right"
            @button-inline "Yes"
    @end-block
@end-block


@comment
CAP Reviewer Cancel, Addressed, Returned - Action Needed Buttons
@end-comment
@block
    @block-visible-when "call('CoreCAP.GetOverallCAPStatus') != 'Closed' and ((call('Core.IsUserCAPReviewer', /store/Core_Audit_CAPs/Shared_Audit/@refId) and Status/Status_Code = 'Addressed by Author') or call('Core.IsUserCAPHybrid', /store/Core_Audit_CAPs/Shared_Audit/@refId))"
    @block-ui-style "pull-right margin-lr-mini"
    
    @block
        @block-visible-when "is-blank(Action)"
        
        @block
            @block-visible-when "not(boolean-from-string(call('Core.IsUserCAPHybrid', /store/Core_Audit_CAPs/Shared_Audit/@refId)))"
        
            @button "Action"
                @button-value "PreReturn"
                @button-text "^^Return|Return^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-action "Save"
                @button-ui-style "btn-danger margin-lr-mini pull-right"
        
        @end-block        
        
        @button "Shared_Corrective_Action_Status_Button"
            @button-value "Addressed"
            @button-show-value "No"
            @button-inline "Yes"
            @button-text "^^Addressed|Addressed^^"
            @button-action "Validate,Save,Next"
            @button-optional "Yes"
            @button-ui-style "btn-success margin-lr-mini pull-right"
    @end-block

    @block "Dummy_Buttons"
        @block-visible-when "is-not-blank(Action) and Action != 'Approve'"
        
        @link "False_Button"
            @link-text "^^Cancel|Cancel^^"
            @link-ui-style "btn btn-warning margin-lr-mini pull-right disabled white"
            
        @link "False_Button"
            @link-text "^^Return|Return^^"
            @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"

        @link "False_Button"
            @link-text "^^Addressed|Addressed^^"
            @link-ui-style "btn btn-success margin-lr-mini pull-right disabled white"
    @end-block
    
    @newline
    
    @block "Return"
        @block-visible-when "Action = 'PreReturn' or Action = 'Returned - Action Needed'"
        @block-ui-style "form-horizontal pull-right"
        
        @text "Reason_For_Return"
            @text-header "^^Reason for Return|Reason for Return^^:"
            @text-rows "3"
            @text-validation-test "is-not-blank(Reason_For_Return) or Action = 'PreReturn'"
            @text-validation-error "^^A reason is required|A reason is required^^"
        
        @button "Action"
            @button-value "Cancel"
            @button-text "^^Cancel|Cancel^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-warning margin-lr-mini pull-right"
            @button-action "Save, Validate"
        
        @button "Action"
            @button-value "Returned - Action Needed"
            @button-show-value "No"
            @button-text "^^Return|Return^^"
            @button-action "Validate,Save,Reload"
            @button-inline "Yes"
            @button-optional "Yes"
            @button-ui-style "btn-danger margin-lr-mini pull-right"
    @end-block
@end-block

@block
@block-visible-when "boolean-from-string(call('Core.CheckPermission', 'Core_Audit_CAPs.ReopenCAP')) and Status/Status_Code = 'Addressed'"

    @block
    @block-visible-when "is-blank(Action)"
    
        @button "Action"
            @button-value "PreReturn"
            @button-text "^^Return|Return^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-action "Save"
            @button-ui-style "btn-danger margin-lr-mini pull-right"
    
    @end-block

    @block "Dummy_Buttons"
        @block-visible-when "is-not-blank(Action) and Action != 'Approve'"
        
        @link "False_Button"
            @link-text "^^Cancel|Cancel^^"
            @link-ui-style "btn btn-warning margin-lr-mini pull-right disabled white"
            
        @link "False_Button"
            @link-text "^^Return|Return^^"
            @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"

    @end-block
    
    @newline
    
    @block "Return"
        @block-visible-when "Action = 'PreReturn' or Action = 'Returned - Action Needed'"
        @block-ui-style "form-horizontal pull-right"
        
        @text "Reason_For_Return"
            @text-header "^^Reason for Return|Reason for Return^^:"
            @text-rows "3"
            @text-validation-test "is-not-blank(Reason_For_Return) or Action = 'PreReturn'"
            @text-validation-error "^^A reason is required|A reason is required^^"
        
        @button "Action"
            @button-value "Cancel"
            @button-text "^^Cancel|Cancel^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-warning margin-lr-mini pull-right"
            @button-action "Save, Validate"
        
        @button "Action"
            @button-value "Returned - Action Needed"
            @button-show-value "No"
            @button-text "^^Return|Return^^"
            @button-action "Validate,Save,Reload"
            @button-inline "Yes"
            @button-optional "Yes"
            @button-ui-style "btn-danger margin-lr-mini pull-right"
    @end-block

@end-block
