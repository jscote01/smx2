﻿@app "Core_Entity_Management_Contact_Changes"
    @app-offline "Yes"
    
@display "No"

@language "all"

@analyze
    @query "EM_Contact_Changes"
    @query-publish "Core_Entity_Management.Contact_Location_Changes"
    
        @column "EM_ID"
            @column-source "StoreId"
        
        @column "Core_Organization_Location"
    @end-query
    
    @query "Locations"
    @query-queue "Core_Organization_Location.Master_Q"
    
        @column "L_ID"
            @column-source "StoreId"
    @end-query
    
    @query "Combined"
        @subquery "EM_Contact_Changes"
        @subquery "Locations"
            @subquery-join "EM_Contact_Changes.Core_Organization_Location = Locations.L_ID"
            @subquery-join-type "Inner"
        
        @column "EM_ID"
            @column-source "EM_Contact_Changes.EM_ID"
    @end-query
    
    @query "Distinct_EM"
    @query-distinct-values "Yes"
        @subquery "Combined"
    
        @column "EM_ID"
    @end-query
    
    @query "EM_Records"
    @query-queue "Core_Entity_Management.Master_Q"
    @query-queue-datafirewall "No"
    
        @column "EMR_ID"
            @column-source "StoreId"    
        @column "Request_Num"
        @column "Request_Type"
        @column "Entity_Type"
        @column "Entity_StoreId"
        @column "Entity_Name"
        @column "Request_Type_Calc"
        @column "Status"
        @column "Status_Code"
        @column "Status_Name"
        @column "Requested_By"
        @column "Requested_By_Informal_Full_Name"
        @column "Requested_Date"
        @column "Discarded_By"
        @column "Discarded_Date"
        @column "Approved_By"
        @column "Approved_By_Informal_Full_Name"
        @column "Approved_Date"
        @column "Organization"    
        @column "Organization_Location" 
        @column "Verify_Status"  
        @column "Pending_Processing"
        @column "Processed_On"
    @end-query
    
    @query "Combined_2"
        @subquery "Distinct_EM"
        @subquery "EM_Records"
            @subquery-join "Distinct_EM.EM_ID = EM_Records.EMR_ID"
            @subquery-join-type "Inner"
    
        @column "EMR_ID"
            @column-source "EM_Records.EMR_ID"    
        @column "Request_Num"
            @column-source "EM_Records.Request_Num"
        @column "Request_Type"
            @column-source "EM_Records.Request_Type"
        @column "Entity_Type"
            @column-source "EM_Records.Entity_Type"
        @column "Entity_StoreId"
            @column-source "EM_Records.Entity_StoreId"
        @column "Entity_Name"
            @column-source "EM_Records.Entity_Name"
        @column "Request_Type_Calc"
            @column-source "EM_Records.Request_Type_Calc"
        @column "Status"
            @column-source "EM_Records.Status"
        @column "Status_Code"
            @column-source "EM_Records.Status_Code"
        @column "Status_Name"
            @column-source "EM_Records.Status_Name"
        @column "Requested_By"
            @column-source "EM_Records.Requested_By"
        @column "Requested_By_Informal_Full_Name"
            @column-source "EM_Records.Requested_By_Informal_Full_Name"
        @column "Requested_Date"
            @column-source "EM_Records.Requested_Date"
        @column "Discarded_By"
            @column-source "EM_Records.Discarded_By"
        @column "Discarded_Date"
            @column-source "EM_Records.Discarded_Date"
        @column "Approved_By"
            @column-source "EM_Records.Approved_By"
        @column "Approved_By_Informal_Full_Name"
            @column-source "EM_Records.Approved_By_Informal_Full_Name"
        @column "Approved_Date"
            @column-source "EM_Records.Approved_Date"
        @column "Organization"    
            @column-source "EM_Records.Organization"
        @column "Organization_Location" 
            @column-source "EM_Records.Organization_Location"
        @column "Verify_Status"  
            @column-source "EM_Records.Verify_Status"
        @column "Pending_Processing"
            @column-source "EM_Records.Pending_Processing"
        @column "Processed_On"  
            @column-source "EM_Records.Processed_On"
    @end-query
    
    @query "Other_EM"
    @query-queue "Core_Entity_Management.Master_Q"
        @column "EMR_ID"
            @column-source "StoreId"    
        @column "Request_Num"
        @column "Request_Type"
        @column "Entity_Type"
        @column "Entity_StoreId"
        @column "Entity_Name"
        @column "Request_Type_Calc"
        @column "Status"
        @column "Status_Code"
        @column "Status_Name"
        @column "Requested_By"
        @column "Requested_By_Informal_Full_Name"
        @column "Requested_Date"
        @column "Discarded_By"
        @column "Discarded_Date"
        @column "Approved_By"
        @column "Approved_By_Informal_Full_Name"
        @column "Approved_Date"
        @column "Organization"    
        @column "Organization_Location" 
        @column "Verify_Status"  
        @column "Pending_Processing"
        @column "Processed_On"  
    @end-query
    
    @query "Combined_3"
    @query-type "Union"
    @query-distinct-values "Yes"
    @query-store-id "EMR_ID"
        @subquery "Combined_2"
        @subquery "Other_EM"
        
    @end-query
@end-analyze

@queue "In_Progress_Q"
@queue-query "Combined_3"
@queue-header "^^Awaiting_Review_Change_Requests|Awaiting Review^^"
@queue-activator "^^Change Requests|Change Requests^^"
@queue-sequence "13115"
@queue-test "call('Core.CheckPermission', 'Core_Entity_Management_In_Progress_Q') and call('Core.CheckCapability', 'CORE.EntityManagement')"
@queue-allow-new "No"
@queue-new-text "^^Create new|Create new^^ ^^Request|Request^^"
@queue-filter "Status_Code = 'EM_Requested'"
@queue-allow-navigate "No"
@queue-sort "Requested_Date DESC"

    @action
        @action-name "Navigate"
        @action-text "Request_Num | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "Request_Num"
        @column-header "^^Request Number|Request Number^^"
    @column "Request_Type_Calc"
        @column-header "^^Request Type|Request Type^^"
    
    @column "Entity_Name"    
        @column-header "^^Entity_Name|Name^^"
    
    @column "Requested_By"
        @column-hidden "Yes"    
    @column "Requested_By_Informal_Full_Name"
        @column-header "^^Requested By|Requested By^^"  
    @column "Requested_Date"
        @column-header "^^Requested Date|Requested Date^^"
        @column-type "Timestamp"
@end-queue

@queue "Approved_Q"
@queue-query "Combined_3"
@queue-header "^^Processed_Change_Requests|Processed^^"
@queue-activator "^^Change Requests|Change Requests^^"
@queue-sequence "13120"
@queue-test "call('Core.CheckPermission', 'Core_Entity_Management_Approved_Q') and call('Core.CheckCapability', 'CORE.EntityManagement')"
@queue-filter "Status_Code IN ('EM_Approved', 'EM_Rejected', 'EM_Processing', 'EM_Error')"
@queue-allow-new "No"
@queue-allow-navigate "No"
@queue-sort "Approved_Date DESC"

    @action
        @action-name "Navigate"
        @action-text "Request_Num | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "Request_Num"
        @column-header "^^Request Number|Request Number^^"
    @column "Request_Type_Calc"
        @column-header "^^Request Type|Request Type^^"
    
    @column "Entity_Name"    
        @column-header "^^Entity_Name|Name^^"
    
    @column "Requested_By"
        @column-hidden "Yes"    
    @column "Requested_By_Informal_Full_Name"
        @column-header "^^Requested By|Requested By^^"  
    @column "Requested_Date"
        @column-header "^^Requested Date|Requested Date^^"
        @column-type "Timestamp"
    
    @column "Approved_By"
        @column-hidden "Yes"    
    @column "Approved_By_Informal_Full_Name"
        @column-header "^^Approved By|Approved By^^"  
    @column "Approved_Date"
        @column-header "^^Processed Date|Processed Date^^"
        @column-type "Timestamp"
    
    @column "Status_Name"
        @column-header "^^Status|Status^^"
        @column-ui-style "{{if(Status_Code = 'EM_Error', 'bcolor-danger white', '')}}"
    
    @column "Status_Code"
        @column-hidden "Yes"
@end-queue


@queue "Embedded_Approved_Q"
@queue-query "Combined_3"
@queue-header "^^Record History|Record History^^"
@queue-filter "Status_Code IN ('EM_Approved') AND (Entity_StoreId = {{store-id()}} OR Entity_StoreId = {{coalesce(call('Core.GetUserContactSID', store-id()), ' ')}})"
@queue-allow-new "No"
@queue-allow-navigate "No"
@queue-sort "Approved_Date DESC"

    @action
        @action-name "Navigate"
        @action-text "Requested_By_Informal_Full_Name | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "Entity_StoreId"    
        @column-hidden "Yes"        
    
    @column "Requested_By_Informal_Full_Name"
        @column-header "^^Requested By|Requested By^^"  
        
    @column "Approved_Date"
        @column-header "^^Processed Date|Processed Date^^"
        @column-type "Timestamp"
        
    @column "Request_Type_Calc"
        @column-header "^^Request Type|Request Type^^"
    
    @column "Status_Code"
        @column-hidden "Yes"
@end-queue