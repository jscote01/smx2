﻿@topic
@topic-title "Assessing each element of the Yum! FSA audit (S)"
@topic-number "52"

@block "_Controls_Block"
@block-container "Controls"
@block-ui-style "form-horizontal"

	@choice "_52_Choice"
	@choice-style "RadioButtonList"
	@choice-columns "3"
	@choice-direction "Horizontal"
	@choice-postback "Yes"

		@option
		@option-text "Yes"
		@option-ui-style-checked "btn btn-medium white bcolor-success"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "No"
		@option-ui-style-checked "btn btn-medium white bcolor-danger"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "N/A"
		@option-ui-style-checked "btn btn-medium white bcolor-inverse"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

	@end-choice

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_52_Choice) and  _52_Choice = 'No'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_52_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_52_Choice) or _52_Choice [.= 'N/A' or .='Yes']"
				@text-expression "'Does not assess each element of the Yum! FSA audit (S)'"
				@text-expression-when "is-changed(_52_Choice) and is-blank(_52_Comments) and _52_Choice = 'No'"
				@text-ui-style "block"

				@block

					@text "_52_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block

					@text "_52_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_52_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "52"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "1"
	@finding-select "_52_Comments"
	@finding-score-when "is-not-blank(_52_Choice) and _52_Choice != 'N/A'"
	@finding-meta-Question_Number "52"

		@case
		@case-type "Positive"
		@case-points "1"
		@case-test "not(_52_Choice = 'No')"

		@case
		@case-type "Secondary"
		@case-points "_52_Selected_Points + 0"
		@case-test "_52_Choice = 'No'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_52_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-blank(_52_Selected_Points), 0, _52_Selected_Points)"
		@text-expression-when "is-blank(_52_Selected_Points)"

	@end-block

@end-block
