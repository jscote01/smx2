﻿@mode "Main, Shared"
@title "^^Audit Parameters|Audit Parameters^^"

@block
@block-ui-style "well well-mini"
@block-visible-when "true()"

    @text "Audit_Guidelines"
    @text-header "^^Audit Guidelines|Audit Guidelines^^:"
    @text-header-ui-style "bold"
    @text-type "RichText"
    @text-optional "Yes"
    @text-footer "^^Audit_Guidelines_Footer|Special instructions for the auditor^^"

@end-block

@choice "Audit_Frequency"
@choice-header "^^Audit Frequency|Audit Frequency^^:"
@choice-header-ui-style "bold"
@choice-columns "3"
    @option
    @option-text "^^Monthly|Monthly^^"
    @option-value "12" 
    @option
    @option-text "^^Quarterly|Quarterly^^"
    @option-value "4"
    @option
    @option-text "^^Semi-Annually|Semi-Annually^^"
    @option-value "2"
    @option
    @option-text "^^Annually|Annually^^"
    @option-value "1"
    @option
    @option-text "^^Other|Other^^"
    @option-value "Other"
@end-choice "Audit_Frequency"
 
@block
@block-visible-when "Audit_Frequency='Other'"

    @text "Audit_Frequency_Other"
    @text-header "^^Audit_Frequency_Other|Other frequency explanation^^:"
    @text-header-ui-style "bold"
    
    @text "Audit_Frequency_Visits_Per_Year"
    @text-header "^^Audit_Frequency_Visits_Per_Year|How many times per year will sites be visited for 'Other' frequency^^?"
    @text-header-ui-style "bold"
    @text-type "Integer"
    @text-mask "999"
@end-block

@text "Audit_Process_Hours"
@text-header "^^Audit Duration|Audit Duration^^:"
@text-header-ui-style "bold"
@text-footer "^^Audit_Duration_Footer|Please enter in 15 minute increments.  For example, 3.25 or 3.5 or 3.75 or 3 hours.^^"
@text-type "Number"
@text-mask "99.99"
@text-optional "Yes"

@text "Target_Date"
@text-header "^^Target_Date|Initial Round Start Date^^:"
@text-header-ui-style "bold"
@text-type "Date"

@choice "Auditor_Skill_Requirements"
@choice-header "^^Auditor_Skill_Requirements|Specialist specific skill requirements^^:"
@choice-header-ui-style "bold"
@choice-style "CheckBoxList"
@choice-columns "2"
@choice-optional "true"
    @option "Spanish"
    @option-text "^^Auditor must speak/read|Auditor must speak/read^^ ^^Spanish|Spanish^^"
    @option-value "Spanish"
    @option "French"
    @option-text "^^Auditor must speak/read|Auditor must speak/read^^ ^^French|French^^"
    @option-value "French"
    @option "CP_FS_certified"
    @option-text "^^CP-FS certified|CP-FS certified^^"
    @option-value "CP_FS_certified"
    @option "Field_training_required"
    @option-text "^^Field training required|Field training required^^"
    @option-value "Field_training_required"
    @option "Online_training_required"
    @option-text "^^On-line training required|On-line training required^^"
    @option-value "Online_training_required"
    @option "Customer_approved_specialists"
    @option-text "^^Customer approved specialists|Customer approved specialists^^"
    @option-value "Customer_approved_specialists"
    @option "SQF_Credentials"
    @option-text "^^SQF credentials|SQF credentials^^"
    @option-value "SQF_credentials"
    @option "Other"
    @option-text "^^Other requirements|Other requirements^^"
    @option-value "Other"
@end-choice "Active_Auditor_Skill_Requirements"

@block
@block-visible-when "Auditor_Skill_Requirements/Other = 'Other'"

@text "Auditor_Skill_Requirements_Other"
@text-header "^^Auditor_Skill_Requirements_Other|Please explain 'Other' requirements^^:"
@text-header-ui-style "bold"
@text-rows "3"
@end-block

@choice "Audit_Security_Requirements"
@choice-header "^^Audit_Security_Requirements|Specialist Security Requirements^^:"
@choice-header-ui-style "bold"
@choice-style "CheckBoxList"
@choice-optional "Yes"
@choice-columns "2"
    @option "Drivers_License"
    @option-text "^^Driver's License|Driver's License^^"
    @option-value "Driver's License"
    @option "US_Citizenship"
    @option-text "^^U.S. Citizenship|U.S. Citizenship^^"
    @option-value "U.S. Citizenship"
    @option "Canada_Citizenship"
    @option-text "^^Canadian Citizenship|Canadian Citizenship^^"
    @option-value "Canadian Citizenship"
    @option "Advance_Notification_24_Hours"
    @option-text "^^Advance Notification - 24 hours|Advance Notification - 24 hours^^"
    @option-value "Advance Notification - 24 hours"
    @option "Advance_Notification_48_Hours"
    @option-text "^^Advance Notification - 48 hours|Advance Notification - 48 hours^^"
    @option-value "Advance Notification - 48 hours"
    @option "Other"
    @option-text "^^Other|Other^^"
    @option-value "Other"
@end-choice "Audit_Security_Requirements"

@block
@block-visible-when "is-not-blank(Audit_Security_Requirements/Other)"
    @text "Audit_Security_Requirements_Other"
    @text-header "^^Audit_Security_Requirements_Other|Please specify other security requirements for the Specialist^^:"
    @text-header-ui-style "bold"
    @text-rows "3"
@end-block

@choice "Audit_Dress_Code"
@choice-header "^^Audit_Dress_Code|Dress code requirements^^:"
@choice-header-ui-style "bold"
@choice-style "CheckBoxList"
@choice-optional "Yes"
@choice-columns "3"
    @option "Hat"
    @option-text "^^Hat|Hat^^"
    @option-value "Hat"
    @option "Hairnet"
    @option-text "^^Hairnet|Hairnet^^"
    @option-value "Hairnet"
    @option "Other"
    @option-text "^^Other|Other^^"  
    @option-value "Other"
@end-choice "Audit_Dress_Code"

@block
@block-visible-when "is-not-blank(Audit_Dress_Code/Other)"
    @text "Audit_Dress_Code_Other"
    @text-header "^^Audit_Dress_Code_Other|Please explain 'Other' dress code requirements in more detail^^:"
    @text-header-ui-style "bold"
    @text-rows "2"
@end-block

@choice "Protocol_Customizations_Start_Of_Audit"
@choice-header "^^Custom protocols for the start of the audit|Custom protocols for the start of the audit^^?"
@choice-header-ui-style "bold"
@choice-columns "2"
    @option "Yes"
    @option-text "^^Yes|Yes^^"
    @option-value "Yes"
            
    @option "No"
    @option-text "^^No|No^^"
    @option-value "No"
@end-choice

@block
@block-visible-when "Protocol_Customizations_Start_Of_Audit='Yes'"

    @text "Protocol_Customizations_Start_Of_Audit_Details"
    @text-header "^^Protocol_Customizations_Start_Of_Audit_Details|Protocol customizations for starting the audit^^:"
    @text-header-ui-style "bold"
    @text-rows "5"
    @text-footer "^^Protocol_Customizations_Start_Of_Audit_Details_Footer|List any customizations related to starting the audit (e.g. no lab coats, always enter from the back door)^^"
    
    @choice "Protocol_Customizations_Start_Of_Audit_Reoccurrence"
    @choice-header "^^Protocol_Customizations_Start_Of_Audit_Reoccurrence|Customizations only for the first audit or every audit^^?"
    @choice-header-ui-style "bold"
    @choice-columns "2"
        @option "First_audit_only"
        @option-text "^^First audit only|First audit only^^"
        @option-value "First audit only"
            
        @option "Every_audit"
        @option-text "^^Every audit|Every audit^^"
        @option-value "Every audit"
    @end-choice

@end-block

@choice "Protocol_Customizations_End_Of_Audit"
@choice-header "^^Protocol_Customizations_End_Of_Audit|Custom protocols for the end of the audit^^?"
@choice-header-ui-style "bold"
@choice-columns "2"
    @option "Yes"
    @option-text "^^Yes|Yes^^"
    @option-value "Yes"
            
    @option "No"
    @option-text "^^No|No^^"
    @option-value "No"
@end-choice

@block
@block-visible-when "Protocol_Customizations_End_Of_Audit='Yes'"

    @text "Protocol_Customizations_End_Of_Audit_Details"
    @text-header "^^Protocol_Customizations_End_Of_Audit_Details|Protocol customizations for ending the audit^^:"
    @text-header-ui-style "bold"
    @text-rows "5"
    @text-footer "^^Protocol_Customizations_End_Of_Audit_Details_Footer|List any customizations for ending the audit (e.g. print an addendum, customized debrief, etc).^^"
    
    @choice "Protocol_Customizations_End_Of_Audit_Reoccurrence"
    @choice-header "^^Customizations only for the first audit or every audit|Customizations only for the first audit or every audit^^?"
    @choice-header-ui-style "bold"
    @choice-columns "2"
        @option "First_audit_only"
        @option-text "^^First audit only|First audit only^^"
        @option-value "First audit only"
            
        @option "Every_audit"
        @option-text "^^Every audit|Every audit^^"
        @option-value "Every audit"
    @end-choice

@end-block

@newline "Medium"

@block
@block-visible-when "false()"

    {label(medium primary):"^^Client_Specific_Terminology_Label|Please explain any Client-Specific Terminology^^"}
    
    @newline "Small"
    
    @text "Client_Specific_Terminology_Audit"
    @text-header "^^Audit|Audit^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Restaurant"
    @text-header "^^Restaurant|Restaurant^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Store"
    @text-header "^^Store|Store^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Employee"
    @text-header "^^Employee|Employee^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Store_Manager"
    @text-header "^^Store Manager|Store Manager^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_District_Manager"
    @text-header "^^District Manager|District Manager^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Regional_Manager"
    @text-header "^^Regional Manager|Regional Manager^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Franchise"
    @text-header "^^Franchise|Franchise^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Quality_Assurance"
    @text-header "^^Quality Assurance|Quality Assurance^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology_Steritech"
    @text-header "^^Steritech|Steritech^^="
    @text-header-ui-style "bold"
    @text-optional "Yes"
    
    @text "Client_Specific_Terminology"
    @text-header "^^Other Client Specific Terminology|Other Client Specific Terminology^^?"
    @text-header-ui-style "bold"
    @text-optional "Yes"

@end-block

@choice "Non_Standard_Equipment"
@choice-header "^^Non_Standard_Equipment|Any non-standard equipment required^^?"
@choice-footer "^^Non_Standard_Equipment_Footer|Standard equipment includes: Thermocouple, Digital Thermometer, Chlorine Test Kit, Ammonium Test Kit, Iodine Test Kit, Infrared Thermometer, and Flashlight^^"
@choice-header-ui-style "bold"
@choice-columns "2"
    @option "Yes"
    @option-text "^^Yes|Yes^^"
    @option-value "Yes"
            
    @option "No"
    @option-text "^^No|No^^"
    @option-value "No"
@end-choice



@block
@block-visible-when "Non_Standard_Equipment = 'Yes'"

    @choice "Non_Standard_Equipment_Required"
    @choice-header "^^Non_Standard_Equipment_Required|What non-standard equipment is required? (Check all that apply)^^"
    @choice-header-ui-style "bold"
    @choice-style "CheckBoxList"
    @choice-columns "2"
        @option "Scale"
        @option-text "^^Scale|Scale^^"
        @option-value "Scale"
        @option "Camera"
        @option-text "^^Camera|Camera^^"
        @option-value "Camera"
        @option "Environmental_Swabs"
        @option-text "^^Environmental Swabs|Environmental Swabs^^"
        @option-value "Environmental_Swabs"
        @option "Product_Sampling"
        @option-text "^^Product Sampling|Product Sampling^^"
        @option-value "Product_Sampling"
        @option "Stop_Watch"
        @option-text "^^Stop Watch|Stop Watch^^"
        @option-value "Stop_Watch"
        @option "Tape_Measure"
        @option-text "^^Tape Measure|Tape Measure^^"
        @option-value "Tape_Measure"
        @option "Black_Light"
        @option-text "^^Black Light|Black Light^^"
        @option-value "Black_Light"
        @option "Client_Material"
        @option-text "^^Client Material|Client Material^^"
        @option-value "Client_Material"
        @option "Scanner"
        @option-text "^^Scanner|Scanner^^"
        @option-value "Scanner"
        @option "Other"
        @option-text "^^Other|Other^^"
        @option-value "Other"
    @end-choice "Non_Standard_Equipment_Required"

    @block "Non_std_eqpt_other"
    @block-visible-when "Non_Standard_Equipment_Required/Other = 'Other'"
        @text "Non_Standard_Equipment_Required_Other"
        @text-header "^^Non_Standard_Equipment_Required_Other|Other non-standard required equipment^^:"
        @text-header-ui-style "bold"
        @text-rows "2"
    @end-block "Non_std_eqpt_other"
 
    @block
    @block-visible-when "Non_Standard_Equipment_Required/Scale='Scale'"
        @choice "Non_Std_Equipment_Req_Scale_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Scale_Cust_Supplied|Client to supply the scale^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice 
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Camera='Camera'"
        @choice "Non_Std_Equipment_Req_Camera_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Camera_Cust_Supplied|Client to supply the camera^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice 
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Environmental_Swabs='Environmental_Swabs'"
        @choice "Non_Std_Equipment_Req_Swabs_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Swabs_Cust_Supplied|Client to supply the environmental swabs^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice 
    @end-block
 
    @block
    @block-visible-when "Non_Standard_Equipment_Required/Product_Sampling='Product_Sampling'"
        @choice "Non_Std_Equipment_Req_Product_Sampling_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Product_Sampling_Cust_Supplied|Client to supply the product sampling^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Stop_Watch='Stop_Watch'"
        @choice "Non_Std_Equipment_Req_Stop_Watch_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Stop_Watch_Supplied|Client to supply the stop watch^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Tape_Measure='Tape_Measure'"
        @choice "Non_Std_Equipment_Req_Tape_Measure_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Tape_Measure_Supplied|Client to supply the tape measure^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Black_Light='Black_Light'"
        @choice "Non_Std_Equipment_Req_Black_Light_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Black_Light_Cust_Supplied|Client to supply the black light^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice 
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Client_Material='Client_Material'"
        @choice "Non_Std_Equipment_Req_Client_Material_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Client_Material_Cust_Supplied|Client to supply the client material^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Scanner='Scanner'"
        @choice "Non_Std_Equipment_Req_Scanner_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Scanner_Cust_Supplied|Client to supply the scanner^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice
    @end-block

    @block
    @block-visible-when "Non_Standard_Equipment_Required/Other='Other'"
        @choice "Non_Std_Equipment_Req_Other_Cust_Supplied"
        @choice-header "^^Non_Std_Equipment_Req_Other_Cust_Supplied|Client to supply the other equipment^^?"
        @choice-header-ui-style "bold"
        @choice-style "CheckBoxList"
        @choice-optional "true"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
        @end-choice 
    @end-block

@end-block