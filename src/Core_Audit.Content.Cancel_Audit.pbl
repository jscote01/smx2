﻿@title " "
    @title-ui-style "hide"

@mode "Cancel_Audit"
@comment
User must have permissions Core_Audit.Admin_Tools and Core_Audit.Cancel_Audit in order to cancel an audit
@end-comment

@comment
Banner to be shown when an audit is canceled while offline
@end-comment
@block
    @block-visible-when "not(is-offline()) and call('Core.CheckPermission', 'Core_Audit.Cancel_Audit') and Status[.='Pending Cancelation'] and (is-blank(Canceled_Audit_Synched) or (is-not-blank(Canceled_Audit_Synched) and user-id() != Audit_Canceled_By/@refId))"
    @block-ui-style "well well-mini well-info-light inverse"
    @icon "icon-info-sign info medium"
    {label(medium margin-lr-mini): '^^Please Note|Please Note^^: ^^Pending_Cancel_Audit_Warning|This Audit has already been synced offline. Contact the Auditor to inform them of the cancelation^^. [^^Auditor Name|Auditor Name^^: {{Auditor/Informal_Full_Name}} / ^^Auditor Email|Audit E-mail^^: {{store(Auditor/@refId)/*[1]/Email}}]'}
@end-block

@comment
Dialog to cancel an audit
@end-comment
@block
    @block-visible-when "not(is-offline()) and call('Core.CheckPermission', 'Core_Audit.Cancel_Audit') and Status[.='Assigned' or .= 'Scheduled' or .= 'In-Progress']"
    @block-ui-style "form-horizontal"

    {label(medium info block):"^^Cancel the Audit|Cancel the Audit^^"}

    @newline "Medium"

    @block
        @block-ui-style "well well-mini well-info-light inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Cancel_Audit_Warning|To cancel the Audit, provide a reason and select the "Cancel Audit" button.  This action can not be undone.^^'}
    @end-block    

    @text "Cancelation_Reason"
        @text-header "^^Reason for Cancelation|Reason for Cancelation^^:"
        @text-columns "40"
        @text-rows "5"

    @button "Confirm_Cancel_Audit"
        @button-value "Cancel"
        @button-text "^^Cancel Audit|Cancel Audit^^"
        @button-optional "Yes"
        @button-ui-style "btn-danger pull-right"    
        @button-inline "Yes" 
        @button-action "Validate, Save, Refresh"
        @button-confirm "^^Confirm_Cancel_Audit|Select Yes to confirm the cancelation of this audit^^"

@end-block

@comment
Dialog to void an audit
NOTE: This is disabled for now.  To be added with RQLPBL-1512
@end-comment
@block
    @block-visible-when "not(is-offline()) and call('Core.CheckPermission', 'Core_Audit.Cancel_Audit') and Status[.='Completed' or .= 'Closed'] and false()"
    @block-ui-style "form-horizontal"

    {label(medium info block):"^^Void the Audit|Void the Audit^^"}

    @newline "Medium"

    @block
        @block-ui-style "well well-mini well-info-light inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Cancel_Audit_Warning|To void the Audit, provide a reason and select the "Void Audit" button.  This action can not be undone.^^'}
    @end-block    

    @text "Cancelation_Reason"
        @text-header "^^Reason for Void|Reason for Void^^:"
        @text-columns "40"
        @text-rows "5"

    @button "Confirm_Cancel_Audit"
        @button-value "Cancel"
        @button-text "^^Void Audit|Void Audit^^"
        @button-optional "Yes"
        @button-ui-style "btn-danger pull-right"    
        @button-inline "Yes" 
        @button-action "Validate, Save, Refresh"
        @button-confirm "^^Confirm_Cancel_Audit|Select Yes to confirm the voiding of this audit^^"

@end-block

@comment
Dialog to accept or confirm cancelation of a previously canceled audit that was offline but has now been brought back online as Completed or Closed
@end-comment
@block
    @block-visible-when "not(is-offline()) and call('Core.CheckPermission', 'Core_Audit.Cancel_Audit') and Status[.='Pending Cancelation'] and is-not-blank(Canceled_Audit_Synched) and user-id() = Audit_Canceled_By/@refId"
    @block-ui-style "form-horizontal"

    {label(medium info block):"^^Confirm Voiding of the Audit|Confirm Voiding of the Audit^^"}

    @newline "Medium"

    @block
        @block-ui-style "well well-mini well-info-light inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini): '^^Please Note|Please Note^^: ^^Audit_Canceled_Offline_Status|This Audit was canceled while offline and has now been synced with a status of^^ {{localize-token(Status_When_Synched)}}. ^^Audit_Canceled_Offline_Prompt|Please indicate if this Audit should be Accepted or Voided. This action can not be undone.^^'}
    @end-block    

    @button "Confirm_Pending_Canceled_Audit"
        @button-value "Cancel"
        @button-text "^^Void Audit|Void Audit^^"
        @button-optional "Yes"
        @button-ui-style "btn-danger pull-right margin-lr-mini"
        @button-inline "Yes" 
        @button-action "Validate, Save, Refresh"
        @button-confirm "^^Confirm_Cancel_Audit|Select Yes to confirm the voiding of this audit^^"

    @button "Confirm_Pending_Canceled_Audit"
        @button-value "Accept"
        @button-text "^^Accept Audit|Accept Audit^^"
        @button-optional "Yes"
        @button-ui-style "btn-success pull-right margin-lr-mini"
        @button-inline "Yes" 
        @button-action "Validate, Save, Refresh"
        @button-confirm "^^Confirm_Accept_Canceled_Audit|Select Yes to confirm this previously canceled audit should now be accepted^^"

@end-block

@newline "Medium"

@comment
IT block to show user that canceled the audit
@end-comment
@block
    @block-visible-when "is-not-blank(Audit_Canceled_By) and call('Core.IsITUser')"
    @block-ui-style "well"
    
    {label(medium info block):'IT Section'}
    
    @newline "Small"
    
    @lookup "Audit_Canceled_By"
        @lookup-header "^^Canceled/Voided By|Canceled/Voided By^^:"
        @lookup-allow-new-when "false()"
        @lookup-allow-navigate-when "call('Core.AllowLookupNavigation','User')"    
        @lookup-queue "User.Active_Lookup_Q"
        @lookup-select "User/User_Name | User/Informal_Full_Name"
        @lookup-field "Informal_Full_Name"
        @lookup-optional "Yes"
        @lookup-ui-style "width-auto"

@end-block