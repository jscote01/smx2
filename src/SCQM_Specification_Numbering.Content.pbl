﻿@title "^^Specification Number|Specification Number^^"

@choice "Numbering_Type"
    @choice-style "DropDownList"
    @choice-header "^^Numbering Type|Numbering Type^^:"
    @option
        @option-text "^^New|New^^"
        @option-value "New"
    @option
        @option-text "^^Clone|Clone^^"
        @option-value "Clone"
    @option
        @option-text "^^Child|Child^^"
        @option-value "Child"
    @option
        @option-text "^^Revision|Revision^^"
        @option-value "Revision"
    @option
        @option-text "^^Variation|Variation^^"
        @option-value "Variation"
@end-choice

@block
    @block-visible-when "Numbering_Type[.='Child' or .='Variation' or .='Revision']"

    @lookup "SCQM_Specification_Numbering"
        @lookup-header "^^Starting Number|Starting Number^^:"
        @lookup-allow-new-when "false()"
        @lookup-queue "SCQM_Specification_Numbering.Master_Q"
        @lookup-filter "StoreId != {{store-id()}}"
        @lookup-select "SCQM_Specification_Numbering/*"
        @lookup-field "Full_Number"
        @lookup-ui-style "width-auto"        

@end-block

@checkbox "Is_Parent"
    @checkbox-header "^^Is Parent?|Is Parent?^^"
    @checkbox-expression "if(Numbering_Type = 'New' or Numbering_Type = 'Clone', 'true', Is_Parent)"

@block
    @block-ui-style "clear well well-mini"

    {label(medium block): '^^Choices_part_of_specification_header|These choices would be selected as part of the specification header^^.'}
    
    @newline "Small"
    
    @text "Specification_Type"
        @text-header "^^Specification Type|Specification Type^^:"
        
    @choice "Product_Hierarchy_1"
        @choice-style "DropDownLookup"
        @choice-header "^^Category|Category^^:"
        @choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_1"
        @choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
        @choice-field "Name"
		@choice-translate "Yes"
        @choice-optional "Yes"
    @end-choice

@end-block

@block
    @block-ui-style "clear form-horizontal"
    
    @text "Product_Category"
        @text-header "^^Product Category|Product Category^^:"
        @text-columns "5"
        @text-optional "Yes"
        @text-expression "if(Numbering_Type = 'New' or Numbering_Type = 'Clone', Category/Code, Product_Category)"
        @text-type "Integer"
    
    @text "Product_Category_Id"
        @text-header "^^Product Category ID|Product Category ID^^:"
        @text-optional "Yes"
    
    @text "Current_Version"
        @text-header "^^Current Version|Current Version^^:"
        @text-columns "5"
        @text-optional "Yes"
    
    @text "UID"
        @text-header "^^Unique Specification ID|Unique Specification ID^^:"
        @text-optional "Yes"
    
    @text "Revision"
        @text-header "^^Revision|Revision^^"
        @text-optional "Yes"
        @text-type "Integer"
    
    @text "Variation"
        @text-header "^^Variation|Variation^^"
        @text-optional "Yes"
        @text-type "Integer"
    
    @lookup "Specification"
        @lookup-header "^^Specification|Specification^^"
        @lookup-allow-new-when "false()"
        @lookup-updatable-when "false()"        
        @lookup-queue "SCQM_Specification.Lookup_Q"
        @lookup-select "SCQM_Specification/Specification_Workbook/Specification_Name"
        @lookup-field "Specification_Name"
        @lookup-allow-navigate-when "call('Core.CheckAppPermission', 'SCQM_Specification')"
        @lookup-optional "Yes"
        @lookup-ui-style "width-auto"        
    
    @block
        @block-visible-when "is-not-blank(Numbering_Type) and Numbering_Type = 'Revision'"
    
        @label "queue('SCQM_Specification_Numbering.Master_Q', concat('UID = ', char(39), UID, char(39), ' and StoreId !=', char(39), store-id(), char(39)), 'Revision DESC', 0, 0, '', false())/item[1]/Revision"
            @label-header "^^Last Revision|Last Revision^^:"
    
    @end-block
    
    @block
        @block-visible-when "is-not-blank(Numbering_Type) and Numbering_Type = 'Variation'"

        @label "queue('SCQM_Specification_Numbering.Master_Q', concat('UID = ', char(39), UID, char(39), ' and StoreId !=', char(39), store-id(), char(39)), 'Variation DESC', 0, 0, '', false())/item[1]/Variation"
            @label-header "^^Last Variation|Last Variation^^:"

    @end-block

    @label "queue-count('SCQM_Specification_Numbering.Master_Q', concat('UID = ', char(39), UID, char(39)))"
        @label-header "^^Current Count of Revisions/Variations|Current Count of Revisions/Variations^^:"
    
    @text "Full_Number"
        @text-header "^^Full Number|Full Number^^:"
        @text-ui-style "clear block padding-tb-sm large"
        @text-read-only-when "true()"
    
    @newline

@end-block
