﻿@title "^^Account Measurement Configuration Information|Account Measurement Configuration Information^^"
@mode "Main"

@text "Measurement_Name"
    @text-header "^^Name|Name^^:"
    @text-type "String"
    
@text "Sequence"
    @text-header "^^Sequence|Sequence^^:"
    @text-type "Integer"    

@choice "Measurement_Classification"
    @choice-style "DropDownLookup"
    @choice-header "^^Measurement_Classification|Classification^^:"
    @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
    @choice-select "Core_Custom_Dropdown_Item/Item_Name|Core_Custom_Dropdown_Item/Item_Value"
    @choice-field "Item_Name"
    @choice-filter "Dropdown_Group_ID = 'Measurement_Classification' and Dropdown_List_Dropdown_Name = 'Measurement Classification'"  

@choice "Measurement_Type"
    @choice-style "DropDownLookup"
    @choice-header "^^Measurement Type|Measurement Type^^:"
    @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
    @choice-select "Core_Custom_Dropdown_Item/Item_Name|Core_Custom_Dropdown_Item/Item_Value"
    @choice-field "Item_Name"
    @choice-filter "Dropdown_Group_ID = 'Measurement_Type' and Dropdown_List_Dropdown_Name = 'Measurement Type'"   


@text "Standard"
    @text-header "^^Standard|Standard^^:"
    @text-type "String"
    @text-optional-when "true()"    
    
@choice "Validation_Method"
    @choice-style "DropDownLookup"
    @choice-header "^^Validation Method|Validation Method^^:"
    @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
    @choice-select "Core_Custom_Dropdown_Item/Item_Name|Core_Custom_Dropdown_Item/Item_Value"
    @choice-field "Item_Name"
    @choice-filter "Dropdown_Group_ID = 'Validation_Method' and Dropdown_List_Dropdown_Name = 'Validation Method'" 


@block
    @block-visible-when "Validation_Method/Item_Value = 'gt' or Validation_Method/Item_Value = 'bt'"
    
    @text "Bottom_Range_Value"
        @text-header "{{if(Validation_Method/Item_Value = 'gt', '^^Greater Than|Greater Than^^', '^^From|From^^')}}"
        @text-type "Number"

@end-block

@block
    @block-visible-when "Validation_Method/Item_Value = 'lt' or Validation_Method/Item_Value = 'bt'"
    
    @text "Top_Range_Value"
        @text-header "{{if(Validation_Method/Item_Value = 'lt', '^^Less Than|Less Than^^', '^^To|To^^')}}"
        @text-type "Number"

@end-block

@block
    @block-visible-when "is-not-blank(Measurement_Type)"
    
    @choice "Measurement_Unit"
        @choice-style "DropDownLookup"
        @choice-header "^^Unit|Unit^^:"
        @choice-queue "Core_Unit.Active_Lookup_Q"
        @choice-select "Core_Unit/Unit_Name|Core_Unit/Abbreviation"
        @choice-field "Unit_Name"
		@choice-translate "Yes"
        @choice-filter "Unit_Type = {{Measurement_Type/@refId}}"
        @choice-depends "Measurement_Type"
        
@end-block

@choice "Frequency"
    @choice-style "DropDownLookup"
    @choice-header "^^Frequency|Frequency^^:"
    @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
    @choice-select "Core_Custom_Dropdown_Item/Item_Name|Core_Custom_Dropdown_Item/Item_Value"
    @choice-field "Item_Name"
    @choice-filter "Dropdown_Group_ID = 'Frequency' and Dropdown_List_Dropdown_Name = 'Frequency'"
    
@block
    @block-visible-when "Frequency/Item_Value = 'D'"
    
    @text "Daily_Time"
        @text-header "^^When_Daily|At what time once a day?^^"
        @text-type "Time"        
        
    @text "Daily_Time_Duration"
        @text-header "^^Daily_Time_Duration|How many hours should the restaurant have to enter the measurement^^?"
        @text-type "Integer"        
        

@end-block

@block
    @block-visible-when "Frequency/Item_Value = 'TD'"
    
    @text "First_Twice_Daily_Time"
        @text-header "^^When_First|At what time for the first time?^^"
        @text-type "Time"  
        
    @text "First_Twice_Daily_Time_Duration"
        @text-header "^^Daily_Time_Duration|How many hours should the restaurant have to enter the measurement^^?"
        @text-type "Integer" 
        
    @text "Second_Twice_Daily_Time"
        @text-header "^^When_Second|At what time for the second time?^^"
        @text-type "Time"
        
    @text "Second_Twice_Daily_Time_Duration"
        @text-header "^^Daily_Time_Duration|How many hours should the restaurant have to enter the measurement^^?"
        @text-type "Integer" 
@end-block   

@block
    @block-visible-when "call('Core.IsITUser')"

    @lookup "Core_Account"
        @lookup-header "^^Valid for Account|Valid for Account^^:"
        @lookup-allow-new-when "false()"
        @lookup-allow-navigate-when "call('Core.AllowLookupNavigation', 'Core_Account')"
        @lookup-queue "Core_Account.Active_Lookup_Q"
        @lookup-select "Core_Account/Account_Name"
        @lookup-field "Account_Name"
        @lookup-updatable-when "true()"
        @lookup-optional "Yes"
@end-block

[[Shared_Core_Status_Active_Choice]]  

[[Shared_Core_EditInPlace_IT]]

