﻿@title " "

@text "Name"
    @text-header "Name"
    
@text "App"
    @text-header "App"
    @text-optional "Yes"    

@choice "Group"
    @choice-style "DropDownLookup"
    @choice-header "Status Group:"
    @choice-queue "Core_Status.Master_Q"
    @choice-select "Core_Status/Status_Group"
    @choice-field "Status_Group"
    @choice-optional "Yes"    
    @end-choice
    
    
    
{label(primary):'The following fields are expressions evaluated against the current context and passed to Core_Task_Assignment.StartTaskAssignmnent'}   

@text "Context_Expression"
    @text-header "Context Expression"
    @text-default-expression "'store-id()'"
    
@text "Context_Module_Expression"
    @text-header "Context Module Expression"
    @text-optional "Yes"
    @text-footer "This will by default find the module of the app. Only set this if the module of the app can vary. ie Audit can be Facility Audits or Activities" 

@text "Parent_Context_Expression"
    @text-header "Parent Context Expression"
    @text-default-expression "'store-id()'"

@text "Parent_Context_Module_Expression"
    @text-header "Parent Context Module Expression"
    @text-optional "Yes"
    @text-footer "This will by default find the module of the app. Only set this if the module of the app can vary. ie Audit can be Facility Audits or Activities"  
    
@checkbox "Use_Current_Context"
    @checkbox-header "Use Current Context"
    @checkbox-optional "Yes"

@text "Meta_Data_1_Expression"
    @text-header "Meta Data 1 Expression"
    @text-default-expression "'/store/*[1]/Meta_Data_1'"    
    @text-optional "Yes"
    
@text "Meta_Data_2_Expression"
    @text-header "Meta Data 2 Expression"
    @text-default-expression "'/store/*[1]/Meta_Data_2'"    
    @text-optional "Yes"
    
@text "Meta_Data_3_Expression"
    @text-header "Meta Data 3 Expression"
    @text-default-expression "'/store/*[1]/Meta_Data_3'"    
    @text-optional "Yes"
    
@text "Meta_Data_4_Expression"
    @text-header "Meta Data 4 Expression"
    @text-default-expression "'/store/*[1]/Meta_Data_4'"    
    @text-optional "Yes"
    
@text "Meta_Data_5_Expression"
    @text-header "Meta Data 5 Expression"
    @text-default-expression "'/store/*[1]/Meta_Data_5'"    
    @text-optional "Yes"
    
@text "Organization_Expression"
    @text-header "Organization Expression"
    @text-default-expression "'/store/*[1]/Core_Organization/@refId'"    
    @text-optional "Yes"
    
@text "Location_Expression"
    @text-header "Location Expression"
    @text-default-expression "'/store/*[1]/Core_Organization_Location/@refId'"    
    @text-optional "Yes"

@text "Estimated_Start_Date_Expression"
    @text-header "Estimated Start Date Expression"
    @text-default-expression "'/store/*[1]/Estimated_Start_Date'"    
    @text-optional "Yes"   
    
@text "Estimated_Complete_Date_Expression"
    @text-header "Estimated Complete Date Expression"
    @text-default-expression "'/store/*[1]/Estimated_Complete_Date'"    
    @text-optional "Yes"     

@text "Estimated_Duration_Expression"
    @text-header "Estimated Duration Expression"
    @text-default-expression "'/store/*[1]/Estimated_Duration'"    
    @text-optional "Yes"   
    
@list "Standard_Assignments_History"
    @list-sort "Ascending"
    @list-style "List"
    @list-header "Assignments:"
    
    @newline
    
    @block
        @block-ui-style "clear span6"
        
        @lookup "Core_Workflow_Role"
            @lookup-header "Role:"
            @lookup-allow-new-when "false()"
            @lookup-queue "Core_Workflow_Role.Active_Lookup_Q"
            @lookup-select "Core_Workflow_Role/Workflow_Role_Name|Core_Workflow_Role/Is_Local_Workflow_Role|Core_Workflow_Role/Local_Role_Type|Core_Workflow_Role/Workflow_Role_ID"
            @lookup-field "Workflow_Role_ID"
    
        @lookup "Core_Token_Mapping"
            @lookup-header "Token Mapping"
            @lookup-allow-new-when "false()"
            @lookup-queue "Core_Token_Mapping.Site_Admin_Q"              
            @lookup-select "Core_Token_Mapping/Mapping_ID"
            @lookup-field "Mapping_ID"
            
        @checkbox "Task_Can_Repeat"
            @checkbox-header "Task Can Repeat"
            @checkbox-optional "Yes"
            @checkbox-footer "Check here if the task can be repeated. By default a task will not be created if it has been completed and the Creation Condition becomes true again. Check here if the task should be created again if the creation condition beomes true again."     
            
        @text "Due_Date_Expression"
            @text-header "Due Date Expression"
            @text-optional "Yes"
            
    @end-block    
    
    @block
        @block-ui-style "span6"
        
        @choice "Use_Status"
            @choice-header "Create/Complete Task using Status:"
            @choice-footer "Not using Status requires knowlede of xpath and the store the condition is evaluated against"            
            @choice-columns "2"            
            @choice-option "Yes"
            @choice-option "No"
            @end-choice   
        
        @block
            @block-visible-when "Use_Status = 'Yes'"
            
            @lookup "Creation_Status"
                @lookup-header "^^Creation_Status|Creation Status^^:"
                @lookup-allow-new-when "false()"
                @lookup-queue "Core_Status.Master_Q"
                @lookup-filter "Status_Group = {{/store/*[1]/Group/Status_Group}}"  
                @lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"
                @lookup-field "Status_Name"
                @lookup-updatable-when "true()"
            
            
            @lookup "Completion_Status"
                @lookup-header "^^Completion_Status|Completion Status^^:"
                @lookup-allow-new-when "false()"
                @lookup-queue "Core_Status.Master_Q"
                @lookup-filter "Status_Group = {{/store/*[1]/Group/Status_Group}}"  
                @lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"
                @lookup-field "Status_Name"
                @lookup-updatable-when "true()"
            
        @end-block    
        

        @text "Creation_Condition"
            @text-header "Creation condition"
            @text-rows "5"            
            @text-footer "Condition evaluated against the Store upon completion to determine if this task is created"
            
        @text "Start_Condition"
            @text-header "Start condition"
            @text-rows "5"            
            @text-footer "Condition evaluated against the Store upon completion to determine if this task is started"            
        
        @text "Completed_Condition"
            @text-header "Completed condition"
            @text-rows "5"            
            @text-footer "Condition evaluated again the Store upon completion to determine if this task is completed"
        
    @end-block
    
    @newline
    
@end-list