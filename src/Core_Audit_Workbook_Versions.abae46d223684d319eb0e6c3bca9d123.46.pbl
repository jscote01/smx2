﻿@topic
@topic-title "Air ventilation through the facility is adequate with no odors or airborne contaminants which could carry over to product."
@topic-number "659"

@block "_Controls_Block"
@block-container "Controls"
@block-container-options "Options[Style:width:300px; line-height:50px;]"
@block-ui-style "form-horizontal pos-rel"

	@block "StyleBlock1"
	@block-ui-style "form-inline"

		@link "_659_Tooltip"
		@link-icon "icon-list-ul"
		@link-href "Yes"
		@link-target "Inline"
		@link-ui-style "btn btn-medium btn-inverse margin-lr-sm margin-tb-sm"
		@link-text-ui-style "hide"
		@link-toggle-ui-style "hide"

		@block "_659_Tooltip"
		@block-ui-style "clear well well-small bcolor-white padding-lr-mini form-horizontal hide medium bold pos-abs zindex-modal align-left"

Air ventilation through the facility is adequate with no odors or airborne contaminants which could carry over to product.

		@end-block

		@block "ActiveControlsBlock"
		@block-ui-style "control-group pull-right"

			@choice "_659_Choice"
			@choice-style "RadioButtonList"
			@choice-columns "5"
			@choice-direction "Horizontal"
			@choice-postback "Yes"
			@choice-ui-style "btn"

				@option "PT1"
				@option-value "5"
				@option-text "5"
				@option-ui-style-checked "btn-medium white bcolor-success"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT2"
				@option-value "3"
				@option-text "3"
				@option-ui-style-checked "btn-medium white bcolor-warning"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT3"
				@option-value "1"
				@option-text "1"
				@option-ui-style-checked "btn-medium white bcolor-danger-light"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT4"
				@option-value "0"
				@option-text "0"
				@option-ui-style-checked "btn-medium white bcolor-danger"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "NA"
				@option-value "N/A"
				@option-text "N/A"
				@option-ui-style-checked "btn-medium white bcolor-inverse"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

			@end-choice

		@end-block

	@end-block

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_659_Choice) and _659_Choice != 'N/A' and _659_Choice != '5'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_659_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_659_Choice) or _659_Choice [. = 'N/A' or . = '5']"
				@text-expression "case(_659_Choice = '0', 'Air ventilation through the facility is not adequate with no odors or airborne contaminants which could carry over to product.','Air ventilation through the facility is adequate with no odors or airborne contaminants which could carry over to product.')"
				@text-expression-when "is-changed(_659_Choice) and not(is-changed(_659_Comments)) and _659_Choice != 'N/A'"
				@text-ui-style "block"

				@block
				@block-visible-when "is-not-blank(_659_Choice) and (_659_Choice = '0')"

					@text "_659_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block
				@block-visible-when "is-not-blank(_659_Choice) and not(_659_Choice = 'N/A' or _659_Choice = '5') and not(_659_Choice = '0')"

					@text "_659_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_659_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "659"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "5"
	@finding-select "_659_Comments"
	@finding-score-when "is-not-blank(_659_Choice) and _659_Choice != 'N/A'"
	@finding-meta-Question_Number "659"

		@case
		@case-type "Positive"
		@case-points "_659_Choice"
		@case-test "not(_659_Choice = '0') and _659_Choice != 'N/A'"

		@case
		@case-type "Secondary"
		@case-points "_659_Choice"
		@case-test "_659_Choice = '0' and _659_Choice != 'N/A'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_659_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "_659_Choice"
		@text-expression-when "is-changed(_659_Choice)"

	@end-block

@end-block
