﻿@display "No"

@comment
Generate a Specification_Number for a non-historical specification
@end-comment
@execute "Create_Specification_Number"

    @update
        @update-target-select "$SpecNumberNodes/Numbering_Type"
        @update-source-select "coalesce(Spec_Creation_Method, 'New')"
    @update
        @update-target-select "$SpecNumberNodes/Specification"
        @update-source-select "store-id()"
    @update    
        @update-target-select "$SpecNumberNodes/Is_Parent"
        @update-source-select "if(is-blank(Is_Child), 'true', '')"
    @update
        @update-target-select "$SpecNumberNodes/Product_Hierarchy_1"
        @update-source-select "if(Spec_Creation_Method[.='Child' or .='Revision' or .='Variation'], '', /store/*[1]/Specification_Workbook/Product_Hierarchy_1/@refId)"
    @update
        @update-target-select "$SpecNumberNodes/Specification_Type"
        @update-source-select "if(Spec_Creation_Method[.='Child' or .='Revision' or .='Variation'], '', call('SCQM.SPEC.SpecificationType_Value', store-id()))"
    @update
        @update-target-select "$SpecNumberNodes/SCQM_Specification_Numbering"
        @update-source-select "if(Spec_Creation_Method[.='Child' or .='Revision' or .='Variation'], call('SCQM.SPEC.SpecNumberingStoreId', Spec_Created_From/@refId), '')"

    @invoke
        @invoke-execute "SCQM_Specification_Numbering.Create"
        @invoke-parameter "SpecNumberNodes"
            @invoke-parameter-expression "$SpecNumberNodes"
            @invoke-parameter-direction "In"
        @invoke-parameter "NewStoreId"
            @invoke-parameter-expression "$SpecNumberingStoreCreated"
            @invoke-parameter-direction "Out"

    @update
        @update-target-select "Specification_Number"
        @update-source-select "store($SpecNumberingStoreCreated)/SCQM_Specification_Numbering/Specification_Number"

@end-execute



@comment
Generate a Specification_Number for a historical specification
NOTE: This will also set Status and Final_Approval_Date
@end-comment
@execute
    @execute-when "is-blank(Specification_Number) and is-blank(Status/Status_Code) and is-not-blank(Is_Historical)"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'SPC_Finalized', 'Specification')"
        @update-source-type "Lookup"    

    @update
        @update-target-select "Final_Approval_Date"
        @update-source-select "now(0)"

    @update
        @update-target-select "$SpecNumberNodes/Numbering_Type"
        @update-source-select "'New'"
    @update
        @update-target-select "$SpecNumberNodes/Specification"
        @update-source-select "store-id()"
    @update    
        @update-target-select "$SpecNumberNodes/Is_Parent"
        @update-source-select "if(is-blank(Is_Child), 'true', '')"
    @update
        @update-target-select "$SpecNumberNodes/Product_Hierarchy_1"
        @update-source-select "/store/*[1]/Specification_Workbook/Product_Hierarchy_1/@refId"
    @update
        @update-target-select "$SpecNumberNodes/Specification_Type"
        @update-source-select "call('SCQM.SPEC.SpecificationType_Value', store-id())"
    @update
        @update-target-select "$SpecNumberNodes/SCQM_Specification_Numbering"
        @update-source-select "''"

    @invoke
        @invoke-execute "SCQM_Specification_Numbering.Create"
        @invoke-parameter "SpecNumberNodes"
            @invoke-parameter-expression "$SpecNumberNodes"
            @invoke-parameter-direction "In"
        @invoke-parameter "NewStoreId"
            @invoke-parameter-expression "$SpecNumberingStoreCreated"
            @invoke-parameter-direction "Out"

    @update
        @update-target-select "Specification_Number"
        @update-source-select "store($SpecNumberingStoreCreated)/SCQM_Specification_Numbering/Specification_Number"

@end-execute