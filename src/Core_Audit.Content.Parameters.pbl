﻿@mode "Information"
@title "^^Parameters|Parameters^^"
@title-icon "icon-tasks"
----

@block
@block-ui-style "well"
@block-visible-when "is-not-blank(Service/Audit_Parameters)"
    {label(bold):"^^Audit Parameters|Audit Parameters^^:"}
    {{html:Service/Audit_Parameters}}
@end-block

@comment
************
JIRA TICKET ONBRAND-2674 - hide all values set in the Audit Parameters section of the Service record (using comment/end-comment below)
************
Tests to see if there is an "override" value at the Service Location level.  If not, uses the value from the Service.
@end-comment
@comment
@label
@label-template "{{concat(Core_Audit_Service/Audit_Process_Hours, ' ', 'Hours'))}}"
@label-header "^^Duration|Duration^^:"

@block
@block-visible-when "is-not-blank(Account_Location/Audit_Security_Requirements)"
@label
@label-template "{{If(is-blank(Account_Location/Audit_Security_Requirements/Other), join(Account_Location/Audit_Security_Requirements/*, ', '), concat(join(Account_Location/Audit_Security_Requirements/*[. != 'Other'], ', '), ', ', Account_Location/Audit_Security_Requirements_Other) )}}"
@label-header "^^Security Requirements|Security Requirements^^:"
@end-block

@block
@block-visible-when "is-blank(Account_Location/Audit_Security_Requirements)"
@label
@label-template "{{If(is-blank(Account_Location/Service/Audit_Security_Requirements/Other), join(Core_Audit_Service/Audit_Security_Requirements/*, ', '), concat(join(Service_Location/Service/Audit_Security_Requirements/*[. != 'Other'], ', '), ', ', Service_Location/Service/Audit_Security_Requirements_Other) )}}"
@label-header "^^Security Requirements|Security Requirements^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Audit_Dress_Code)"
@label
@label-template "{{If(is-blank(Service_Location/Audit_Dress_Code/Other), join(Service_Location/Audit_Dress_Code/*, ', '), concat(join(Service_Location/Audit_Dress_Code/*[. != 'Other'], ', '), ', ', Service_Location/Audit_Dress_Code_Other))}}"
@label-header "^^Dress Code|Dress Code^^:"
@end-block

@block
@block-visible-when "is-blank(Service_Location/Audit_Dress_Code)"
@label
@label-template "{{If(is-blank(Service_Location/Service/Audit_Dress_Code/Other), join(Service_Location/Service/Audit_Dress_Code/*, ', '), concat(join(Service_Location/Service/Audit_Dress_Code/*[. != 'Other'], ', '), ', ', Service_Location/Service/Audit_Dress_Code_Other) )}}"
@label-header "^^Dress Code|Dress Code^^:"
@end-block

@block
@block-visible-when "Service_Location/Service/Protocol_Customizations_Start_Of_Audit = 'Yes'"
{label(bold):"^^Start of Audit Protocols|Start of Audit Protocols^^:"}
{{html:Service_Location/Service/Protocol_Customizations_Start_Of_Audit_Details}}
@block
@block-ui-style "primary bold margin-tb-med"
@block-visible-when "Service_Location/Service/Protocol_Customizations_Start_Of_Audit_Reoccurrence = 'First audit only'"
FIRST AUDIT ONLY
@end-block
@end-block

@block
@block-visible-when "Service_Location/Service/Protocol_Customizations_End_Of_Audit = 'Yes'"
@label "Service_Location/Service/Protocol_Customizations_End_Of_Audit_Details"
@label-header "^^End of Audit Protocols|End of Audit Protocols^^:"
@block
@block-ui-style "primary bold margin-tb-med"
@block-visible-when "Service_Location/Service/Protocol_Customizations_End_Of_Audit_Reoccurrence = 'First audit only'"
FIRST AUDIT ONLY
@end-block
@end-block

@block
@block-ui-style "well"
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Audit) or
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Restaurant) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Store) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Employee) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Store_Manager) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_District_Manager) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Regional_Manager) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Franchise) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Quality_Assurance) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology_Steritech) or 
is-not-blank(Service_Location/Service/Client_Specific_Terminology)"

{label(primary large block):"^^Client-Specific Terminology|Client-Specific Terminology^^"}

@label "concat('Name', ' ')"
@label-ui-style "primary bold"
@label-header "^^Customer's Name|Customer's Name^^:"
@label-header-ui-style "primary bold"

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Audit)"
@label "Service_Location/Service/Client_Specific_Terminology_Audit"
@label-header "^^Audit|Audit^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Restaurant)"
@label "Service_Location/Service/Client_Specific_Terminology_Restaurant"
@label-header "^^Restaurant|Restaurant^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Store)"
@label "Service_Location/Service/Client_Specific_Terminology_Store"
@label-header "^^Store|Store^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Employee)"
@label "Service_Location/Service/Client_Specific_Terminology_Employee"
@label-header "^^Employee|Employee^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Store_Manager)"
@label "Service_Location/Service/Client_Specific_Terminology_Store_Manager"
@label-header "^^Store Manager|Store Manager^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_District_Manager)"
@label "Service_Location/Service/Client_Specific_Terminology_District_Manager"
@label-header "^^District Manager|District Manager^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Regional_Manager)"
@label "Service_Location/Service/Client_Specific_Terminology_Regional_Manager"
@label-header "^^Regional Manager|Regional Manager^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Franchise)"
@label "Service_Location/Service/Client_Specific_Terminology_District_Franchise"
@label-header "^^Franchise|Franchise^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Quality_Assurance)"
@label "Service_Location/Service/Client_Specific_Terminology_Quality_Assurance"
@label-header "^^Quality Assurance|Quality Assurance^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology_Steritech)"
@label "Service_Location/Service/Client_Specific_Terminology_Quality_Steritech"
@label-header "^^Steritech|Steritech^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Client_Specific_Terminology)"
@label "Service_Location/Service/Client_Specific_Terminology"
@label-header "^^Other|Other^^:"
@end-block

@end-block

@block
@block-ui-style "well"
@block-visible-when "Service_Location/Service/Non_Standard_Equipment = 'Yes'"

{label(primary large block):"^^Non-Standard Equipment Required|Non-Standard Equipment Required^^"}

@label "concat('Customer Supplied?', ' ')"
@label-ui-style "primary bold"
@label-header "^^Equipment|Equipment^^:"
@label-header-ui-style "primary bold"

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Scale)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Scale_Cust_Supplied),'Yes','No')}}"
@label-header "^^Scale|Scale^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Camera)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Camera_Cust_Supplied),'Yes','No')}}"
@label-header "^^Camera|Camera^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Environmental_Swabs)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Swabs_Req__Cust_Supplied),'Yes','No')}}"
@label-header "^^Environmental Swabs|Environmental Swabs^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Product_Sampling)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Product_Sampling_Cust_Supplied),'Yes','No')}}"
@label-header "^^Product Sampling|Product Sampling^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Stop_Watch)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Stop_Watch_Cust_Supplied),'Yes','No')}}"
@label-header "^^Stop Watch|Stop Watch^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Tape_Measure)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Tape_Measure_Cust_Supplied),'Yes','No')}}"
@label-header "^^Tape Measure|Tape Measure^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Black_Light)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Black_Light_Cust_Supplied),'Yes','No')}}"
@label-header "^^Black Light|Black Light^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Client_Material)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Client_Material_Cust_Supplied),'Yes','No')}}"
@label-header "^^Client Material|Client Material^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Scanner)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Std_Equipment_Req_Scanner_Cust_Supplied),'Yes','No')}}"
@label-header "^^Scanner|Scanner^^:"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required/Other)"
@label
@label-template "{{if(is-not-blank(Service_Location/Service/Non_Standard_Equipment_Required_Other),'Yes','No')}}"
@label-header "{{Service_Location/Service/Non_Standard_Equipment_Required_Other}}"
@end-block

@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Pre_Audit_Document)"
@file "Pre_Audit_Document"
@file-select "Service_Location/Service/Pre_Audit_Document"
@file-header "Document to print before the Audit:"
@file-optional "Yes"
@file-read-only "Yes"
@end-block

@block
@block-visible-when "is-not-blank(Service_Location/Service/Post_Audit_Document)"
@file "Post_Audit_Document"
@file-select "Service_Location/Service/Post_Audit_Document"
@file-header "Document to print after the Audit:"
@file-optional "Yes"
@file-read-only "Yes"
@end-block

@end-comment