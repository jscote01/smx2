﻿@title "^^Program Admin Tools|Program Admin Tools^^"
@mode "Admin_Tools"

@comment
======================================
RIGHT SIDE OF SCREEN
======================================
@end-comment

@block
@block-ui-style "clear span6"

    @block "PROGRAM_ADMIN_DISCONTINUE_BUTTON"
    @block-visible-when "Status/Status_Code[.!='AR_Discarded' and .!='AR_Discontinued' and .!='AR_Pending_APL' and .!='AR_Approved']"
    
        @block
        @block-visible-when "Status/Status_Code = 'AR_New'"
        {label(info medium block):'^^Discard|Discard^^ ^^Approval Request|Approval Request^^'}
        
        {label(inverse small block):'^^Admin_Discard_Approval_Request|To Discard the Approval Request, select the "Discard" button and provide a reason to complete^^.'}
        @end-block        

        @block
        @block-visible-when "Status/Status_Code != 'AR_New'"
        {label(info medium block):'^^Discontinue|Discontinue^^ ^^Approval Request|Approval Request^^'}
        
        {label(inverse small block):'^^Admin_Discontinue_Approval_Request|To Discontinue the Approval Request, select the "Discontinue" button and provide a reason to complete^^.'}
        @end-block        
        
        @newline "Small"
    
        @block
        @block-visible-when "is-blank(Admin_PreAction) and is-blank(/store/*[1]/Processing)"
        
            @button "Admin_PreAction"
            @button-value "Discontinue"
            @button-text "{{if(Status/Status_Code = 'AR_New', '^^Discard|Discard^^', '^^Discontinue|Discontinue^^')}}"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-danger margin-lr-mini"
            @button-action "Save,Refresh"
    
        @end-block
        
        @block
        @block-visible-when "is-not-blank(Admin_PreAction) and Status/Status_Code = 'AR_New' and is-blank(/store/*[1]/Processing)"
        
            @link "False_Button"
            @link-text "^^Discard|Discard^^"
            @link-ui-style "btn btn-danger margin-lr-mini disabled white"
        
        @end-block
    
        @block
        @block-visible-when "is-not-blank(Admin_PreAction) and Status/Status_Code != 'AR_New' and is-blank(/store/*[1]/Processing)"
        
            @link "False_Button"
            @link-text "^^Discontinue|Discontinue^^"
            @link-ui-style "btn btn-danger margin-lr-mini disabled white"
        
        @end-block
    
        @newline
        
        @block "Admin_Discontinue"
        @block-visible-when "(Admin_PreAction = 'Discontinue' and is-blank(Admin_Action)) or Admin_Action = 'Discontinue'"
        @block-ui-style "form-horizontal"
        
            @text "Admin_Discontinue_Reason"
            @text-header "{{concat('^^Reason to|Reason to^^', ' ', if(Status/Status_Code = 'AR_New', '^^Discard|Discard^^', '^^Discontinue|Discontinue^^'), ':')}}"
            @text-rows "3"
            @text-validation-test "(is-not-blank(Admin_Discontinue_Reason) and Admin_Action = 'Discontinue') or Admin_Action = 'Cancel' or is-blank(Admin_Action)"
            @text-validation-error "^^A discontinue reason is required|A discontinue reason is required^^"

            @block
            @block-visible-when "is-blank(/store/*[1]/Processing)"
            
                @button "Admin_Action"
                @button-value "Discontinue"
                @button-text "{{if(Status/Status_Code = 'AR_New', '^^Discard|Discard^^', '^^Discontinue|Discontinue^^')}}"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-danger margin-lr-mini"
                @button-action "Save,Validate,Reload"
    
                @button "Admin_PreAction"
                @button-value "Cancel"
                @button-text "^^Cancel|Cancel^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-warning margin-lr-mini"
                @button-action "Save,Validate,Refresh"

            @end-block            

        @end-block
    
    @end-block
    
    @comment
    PROGRAM ADMIN MESSAGE ABOUT DISCONTINUE/DISCARD THROUGH APL WHEN RELATED
    @end-comment
    @block
        @block-visible-when "Status/Status_Code = 'AR_Pending_APL'"
        @block-ui-style "well well-mini well-info-light inverse"
        
        @icon "icon-info-sign info medium"
        
        {label(medium margin-lr-mini):'^^Program_Admin_AR_Discontinue_Via_APL_Message|To Discontinue this Approval Request, discard all related Approved Products List records (see Summary tab)^^.'}
    
    @end-block    
    
    @comment
    PROGRAM ADMIN UNDO DISCARD OR DISCONTINUE BUTTON - currently disabled 12/07/2015 WMS
    @end-comment
    @block
    @block-visible-when "Status/Status_Code[.='AR_Discarded' or .='AR_Discontinued'] and call('SCQM.AR.Undo_Feature_Allowed')"
    
        {label(info medium block):'^^Restart Approval Request|Restart Approval Request^^'}
    
        {label(inverse small block):'^^Program_Admin_AR_Restart_APL_Message|To restart the Approval Request, select the "Restart" button and provide a reason to complete^^.'}
        
        @newline "Small"
    
        @block
        @block-visible-when "is-blank(Admin_PreAction) and is-blank(/store/*[1]/Processing)"
        
            @button "Admin_PreAction"
            @button-value "Restart"
            @button-text "^^Restart|Restart^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-success margin-lr-mini"
            @button-action "Save,Refresh"
    
        @end-block
        
        @block
        @block-visible-when "is-not-blank(Admin_PreAction) and is-blank(/store/*[1]/Processing)"
        
            @link "False_Button"
            @link-text "^^Restart|Restart^^"
            @link-ui-style "btn btn-success margin-lr-mini disabled white"
        
        @end-block
    
        @newline
        
        @block "Admin_Restart"
        @block-visible-when "(Admin_PreAction = 'Restart' and is-blank(Admin_Action)) or Admin_Action = 'Restart'"
        @block-ui-style "form-horizontal"
        
            @text "Admin_Restart_Reason"
            @text-header "^^Reason to Restart|Reason to Restart^^:"
            @text-rows "3"
            @text-validation-test "(is-not-blank(Admin_Restart_Reason) and Admin_Action = 'Restart') or Admin_Action = 'Cancel' or is-blank(Admin_Action)"
            @text-validation-error "^^A restart reason is required|A restart reason is required^^"

            @block
            @block-visible-when "is-blank(/store/*[1]/Processing)"

                @button "Admin_Action"
                @button-value "Restart"
                @button-text "^^Restart|Restart^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success margin-lr-mini"
                @button-action "Save,Validate,Reload"
    
                @button "Admin_PreAction"
                @button-value "Cancel"
                @button-text "^^Cancel|Cancel^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-warning margin-lr-mini"
                @button-action "Save,Validate,Refresh"

            @end-block            

        @end-block
    
    @end-block

@end-block

@comment
======================================
LEFT SIDE OF SCREEN
======================================
@end-comment

@block
@block-ui-style "span6"

    @block "ORGANIZATION_EMAIL_CHANGE"
    @block-visible-when "is-not-blank(Org_Created) and not(call('SCQM.AR.HasExistingOrg'))"
    
        {label(medium info block):'^^Organization Contact|Organization Contact^^'}
                
        {label(small inverse block):concat('^^Organization|Organization^^: ', store(Org_Created)/SCQM_Approval_Request_Organization/Org_Name)}

        @block
        @block-visible-when "is-blank(Change_Org_Contact)"

            @label "store(Org_Created)/SCQM_Approval_Request_Organization/Contact_First_Name"
            @label-header "^^Contact First Name|Contact First Name^^:"

            @label "store(Org_Created)/SCQM_Approval_Request_Organization/Contact_Last_Name"
            @label-header "^^Contact Last Name|Contact Last Name^^:"

            @label "store(Org_Created)/SCQM_Approval_Request_Organization/Contact_Email"
            @label-header "^^Contact Email|Contact Email^^:"

        @end-block

        @block
        @block-visible-when "is-not-blank(Change_Org_Contact)"
        
            @text "New_Org_Contact_First_Name"
            @text-header "^^Contact First Name|Contact First Name^^:"
            @text-optional "Yes"
            @text-footer "{{store(Org_Created)/SCQM_Approval_Request_Organization/Contact_First_Name}}"
            @text-footer-ui-style "inverse"

            @text "New_Org_Contact_Last_Name"
            @text-header "^^Contact Last Name|Contact Last Name^^:"
            @text-optional "Yes"
            @text-footer "{{store(Org_Created)/SCQM_Approval_Request_Organization/Contact_Last_Name}}"
            @text-footer-ui-style "inverse"

            @text "New_Org_Contact_Email"
            @text-header "^^Contact Email|Contact Email^^:"
            @text-optional "Yes"
            @text-footer "{{store(Org_Created)/SCQM_Approval_Request_Organization/Contact_Email}}"
            @text-validation-test "call('Core.IsValidEmail', New_Org_Contact_Email)"
            @text-footer-ui-style "inverse"
    
        @end-block

        @block
            @block-visible-when "is-not-blank(call('SCQM.ContactEmail.UsedByAnotherOrg', New_Org_Contact_Email, /store/SCQM_Approval_Request/Main_Workbook_Data/Core_Organization/@refId))"
            @block-ui-style "well well-mini well-danger-light inverse"
            @icon "icon-warning-sign danger medium"
            {label(medium margin-lr-mini):concat('^^Attention|Attention^^: ^^Contact_Already_Exists|A User/Contact already exists with this e-mail address for a different Supplier^^ ', char(40), store(call('SCQM.ContactEmail.UsedByAnotherOrg', New_Org_Contact_Email, store(/store/SCQM_Approval_Request/Orgs_Created)/SCQM_Approval_Request_Organization/Core_Organization/@refId))/*[1]/Organization_Name, char(41), ' ^^and can not be used for this request|and can not be used for this request^^.')}
            @newline "Small"
        @end-block   

        @block
        @block-visible-when "is-blank(//Change_Org_Contact) and is-blank(//Change_OrgLoc_Contact) and /store/SCQM_Approval_Request/Status/Status_Code[.!='AR_Discarded' and .!='AR_Discontinued' and .!='AR_Pending_APL' and .!='AR_Approved'] and is-blank(Admin_PreAction)"
    
            @button "Change_Org_Contact"
            @button-value "Change"
            @button-text "^^Edit|Edit^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-info pull-left"
    
        @end-block
        
        @block
        @block-visible-when "is-not-blank(Change_Org_Contact) and /store/SCQM_Approval_Request/Status/Status_Code[.!='AR_Discarded' and .!='AR_Discontinued' and .!='AR_Pending_APL' and .!='AR_Approved']"
    
            @block
            @block-visible-when "call('SCQM.AR.UserCanResendOrg', store-id(), Org_Created)"
                @button "Change_Org_Contact_Action"
                @button-value "SaveandResend"
                @button-text "^^Save and Resend Worksheet|Save and Resend Worksheet^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-inverse pull-left margin-lr-mini"
            @end-block        
    
            @button "Change_Org_Contact_Action"
            @button-value "Save"
            @button-text "^^Save|Save^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-success pull-left margin-lr-mini"
            
            @button "Change_Org_Contact_Action"
            @button-value "Cancel"
            @button-text "^^Cancel|Cancel^^"
            @button-optional "Yes"
            @button-inline "Yes"
            @button-ui-style "btn-warning pull-left margin-lr-mini"
    
        @end-block
    @end-block


    @block "ORGANIZATION_LOCATION_EMAIL_CHANGE"
    @block-visible-when "is-not-blank(OrgLocs_Created)"
    
        @newline "Large"
    
        {label(medium info block):'^^Organization Location Contact(s)|Organization Location Contact(s)^^'}
    
        @list "OrgLocs_Created"
        @list-sort "Ascending"
        @list-style "EnhancedList"
        @list-header " "
        @list-header-ui-style "hide"
        @list-allow-select "No"
        @list-allow-order "No"
        @list-allow-delete "No"
        @list-allow-new "No"
        @list-item-summary "concat('^^Location|Location^^: ', store(StoreId)/SCQM_Approval_Request_Organization_Location/Loc_Name)"

            @block
            @block-visible-when "is-blank(Change_OrgLoc_Contact)"

                @label "store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_First_Name"
                @label-header "^^Contact First Name|Contact First Name^^:"

                @label "store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_Last_Name"
                @label-header "^^Contact Last Name|Contact Last Name^^:"

                @label "store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_Email"
                @label-header "^^Contact Email|Contact Email^^:"

            @end-block            

            @block
            @block-visible-when "is-not-blank(Change_OrgLoc_Contact)"
            
                @text "New_Contact_First_Name"
                @text-header "^^Contact First Name|Contact First Name^^:"
                @text-optional "Yes"
                @text-footer "{{store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_First_Name}}"
                @text-footer-ui-style "inverse"                

                @text "New_Contact_Last_Name"
                @text-header "^^Contact Last Name|Contact Last Name^^:"
                @text-optional "Yes"
                @text-footer "{{store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_Last_Name}}"
                @text-footer-ui-style "inverse"                

                @text "New_Contact_Email"
                @text-header "^^Contact Email|Contact Email^^:"
                @text-optional "Yes"
                @text-footer "{{store(StoreId)/SCQM_Approval_Request_Organization_Location/Contact_Email}}"
                @text-validation-test "call('Core.IsValidEmail', New_Contact_Email)"
                @text-footer-ui-style "inverse"                
    
            @end-block
        
            @block
                @block-visible-when "is-not-blank(call('SCQM.ContactEmail.UsedByAnotherOrg', New_Contact_Email, /store/SCQM_Approval_Request/Main_Workbook_Data/Core_Organization/@refId))"
                @block-ui-style "well well-mini well-danger-light inverse"
                @icon "icon-warning-sign danger medium"
                {label(medium margin-lr-mini):concat('^^Attention|Attention^^: ^^Contact_Already_Exists|A User/Contact already exists with this e-mail address for a different Supplier^^', char(40), store(call('SCQM.ContactEmail.UsedByAnotherOrg', New_Org_Contact_Email, store(/store/SCQM_Approval_Request/Orgs_Created)/SCQM_Approval_Request_Organization/Core_Organization/@refId))/*[1]/Organization_Name, char(41), ' ^^and can not be used for this request|and can not be used for this request^^.')}
                @newline "Small"
            @end-block   

            @block
            @block-visible-when "is-blank(//Change_Org_Contact) and is-blank(//Change_OrgLoc_Contact) and /store/SCQM_Approval_Request/Status/Status_Code[.!='AR_Discarded' and .!='AR_Discontinued' and .!='AR_Pending_APL' and .!='AR_Approved'] and is-blank(//Admin_PreAction)"
    
                @button "Change_OrgLoc_Contact"
                @button-value "Change"
                @button-text "^^Edit|Edit^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-info"
    
            @end-block
            
            @block
            @block-visible-when "is-not-blank(Change_OrgLoc_Contact) and /store/SCQM_Approval_Request/Status/Status_Code[.!='AR_Discarded' and .!='AR_Discontinued' and .!='AR_Pending_APL' and .!='AR_Approved']"
    
                @block
                @block-visible-when "call('SCQM.AR.UserCanResendOrgLoc', store-id(), StoreId)"
                    @button "Change_OrgLoc_Contact_Action"
                    @button-value "SaveandResend"
                    @button-text "^^Save and Resend Worksheet|Save and Resend Worksheet^^"
                    @button-optional "Yes"
                    @button-inline "Yes"
                    @button-ui-style "btn-inverse pull-left margin-lr-mini"
                @end-block        
    
                @button "Change_OrgLoc_Contact_Action"
                @button-value "Save"
                @button-text "^^Save|Save^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success"
                
                @button "Change_OrgLoc_Contact_Action"
                @button-value "Cancel"
                @button-text "^^Cancel|Cancel^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-warning"
    
            @end-block
        @end-list
    @end-block
@end-block