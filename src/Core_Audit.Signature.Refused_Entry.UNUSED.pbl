﻿@mode "Refused_Entry"
@title "^^Signature|Signature^^"
@title-ui-style "xx-large primary"
@title-icon "icon-pencil"

@newline "Small"

@block
@block-visible-when "Status = 'Completed-Refused Entry' and is-blank(Ready_To_Close) and not(is-not-blank(Manager_First_Name) and is-not-blank(Manager_Last_Name) and ((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report))"
@button "Ready_To_Close"
@button-value "1"
@button-text "Verify Audit Is Complete"
@button-show-value "No"
@button-optional "Yes"
@button-causes-validation "Yes"
@button-causes-reload "Yes"
@button-validation-test "is-not-blank(Audit_Report)"
@button-validation-error "The audit report has not been generated.  Please generate the report before completing the audit."
@button-inline "Yes"
@button-ui-style "btn-large btn-primary"

{label(medium bold margin-lr-med):"Please click the 'Verify Audit Is Complete' button after gathering signatures and generating the report."}

@newline "Small"

@block
@block-ui-style "well well-danger-light"
No changes can be made to the report or to signatures after you click the 'Verify Audit Is Complete' button.
@end-block

@end-block

@block
@block-visible-when "Status = 'Completed-Refused Entry' and ((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report) and is-not-blank(Ready_To_Close)"
@button "Status"
@button-value "Refused Entry"
@button-text "Close Audit"
@button-show-value "No"
@button-action "Save"
@button-causes-reload "Yes"
@button-inline "Yes"
@button-ui-style "btn-large btn-primary"

{label(medium bold margin-lr-med):"Please click the 'Close Audit' button to finish the audit."}

@end-block

@block "Manager_Name_Block"
@block-visible-when "Status = 'Completed-Refused Entry' or Status = 'Refused Entry'"

@choice "Audit_Time_Zone"
@choice-style "DropDownList"
@choice-header "Time Zone:"
@choice-option "NST"
@choice-option "AST"
@choice-option "EST"
@choice-option "CST"
@choice-option "MST"
@choice-option "Arizona"
@choice-option "PST"
@choice-option "AKST"
@choice-option "HAST"
@choice-expression "time-zone-of(now())"
@choice-footer "NST - Newfoundland, AST - Atlantic, AKST - Alaska, HAST - Hawaii/Aleutian"

@block
@block-visible-when "is-blank(No_Signature)"
@signature "Manager_Signature"
@signature-header "Manager Signature:"
@signature-read-only-when "((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report)"
@end-block

@checkbox "No_Signature"
@checkbox-header "Unable to get manager's signature?"
@checkbox-read-only-when "((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report)"

@block
@block-visible-when "is-not-blank(No_Signature)"

@text "No_Signature_Reason"
@text-header "Please describe why you were unable to get a manager's signature:"
@text-rows "4"
@text-read-only-when "((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report)"

@end-block

@label "Specialist_Full_Name"
@label-header "QMS Name:"

@signature "Specialist_Signature"
@signature-header "{Specialist_Full_Name} Signature:"
@signature-read-only-when "((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report)"
@end-block "Manager_Name_Block"

@block
@block-visible-when "Status = 'Refused Entry'"
@label "Audit_Completion_Date"
@label-header "Completion Date/Time:"
@end-block