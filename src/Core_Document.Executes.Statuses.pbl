﻿@display "No"
@print "No"


@execute "SetStatus"
@comment
Set the status lookup field (name assumed to be 'Status') to the status code given
  #param String StatusCode The code of the status to set to.
@end-comment

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', $Parameter:StatusCode, 'Document Management')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"
@end-execute


@execute "SetInitialStatus"
@comment
Set the status when no status has yet been set
@end-comment

    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "call('CoreDoc.GetInitialStatus')"
@end-execute

@execute "SetStatusAcknowledged"
@comment
Set status to DM_Acknowledged
@end-comment

    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Acknowledged'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Acknowledged'"

    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Acknowledged'"
@end-execute

@execute "SetStatusApproved"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Approved'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Approved'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Approved'"
    
    @update
        @update-target-select "Return_Reason_Message"
        @update-source-type "Node"
@end-execute

@execute "SetStatusActive"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Active'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Active'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Active'"

@end-execute

@execute "SetStatusInactive"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Inactive'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Inactive'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Inactive'"
@end-execute

@execute "SetStatusInReview"
@comment
Set status to DM_In_Review
@end-comment
    
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_In_Review'"

    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.In_Review'"

    @update
        @update-target-select "Begin_Review"
        @update-source-type "Node"
@end-execute

@execute "SetStatusPendingAcknowledgment"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Pending_Acknowledgment'"

    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Requested_Ack'"

    @invoke
        @invoke-execute "Core_Document.SendNotification"
        @invoke-parameter "NotificationName"
            @invoke-parameter-expression "'Core_Document_Pending_Acknowledgment'"

    @update
        @update-target-select "Requested_On"
        @update-source-type "Node"
 
    @update
        @update-target-select "Requested_By"
        @update-source-type "Node"
@end-execute
 
@execute "SetStatusPendingApproval"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Pending_Approval'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Submitted'"

    @invoke
        @invoke-execute "Core_Document.SendNotification"
        @invoke-parameter "NotificationName"
            @invoke-parameter-expression "'Core_Document_Pending_Approval'"

    @update
        @update-target-select "Requested_On"
        @update-source-type "Node"
    
    @update
        @update-target-select "Requested_By"
        @update-source-type "Node"
@end-execute

@execute "SetStatusRequested"
@comment
Set status to DM_Requested
@end-comment
    
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Requested'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Requested'"

    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-when "is-blank(Return_Reason)"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "if(Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%acknowledge%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%', 'DOC.Event.Requested_Ack', 'DOC.Event.Requested')"

    @update
        @update-target-select "Action"
        @update-source-type "Node"
@end-execute


@execute "Archive"
@comment
Set status of previously executed document to Archived when:
- the document is executed
- there is an existing Active or expired document
Workflow sets the context to the store to be archived
@end-comment

    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Archived'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Archived'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Archived'"
@end-execute

@execute "Expire"
    @invoke
        @invoke-execute "Core_Document.SetStatus"
        @invoke-parameter "StatusCode"
            @invoke-parameter-expression "'DM_Expired'"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Expired'"
    
    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Expired'"
@end-execute

@execute "Return"
    @invoke
        @invoke-execute "Core_Document.SetStatusRequested"
    
    @invoke
        @invoke-execute "Core_Document.CaptureEvent"
        @invoke-parameter "EventName"
            @invoke-parameter-expression "'Returned'"

    @invoke
        @invoke-execute "Core_Event_Log.RecordContextEvent"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'DOC.Event.Returned'"
    
    @update
        @update-target-select "Send_Returned_Notification"
        @update-source-select "'Yes'"
    
    @update
        @update-target-select "Return_Reason_Message"
        @update-source-select "Return_Reason"
        @update-source-type "Node"
    
    @update
        @update-target-select "Return_Reason"
        @update-source-type "Node"
@end-execute