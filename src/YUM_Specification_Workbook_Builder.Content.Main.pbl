﻿@topic
@topic-title "Workbook"
@topic-mode "Main"

@monitor "Build_Monitor"
@monitor-active-when "Build/Status = 'In Progress'"
@monitor-interval "20 seconds"
@monitor-action "Reload"

@block
@block-visible-when "Build/Status = 'In Progress'"
@block-ui-style "well well-small well-info-light medium"
    Please wait. Building...
@end-block

@text "Name"
@text-header "Name"
@text-columns "80"

@choice "Type"
@choice-style "DropDownList"
@choice-header "Type:"

    @option
    @option-text "Audit"
    @option-value "Audit"

    @option
    @option-text "Policy"
    @option-value "Policy"
        
    @option
    @option-text "Workbook"
    @option-value "Workbook"
        
@end-choice

@choice "Status"
@choice-style "DropDownList"
@choice-header "Status:"

    @option
    @option-text "Draft"
    @option-value "Draft"

    @option
    @option-text "Published"
    @option-value "Published"

    @option
    @option-text "Deleted"
    @option-value "Deleted"
        
@end-choice

@text "Languages"
@text-header "Languages"
@text-columns "80"
@text-optional "Yes"
@text-footer "Comma separated list. Example: fr, es"

@text "Description"
@text-header "Description"
@text-columns "80"
@text-rows "3"
@text-optional "Yes"

@block
@block-visible-when "Versions/item/Number"

    @label "Versions/item[last()]/Number"
    @label-header "Version #"

    @label "Versions/item[last()]/Author/Full_Name"
    @label-header "Version Author"

    @block
    @block-visible-when "Versions/item[last()][Start_Date != '' or End_Date != '']"

        @label "concat(localize-date(Versions/item[last()]/Start_Date), ' - ', localize-date(Versions/item[last()]/End_Date))"
        @label-header "Version Available"
    
    @end-block    

@end-block

@lookup "Descriptor"
@lookup-header "Descriptor"
@lookup-allow-new-when "false()"
@lookup-queue "System_Workbook_Builder_Descriptor.Active_List"
@lookup-select "System_Workbook_Builder_Descriptor/Name"
@lookup-field "Name"
@lookup-updatable-when "true()"
@lookup-optional "Yes"

@checkbox "Keep_PBL_Pages"
@checkbox-header "Keep PBL Pages"
@checkbox-footer "Determines whether temporary PBL pages are to be kept and available in PBL Studio."

@label "Build/Status"
@label-header "Building Status:"

@block
@block-visible-when "is-not-blank(Name) and Builder/* and not(Build/Status = 'In Progress')"

    @button "Action"
    @button-value "Build"
    @button-text "Build"
    @button-show-value "No"
    @button-optional "Yes"
    @button-ui-style "btn-primary"
    @button-confirm "Are you sure you what to build?"
    
    @button "Action"
    @button-value "Clone"
    @button-text "Clone"
    @button-show-value "No"
    @button-optional "Yes"
    @button-ui-style "btn-primary"
    @button-confirm "Are you sure you what to create a clone?"
    @button-action "NewAndPublish"
    @button-argument "YUM_Specification_Workbook_Builder"
    @button-argument "Clone_ID"    
    @button-argument-value "{{store-id()}}"    
    
@end-block
