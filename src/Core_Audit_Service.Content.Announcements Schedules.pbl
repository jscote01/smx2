﻿@mode "Main, Shared"
@title "^^Announcements/Schedules|Announcements/Schedules^^"

@choice "Audits_Announced_Or_Scheduled"
    @choice-header "^^Audits announced or scheduled|Audits announced or scheduled^^?"
    @choice-header-ui-style "bold"
    @choice-columns "3"
        @option "Unannounced"
        @option-text "^^Unannounced|Unannounced^^"
        @option-value "Unannounced"
        
        @option "Scheduled"
        @option-text "^^Scheduled|Scheduled^^"
        @option-value "Scheduled"

        @option "Announced"
        @option-text "^^Announced|Announced^^"
        @option-value "Announced"
    @end-choice

@block "Audits_Announced"
@block-visible-when "Audits_Announced_Or_Scheduled='Announced'"

    @choice "Audits_Announced_Method"
        @choice-header "^^Audits_Announced_Method|How are audits announced^^?"
        @choice-header-ui-style "bold"
        
        @option "Email"
        @option-text "^^Email|Email^^"
        @option-value "Email"
            
        @option "Phone"
        @option-text "^^Phone|Phone^^"
        @option-value "Phone"
    
        @option "Schedule_Report"
        @option-text "^^Schedule Report|Schedule Report^^"
        @option-value "Schedule Report"
            
        @option "Other"
        @option-text "^^Other|Other^^"
        @option-value "Other"
    @end-choice
        
    @block "Method_Other"
    @block-visible-when "Audits_Announced_Method = 'Other'"
    
        @text "Audits_Announced_Method_Other"
            @text-header "^^Audits_Announced_Method_Other|Explain other announcement method^^:"
            @text-header-ui-style "bold"
            @text-rows "2"
    @end-block "Method_Other"
    
    @choice "Audits_Announced_Initial_Audit_Only"
        @choice-header "^^Audits_Announced_Initial_Audit_Only|Only the Initial Audit announced^^?"
        @choice-header-ui-style "bold"
        @choice-columns "3"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
            
            @option "No"
            @option-text "^^No|No^^"
            @option-value "No"
        @end-choice
    
    @block "Advance_Announcement_Period_Type"
    @block-visible-when "Audits_Announced_Initial_Audit_Only = 'No'"
    
        @choice "Advance_Announcement_Period_Type"
            @choice-header "^^Advance_Announcement_Period_Type_Header|Advance announcement period before actual audit date^^:"
            @choice-header-ui-style "bold"
            @choice-columns "3"
                @option "Days"
                @option-text "^^Days|Days^^"
                @option-value "Days"
                
                @option "Weeks"
                @option-text "^^Weeks|Weeks^^"
                @option-value "Weeks"
                
                @option "Specific_Day_of_Month"
                @option-text "^^Specific Day of Month (for following month)|Specific Day of Month (for following month)^^"
                @option-value "Specific Day of Month (for following month)"
        @end-choice            
            
        @block
        @block-visible-when "Advance_Announcement_Period_Type = 'Days'"
            @text "Advance_Announcement_Period_Days"
                @text-header "^^Advance_Announcement_Period_Days|Number of days in advance^^:"
                @text-header-ui-style "bold"
                @text-type "Integer"
                @text-mask "99"
        @end-block
        
        @block
        @block-visible-when "Advance_Announcement_Period_Type = 'Weeks'"
            @text "Advance_Announcement_Period_Weeks"
                @text-header "^^Advance_Announcement_Period_Weeks|Number of weeks in advance^^:"
                @text-header-ui-style "bold"
                @text-type "Integer"
                @text-mask "99"
        @end-block
    
        @block
        @block-visible-when "Advance_Announcement_Period_Type = 'Specific Day of Month (for following month)'"
            @text "Advance_Announcement_Period_Day_Of_Month"
                @text-header "^^Advance_Announcement_Period_Day_Of_Month|Day of Month^^:"
                @text-header-ui-style "bold"
                @text-type "Integer"
                @text-mask "99"
        @end-block
    @end-block "Advance_Announcement_Period_Type"
@end-block "Audits_Announced"
 
@block
@block-visible-when "Audits_Announced_Or_Scheduled='Scheduled'"

    @choice "Audits_Scheduled_Initial_Audit_Only"
        @choice-header "^^Audits_Scheduled_Initial_Audit_Only|Will only the Initial Audit be scheduled^^?"
        @choice-header-ui-style "bold"
        @choice-columns "3"
            @option "Yes"
            @option-text "^^Yes|Yes^^"
            @option-value "Yes"
            
            @option "No"
            @option-text "^^No|No^^"
            @option-value "No"
    @end-choice
@end-block