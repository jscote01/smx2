﻿@app "SCQM_OrgLoc_APL"

@language "all"

@analyze
    
    @query "APLGroups"
        @query-queue "SCQM_Approved_Product_List_Group.Master_Q"
        @query-filter "{{filter:concat('Status_Code in (', call('SCQM.APL.GetActiveStatusesSQLFilter'), ',', call('SCQM.APL.GetApprovedStatusesSQLFilter'), ')')}}"
        @query-store-id "APL_GROUP_STORE_ID"
        
        @column "APL_GROUP_STORE_ID"
            @column-source "StoreId"
        @column "Specification"
        @column "Org_Location"
        @column "APL_Group_Id"
        @column "Approved_Countries"
        @column "Additional_Countries_List"
        @column "Active_Regions_List"
        @column "Status_Status_Name"
        @column "Created_Date"
        
    @end-query
    
    @query "OrgLoc"
        @query-queue "Core_Organization_Location.Master_Q"
        @query-store-id "ORGLOC_STORE_ID"
        
        @column "ORGLOC_STORE_ID"
            @column-source "StoreId"
        @column "Location_Id"
        @column "Location_Name"
        
    @end-query
    
    @query "Specification"
        @query-queue "SCQM_Specification.Master_Q"
        @query-store-id "SPEC_STORE_ID"
        
        @column "SPEC_STORE_ID"
            @column-source "StoreId"
        @column "Status_Code"
        @column "Specification_Name"
        @column "Specification_Number"
        @column "Product_Hierarchy_1"
        @column "Product_Hierarchy_2"
        @column "Product_Hierarchy_3"
        @column "Product_Hierarchy_4"
        @column "Product_Hierarchy_5"
        
    @end-query
    
    @query "Combined"
        @query-distinct-values "Yes"        
        @subquery "APLGroups"
        @subquery "OrgLoc"
            @subquery-join "OrgLoc.ORGLOC_STORE_ID = APLGroups.Org_Location"
            @subquery-join-type "Inner"            
        @subquery "Specification"
            @subquery-join "Specification.SPEC_STORE_ID = APLGroups.Specification"            
            @subquery-join-type "Inner"
            
        @column "SPEC_STORE_ID"
            @column-source "Specification.SPEC_STORE_ID"            
        @column "APL_GROUP_STORE_ID"
            @column-source "APLGroups.APL_GROUP_STORE_ID"
        @column "ORGLOC_STORE_ID"
            @column-source "APLGroups.Org_Location"
        @column "Facility_Id"
            @column-source "OrgLoc.Location_Id"
        @column "Facility_Name"
            @column-source "OrgLoc.Location_Name"
        @column "APL_Id"
            @column-source "APLGroups.APL_Group_Id"
        @column "Specification_Name"
            @column-source "Specification.Specification_Name"
        @column "Specification_Number"
            @column-source "Specification.Specification_Number"
        @column "Product_Hierarchy_1"
            @column-source "Specification.Product_Hierarchy_1"
        @column "Product_Hierarchy_2"
            @column-source "Specification.Product_Hierarchy_2"
        @column "Product_Hierarchy_3"
            @column-source "Specification.Product_Hierarchy_3"
        @column "Product_Hierarchy_4"
            @column-source "Specification.Product_Hierarchy_4"
        @column "Product_Hierarchy_5"
            @column-source "Specification.Product_Hierarchy_5"
        @column "Approved_Country_List"
            @column-source "APLGroups.Approved_Countries"
        @column "Additional_Country_List"
            @column-source "APLGroups.Additional_Countries_List"
        @column "Region_List"
            @column-source "APLGroups.Active_Regions_List"
        @column "Status"
            @column-source "APLGroups.Status_Status_Name"
        @column "APL_Create_Date"
            @column-source "APLGroups.Created_Date"
        
    @end-query

@end-analyze


@comment
This queue is in scope of a specific Location.
User must have permission to view this queue.
Queue is available only in the scope of Locations with business type of "Supplier".
@end-comment
@queue "Aligned_Products_InScope_Q"
@queue-query "Combined"
@queue-key "APL_GROUP_STORE_ID"
@queue-header "^^Aligned Products|Aligned Products^^"
@queue-activator "^^Aligned Products|Aligned Products^^"
@queue-sequence "500"
@queue-test "call('Core.CheckPermission', 'SCQM_OrgLoc_APL.Aligned_Products_InScope_Q') and call('Core.CheckCapability', 'SMX.APL') and /store/*[1]/Business_Type/Business_Type_Id = 'Supplier'"
@queue-scope "Core_Organization_Location"
@queue-scope-column "ORGLOC_STORE_ID"
@queue-allow-new "No"
@queue-allow-navigate "No"

    @action
        @action-name "Navigate"
        @action-argument "{{APL_GROUP_STORE_ID}}"
        @action-text "APL_Id | ^^OPEN|OPEN^^"
        @action-container "Column"

    @action
        @action-name "Navigate"
        @action-argument "{{SPEC_STORE_ID}}"
        @action-text "Specification_Name | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "APL_GROUP_STORE_ID"
        @column-hidden "Yes"
    @column "ORGLOC_STORE_ID"
        @column-hidden "Yes"
    @column "SPEC_STORE_ID"
        @column-hidden "Yes"
    @column "APL_Id"
        @column-header "^^APL ID|APL ID^^"
    @column "Specification_Name"
        @column-header "^^Specification Name|Specification Name^^"
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"
    
    @column "Product_Hierarchy_1"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1))}}"
        
    @column "Product_Hierarchy_2"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2))}}"
        
    @column "Product_Hierarchy_3"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3))}}"
        
    @column "Product_Hierarchy_4"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4))}}"
        
    @column "Product_Hierarchy_5"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5))}}"

    @column "Approved_Country_List"
        @column-header "^^Approved Country(-ies)|Approved Country(-ies)^^"
    @column "Additional_Country_List"
        @column-header "^^Additional Country(-ies)|Additional Country(-ies)^^"
    @column "Region_List"
        @column-header "^^Region(s)|Region(s)^^"
    @column "Status"
        @column-header "^^APL Status|APL Status^^"

@end-queue


@queue "Aligned_Products_All_Facilities_Q"
@queue-query "Combined"
@queue-key "APL_GROUP_STORE_ID"
@queue-header "^^Aligned Products|Aligned Products^^"
@queue-activator "^^Products|Products^^ | ^^Aligned Products|Aligned Products^^"
@queue-data-firewall "Yes"
@queue-sequence "4400"
@queue-test "call('Core.CheckPermission', 'SCQM_OrgLoc_APL.Aligned_Products_All_Facilities_Q') and call('Core.CheckCapability', 'SMX.APL')"
@queue-sort "APL_Create_Date DESC"
@queue-allow-new "No"
@queue-allow-navigate "No"

    @action
        @action-name "Navigate"
        @action-argument "{{APL_GROUP_STORE_ID}}"
        @action-text "APL_Id | ^^OPEN|OPEN^^"
        @action-container "Column"

    @action
        @action-name "Navigate"
        @action-argument "{{ORGLOC_STORE_ID}}"
        @action-text "Facility_Id | ^^OPEN|OPEN^^"
        @action-container "Column"

    @action
        @action-name "Navigate"
        @action-argument "{{SPEC_STORE_ID}}"
        @action-text "Specification_Name | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "APL_GROUP_STORE_ID"
        @column-hidden "Yes"
    @column "ORGLOC_STORE_ID"
        @column-hidden "Yes"
    @column "SPEC_STORE_ID"
        @column-hidden "Yes"
    @column "APL_Id"
        @column-header "^^APL ID|APL ID^^"
    @column "Facility_Id"
        @column-header "^^Facility ID|Facility ID^^"
    @column "Facility_Name"
        @column-header "^^Facility Name|Facility Name^^"
    @column "Specification_Name"
        @column-header "^^Specification Name|Specification Name^^"
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"
    
    @column "Product_Hierarchy_1"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1))}}"
        
    @column "Product_Hierarchy_2"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2))}}"
        
    @column "Product_Hierarchy_3"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3))}}"
        
    @column "Product_Hierarchy_4"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4))}}"
        
    @column "Product_Hierarchy_5"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5))}}"

    @column "Approved_Country_List"
        @column-header "^^Approved Country(-ies)|Approved Country(-ies)^^"
    @column "Additional_Country_List"
        @column-header "^^Additional Country(-ies)|Additional Country(-ies)^^"
    @column "Region_List"
        @column-header "^^Region(s)|Region(s)^^"
    @column "Status"
        @column-header "^^APL Status|APL Status^^"

@end-queue