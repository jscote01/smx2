﻿@display "No"

@comment
Parameter:APL - store id of SCQM_Approved_Product_List expected
Parameter:APL_Subprocesses_Created - comma-delimited list of the store ids of SCQM_Approved_Product_List_Subprocesses expected
Parameter:NewStoreId - OUT parameter containing the new store's ID

Example Invoke:
    invoke
    invoke-execute "SCQM_Approved_Product_List_Subprocess_Pairing.Create"
    invoke-parameter "APL"
    invoke-parameter-expression "$APL"
    invoke-parameter-direction "In"
    invoke-parameter "APL_Subprocesses_Created"
    invoke-parameter-expression "$APL_Subprocesses_Created"
    invoke-parameter-direction "In"
    invoke-parameter "NewStoreId"
    invoke-parameter-expression "$NewStoreId"
    invoke-parameter-direction "Out"
@end-comment

@execute "Create"
@execute-select "split($Parameter:APL_Subprocesses_Created, ',')"
@execute-select-key "/"

    @update
    @update-target-select "$CreateNodes/SPP"
    @update-source-select "''"
    @update
    @update-target-select "$CreateNodes/SPP/APL"
    @update-source-select "$Parameter:APL"
    @update
    @update-target-select "$CreateNodes/SPP/Subprocess"
    @update-source-select "execute-select-context()"

    @comment
    Create the new store if all required fields are present and return the newly created store id in the given parameter.
    @end-comment    
    @invoke
    @invoke-execute "Core_Executes.CreateAppStore"
    @invoke-when "is-not-blank($CreateNodes/SPP/APL) and is-not-blank($CreateNodes/SPP/Subprocess)"
    @invoke-parameter "AppName"
    @invoke-parameter-expression "'%app%SCQM_Approved_Product_List_Subprocess_Pairing%app%'"
    @invoke-parameter "Nodes"
    @invoke-parameter-expression "$CreateNodes/SPP"
    @invoke-parameter "StoreId"
    @invoke-parameter-expression "$NewSPPStoreId"
    @invoke-parameter-direction "Out"

    @update
    @update-target-select "$Parameter:NewStoreId"
    @update-source-select "join($Parameter:NewStoreId, $NewSPPStoreId, ', ')"

    @update
    @update-store-id "$CreateNodes/SPP/Subprocess"
    @update-target-select "/store/*[1]/Checkin"
    @update-source-type "Node"
    @update-run-workbook "Yes"
    @update-publish "Yes"    

    @update
    @update-store-id "$CreateNodes/APL_Nodes/APL_Group"
    @update-target-select "/store/Touch"
    @update-source-type "Node"
    @update-run-workbook "Yes"
    @update-publish "Yes"    

@end-execute


@comment
This execute is called when we need to set/change the Subprocess to which this pairing is related and update the status of the subprocess to Active
    $Parameter:SubprocessStoreId The StoreId of the Subprocess to be paired with
@end-comment
@execute "ApproveNewSubprocess"

    @comment
    Discontinue the subprocess that will now be orphaned because it is not to be used.
    @end-comment    
    @invoke
        @invoke-execute "SCQM_Approved_Product_List_Subprocesses.Discontinue"
        @invoke-store-id "Subprocess/@refId"

    @comment
    Switch the Pairing store to point to the already existing Subprocess
    @end-comment    
    @update
        @update-target-select "Subprocess"
        @update-source-select "$Parameter:SubprocessStoreId"

    @comment
    Change the Pairing status to Active
    @end-comment    
    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'APL_SP_Active', 'Approved Product List Subprocess')"
        @update-run-workbook "Yes"

    @comment
    Complete any open task for this pairing - currently the only task will be when the subprocess needs to be designated.
    @end-comment    
    @invoke
        @invoke-execute "SCQM_Shared_Executes.Complete_WRM_Assignment"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "find-store-id('Core_Task_Assignment.Master_Q', concat('Context = ', char(39), store-id(), char(39), ' and Status_Code IN ', char(40), char(39), 'AS_Open', char(39), char(44), char(39), 'AS_Pending', char(39), char(41)), 'Opened_Date')"

    @comment
    If designating an existing subprocess for this country, create an event log
    @end-comment    
    @invoke
        @invoke-execute "Core_Event_Log.RecordEvent"
        @invoke-when "$Parameter:PairingType = 'Approve_New_Pairings'"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "store-id()"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'APL.Event.Additional_Country_Subprocess_Designated'"

@end-execute

@comment
This execute is called when we need to set/change the Subprocess to which this pairing is related and update the status of the subprocess to Proposed
    $Parameter:SubprocessStoreId The StoreId of the Subprocess to be paired with
@end-comment
@execute "ProposeNewSubprocess"

    @update
        @update-target-select "Subprocess"
        @update-source-select "$Parameter:SubprocessStoreId"
    
    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'APL_SP_Proposed', 'Approved Product List Subprocess')"
        @update-run-workbook "Yes"        
        
@end-execute

@comment
This execute is called when we need to clear the Subprocess to which this pairing is related and update the status of the subprocess to Initialized
@end-comment
@execute "ReInitializeSubprocess"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'APL_SP_Initialized', 'Approved Product List Subprocess')"
        @update-run-workbook "Yes"        
        
@end-execute


@comment
Call this execute to Discontinue the APL Subprocess Pairing record.
@end-comment
@execute "Discontinue"

    @update
        @update-target-select "/store/*[1]/Status"
        @update-source-select "call('Core.GetStatusStoreId', 'APL_SP_Discontinued', 'Approved Product List Subprocess')"
        @update-source-type "Lookup"

    @invoke
        @invoke-execute "CancelAllOpenAssignments"
        @invoke-store-id "store-id()"
@end-execute

@comment
Loop through all open and pending task assignments for the Subprocess and cancel them.
@end-comment
@execute "CancelAllOpenAssignments"
    @execute-select "queue('Core_Task_Assignment.Master_Q', concat('Context = ', char(39), store-id(), char(39), ' and Status_Code in (', char(39), 'AS_Open', char(39), char(44), char(39), 'AS_Pending', char(39), ')'), ' ', 0, 0, ' ', false())/item"
    @execute-select-key "StoreId"

    @invoke
        @invoke-execute "Core_Task_Assignment.MarkTaskCancelled"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "execute-select-context()/StoreId"

@end-execute