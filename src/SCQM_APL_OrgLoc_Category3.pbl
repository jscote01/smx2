﻿@app "SCQM_APL_OrgLoc_Category3"

@language "all"

@analyze

    @query "OrgLoc"
        @query-queue "Core_Organization_Location.Master_Q"
        @query-filter "1=1"
        @query-store-id "ORGLOC_STORE_ID"
        
        @column "ORGLOC_STORE_ID"
            @column-source "StoreId"
            
    @end-query
    
    @query "APLs"
        @query-queue "SCQM_Approved_Product_List.Master_Q"
        @query-filter "{{filter:concat('Status_Code in (', call('SCQM.APL.GetApprovedStatusesSQLFilter'), ')')}}"
        @query-store-id "APL_STORE_ID"
        
        @column "APL_STORE_ID"
            @column-source "StoreId"
        @column "APL_Group"
        @column "Country"
        
    @end-query
    
    @query "APLGroups"
        @query-queue "SCQM_Approved_Product_List_Group.Master_Q"
        @query-filter "1=1"
        @query-store-id "APL_GROUP_STORE_ID"
        
        @column "APL_GROUP_STORE_ID"
            @column-source "StoreId"
        @column "Org_Location"
        @column "Specification"
        @column "APL_Group_Id"
        
    @end-query
    
    @query "Specifications"
        @query-queue "SCQM_Specification.Master_Q"
        @query-store-id "SPEC_STORE_ID"
        
        @column "SPEC_STORE_ID"
            @column-source "StoreId"
        @column "Product_Hierarchy_3"
        
    @end-query

    @query "Combined"
        @query-store-id "ORGLOC_STORE_ID"
        @query-distinct-values "Yes"        
        
        @subquery "APLs"
        @subquery "APLGroups"
            @subquery-join "APLGroups.APL_GROUP_STORE_ID = APLs.APL_Group"
            @subquery-join-type "Inner"        
        @subquery "OrgLoc"
            @subquery-join "OrgLoc.ORGLOC_STORE_ID = APLGroups.Org_Location"
            @subquery-join-type "Inner"
        @subquery "Specifications"
            @subquery-join "Specifications.SPEC_STORE_ID = APLGroups.Specification"            
            @subquery-join-type "Inner"            
            
        @column "ORGLOC_STORE_ID"
            @column-source "OrgLoc.ORGLOC_STORE_ID"
        @column "Product_Hierarchy_3"
            @column-source "Specifications.Product_Hierarchy_3"
        
    @end-query

@end-analyze


@queue "APL_OrgLocs_Distinct_Approved_Category3"
@queue-query "Combined"
@queue-key "ORGLOC_STORE_ID"
@queue-header "^^Approved Facilities Product Category|Approved Facilities Product Category^^ 3 ({{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3)}})"
@queue-activator "^^Internal_Administration_Menu|Admin^^ | ^^Approved Product List|Approved Product List^^ | ^^Report Queues Interim|Report Queues Interim^^"
@queue-test "call('Core.IsITUser')"
@queue-filter "1=1"
@queue-sort "ORGLOC_STORE_ID, Product_Hierarchy_3"
@queue-allow-navigate "No"
@queue-allow-new "No"

    @column "ORGLOC_STORE_ID"
        @column-header "^^OrgLoc StoreID|OrgLoc StoreID^^"
        
    @column "Product_Hierarchy_3"
        @column-header "{{call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3)}}"
        @column-hidden "{{is-blank(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3))}}"
    
@end-queue