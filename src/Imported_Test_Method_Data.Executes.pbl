﻿@display "No"

@execute
    @execute-when "is-not-blank(MakeChange)"

    @update
        @update-target-select "MakeChange"
        @update-source-type "Node"

    @invoke
        @invoke-execute "MakeChangesRequested"
        @invoke-when "is-not-blank(Where_Type_Is) and is-blank(Skip_Already_Extrapolated)"
        @invoke-parameter "Where_Type_Is"
            @invoke-parameter-expression "Where_Type_Is"
        @invoke-parameter "Where_Description_Contains"
            @invoke-parameter-expression "Where_Description_Contains"
        @invoke-parameter "Extrapolated_Type"
            @invoke-parameter-expression "Extrapolated_Type"
        @invoke-parameter "NumberChanged"
            @invoke-parameter-expression "NumberChanged"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "MakeChangesRequestedOnRemaining"
        @invoke-when "is-not-blank(Where_Type_Is) and is-not-blank(Skip_Already_Extrapolated)"
        @invoke-parameter "Where_Type_Is"
            @invoke-parameter-expression "Where_Type_Is"
        @invoke-parameter "Where_Description_Contains"
            @invoke-parameter-expression "Where_Description_Contains"
        @invoke-parameter "Extrapolated_Type"
            @invoke-parameter-expression "Extrapolated_Type"
        @invoke-parameter "NumberChanged"
            @invoke-parameter-expression "NumberChanged"
            @invoke-parameter-direction "InOut"

@end-execute

@execute "MakeChangesRequested"
    @execute-select "queue('Imported_Test_Method_Data.Master_Q', concat('[Type] = ', char(39), $Parameter:Where_Type_Is, char(39), ' and [Description] like ', char(39), char(37), $Parameter:Where_Description_Contains, char(37), char(39)))/item"
    @execute-select-key "StoreId"

    @update
        @update-store-id "execute-select-context()/StoreId"
        @update-target-select "/store/Imported_Test_Method_Data/Extrapolated_Type"
        @update-source-select "$Parameter:Extrapolated_Type"
        @update-source-type "Node"

    @update
        @update-target-select "$Parameter:NumberChanged"
        @update-source-select "coalesce($Parameter:NumberChanged, 0) + 1"

@end-execute

@execute "MakeChangesRequestedOnRemaining"
    @execute-select "queue('Imported_Test_Method_Data.Master_Q', concat('[Type] = ', char(39), $Parameter:Where_Type_Is, char(39), ' and [Description] like ', char(39), char(37), $Parameter:Where_Description_Contains, char(37), char(39), ' and Extrapolated_Type IS NULL'))/item"
    @execute-select-key "StoreId"

    @update
        @update-store-id "execute-select-context()/StoreId"
        @update-target-select "/store/Imported_Test_Method_Data/Extrapolated_Type"
        @update-source-select "$Parameter:Extrapolated_Type"
        @update-source-type "Node"

    @update
        @update-target-select "$Parameter:NumberChanged"
        @update-source-select "coalesce($Parameter:NumberChanged, 0) + 1"

@end-execute



@execute
    @execute-when "is-not-blank(CreateStores)"

    @update
        @update-target-select "CreateStores"
        @update-source-type "Node"

    @invoke
        @invoke-execute "CreateAllStores"
        @invoke-parameter "Creation_Count"
            @invoke-parameter-expression "Creation_Count"
            @invoke-parameter-direction "InOut"

@end-execute

@execute
    @execute-when "is-not-blank(CreateOneStore)"

    @update
        @update-target-select "CreateOneStore"
        @update-source-type "Node"

    @invoke
        @invoke-execute "CreateOneStore"
        @invoke-parameter "Creation_Count"
            @invoke-parameter-expression "Creation_Count"
            @invoke-parameter-direction "InOut"

@end-execute

@execute "CreateOneStore"

    @update
        @update-target-select "$Local:NewItem/Item_Name"
        @update-source-select "Description"

    @update
        @update-target-select "$Local:NewItem/Item_Value"
        @update-source-select "Description"

    @update
        @update-target-select "$Local:NewItem/Item_ID"
        @update-source-select "Description"

    @update
        @update-target-select "$Local:NewItem/Item_ID/@calculated"
        @update-source-select "Description"

    @update
        @update-target-select "$Local:Type"
        @update-source-select "concat(Extrapolated_Type, ' Values')"

    @update
        @update-target-select "$Local:NewItem/Dropdown_Group"
        @update-source-select "find-store-id('Core_Custom_Dropdown_Group.Master_Q', concat('Group_ID = ', char(39), $Local:Type, char(39)))"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Custom_Dropdown_Group/*"

    @update
        @update-target-select "$Local:NewItem/Dropdown_List"
        @update-source-select "find-store-id('Core_Custom_Dropdown_List.Master_Q', concat('Dropdown_Group_ID = ', char(39), $Local:Type, char(39)))"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Custom_Dropdown_List/Dropdown_Name | Core_Custom_Dropdown_List/Group_Level"
    
    @update
        @update-target-select "$Local:NewItem/Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Active', 'Active States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"

    @invoke
        @invoke-execute "Core_Executes.CreateAppStore"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "'Core_Custom_Dropdown_Item'"
        @invoke-parameter "Nodes"
            @invoke-parameter-expression "$Local:NewItem"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "$Parameter:NewStoreId"
            @invoke-parameter-direction "Out"

    @update
        @update-target-select "$Parameter:Creation_Count"
        @update-source-select "coalesce($Parameter:Creation_Count, 0) + 1"

@end-execute

@execute "CreateAllStores"
    @execute-select "queue('Imported_Test_Method_Data.Master_Q')/item"
    @execute-select-key "StoreId"

    @update
        @update-target-select "$Local:Description"
        @update-source-select "store(execute-select-context()/StoreId)/*[1]/Description"

    @update
        @update-target-select "$Local:NewItem/Item_Name"
        @update-source-select "$Local:Description"

    @update
        @update-target-select "$Local:NewItem/Item_Value"
        @update-source-select "$Local:Description"

    @update
        @update-target-select "$Local:NewItem/Item_ID"
        @update-source-select "$Local:Description"

    @update
        @update-target-select "$Local:NewItem/Item_ID/@calculated"
        @update-source-select "$Local:Description"

    @update
        @update-target-select "$Local:Type"
        @update-source-select "concat(store(execute-select-context()/StoreId)/*[1]/Extrapolated_Type, ' Values')"

    @update
        @update-target-select "$Local:NewItem/Dropdown_Group"
        @update-source-select "find-store-id('Core_Custom_Dropdown_Group.Master_Q', concat('Group_ID = ', char(39), $Local:Type, char(39)))"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Custom_Dropdown_Group/*"

    @update
        @update-target-select "$Local:NewItem/Dropdown_List"
        @update-source-select "find-store-id('Core_Custom_Dropdown_List.Master_Q', concat('Dropdown_Group_ID = ', char(39), $Local:Type, char(39)))"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Custom_Dropdown_List/Dropdown_Name | Core_Custom_Dropdown_List/Group_Level"
    
    @update
        @update-target-select "$Local:NewItem/Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Active', 'Active States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"

    @invoke
        @invoke-execute "Core_Executes.CreateAppStore"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "'Core_Custom_Dropdown_Item'"
        @invoke-parameter "Nodes"
            @invoke-parameter-expression "$Local:NewItem"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "$Parameter:NewStoreId"
            @invoke-parameter-direction "Out"

    @update
        @update-target-select "$Parameter:Creation_Count"
        @update-source-select "coalesce($Parameter:Creation_Count, 0) + 1"

@end-execute
