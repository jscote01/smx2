﻿================================================================
================================================================
'''PRODUCT v4.0 - CORE'''
================================================================
================================================================
'''Global and Shared'''
[[Core_Global_Functions]]
[[Core_Global_Macros]]
[[Core_Master_Data_Translation_Tokens]]
[[Shared_Wikis]]
[[Core_Executes]]
[[Core_Batch_Store_Updater]]
[[Test_App]]

'''Base System Apps'''
[[User]]
[[Core_User_Location_Access]]
[[Core_User_Organization_Access]]
[[Core_User_Org_and_Loc_Access]]
[[Core_Configuration]]
[[Core_Admin_Login]]
[[Access_Code]]
[[Admin_Notification_History]]
[[EULA Templates]]
[[Core_IT_Home]]
[[EULA Templates]]
[[EULA_Templates_Languages]]
[[EULA_Templates_Configuration]]


'''Data Firewall'''
[[Core_Data_Firewall]]
[[Core_Data_Firewall_Analyze]]
[[Core_Data_Firewall_Counts_By_User]]

'''Page Filters'''
[[Core_Page_Filter_Concept]]
[[Core_Page_Filter_Region]]
[[Core_Page_Filter_Country]]
[[Core_Page_Filter_State]]
[[Core_Page_Filter_Location]]
[[Core_Page_Filter_Location_Restaurant]]


'''IT Tools'''
[[Data_Importer]]
[[Data_Transporter_Groups]]
[[Data_Transporter_Job_Stores]]
[[Expression_Tester]]
[[Store_Alterations]]
[[Pentaho_Reporting]]
[[Core_Token_Mapping_Tester]]
[[Core_Deployment_Logs]]

'''Test Runner'''
[[Core_Test_Runner]]
[[Core_Test_Fixture]]

[[Core_Global_Function_Password_Fixture]]
[[Core_Conversion_Fixture]]
[[Core_Global_Function_Email_Validation_Fixture]]

'''Queue and View Permissions Management'''
[[Profiles Lookup | Profiles Management App]]
[[Core_Permissions_Valid_Profiles | Valid Profiles Lookup]]
[[Core_Permissions_Valid_Users | Valid Users Lookup]]
[[Core_Permissions_Valid_Queues | Valid Queues Lookup]]
[[Core_Permissions_Queues | Permissions - All Queues and Views]]
[[Core_Permissions | Permissions]]
[[Core_Permissions_Report_Queues | Report Permissions]]
[[Core_Permissions_Report_Eligibility | Permissions - Report Eligibility]]
[[Core_Permissions_Profiles_Matrix | Permissions - Analyze Matrix]]
[[Core_Permissions_Offline]]


'''Rules Engine'''
[[Core_Rules.Rule_Categories | Rule Categories Library]]
[[Core_Rules.Rule_Collections | Rule Collections]]
[[Core_Rules.Rule_Sets_Library | Rule Sets Management Library]]
[[Core_Rules.Rules_Library | Rules Management Library]]
[[Core_Rules.Rule_Collections_Summary | Rule Collections Summary Report]]
[[Core_Rules.Rule_Collections_Report | Rule Collections Report]]
[[Core_Rules.Rule_Actions | Published Rule Actions Report]]


'''Notifications'''
[[Core_Notifications_Reference
[[Core_Notifications_Template]]
[[Core_Notifications_Eligibility]]
[[Core_Notifications_Reference_Library]]


'''Announcements'''
[[Core_Announcement]]
[[Core_Announcement_Locations]]
[[Core_Announcement_Organizations]]
[[Core_Announcement_Users]]
[[Core_Announcement_Analyze]]


'''Admin Apps'''
[[Core_Admin_Business_Type]]
[[Core_Admin_Region]]
[[Core_Admin_Country]]
[[Core_Admin_Country_Region]]
[[Core_Admin_Country_Region_Analyze]]
[[Core_Admin_Currency]]
[[Core_Admin_State_Province]]
[[Core_Time_Zone]]
[[Core_Status]]
[[Core_Organization]]
[[Core_Organization_Parent_Queue]]
[[Core_Organization_Active_Suppliers]]
[[Core_Organization_Inactive_Suppliers]]

[[Core_Organization_Active_Franchisees]]
[[Core_Organization_Deleted_Franchisees]]

[[Core_Organization_Deleted_Suppliers]]
[[Core_Organization_Active_Distributors]]
[[Core_Organization_Inactive_Distributors]]
[[Core_Organization_Deleted_Distributors]]
[[Core_Organization_Active_Service_Providers]]
[[Core_Organization_Deleted_Service_Providers]]
[[Core_Organization_Location]]
[[Core_Organization_Location_Active_Suppliers]]
[[Core_Organization_Location_Deleted_Suppliers]]
[[Core_Organization_Location_Active_Distributors]]
[[Core_Organization_Location_Deleted_Distributors]]
[[Core_Organization_Location_Active_Restaurants]]
[[Core_Organization_Location_Distinct_Organizations]]
[[Core_Account]]
[[Core_Custom_Attribute]]
[[Core_Custom_Attribute_Options]]
[[Core_Custom_Dropdown_Group]]
[[Core_Custom_Dropdown_List]]
[[Core_Custom_Dropdown_Item]]
[[Core_Contact]]
[[Core_Contact_Organizations]]
[[Core_Contact_Organizations_Via_Locations]]
[[Core_Contact_Organizations_List]]
[[Core_Contact_Locations_List]]
[[Core_Contact_Locations]]
[[Core_Contact_My_Contacts]]
[[Core_Contact_Supplier_Contacts]]
[[Core_Concept]]
[[Core_Conversion]]
[[Core_Sequential_Identifier]]
[[Core_Event_Log]]
[[Core_Token_Expression]]
[[Core_Token_Mapping]]
[[Core_Execute_Field_Map]]
[[Core_Master_Data_Console]]
[[Core_Unit]]


''' Task Management ''' 
[[Core_Project_Plan]]
[[Core_Task_Assignment]]
[[Core_Task_Assignment_Analyze]]
[[Core_Task_Assignment_Report]]
[[Core_Task_Assignment_Override]]
[[Core_Task_Assignment_All_Assignments]]
[[Core_Task_Assignment_Workflow_Assignments]]
[[Core_Task_Assignment_Eligible_Users]]
[[Core_Task_Assignment_Local_Assignments]]
[[Core_Task_Assignment_Local_Eligible_Users]]
[[Core_Task_Assignment_ProgAdmin_Assignments]]
[[Core_Task_Assignment_Assignment_Manager]]
[[Core_Action_Items]]
[[Core_Task_Assignment_Preview]]
[[Core_Task_Assignment_Preview_Admin]]
[[Core_Task_Assignment_Preview_Migration]]


'''Product Suite apps'''
[[Core_Admin_Product_Suite_Manager]]
[[Core_Admin_Product_Suite_Manager_Account_Products]]
[[Core_Admin_Product_Suite_Manager_Account_Modules]]
[[Core_Admin_Product_Suite]]
[[Core_Admin_Product_Suite_Version]]
[[Core_Admin_Module]]
[[Core_Admin_Module_Apps]]
[[Core_Admin_Module_Per_User]]
[[Core_Admin_Modules_With_Active_Roles]]
[[Core_Admin_Capability]]
[[Core_Admin_Capability_By_Account]]
[[Core_Admin_Capability_Offline]]


''' Workflow Apps '''
[[Core_Workflow_Role]]
[[Core_Workflow_Role_Default_Approver]]
[[Core_Workflow_Role_Default_Approver_Populate]]
[[Core_Workflow_Role_Default_Approver_Analyze]]
[[Core_Workflow_Assignment]]
[[Core_Workflow_Role_Management]]
[[Core_Meta_Data]]


''' Documents & Resources '''
[[Core_Document]]
[[Core_Document_Entity]]
[[Core_Document_Type]]
[[Core_Document_Type_Permissioned]]
[[Core_Document_Legal_Documents]]
[[Core_Document_Facility_Documents]]


''' Entity Management '''
[[Core_Entity_Management]]
[[Core_Entity_Management_Organization]]
[[Core_Entity_Management_Organization_Location]]
[[Core_Entity_Management_User_Contact]]
[[Core_Entity_Management_Contact_Changes]]
[[Core_Entity_Management_Contact_Firewall_Permissions_Analyze]]
[[Core_Entity_Management_Request_Type]]
[[Core_Entity_Management_Request_Type_Permissioned]]

'''Auditing Apps'''
[[Core_Audit]]
[[Core_Audit_Service]]
[[Core_Audit_Service_Version]]
[[Core_Audit_Round]]

[[Core_Audit_Findings]]
[[Core_Audit_All_Findings]]
[[Core_Audit_All_Photos]]
[[Core_Audit_Published_Departments]]
[[Core_Audit_Findings_Detail]]
[[Core_Audit_Findings_Detail_Published_Q]]
[[Core_Audit_Findings_Detail_Goal]]
[[Core_Audit_Findings_Service
[[Core_Audit_Findings_Round
[[Core_Audit_Findings_Service_Detail
[[Core_Audit_Findings_Round_Detail

[[Core_Audit_CAPs]]
[[Core_Audit_CAPs_Analyze]]
[[Core_Audit_CAPs_Goal]]

[[Core_Audit_Report_Templates]]

[[Core_Audit_Question_Type]]
[[Core_Audit_Question]]
[[Core_Audit_Form]]
[[Core_Audit_Form_Version]]

'''Core Audit Dashboard Page'''
[[Core_Audit_Dashboard_Page]]

'''Core Audit Dashboard Page Filters'''
[[Core_Audit_Service_Filter]]
[[Core_Audit_Round_Filter]]
[[Core_Audit_AuditType_Filter]]
[[Core_Audit_Periods_Filter]]

'''Core Audit Dashboards'''
[[Core_Audit_Department_Names]]
[[Core_Audit_Findings_Dashboard_By_Categories]]
[[Core_Audit_CAP_Completion_Dashboard_By_Round]]
[[Core_Audit_Findings_Dashboard_Department]]
[[Core_Audit_CAPs_Dashboard_Analyze]]
[[Core_Audit_Department_Scores_Analyze]]
[[Core_Audit_Scores_Dashboard]]
[[Core_Audit_Ratings_Dashboard]]
[[Core_Audit_Findings_Dashboard_Types_By_Round]]
[[Core_Audit_Findings_Dashboard_Types]]

'''Platform Form Builder Integration'''
[[Core_Audit_Question_Templates]]
[[Core_Audit_Question_Library]]
[[Core_Audit_Activity_Library]]
[[Core_Audit_Workbook_Versions]]

'''Risk Program'''
[[Core_Period]]

'''Core Extension Wiki Placeholders'''
[[Extensions_Core_Audit_Audit_Client_Information]]
[[Extensions_Core_Audit_After_Executes]]
[[Extensions_Core_Audit_Question_Library]]

================================================================
================================================================
'''PRODUCT v3.2 - SMX'''
================================================================
================================================================

'''Supply Chain Apps'''
[[SCQM_Shared_Executes]]
[[SCQM_Master_Data_Translation_Tokens]]


'''Supply Chain Admin'''
[[SCQM_Admin_Product_Hierarchy_Level]]
[[SCQM_Admin_Product_Categories]]

'''Supply Chain Shared Apps'''
[[SCQM_Test_Method]]
[[SCQM_Product_Standard]]
[[SCQM_Product_Standard_Test_Method]]
[[SCQM_Test_Method_Analyze]]
[[SCQM_Product_Standard_Analyze]]
[[SCQM_Product_Standard_Test_Method_Analyze]]
[[SCQM_Product_Standard_Categories_Analyze]]

'''Approval Request'''
[[SCQM_Approval_Request_Workflow]]
[[SCQM_Approval_Request_Executes]]

[[SCQM_Admin_Approval_Request_Type]]
[[SCQM_Admin_Approval_Request_Config]]

[[SCQM_Approval_Request_Form]]
[[SCQM_Approval_Request]]
[[SCQM_Approval_Request_Combined_Queues]]
[[SCQM_Approval_Request_Organization]]
[[SCQM_Approval_Request_Organization_Location]]
[[SCQM_Approval_Request_PageFilter]]

'''Approval Request TEMPLATES'''
[[SCQM_Approval_Request_Initial_Workbook]]
[[SCQM_Approval_Request_Main_Workbook]]
[[SCQM_Approval_Request_Organization_Workbook_Stage2]]
[[SCQM_Approval_Request_Organization_Workbook_Stage3]]
[[SCQM_Approval_Request_Organization_Workflow_Stage3]]
[[SCQM_Approval_Request_Organization_Queue_Stage3]]
[[SCQM_Approval_Request_Organization_Location_Workbook_Stage2]]
[[SCQM_Approval_Request_Organization_Location_Workbook_Stage3]]
[[SCQM_Approval_Request_Organization_Location_Workflow_Stage3]]


'''Specification'''
[[SCQM_Specification]]
[[SCQM_Specification_Loc_Org_Q]]
[[SCQM_Specification_Workflow]]
[[SCQM_Specification_Configuration]]
[[SCQM_Specification_Numbering]]
[[SCQM_Specification_Variation_Request]]
[[SCQM_SpecVar_Request_Workflow]]

[[SCQM_Specification_Filter_Product_Category_1]]
[[SCQM_Specification_Filter_Product_Category_2]]
[[SCQM_Specification_Filter_Concept]]

[[SCQM_Specification_Ingredient_Report]]

'''Specification TEMPLATES'''
[[SCQM_Specification_Workbook]]


'''Approved Product List'''
[[SCQM_Approved_Product_List_Configuration]]
[[SCQM_Approved_Product_List_Subprocess_Configuration]]
[[SCQM_Approved_Product_List_Tester]]
[[SCQM_Approved_Product_List_Group_PageFilter]]

[[SCQM_Approved_Product_List_Group]]
[[SCQM_Approved_Product_List]]
[[SCQM_Approved_Product_List_Subprocess_Pairing]]
[[SCQM_Approved_Product_List_Subprocesses]]
[[SCQM_Approved_Product_List_Workflow]]
[[SCQM_Approved_Product_List_Group_Workflow]]
[[SCQM_Approved_Product_List_Countries_of_Export_Queue]]
[[SCQM_Approved_Product_List_Historical_Importer]]

'''Approved Product List TEMPLATES'''
[[SCQM_APL_Template_Executes]]
[[SCQM_APL_Facility_Audit_Workbook]]
[[SCQM_APL_Facility_Audit_Workflow]]
[[SCQM_APL_Nutrition_Workbook]]
[[SCQM_APL_Nutrition_Workflow]]
[[SCQM_APL_Nutrition_Queues]]
[[SCQM_APL_Label_Graphics_Workbook]]
[[SCQM_APL_Label_Graphics_Workflow]]
[[SCQM_APL_Label_Graphics_Queues]]
[[SCQM_APL_Lab_Results_Workbook]]
[[SCQM_APL_Lab_Results_Workflow]]
[[SCQM_APL_Lab_Results_Queues]]

'''Analyze Queues and Filters (AR + APL + Spec + Facility)'''
[[SCQM_APL_Filter_Region]]
[[SCQM_APL_Filter_Country]]
[[SCQM_APL_Filter_Category1]]
[[SCQM_APL_Filter_Category2]]
[[SCQM_APL_Filter_Category3]]
[[SCQM_APL_Filter_Category4]]
[[SCQM_APL_Filter_Category5]]

[[SCQM_APL_Specification]]
[[SCQM_APL_Specification_Countries]]
[[SCQM_APL_Specification_Regions]]
[[SCQM_APL_Specification_APLs]]
[[SCQM_APL_Specification_Queues]]

[[SCQM_APL_OrgLoc]]
[[SCQM_APL_OrgLoc_Countries]]
[[SCQM_APL_OrgLoc_Regions]]
[[SCQM_APL_OrgLoc_APLs]]
[[SCQM_APL_OrgLoc_Concepts]]
[[SCQM_APL_OrgLoc_Franchisees]]

[[SCQM_APL_OrgLoc_Category1]]
[[SCQM_APL_OrgLoc_Category2]]
[[SCQM_APL_OrgLoc_Category3]]
[[SCQM_APL_OrgLoc_Category4]]
[[SCQM_APL_OrgLoc_Category5]]
[[SCQM_APL_OrgLoc_Queues]]

[[SCQM_AR_APL]]
[[SCQM_SpecVar_APL]]
[[SCQM_SpecVar_Orgs_Analyze]]
[[SCQM_SpecVar_OrgLocs_Analyze]]

[[SCQM_APLGroup_Active_Countries]]
[[SCQM_APLGroup_Active_Regions]]

[[SCQM_Specification_APL]]
[[SCQM_Specification_APL_Historical]]
[[SCQM_OrgLoc_APL]]

[[SCQM_APL_Group_Specifications]]
[[SCQM_APL_Group_Organization_Locations]]

[[SCQM_Franchisee_Concept_Association]]

================================================================
================================================================
'''PRODUCT v3.2 - OMX'''
================================================================
================================================================

[[OMX_Account_Measurement_Config]]
[[OMX_Override_Measurement_Config]]
[[OMX_Override_Configuration]]
[[OMX_Restaurant_Concept_Association]]
[[OMX_Restaurant_Shift_Config]]
[[OMX_Measurement_Log]]
[[OMX_Measurement_Log_Creator]]
[[OMX_Measurement_Log_Dashboard_Analyze]]

[[OMX_Management_Log]]
[[OMX_Management_Log_Config]]
[[OMX_Management_Log_Checklist_Questions]]
[[OMX_Management_Log_Dashboard_Analyze_Log_Completion]]
[[OMX_Management_Log_Dashboard_Analyze_Activity_Completion]]
[[OMX_Management_Log_Dashboard_Analyze_Walkthrough_Compliance]]
[[OMX_Management_Log_Dashboard_Analyze_Walkthrough_LineItems]]
[[OMX_Management_Log_Dashboard_Analyze_Checklist_LineItems]]
[[OMX_Management_Log_Default_Workbook]]
[[OMX_Management_Log_AM_Checklist_Template_Workbook]]
[[OMX_Management_Log_PM_Checklist_Template_Workbook]]
[[OMX_Management_Log_Walkthrough_Template_Workbook]]

[[OMX_Dashboard]]


================================================================
================================================================
'''YUM Specific'''
================================================================
================================================================


[[YUM_Global_Functions]]

[[YUM_Specification_Workbook]]
[[YUM_Specification_Collaboration]]
[[YUM_Specification_Collaboration_Workbook]]

[[YUM_Approval_Request_Organization_Workbook_Stage2]]
[[YUM_Approval_Request_Organization_Location_Workbook_Stage2]]
[[YUM_Approval_Requests_By_OrgLoc]]

[[YUM_Risk_Rating]]
[[YUM_Risk_Rating_Default_Ratings]]
[[YUM_Risk_Rating_Overwriting_Rules]]
[[YUM_Risk_Rating_Location_Rating]]

[[YUM_APL_OrgLoc_Risk_Rating]]
[[YUM_Specification_Risk_Rating]]

[[YUM_APL_Sourcing_Queues]]
[[YUM_APL_Sourcing_Workbook]]
[[YUM_APL_Sourcing_Workflow]]
[[YUM_Facility_Sourcing_Suppliers]]
[[YUM_Facility_Sourcing_All]]
[[YUM_Facility_Distinct_Sourcing_Suppliers]]

[[YUM_Owner_Of_Compliance]]

[[YUM_POC_User_Swapping]]

[[YUM_Discussions]]

[[YUM_Second_Suppliers]]

[[YUM_Second_Suppliers]]

''''Cuttings & Scheduling''''

[[YUM_KPI_Holding_App]]

[[KPI_Cuttings]]
[[Published_KPIs]]
[[Test_Published_KPIs]]

[[Sensory_Published_KPIs]]
[[KPI_Report_Tester]]
[[KPI_Reports]]
[[KPI_Reports_Org_Loc]]

[[Org_Loc_Bulk_Select]]

[[YUM_KPI_Holding_App]]

[[YUM_Specification_Workbook_Builder]]
[[YUM_Specification_Workbook_Builders_To_Snippets]]
[[YUM_Specification_Snippet_Builder]]
[[YUM_Specification_Snippets_To_Snippets]]

[[Product_Evaluation_Form]]

****BV Certifications***

[[Certification_Management]]
[[Processes]]
[[Clauses]]
[[Audit_Planning]]

[[Certification_Form]]

[[Certification_Languages]]

[[All_Process_Time_Table]]

[[CER_Process]]
[[CER_Process_Workflow]]
[[CER_Process_Service]]
[[CER_Process_Service_Item]]


Yum! Brands Reports
[[Reports.Report Center | Report Center]]

[[Reports.Supplier Filter | Supplier Filter]]
[[Reports.Sigma Tracking.Periods | Periods Filter]]
[[Reports.Sigma Tracking.Specifications | Specifications Filter]]
[[Reports.Sigma Tracking.Standards | Standards Filter]]

[[Reports.Audit Compliance | Audit Compliance Report (Findings)]]
[[Reports.FTF Compliance | FTF Compliance Report]]
[[Reports.Audit Performance | Audit Performance Report]]
[[Reports.Cutting Performance | Cutting Performance Report]]

[[Reports.Sigma Tracking.Alpha | Sigma Tracking Report 1]]
[[Reports.Sigma Tracking.Beta | Sigma Tracking Report 2]]
[[Reports.Sigma Tracking.Gamma | Sigma Tracking Report 3]]
[[Reports.Sigma Tracking.Delta | Sigma Tracking Report 4]]

[[Imported_Raw_Spec_Data]]
[[Imported_Raw_Spec_FPS_Data]]
[[Imported_Raw_Spec_Concept_Data]]
[[Imported_Raw_Spec_Countries_Data]]
[[Imported_Create_Specs]]
[[Imported_Add_FPS_to_Specs]]
[[Imported_Test_Method_Data]]
[[Imported_KPI_Data]]
[[Imported_Cuttings_Data]]

[[Findings_Type_Filter]]
[[Findings_Year_Filter]]
[[Findings_Classification_Filter]]
[[Findings_Map]]

[[Core_Audit_Workbook_Versions.fa46bdb1964a44608b7909cfc631c533]]
[[WB.f039845632034a4aa2b5c9f63eeabade|Yum - Specification Template INGREDIENT]]
[[WB.e45af41ce3cc4a5dade909df78ae41cb|Yum - Specification Template PACKAGING]]
[[WB.f772bc44a8bc4e4ea29c5aebef5e55c8|Yum - Specification Template PREMIUMS]]
[[WB.b07b9f0762024090a2399a05cdd71e90|Yum - Specification Template PRODUCT]]
[[Core_Audit_Workbook_Versions.fa46bdb1964a44608b7909cfc631c533|1]]
[[WB.f039845632034a4aa2b5c9f63eeabade.V1|Yum - Specification Template INGREDIENT]]
[[Core_Audit_Workbook_Versions.abae46d223684d319eb0e6c3bca9d123|v1.0]]


[[WB.853f8e050959470cbebcae9e36cb729a.V1|Yum - Specification Template INGREDIENT CR]]

[[WB.fd22bfa8c4844bd0b0f18f32926b3a0c.V3|Yum - Specification Template INGREDIENT CR CLONE]]

[[WB.0728ce9e47c0415589179f58cfd3933c.V1|Yum - Specification Template INGREDIENT CR CLONE DEMO 2]]

[[WB.850be8ae6063493d80099ac5ec27f17a|Yum - Specification Template PRODUCT]]

[[Walkme