﻿@topic
    @topic-title " "
    @topic-title-ui-style "hide"

@monitor "Initial"
    @monitor-active-when "is-blank(Initial_Monitor)"
    @monitor-polling-interval "2 seconds"
    @monitor-action "Reload"

@monitor "Submit_Monitor"
    @monitor-active-when "Submit = 'Submit'"
    @monitor-polling-interval "2 seconds"
    @monitor-action "Reload"

@monitor "Request_Monitor"
    @monitor-active-when "is-not-blank(Request_Monitor)"
    @monitor-polling-interval "2 seconds"
    @monitor-action "Reload"

@monitor "Reload_Monitor"
    @monitor-active-when "is-not-blank(Verify_Status)"
    @monitor-interval "1 seconds"
    @monitor-action "Reload"

@block
    @block-visible-when "is-not-blank(Verify_Status) or is-not-blank(Submit) or is-blank(Initial_Monitor) or is-not-blank(Request_Monitor)"
    @block-ui-style "well well-mini well-info-light inverse"
        @icon "icon-info-sign info medium"
        {label(margin-lr-mini medium): '^^Monitor_message|The system is processing your request. You may wait or navigate away at any time^^.'}
@end-block

@block
    @block-visible-when "Status/Status_Code = 'Approved' and Optional_Request = 'No'"
    @block-ui-style "well well-info-light well-mini inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Nutrition_Not_Required_Message|The Nutrition & Allergens process was not required at the time of Approval for the following reason^^:'}
        @newline "Small"
        {{Reason_Not_Required}}
@end-block

@block
    @block-visible-when "Need_Reason = 'Yes' and is-blank(Reason_Not_Required)"
    @block-ui-style "well well-mini well-danger-light inverse"
    @icon "icon-warning-sign medium danger"
    {label(margin-lr-mini): '^^Reason_Nutrition_not_required|Please enter a reason the Nutrition & Allergens process is not required^^.'}
@end-block

@block
    @block-ui-style "clear span6"
    
    @block
        @block-visible-when "is-not-blank(Return_Reasons) and Status/Status_Code != 'Approved'"
        @block-ui-style "well well-danger-light well-mini inverse"
        @icon "icon-warning-sign danger medium"
        {label(medium margin-lr-mini):'^^Attention|Attention^^: ^^The worksheet was returned with the following reason|The worksheet was returned with the following reason^^:'}
        @newline "Small"
        {{Return_Reasons/item[last()]/Reason}}
    @end-block
    
    @block
        @block-visible-when "Status/Status_Code = 'Rejected'"
        @block-ui-style "well well-info-light well-mini inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^The worksheet was rejected with the following reason|The worksheet was rejected with the following reason^^:'}
        @newline "Small"
        {{Reject_Reason}}
    @end-block

    @block
        @block-visible-when "is-not-blank(Initial_Monitor) and is-blank(Request) and call('SCQM.APLNutrition.IsNutritionRequired') = 'False' and (call('SCQM.APLNutrition.IsApprover', store-id(), 'SCQM_APL_Nutrition', user-id()) or call('Core.IsITUser')) and (Status/Status_Code != 'Approved' or is-blank(Status/Status_Code))"
        @block-ui-style "form-horizontal"        
        
        @choice "Optional_Request"
            @choice-style "RadioButtonList"
            @choice-header "^^Nutrition_Required|Is a Nutrition & Allergens worksheet required at this time?^^"
            @choice-columns "2"

			@option
				@option-text "^^Yes|Yes^^"
				@option-value "Yes"
			@option
				@option-text "^^No|No^^"
				@option-value "No"
            @end-choice
            
        @block
            @block-visible-when "Optional_Request = 'Yes'"
            
            @button "Request"
                @button-value "Yes"
                @button-text "^^Request|Request^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success"
                @button-action "Save, Reload"
            
        @end-block
            
        @block
            @block-visible-when "Optional_Request = 'No' and (Status/Status_Code != 'Approved' or is-blank(Status/Status_Code))"
        
            @text "Reason_Not_Required"
                @text-header "^^Reason not required at this time|Reason not required at this time^^:"
                @text-rows "5"
                @text-columns "80"            
                @text-footer "^^Nutrition_Reason_Not_Required_Footer|This will conclude the Nutrition & Allergens portion of the process.^^"
                @text-read-only-when "Status/Status_Code = 'Approved'"                    
                
            @button "Request"
                @button-value "No"
                @button-text "^^Approve|Approve^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success"
                @button-action "Save, Reload"

        @end-block

        @newline "Small"

    @end-block    
        
    @block
        @block-visible-when "not(call('SCQM.APLNutrition.IsApprover', store-id(),'SCQM_APL_Local_User',user-id())) and is-not-blank(Conditional_Approval_Saved) and is-not-blank(Conditional_Approval) and is-not-blank(Conditional_Approval_Reason)"
        @block-ui-style "well well-mini well-info-light inverse"
        
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Nutritional_Worksheet_Not_Required|The worksheet is not required for Conditional Approval at this time for the following reason^^:'}
        @newline "Small"
        {{Conditional_Approval_Reason}}
        
        @block
            @block-visible-when "(boolean(user-id() = call('SCQM.GetReviewerUser', store-id(), 'SCQM_APL_Nutrition')) or call('Core.IsITUser')) and call('SCQM.APLGroup.ExistingAPLGroupStatus', /store/SCQM_Approved_Product_List_Subprocesses/APL_Group/@refId)[.='APL_Draft' or .='APL_In_Process']"
            @block-ui-style "pull-right"    
            
             @button "Conditional_Approval_Saved"
                @button-value "Removed"
                @button-text "^^Remove Conditional Approval|Remove Conditional Approval^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success pull-right"
                @button-action "Save"
                
        @end-block        
        
    @end-block  
        
    @comment
    === Need to only allow Conditional Approval Checkbox when the APL GROUP is in a status of Draft or In Progress
    @end-comment    
    @block
        @block-visible-when "(is-not-blank(Initial_Monitor) and (is-blank(Status) or Status/Status_Code != 'Approved') and is-blank(Conditional_Approval_Saved) and call('SCQM.APLGroup.ExistingAPLGroupStatus', /store/SCQM_Approved_Product_List_Subprocesses/APL_Group/@refId)[.='APL_Draft' or .='APL_In_Process']) and (boolean(user-id() = call('SCQM.GetReviewerUser', store-id(), 'SCQM_APL_Nutrition')) or call('Core.IsITUser'))"
        @block-ui-style "clear form-horizontal"
        
        @checkbox "Conditional_Approval"
            @checkbox-text "^^Nutritional_Conditional_Approval_Checkbox|Allow Conditional Approval without completing the worksheet at this time?^^"

        @block
            @block-visible-when "is-not-blank(Conditional_Approval)"

            @text "Conditional_Approval_Reason"
                @text-header "^^Reason|Reason^^:"
                @text-rows "5"

            @button "Conditional_Approval_Saved"
                @button-value "Save"
                @button-text "^^Conditionally Approve|Conditionally Approve^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success"
                @button-action "Save"

        @end-block

    @end-block

@end-block

@block
    @block-ui-style "span6"
    @block-visible-when "(call('SCQM.APLNutrition.IsApprover', store-id(),'SCQM_APL_Nutrition',user-id()) or call('Core.IsITUser'))"    
    
    @block
        @block-visible-when "Status/Status_Code = 'Submitted'"  
        @block-ui-style "clear"
        
        @block
            @block-visible-when "is-blank(Return_Status) and is-blank(Reject_Status) and is-blank(Verify_Status)"
        
            @button "Reject_Status"
                @button-value "Rejected"
                @button-text "^^Reject|Reject^^"
                @button-optional "Yes"
                @button-ui-style "btn-danger margin-lr-mini pull-right"
                @button-inline "Yes"
                @button-action "Save,Validate,Reload"            

            @button "Return_Status"
                @button-value "Returned"
                @button-text "^^Return|Return^^"
                @button-optional "Yes"
                @button-ui-style "btn-danger margin-lr-mini pull-right"
                @button-inline "Yes"
                @button-action "Save,Validate,Reload"
            
            @button "Verify_Status"
                @button-value "Approved"
                @button-text "^^Approve|Approve^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-success margin-lr-mini pull-right"
                @button-action "Save,Validate,Reload"

        @end-block

        @block
            @block-visible-when "is-not-blank(Reject_Status) or is-not-blank(Return_Status)"
            
            @link "False_Button"
                @link-text "^^Reject|Reject^^"
                @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"
                
            @link "False_Button"
                @link-text "^^Return|Return^^"
                @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"
                
            @link "False_Button"
                @link-text "^^Approve|Approve^^"
                @link-ui-style "btn btn-success margin-lr-mini pull-right disabled white"
            
        @end-block
        
        @block "Return"
            @block-visible-when "(is-not-blank(Return_Status) and is-blank(Verify_Status)) "
            @block-ui-style "form-horizontal pull-right clear"            
                
            @text "Return_Reason"
                @text-header "^^Reason for Return|Reason for Return^^:"
                @text-columns "40"
                @text-rows "5"
                @text-validation-test "(is-not-blank(Return_Reason)) or Verify_Status = 'Cancel' or is-blank(Verify_Staus)"
                @text-validation-error "^^A return reason is required|A return reason is required^^"
                
            @newline
             
            @button "Verify_Status"
                @button-value "Cancel"
                @button-text "^^Cancel|Cancel^^"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-warning margin-lr-mini pull-right"
                @button-action "Save,Validate,Refresh"
                
            @block
                @block-visible-when "Status/Status_Code = 'Submitted' and is-not-blank(Return_Status)"
                
                @button "Return_Status"
                    @button-value "Returned"
                    @button-text "^^Return|Return^^"
                    @button-optional "Yes"
                    @button-inline "Yes"
                    @button-ui-style "btn-danger margin-lr-mini pull-right"
                    @button-action "Save,Validate,Reload"
                        
            @end-block
                
            @newline "Small" 
            {label(small pull-right):'^^This will return the process to the submitter|This will return the process to the submitter^^'}

        @end-block

        @newline "Small"

        @block
            @block-visible-when "is-blank(Return_Reason) and is-not-blank(Return_Status) and (is-blank(Verify_Status) or Verify_Status != 'Cancel')"
            @block-ui-style "well well-mini well-danger-light inverse span6 pull-right"
                @icon "icon-warning-sign danger"
                {label(margin-lr-mini): '^^A return reason is required|A return reason is required^^'}
        @end-block

        @block "Reject"
            @block-visible-when "(is-not-blank(Reject_Status)  and is-blank(Verify_Status)) "
            @block-ui-style "form-horizontal pull-right clear"
                
                @text "Reject_Reason"
                    @text-header "^^Reason for Rejection|Reason for Rejection^^:"
                    @text-columns "40"
                    @text-rows "5"
                    @text-validation-test "(is-not-blank(Reject_Reason)) or Verify_Status = 'Cancel' or is-blank(Verify_Staus)"
                    @text-validation-error "^^A rejection reason is required|A rejection reason is required^^"
                    
                @newline
                 
                @button "Verify_Status"
                    @button-value "Cancel"
                    @button-text "^^Cancel|Cancel^^"
                    @button-optional "Yes"
                    @button-inline "Yes"
                    @button-ui-style "btn-warning margin-lr-mini pull-right"
                    @button-action "Save,Validate,Refresh"
                
                @block
                    @block-visible-when "Status/Status_Code = 'Submitted' and is-not-blank(Reject_Status)"

                        @button "Reject_Status"
                            @button-value "Rejected"
                            @button-text "^^Reject|Reject^^"
                            @button-optional "Yes"
                            @button-inline "Yes"
                            @button-ui-style "btn-danger margin-lr-mini pull-right"
                            @button-action "Save,Validate,Reload"

                @end-block
                
                @newline "Small"
                {label(small pull-right):'^^This will end the process|This will end the process^^'}

        @end-block

        @newline "Small"

        @block
            @block-visible-when "is-blank(Reject_Reason) and is-not-blank(Reject_Status) and (is-blank(Verify_Status) or Verify_Status != 'Cancel')"
            @block-ui-style "well well-mini well-danger-light inverse span6 pull-right"
                @icon "icon-warning-sign danger"
                {label(margin-lr-mini): '^^A rejection reason is required|A rejection reason is required^^'}
        @end-block

    @end-block
    
@end-block

@block
    @block-visible-when "is-not-blank(Initial_Monitor) and (call('SCQM.APLNutrition.IsApprover', store-id(),'SCQM_APL_Local_User',user-id()) or call('Core.IsITUser')) and (Status/Status_Code = 'Requested' or Status/Status_Code = 'Returned') and is-blank(Submit)"
    @block-ui-style "clear block"

    @button "Save"
        @button-value "Save"
        @button-text "^^Save|Save^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "pull-right margin-lr-mini"
        @button-action "Save"
    
    @button "Guide_Me"
        @button-text "^^Guide Me|Guide Me^^"
        @button-value "Guide Me"
        @button-ui-style "btn-info pull-right margin-lr-mini"
        @button-align "Left"
        @button-optional "Yes"
        @button-action "guideMe"

    @block
        @block-visible-when "call('Core.IsITUser') or contains(lower-case(domain()), 'smx2')"

        @button "IT_Complete"
            @button-value "IT_Complete"
            @button-text "^^Demo Complete|Demo Complete^^"
            @button-inline "Yes" 
            @button-ui-style "btn-info-light pull-right margin-lr-mini"
            @button-align "Left"   
            @button-optional "Yes"

    @end-block
    
    @block
        @block-visible-when "(/store/SCQM_Approved_Product_List_Subprocesses/Subprocess_Workbook/tracking/topic[@id='SCQM_APL_Nutrition_Workbook.Nutritional_Information']/@completed = 'true' and /store/SCQM_Approved_Product_List_Subprocesses/Subprocess_Workbook/tracking/topic[@id='SCQM_APL_Nutrition_Workbook.Allergen_Information']/@completed = 'true') or contains(call('Core.GetClientURL'), 'dev') = 'True'"

        @button "Submit"
            @button-value "Submit"
            @button-text "^^Submit|Submit^^"
            @button-inline "Yes" 
            @button-ui-style "btn-success pull-right margin-lr-mini"
            @button-align "Left"   
            @button-optional "Yes"

    @end-block

@end-block