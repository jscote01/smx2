﻿@topic
@topic-title "Employees are aware of CCP‘s and the critical limits in their area and take appropriate action."
@topic-number "622"

@block "_Controls_Block"
@block-container "Controls"
@block-container-options "Options[Style:width:300px; line-height:50px;]"
@block-ui-style "form-horizontal pos-rel"

	@block "StyleBlock1"
	@block-ui-style "form-inline"

		@link "_622_Tooltip"
		@link-icon "icon-list-ul"
		@link-href "Yes"
		@link-target "Inline"
		@link-ui-style "btn btn-medium btn-inverse margin-lr-sm margin-tb-sm"
		@link-text-ui-style "hide"
		@link-toggle-ui-style "hide"

		@block "_622_Tooltip"
		@block-ui-style "clear well well-small bcolor-white padding-lr-mini form-horizontal hide medium bold pos-abs zindex-modal align-left"

Employees are aware of CCP‘s and the critical limits in their area and take appropriate action.

		@end-block

		@block "ActiveControlsBlock"
		@block-ui-style "control-group pull-right"

			@choice "_622_Choice"
			@choice-style "RadioButtonList"
			@choice-columns "5"
			@choice-direction "Horizontal"
			@choice-postback "Yes"
			@choice-ui-style "btn"

				@option "PT1"
				@option-value "10"
				@option-text "10"
				@option-ui-style-checked "btn-medium white bcolor-success"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT2"
				@option-value "7"
				@option-text "7"
				@option-ui-style-checked "btn-medium white bcolor-warning"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT3"
				@option-value "3"
				@option-text "3"
				@option-ui-style-checked "btn-medium white bcolor-danger-light"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "PT4"
				@option-value "0"
				@option-text "0"
				@option-ui-style-checked "btn-medium white bcolor-danger"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

				@option "NA"
				@option-value "N/A"
				@option-text "N/A"
				@option-ui-style-checked "btn-medium white bcolor-inverse"
				@option-ui-style-unchecked "btn-medium white bg-gray3"

			@end-choice

		@end-block

	@end-block

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_622_Choice) and _622_Choice != 'N/A' and _622_Choice != '10'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_622_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_622_Choice) or _622_Choice [. = 'N/A' or . = '10']"
				@text-expression "case(_622_Choice = '0', 'Employees were not aware of CCP‘s and the critical limits in their area and did not take appropriate action.','Employees are aware of CCP‘s and the critical limits in their area and take appropriate action.')"
				@text-expression-when "is-changed(_622_Choice) and not(is-changed(_622_Comments)) and _622_Choice != 'N/A'"
				@text-ui-style "block"

				@block
				@block-visible-when "is-not-blank(_622_Choice) and (_622_Choice = '0')"

					@text "_622_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block
				@block-visible-when "is-not-blank(_622_Choice) and not(_622_Choice = 'N/A' or _622_Choice = '10') and not(_622_Choice = '0')"

					@text "_622_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_622_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "622"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "10"
	@finding-select "_622_Comments"
	@finding-score-when "is-not-blank(_622_Choice) and _622_Choice != 'N/A'"
	@finding-meta-Question_Number "622"

		@case
		@case-type "Positive"
		@case-points "_622_Choice"
		@case-test "not(_622_Choice = '0') and _622_Choice != 'N/A'"

		@case
		@case-type "Secondary"
		@case-points "_622_Choice"
		@case-test "_622_Choice = '0' and _622_Choice != 'N/A'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_622_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "_622_Choice"
		@text-expression-when "is-changed(_622_Choice)"

	@end-block

@end-block
