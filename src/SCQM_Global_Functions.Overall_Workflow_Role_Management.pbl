﻿@comment
==============================================================================
FUNCTIONS TO BE USED WITH THE WORKFLOW ROLE MANAGEMENT MODULE TO DETERMINE REVIEWERS
==============================================================================
@end-comment

@comment
Get the user assigned as the reviewer for a given role and WRM-enabled store
Attempts to get an active task assignment first - if none is available, looks at who WOULD be assigned (either the primary or the default)
  #param StoreId $StoreId Store Id of the WRM-enabled store
  #param String $RoleId Id of the Role
  #returns StoreId store-id of the User assigned to the role
@end-comment
@function "SCQM.GetReviewerUser"
@parameter "$StoreId"
@parameter "$RoleId"
if(
    is-not-blank(
        call('Core.GetTaskAssignment', 
            $StoreId, 
            call('Core.GetRoleIdByID', $RoleId)
        )/Assignee
    ),
    call('Core.GetTaskAssignment', 
        $StoreId, 
        call('Core.GetRoleIdByID', $RoleId)
    )/Assignee,
    if(
        is-not-blank(
            call('Core.GetPrimaryOrLocalUserForRole', 
                call('Core.GetRoleIdByID', $RoleId), 
                store($StoreId)/*[1]/WRM_Meta_Data_1, 
                store($StoreId)/*[1]/WRM_Meta_Data_2, 
                store($StoreId)/*[1]/WRM_Meta_Data_3, 
                store($StoreId)/*[1]/WRM_Meta_Data_4, 
                store($StoreId)/*[1]/WRM_Meta_Data_5,
                store($StoreId)/*[1]/Core_Organization/@refId,
                ''
            )        
        ),
        call('Core.GetPrimaryOrLocalUserForRole', 
            call('Core.GetRoleIdByID', $RoleId), 
            store($StoreId)/*[1]/WRM_Meta_Data_1, 
            store($StoreId)/*[1]/WRM_Meta_Data_2, 
            store($StoreId)/*[1]/WRM_Meta_Data_3, 
            store($StoreId)/*[1]/WRM_Meta_Data_4, 
            store($StoreId)/*[1]/WRM_Meta_Data_5,
            store($StoreId)/*[1]/Core_Organization/@refId,
            ''
        ),
        call('Core.GetDefaultApproverByRoleID', $RoleId)
    )
)
@end-function

@function "SCQM.GetReviewerUserName"
@parameter "$StoreId"
@parameter "$RoleId"
store(call('SCQM.GetReviewerUser', $StoreId, $RoleId), '/store/*[1]/User_Name')
@end-function

@comment
Get the user name of the reviewer for a given role and approval request
  #param StoreId $ApprovalRequestId Store Id of the Approval Request
  #param String $RoleId Id of the Role
  #returns String Username
@end-comment
@function "SCQM.AR.GetReviewerUserName"
@parameter "$ApprovalRequestId"
@parameter "$RoleId"
call('SCQM.GetReviewerUserName',$ApprovalRequestId, $RoleId)
@end-function

@function "SCQM.IsUserEligibleForRole"
@comment
Wrapper for Core function - check whether the current user is eligible for the Role identified by ID for the given SCQM store
  #param StoreId $StoreId Store Id of the WRM-enabled store
  #param String $RoleId Id of the Role
  #returns Boolean True if the user is eligible for the given role on the given store
@end-comment
@parameter "$StoreId"
@parameter "$RoleId"
call(
    'Core.IsUserEligibleForRole',
    user(),
    call('Core.GetRoleIdByID', $RoleId),
    store($StoreId)/*[1]/WRM_Meta_Data_1, 
    store($StoreId)/*[1]/WRM_Meta_Data_2, 
    store($StoreId)/*[1]/WRM_Meta_Data_3, 
    store($StoreId)/*[1]/WRM_Meta_Data_4, 
    store($StoreId)/*[1]/WRM_Meta_Data_5
)
@end-function