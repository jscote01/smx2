﻿@title " "
    @title-ui-style "hide"
@mode "Main"

@block "Quick_Upload_Popup"
    @block-visible-when "is-blank(Quick_Upload_StoreId) and (Status/Status_Code = 'DM_Pending_Expiration' or Status/Status_Code = 'DM_Expired') and not(call('CoreDoc.HasCurrentVersion', 'Pending')) and not(call('CoreDoc.HasCurrentVersion', 'Active')) and call('CoreDoc.IsSubmitter')"
    @block-ui-style "span6 well well-mini well-danger-light inverse"

    @icon "icon-warning-sign danger medium"

    @block "Acknowledge_Type"
        @block-visible-when "Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%acknowledge%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%'"
        
        @block
            @block-visible-when "call('CoreDoc.TypeHasTemplate', Document_Type/Name)"
            
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Pending_Expiration'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Pending_Expiration_Message|The current document is pending expiration^^. ^^Quick_Upload_Instructions_Acknowledge_Template|Please download and read the linked version and select the "Acknowledge" button to continue^^.'}
            @end-block
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Expired'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Expired_Message|The current document has now expired^^. ^^Quick_Upload_Instructions_Acknowledge_Template|Please download and read the linked version and select the "Acknowledge" button to continue^^.'}
            @end-block

            @newline "Small"
            
            @link "{{call('CoreDoc.GetActiveTemplateLink', Document_Type/Name)}}"
                @link-href "Yes"
                @link-text-expression "Name"
                @link-align "Left"
                @link-ui-style "btn"
                @link-icon "icon-download-alt"
        @end-block
        
        @block
            @block-visible-when "not(call('CoreDoc.TypeHasTemplate', Document_Type/Name))"
            
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Pending_Expiration'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Pending_Expiration_Message|The current document is pending expiration^^. ^^Quick_Upload_Instructions_Acknowledge|Please upload the new document and select the "Acknowledge" button to continue^^.'}
            @end-block
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Expired'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Expired_Message|The current document has now expired^^. ^^Quick_Upload_Instructions_Acknowledge|Please upload the new document and select the "Acknowledge" button to continue^^.'}
            @end-block

            @file "Quick_Upload_File"
                @file-header "^^New_File_Version|New Version^^:"
                @file-optional "Yes"
        @end-block
        
        @newline "Small"
        {label(default):'^^Quick_Upload_Acknowledgment|Read the new document then check the box below to indicate your acknowledgment^^.'}        
        @newline "Small"
        
        @choice "Quick_Upload_Acknowledged"
            @choice-style "CheckBoxList"
            @choice-header "^^Required|Required^^:"
            @choice-header-ui-style "hide"
            
            @option "Acknowledge"
                @option-text "{{coalesce(Document_Type/Acknowledge_Message, '^^Default_Acknowledge_Message|Check here to acknowledge this document.^^')}}"
                @option-value "true"
        @end-choice
    @end-block
    
    @block "Approve_Type"
        @block-visible-when "Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_APPROVE%approve%APPROVAL_METHOD_TYPE_APPROVE%'"
        
        @block
            @block-visible-when "call('CoreDoc.TypeHasTemplate', Document_Type/Name)"
            
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Pending_Expiration'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Pending_Expiration_Message|The current document is pending expiration^^. ^^Quick_Upload_Instructions_Approval_Template|Please download and complete the new version, upload the new copy here, and select the "Submit" button to submit for approval^^.'}
            @end-block
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Expired'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Expired_Message|The current document has now expired^^. ^^Quick_Upload_Instructions_Approval_Template|Please download and complete the new version, upload the new copy here, and select the "Submit" button to submit for approval^^.'}
            @end-block
            
            @newline "Small"
            
            @link "{{call('CoreDoc.GetActiveTemplateLink', Document_Type/Name)}}"
                @link-href "Yes"
                @link-text-expression "Name"
                @link-align "Left"
                @link-ui-style "btn"
                @link-icon "icon-download-alt"
        @end-block
        
        @block
            @block-visible-when "not(call('CoreDoc.TypeHasTemplate', Document_Type/Name))"
            
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Pending_Expiration'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Pending_Expiration_Message|The current document is pending expiration^^. ^^Quick_Upload_Instructions_Approval|Please upload a new document and select the "Submit" button to submit for approval^^.'}
            @end-block
            @block
                @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Expired'"
                {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Expired_Message|The current document has now expired^^. ^^Quick_Upload_Instructions_Approval|Please upload a new document and select the "Submit" button to submit for approval^^.'}
            @end-block
        @end-block
        
        @newline "Small"
        
        @file "Quick_Upload_File"
            @file-header "^^Upload|Upload^^:"
            @file-optional "No"
    @end-block
    
    @block "Other_Types"
        @block-visible-when "is-blank(Document_Type/Approval_Method) or Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_NONE%none%APPROVAL_METHOD_TYPE_NONE%'"
            
        @block
            @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Pending_Expiration'"
            {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Pending_Expiration_Message|The current document is pending expiration^^. ^^Quick_Upload_Instructions_Other|Please upload a new document and select the "Upload" button to continue^^.'}
        @end-block
        @block
            @block-visible-when "/store/*[1]/Status/Status_Code = 'DM_Expired'"
            {label(medium margin-lr-mini): '^^Attention|Attention^^: ^^Document_Expired_Message|The current document has now expired^^. ^^Quick_Upload_Instructions_Other|Please upload a new document and select the "Upload" button to continue^^.'}
        @end-block
        
        @newline "Small"
        
        @file "Quick_Upload_File"
            @file-header "^^Upload|Upload^^:"
            @file-optional "No"
    @end-block
    
    @block "Inactive_Date"
        @block-visible-when "Document_Type/Auto_Inactive_Type = '%INACTIVE_TYPE_DATE%date%INACTIVE_TYPE_DATE%'"
        
        @text "Quick_Upload_Auto_Inactive_Date"
            @text-type "Date"
            @text-header "^^Inactive Date|Inactive Date^^:"
            @text-footer "^^Inactive_Date_Footer|Date the document will no longer be visible in the system^^"
            @text-validation-test "date-less-than(date-of(now(0)), Quick_Upload_Auto_Inactive_Date)"
            @text-validation-error "^^Invalid_Inactive_Date_Message|Inactivation Date must be in the future (UTC time).^^"
    @end-block    
    
    @block "Expire_Date"
        @block-visible-when "Document_Type/Auto_Expire_Type = '%EXPIRE_TYPE_DATE%date%EXPIRE_TYPE_DATE%'"
        
        @text "Quick_Upload_Auto_Expire_Date"
            @text-type "Date"
            @text-header "^^Expiration Date|Expiration Date^^:"
            @text-footer "^^Expire_Date_Footer|Date the document will expire and need to be replaced^^"
            @text-read-only-when "call('CoreDoc.IsFormReadOnly')"
            @text-validation-test "date-less-than(date-of(now(0)), Quick_Upload_Auto_Expire_Date)"
            @text-validation-error "^^Invalid_Expire_Date_Message|Expiration Date must be in the future (UTC time).^^"
    @end-block
    
    @button "Quick_Upload"
        @button-value "Quick_Upload"
        @button-text "{{if(Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%acknowledge%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%', '^^Acknowledge|Acknowledge^^', '^^Submit|Submit^^')}}"
        @button-inline "Yes"
        @button-optional "Yes"
        @button-ui-style "btn-success pull-right"
@end-block

@block "Quick_Upload_Success"
    @block-visible-when "is-not-blank(Quick_Upload_StoreId) and Status/Status_Code != 'DM_Archived' and is-blank(Trigger_Update_Tasks)"
    @block-ui-style "span6 well well-mini well-info-light inverse"
    
    @icon "icon-info-sign info medium"
    
    @block "QUS_Acknowledge"
        @block-visible-when "Document_Type/Approval_Method = '%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%acknowledge%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%'"
        
        {label(medium margin-lr-mini): '^^Please Note|Please Note^^: ^^Expired_Document_Quick_Upload_Success_Message_Acknowledged|The document has been successfully acknowledged. Select the "Acknowledged Document" link to view the new record^^.'}
        @newline "Small"
        
        @link "{{store-url(Quick_Upload_StoreId)}}"
            @link-text "^^Acknowledged|Acknowledged^^ ^^Document|Document^^"
            @link-ui-style "btn btn-info"
            @link-href "Yes"
    @end-block
    
    @block "QUS_General"
        @block-visible-when "Document_Type/Approval_Method != '%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%acknowledge%APPROVAL_METHOD_TYPE_ACKNOWLEDGE%'"
    
        {label(medium margin-lr-mini): '^^Please Note|Please Note^^: ^^Expired_Document_Quick_Upload_Success_Message|The document has been successfully uploaded. Select the "Uploaded Document" link to view the new record^^.'}
        @newline "Small"
        
        @link "{{store-url(Quick_Upload_StoreId)}}"
            @link-text "^^Uploaded|Uploaded^^ ^^Document|Document^^"
            @link-ui-style "btn btn-info"
            @link-href "Yes"
    @end-block
@end-block