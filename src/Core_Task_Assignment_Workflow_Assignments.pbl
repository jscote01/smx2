﻿@app "Core_Task_Assignment_Workflow_Assignments"

@language "all"

@display "No"

@language "all"

@@  This analyze queue will return a list of workflow assignments for active users (or system user).

@analyze

    @query "Assignments"
    @query-queue "Core_Workflow_Assignment.Master_Q"
    @query-filter "Status_Code = 'Active'"
    
        @column "Role"
        @column "Meta_Data_1"
        @column "Meta_Data_2"
        @column "Meta_Data_3"
        @column "Meta_Data_4"
        @column "Meta_Data_5"
        @column "Assignee"
        @column "PrimaryFlag"
            @column-source "Primary"
        @column "Type"
            @column-source "CASE WHEN Meta_Data_1 = 'NA' OR Meta_Data_2 = 'NA' THEN 'Meta Data Backup' WHEN Primary IS NOT NULL THEN 'Primary' ELSE 'Secondary' END"
        @column "Status_Code"
    
    @end-query

    @query "Default_Assignments"
    @query-queue "Core_Workflow_Role_Default_Approver.Master_Q"
    @query-filter "Status_Code = 'Active'"

        @column "Role"
            @column-source "Core_Workflow_Role"
        @column "Meta_Data_1"
            @column-source "'NA'"
        @column "Meta_Data_2"
            @column-source "'NA'"
        @column "Meta_Data_3"
            @column-source "'NA'"
        @column "Meta_Data_4"
            @column-source "'NA'"
        @column "Meta_Data_5"
            @column-source "'NA'"
        @column "Assignee"
            @column-source "Default_Approver"
        @column "PrimaryFlag"
            @column-source "''"
        @column "Type"
            @column-source "'Default'"
        @column "Status_Code"
    @end-query

    @query "All_Assignments"
    @query-type "Union"
        @subquery "Assignments"
        @subquery "Default_Assignments"
    @end-query

    @query "Users"
    @query-queue "User.Master_Q"
    @query-filter "Status IN ('Active', 'Approved') AND System_Role = 'User' AND User_Name NOT IN ('Anonymous')"    
    
        @column "UID"
            @column-source "StoreId"
        @column "Informal_Full_Name"    
    
    @end-query


    @query "Combined_User_Details"
    @query-distinct-values "Yes"
    @subquery "All_Assignments"
    @subquery "Users"
    @subquery-join "All_Assignments.Assignee = Users.UID"
    @subquery-join-type "Inner"    
    
        @column "Assignee"
            @column-source "All_Assignments.Assignee"   
        @column "User_Informal_Full_Name"
            @column-source "Users.Informal_Full_Name"  
        @column "Role"
            @column-source "All_Assignments.Role"            
        @column "Meta_Data_1"
            @column-source "All_Assignments.Meta_Data_1"            
        @column "Meta_Data_2"
            @column-source "All_Assignments.Meta_Data_2"   
        @column "Meta_Data_3"
            @column-source "All_Assignments.Meta_Data_3"   
        @column "Meta_Data_4"
            @column-source "All_Assignments.Meta_Data_4"   
        @column "Meta_Data_5"
            @column-source "All_Assignments.Meta_Data_5"   
        @column "Primary"
            @column-source "All_Assignments.PrimaryFlag" 
        @column "Type"
            @column-source "All_Assignments.Type" 
        @column "Status_Code"
            @column-source "All_Assignments.Status_Code"            
    @end-query

@end-analyze



@queue "All_Q"
@queue-query "Combined_User_Details"
@queue-test "call('Core.IsITUser')"
@queue-activator "^^Internal_Administration_Menu|Admin^^ | ^^Core_App_Master_Queues|Core App Master Queues^^ | ^^Workflow_Role_Management_Sub_Menu|WRM^^ | ^^Core_Task_Assignment_Analyze|Task Assignment Analyze^^"

    @column "Role"
    @column "Meta_Data_1"
    @column "Meta_Data_2"
    @column "Meta_Data_3"
    @column "Meta_Data_4"
    @column "Meta_Data_5"
    @column "Assignee"
    @column "Primary"
    @column "Type"
        @column-header "^^Type|Type^^"   
    @column "User_Informal_Full_Name"   
    @column "Status_Code"
@end-queue
