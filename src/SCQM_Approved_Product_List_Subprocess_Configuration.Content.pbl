﻿@title "^^Approved Product List Subprocess Configuration|Approved Product List Subprocess Configuration^^"

@mode "Main"

@block
    @block-ui-style "form-horizontal"

    @block
        @block-ui-style "clear span6"

        @label "Subprocess_Config_Id"
            @label-header "^^Subprocess Config Id|Subprocess Config Id^^:"
        
        @text "Subprocess_Name"
            @text-header "^^Subprocess Name|Subprocess Name^^:"

        @text "Subprocess_Workbook_Name"
            @text-header "^^Subprocess Workbook Name|Subprocess Workbook Name^^:"
            @text-columns "80"

        @choice "Status"
            @choice-style "DropDownLookup"
            @choice-header "^^Status|Status^^:"
            @choice-queue "Core_Status.%status_group%Active_States%status_group%"
            @choice-field "Status_Name"
            @choice-select "Core_Status/*"
            @choice-optional "Yes"    
        @end-choice

    @end-block

    @block
        @block-ui-style "span6"

        @text "Subprocess_Sequence"
            @text-header "^^Subprocess Sequence|Subprocess Sequence^^:"
            @text-type "Integer"

        @choice "Subprocess_as_Workbook_or_Form"
            @choice-style "RadioButtonList"
            @choice-header "^^Display as a workbook or a form?|Display as a workbook or a form?^^"
            @choice-default-expression "'Workbook'"
            @choice-columns "2"        
            
            @option
                @option-text "^^Workbook|Workbook^^"
                @option-value "Workbook"
            @option
                @option-text "^^Form|Form^^"
                @option-value "Form"
            
        @end-choice        

        @text "Subprocess_Discontinue_Status_Code"
            @text-header "^^Subprocess Discontinue Status Code|Subprocess Discontinue Status Code^^:"

        @text "Subprocess_Status_Group"
            @text-header "^^Subprocess Status Group|Subprocess Status Group^^:"

    @end-block

    @newline "Medium"

    @block
        @block-ui-style "clear span6 well well-mini"
        
        {label(medium info):'^^Subprocess Usage Conditions|Subprocess Usage Conditions^^'}
        
        @newline "Small"

        {label(bold):'^^Subprocess Usage Condition|Subprocess Usage Condition^^: ^^Specification|Specification^^:'}

        @newline
        
        {label(default):'^^evaluated_against_Specification_record|(evaluated against the Specification record /store/SCQM_Specification - True if blank)^^'}
        
        @text "Subprocess_Spec_Usage_Condition"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

        {label(bold):'^^Subprocess Usage Condition|Subprocess Usage Condition^^: ^^Facility|Facility^^:'}
        
        @newline
        
        {label(default):'^^evaluated_against_Facility_record|(evaluated against the Facility record /store/Core_Organization_Location - True if blank)^^'}
        
        @text "Subprocess_Facility_Usage_Condition"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"
            
        @newline
    
        @checkbox "Once_Per_Group"
            @checkbox-text "^^Only complete once per APL Group|Only complete once per APL Group^^?"

    @end-block

    @block
        @block-ui-style "span6 well well-mini"

        {label(medium info):'^^Subprocess Approval Conditions|Subprocess Approval Conditions^^'}
        
        @newline "Small"

        {label(bold):'^^Subprocess Condition Approval Criteria|Subprocess Condition Approval Criteria^^:'}
        
        @newline
        
        {label(default):'^^evaluated_against_Subprocess_Workbook|(evaluated against /store/SCQM_Approved_Product_List_Subprocesses/Subprocess_Workbook - False if blank)^^'}
        
        @text "Subprocess_Conditional_Approval_Criteria"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

        {label(bold):'^^Subprocess Approval Criteria|Subprocess Approval Criteria^^:'}
        
        @newline
        
        {label(default):'^^evaluated_against_Subprocess_Workbook|(evaluated against /store/SCQM_Approved_Product_List_Subprocesses/Subprocess_Workbook - False if blank)^^'}
        
        @text "Subprocess_Approval_Criteria"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

    @end-block

    @block
        @block-ui-style "clear span6 well well-mini"

        {label(medium info):'^^Subprocess Additional Country Conditions|Subprocess Additional Country Conditions^^'}
        
        @newline "Small"

        {label(bold):'^^Additional Country Request Criteria|Additional Country Request Criteria^^:'}
        
        @newline
        
        {label(default):'^^evaluated_against_Subprocess_Group|(evaluated against /store/SCQM_Approved_Product_List_Group - False if blank)^^'}
        
        @text "Add_Country_Request_Criteria"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

        {label(bold):'^^Additional Country Approval Role|Additional Country Approval Role^^:'}
        
        @newline
        
        {label(default):'^^Add_Country_Approval_Role_Label|(Workflow Role ID used to determine Additional Country Approver)^^'}
        
        @text "Add_Country_Approval_Role"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-optional "Yes"

    @end-block

    @block
        @block-ui-style "span6 well well-mini"

        {label(medium info):'^^Subprocess Allow Edit When Conditions|Subprocess Allow Edit When Conditions^^'}
        
        @newline "Small"

        {label(bold):'^^Subprocess Allow Edit When|Subprocess Allow Edit When^^:'}
        
        @newline

        {label(default):'^^evaluated_against_Subprocess_Workbook|(evaluated against /store/SCQM_Approved_Product_List_Subprocesses/Subprocess_Workbook - False if blank)^^'}
        
        @text "Subprocess_Allow_Edit_When"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

    @end-block

    @block
        @block-ui-style "clear span6 well well-mini"

        {label(medium info):'^^Subprocess Visibility Conditions|Subprocess Visibility Conditions^^'}
        
        @newline "Small"

        {label(bold):'^^Subprocess Tab Visibility Criteria|Subprocess Tab Visibility Criteria^^:'}
        
        @newline
        
        {label(default):'^^evaluated_against_Subprocess_Group|(evaluated against /store/SCQM_Approved_Product_List_Group - True if blank)^^'}
        
        @text "Subprocess_Tab_Visible"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

        {label(bold):'^^Subprocess Queue Visibility Criteria|Subprocess Queue Visibility Criteria^^:'}

        @newline
        
        {label(default):'^^evaluated_against_Subprocess_Group|(evaluated against /store/SCQM_Approved_Product_List_Group - True if blank)^^'}
        
        @text "Subprocess_Queue_Visible"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

        {label(bold):'^^Subprocess Message in lieu of Queue|Subprocess Message in lieu of Queue^^:'}
        
        @newline
        
        {label(default):'^^evaluated_as_xpath_against_Subprocess_Group|(evaluated as XPath against /store/SCQM_Approved_Product_List_Group)^^'}
        
        @newline
        
        {label(default):'^^Defaults to|Defaults to^^: ^^There is no action required at this time|There is no action required at this time^^'}
        
        @text "Subprocess_No_Queue_Visible_Message"
            @text-header " "
            @text-header-ui-style "hide"
            @text-columns "80"
            @text-rows "2"
            @text-optional "Yes"

    @end-block    

@end-block
