﻿@title " "
@title-ui-style "hide"
@mode "Summary, APLGroupMessaging, DiscontinueGroup, Subprocesses, Current_Assignment, User_Assignments, Override_Ownership"

[[SCQM_Approved_Product_List_Group.Content.Summary.Messaging]]

@block
@block-ui-style "clear span6"
    [[Shared_Core_Task_Assignments.Content.User_Assignments]]

    [[Shared_Core_Task_Assignments.Override_Ownership.Content]]
    [[Shared_Core_Task_Assignments.Override_Ownership.Executes]]

    @comment
    This is the Approval Letter section when there are additional countries in play
    @end-comment    
    @embed "SCQM_Approved_Product_List.Review_View"
    @embed-store-id "{{call('SCQM.APLGroup.GetAPLReadyFinalApproval', store-id())}}"
    @embed-visible-when "is-not-blank(call('SCQM.APLGroup.GetAPLReadyFinalApproval', store-id())) and is-blank(Discontinuation_Letter)"

    @comment
    This is the Conditional Approval Letter and Approval Letter section when this is the first country
    @end-comment    
    @embed "SCQM_Approved_Product_List.Review_View"
    @embed-store-id "{{call('SCQM.APLGroup.GetMainAPL', store-id())}}"
    @embed-visible-when "is-blank(call('SCQM.APLGroup.GetAPLReadyFinalApproval', store-id())) and (call('SCQM.APL.APLRecord_ReadyforCondApproval', call('SCQM.APLGroup.GetMainAPL', store-id())) or call('SCQM.APL.APLRecord_ReadyforApproval', call('SCQM.APLGroup.GetMainAPL', store-id())) or Status/Status_Code = 'APL_Conditionally_Approved') and is-blank(Discontinuation_Letter)"

    [[SCQM_Approved_Product_List_Group.Content.Summary.Subprocesses]]

@end-block

@block
@block-ui-style "span6"

    [[SCQM_Approved_Product_List_Group.Content.Summary.Discard_Discontinue]]

    {label(block large info):'^^Details|Details^^'}
    
    @newline "Large"
    
    {label(block medium info):'^^Specification Information|Specification Information^^'}
    
    @newline "Small"
    
    @lookup "Specification"
    @lookup-header "^^Specification Name|Specification Name^^:"
    @lookup-allow-new-when "false()"
    @lookup-updatable-when "false()"
    @lookup-read-only-when "true()"
    @lookup-queue "SCQM_Specification.Master_Q"
    @lookup-select "SCQM_Specification/Specification_Name | SCQM_Specification/Specification_Number | SCQM_Specification/Status"
    @lookup-field "Specification_Name"
    @lookup-allow-navigate-when "call('Core.AllowLookupNavigation', 'SCQM_Specification')"
    @lookup-ui-style "width-auto"
    
    @label "store(Specification/@refId)/*[1]/Specification_Number"
    @label-header "^^Specification #|Specification #^^:"

    @block
    @block-visible-when "call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 1)"
        @label "call('SCQM.SPEC.ProductHierarchyValue', Specification/@refId, 1)"
        @label-header "{{concat(localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1)), ':')}}"
    @end-block    

    @block
    @block-visible-when "call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 2)"
        @label "call('SCQM.SPEC.ProductHierarchyValue', Specification/@refId, 2)"
        @label-header "{{concat(localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2)), ':')}}"
    @end-block    

    @block
    @block-visible-when "call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 3)"
        @label "call('SCQM.SPEC.ProductHierarchyValue', Specification/@refId, 3)"
        @label-header "{{concat(localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3)), ':')}}"
    @end-block    

    @block
    @block-visible-when "call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 4)"
        @label "call('SCQM.SPEC.ProductHierarchyValue', Specification/@refId, 4)"
        @label-header "{{concat(localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4)), ':')}}"
    @end-block    

    @block
    @block-visible-when "call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 5)"
        @label "call('SCQM.SPEC.ProductHierarchyValue', Specification/@refId, 5)"
        @label-header "{{concat(localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5)), ':')}}"
    @end-block

    @label "localize-token(store(Specification/@refId)/*[1]/Status/Status_Name)"
    @label-header "^^Status|Status^^:"
    
    @block
        @block-visible-when "call('SCQM.APL.APLRecord_ReadyforApprovalsExceptSpecification', call('SCQM.APLGroup.GetMainAPL', store-id()))"
        @block-ui-style "well well-mini well-info-light inverse"
        @icon "icon-info-sign info medium"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^The Specification must be finalized to proceed with the approval for this facility|The Specification must be finalized to proceed with the approval for this facility^^'}    
    @end-block    
    
    @newline "Medium"
    
    {label(block medium info):'^^Facility Information|Facility Information^^'}
    
    @newline "Small"
    
    @lookup "Org_Location"
    @lookup-header "%Organization_Location%^^Organization Location|Organization Location^^%Organization_Location%:"
    @lookup-allow-new-when "false()"
    @lookup-read-only-when "true()"    
    @lookup-queue "Core_Organization_Location.Master_Q"
    @lookup-field "Location_Name"
    @lookup-allow-navigate-when "call('Core.AllowLookupNavigation', 'Core_Organization_Location')"    
    @lookup-ui-style "width-auto"

    @label "Org_Location/Location_ID"
    @label-header "%Organization_Location_ID%^^Facility ID|Facility ID^^%Organization_Location_ID%:"

    @label "call('Core.FormatPhysicalAddressString', Org_Location/@refId)"
    @label-header "^^Physical Address|Physical Address^^:"
    @label-ui-style "pre-wrap"

    @lookup "Org"
    @lookup-header "%Organization%^^Organization|Organization^^%Organization%:"
    @lookup-allow-new-when "false()"
    @lookup-read-only-when "true()"    
    @lookup-queue "Core_Organization.Active_Lookup_Q"
    @lookup-field "Organization_Name"
    @lookup-expression "Org_Location/Core_Organization/@refId"
    @lookup-expression-when "true()"
    @lookup-allow-navigate-when "call('Core.AllowLookupNavigation', 'Core_Organization')"    
    @lookup-ui-style "width-auto"

@end-block