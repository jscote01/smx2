﻿@title " "
@display "No"

@analyze
  
@query "Periods"
    @query-queue "Core_Period.All_Q"
    @query-filter "Status_Code = 'Active' AND (Parent_Calendar = {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.PID')}} OR (StoreId = {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.PID')}} AND Period_Count = 0))"

    @column "Name"
    @column "Begin_Date"
    @column "End_Date"
    @column "Parent_Calendar" 
    @column "StoreId"    

@end-query

@query "ALL_Audits"
    @query-queue "Core_Audit.Master_Q"
    @query-filter "Core_Audit_Service = {{page-filter('Core_Audit_Service_Filter.List.Service_ID')}} AND Status = 'Closed' AND Audit_Rating IS NOT NULL AND ({{page-filter('Core_Audit_AuditType_Filter.List.Name')}} = 'All' or (Audit_Type = {{page-filter('Core_Audit_AuditType_Filter.List.Name')}} and Audit_Type is NOT NULL)) AND (Audit_Completion_Date_Column BETWEEN {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.Begin_Date')}} AND {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.End_Date')}})"
    @query-queue-datafirewall "No"

    @column "Audit_Rating"
    @column "Audit_Completion_Date_Column"
    @column "Audit_Score_Calc"    
    
@end-query

@query "My_Audits"
    @query-queue "Core_Audit.Master_Q"
    @query-filter "Core_Audit_Service = {{page-filter('Core_Audit_Service_Filter.List.Service_ID')}} AND Status = 'Closed' AND Audit_Rating IS NOT NULL AND ({{page-filter('Core_Audit_AuditType_Filter.List.Name')}} = 'All' or (Audit_Type = {{page-filter('Core_Audit_AuditType_Filter.List.Name')}} and Audit_Type is NOT NULL)) AND (Audit_Completion_Date_Column BETWEEN {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.Begin_Date')}} AND {{page-filter('Core_Audit_Periods_Filter.Periods_Filter.End_Date')}})"
    @query-queue-datafirewall "Yes"
    
    @column "Audit_Rating"
    @column "Audit_Completion_Date_Column"
    @column "Audit_Score_Calc"    

@end-query

@query "Audit_Ratings"
    @subquery "ALL_Audits"    

    @column "Ranking_Group"
        @column-source "'Ratings'"

    @column "Audit_Rating"  
        @column-source "ALL_Audits.Audit_Rating"        
    
    @column "High_Score" 
        @column-source "ALL_Audits.Audit_Score_Calc"
        @column-aggregate "Maximum"    
    
@end-query

@query "ALL_Ratings"
    @subquery "Audit_Ratings"

    @column "Audit_Rating"
        @column-source "Audit_Ratings.Audit_Rating"        

    @column "Rating_Number"
        @column-rank "Row Number"
        @column-rank-partition "Audit_Ratings.Ranking_Group"
        @column-rank-order "Audit_Ratings.High_Score DESC" 
        
@end-query

@query "Combined_Audits"
    @subquery "My_Audits"
    @subquery "ALL_Ratings"
    @subquery-join "My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating"     
    @subquery "Periods"
    @subquery-join "My_Audits.Audit_Completion_Date_Column BETWEEN Periods.Begin_Date AND Periods.End_Date"
        
    @column "Name"
        @column-source "Periods.Name"
        
    @column "Begin_Date"    
        @column-source "Periods.Begin_Date"
        @column-aggregate "Minimum"        
        
    @column "End_Date"
        @column-source "Periods.End_Date"
        @column-aggregate "Minimum"        
        
    @column "R1_Count"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 1 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "R2_Count"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 2 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "R3_Count"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 3 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "R4_Count"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 4 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "R5_Count"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 5 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "Ratings_Total"
        @column-source "1"
        @column-aggregate "Sum"        
        
    @column "Rating_1_Name"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 1 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
    
    @column "Rating_2_Name"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 2 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        

    @column "Rating_3_Name"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 3 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
    
    @column "Rating_4_Name"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 4 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
        
    @column "Rating_5_Name"
        @column-source "CASE WHEN My_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 5 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
        
@end-query

@query "Aggregates"
    @subquery "Combined_Audits"
        
    @column "Name"
        @column-source "Combined_Audits.Name"
        
    @column "Begin_Date"    
        @column-source "Combined_Audits.Begin_Date"
        
    @column "End_Date"
        @column-source "Combined_Audits.End_Date"

    @column "Ratings_Total"
        @column-source "Combined_Audits.Ratings_Total"

     @column "R1_Count"
        @column-source "Combined_Audits.R1_Count"
    
    @column "R2_Count"
        @column-source "Combined_Audits.R2_Count"
    
    @column "R3_Count"
        @column-source "Combined_Audits.R3_Count"
    
    @column "R4_Count"
        @column-source "Combined_Audits.R4_Count"
    
    @column "R5_Count"
        @column-source "Combined_Audits.R5_Count"
    
    @column "Rating_1_PCT"
        @column-source "CASE WHEN Combined_Audits.Ratings_Total = 0 THEN 0 ELSE (100.0 * Combined_Audits.R1_Count / Combined_Audits.Ratings_Total) END"
    
    @column "Rating_2_PCT"
        @column-source "CASE WHEN Combined_Audits.Ratings_Total = 0 THEN 0 ELSE (100.0 * Combined_Audits.R2_Count / Combined_Audits.Ratings_Total) END"
    
    @column "Rating_3_PCT"
        @column-source "CASE WHEN Combined_Audits.Ratings_Total = 0 THEN 0 ELSE (100.0 * Combined_Audits.R3_Count / Combined_Audits.Ratings_Total) END"
    
    @column "Rating_4_PCT"
        @column-source "CASE WHEN Combined_Audits.Ratings_Total = 0 THEN 0 ELSE (100.0 * Combined_Audits.R4_Count / Combined_Audits.Ratings_Total) END"
    
    @column "Rating_5_PCT"
        @column-source "CASE WHEN Combined_Audits.Ratings_Total = 0 THEN 0 ELSE (100.0 * Combined_Audits.R5_Count / Combined_Audits.Ratings_Total) END"
    
    @column "Rating_1_Name"
        @column-source "Combined_Audits.Rating_1_Name"
        
    @column "Rating_2_Name"
        @column-source "Combined_Audits.Rating_2_Name"
        
    @column "Rating_3_Name"
        @column-source "Combined_Audits.Rating_3_Name"
        
    @column "Rating_4_Name"
        @column-source "Combined_Audits.Rating_4_Name"
        
    @column "Rating_5_Name"
        @column-source "Combined_Audits.Rating_5_Name"
        
@end-query

@query "National_Audits"
    @subquery "ALL_Audits"
    @subquery "ALL_Ratings"
    @subquery-join "ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating"     
    @subquery "Periods"
    @subquery-join "ALL_Audits.Audit_Completion_Date_Column BETWEEN Periods.Begin_Date AND Periods.End_Date"
        
    @column "Name"
        @column-source "Periods.Name"
        
    @column "Begin_Date"    
        @column-source "Periods.Begin_Date"
        @column-aggregate "Minimum"        
        
    @column "End_Date"
        @column-source "Periods.End_Date"
        @column-aggregate "Minimum"        
        
    @column "National_R1_Count"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 1 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "National_R2_Count"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 2 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "National_R3_Count"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 3 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "National_R4_Count"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 4 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "National_R5_Count"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 5 THEN 1 ELSE 0 END"
        @column-aggregate "Sum"        
        
    @column "National_Ratings_Total"
        @column-source "1"
        @column-aggregate "Sum"        
        
    @column "Rating_1_Name"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 1 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
    
    @column "Rating_2_Name"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 2 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        

    @column "Rating_3_Name"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 3 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
    
    @column "Rating_4_Name"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 4 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"        
        
    @column "Rating_5_Name"
        @column-source "CASE WHEN ALL_Audits.Audit_Rating = ALL_Ratings.Audit_Rating AND ALL_Ratings.Rating_Number = 5 THEN ALL_Ratings.Audit_Rating ELSE NULL END"
        @column-aggregate "Minimum"
        
@end-query

@query "National_Aggregates"
    @subquery "National_Audits"
        
    @column "Name"
        @column-source "National_Audits.Name"
        
    @column "Begin_Date"    
        @column-source "National_Audits.Begin_Date"
        
    @column "End_Date"
        @column-source "National_Audits.End_Date"

    @column "National_Ratings_Total"
        @column-source "National_Audits.National_Ratings_Total"

     @column "National_R1_Count"
        @column-source "National_Audits.National_R1_Count"
    
    @column "National_R2_Count"
        @column-source "National_Audits.National_R2_Count"
    
    @column "National_R3_Count"
        @column-source "National_Audits.National_R3_Count"
    
    @column "National_R4_Count"
        @column-source "National_Audits.National_R4_Count"
    
    @column "National_R5_Count"
        @column-source "National_Audits.National_R5_Count"
    
    @column "National_Rating_1_PCT"
        @column-source "CASE WHEN National_Audits.National_Ratings_Total = 0 THEN 0 ELSE (100.0 * National_Audits.National_R1_Count / National_Audits.National_Ratings_Total) END"
    
    @column "National_Rating_2_PCT"
        @column-source "CASE WHEN National_Audits.National_Ratings_Total = 0 THEN 0 ELSE (100.0 * National_Audits.National_R2_Count / National_Audits.National_Ratings_Total) END"
    
    @column "National_Rating_3_PCT"
        @column-source "CASE WHEN National_Audits.National_Ratings_Total = 0 THEN 0 ELSE (100.0 * National_Audits.National_R3_Count / National_Audits.National_Ratings_Total) END"
    
    @column "National_Rating_4_PCT"
        @column-source "CASE WHEN National_Audits.National_Ratings_Total = 0 THEN 0 ELSE (100.0 * National_Audits.National_R4_Count / National_Audits.National_Ratings_Total) END"
    
    @column "National_Rating_5_PCT"
        @column-source "CASE WHEN National_Audits.National_Ratings_Total = 0 THEN 0 ELSE (100.0 * National_Audits.National_R5_Count / National_Audits.National_Ratings_Total) END"
    
    @column "Rating_1_Name"
        @column-source "ISNULL(National_Audits.Rating_1_Name,'Rating 1 Undefined')"
        
    @column "Rating_2_Name"
        @column-source "ISNULL(National_Audits.Rating_2_Name,'Rating 2 Undefined')"
        
    @column "Rating_3_Name"
        @column-source "ISNULL(National_Audits.Rating_3_Name,'Rating 3 Undefined')"
        
    @column "Rating_4_Name"
        @column-source "ISNULL(National_Audits.Rating_4_Name,'Rating 4 Undefined')"
        
    @column "Rating_5_Name"
        @column-source "ISNULL(National_Audits.Rating_5_Name,'Rating 5 Undefined')"

@end-query

@query "Combined"
    @subquery "Periods"
    @subquery "National_Aggregates"
    @subquery-join "National_Aggregates.Name = Periods.Name"
    @subquery "Aggregates"
    @subquery-join "Aggregates.Name = National_Aggregates.Name"

    @column "Name"
        @column-source "Periods.Name"

    @column "Begin_Date"    
        @column-source "Periods.Begin_Date"
        
    @column "End_Date"
        @column-source "Periods.End_Date"
        
    @column "R1_Count"
        @column-source "Aggregates.R1_Count"
    
    @column "R2_Count"
        @column-source "Aggregates.R2_Count"
    
    @column "R3_Count"
        @column-source "Aggregates.R3_Count"
    
    @column "R4_Count"
        @column-source "Aggregates.R4_Count"
    
    @column "R5_Count"
        @column-source "Aggregates.R5_Count"
    
    @column "Rating_1_PCT"
        @column-source "ISNULL(Aggregates.Rating_1_PCT, 0)"
    
    @column "Rating_2_PCT"
        @column-source "ISNULL(Aggregates.Rating_2_PCT, 0)"
    
    @column "Rating_3_PCT"
        @column-source "ISNULL(Aggregates.Rating_3_PCT, 0)"
    
    @column "Rating_4_PCT"
        @column-source "ISNULL(Aggregates.Rating_4_PCT, 0)"
    
    @column "Rating_5_PCT"
        @column-source "ISNULL(Aggregates.Rating_5_PCT, 0)"
    
    @column "National_R1_Count"
        @column-source "National_Aggregates.National_R1_Count"
    
    @column "National_R2_Count"
        @column-source "National_Aggregates.National_R2_Count"
    
    @column "National_R3_Count"
        @column-source "National_Aggregates.National_R3_Count"
    
    @column "National_R4_Count"
        @column-source "National_Aggregates.National_R4_Count"
    
    @column "National_R5_Count"
        @column-source "National_Aggregates.National_R5_Count"
    
    @column "National_Rating_1_PCT"
        @column-source "ISNULL(National_Aggregates.National_Rating_1_PCT, 0)"
    
    @column "National_Rating_2_PCT"
        @column-source "ISNULL(National_Aggregates.National_Rating_2_PCT, 0)"
    
    @column "National_Rating_3_PCT"
        @column-source "ISNULL(National_Aggregates.National_Rating_3_PCT, 0)"
    
    @column "National_Rating_4_PCT"
        @column-source "ISNULL(National_Aggregates.National_Rating_4_PCT, 0)"
    
    @column "National_Rating_5_PCT"
        @column-source "ISNULL(National_Aggregates.National_Rating_5_PCT, 0)"
    
    @column "Rating_1_Name"
        @column-source "National_Aggregates.Rating_1_Name"
        
    @column "Rating_2_Name"
        @column-source "National_Aggregates.Rating_2_Name"
        
    @column "Rating_3_Name"
        @column-source "National_Aggregates.Rating_3_Name"
        
    @column "Rating_4_Name"
        @column-source "National_Aggregates.Rating_4_Name"
        
    @column "Rating_5_Name"
        @column-source "National_Aggregates.Rating_5_Name"
        
    @column "National"
        @column-source "'%National%Natl%National%'"

    @column "Ratings_Total"
        @column-source "National_Aggregates.National_Ratings_Total"

@end-query

@end-analyze