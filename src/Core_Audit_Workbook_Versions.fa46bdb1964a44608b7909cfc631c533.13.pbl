﻿@topic
@topic-title "Scientific evidence/literature used to set/determine the critical limits"
@topic-number "20"

@block "_Controls_Block"
@block-container "Controls"
@block-ui-style "form-horizontal"

	@choice "_20_Choice"
	@choice-style "RadioButtonList"
	@choice-columns "3"
	@choice-direction "Horizontal"
	@choice-postback "Yes"

		@option
		@option-text "Yes"
		@option-ui-style-checked "btn btn-medium white bcolor-success"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "No"
		@option-ui-style-checked "btn btn-medium white bcolor-danger"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

		@option
		@option-text "N/A"
		@option-ui-style-checked "btn btn-medium white bcolor-inverse"
		@option-ui-style-unchecked "btn btn-medium white bg-gray3"

	@end-choice

@end-block

@block "_Content_Block"
@block-expanded-when "is-not-blank(_20_Choice) and  _20_Choice = 'No'"
@block-container "Content"

	@block "_Comment_Photo_Block"
	@block-ui-style "form-horizontal"

		@block
		@block-ui-style "row-fluid"

			@block
			@block-ui-style "span6"

				@text "_20_Comments"
				@text-header "Comments:"
				@text-rows "5"
				@text-optional-when "is-blank(_20_Choice) or _20_Choice [.= 'N/A' or .='Yes']"
				@text-expression "'Scientific evidence/literature does not set/determine the critical limits'"
				@text-expression-when "is-changed(_20_Choice) and is-blank(_20_Comments) and _20_Choice = 'No'"
				@text-ui-style "block"

				@block

					@text "_20_Corrective_Action"
					@text-header "^^Corrective_Action_Header|Recommended Corrective Action^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

				@block

					@text "_20_Continuous_Improvement"
					@text-header "^^Continuous_Improvement_Header|Continuous Improvement Recommendation^^:"
					@text-rows "5"
					@text-optional "Yes"
					@text-ui-style "block"

				@end-block

			@end-block

			@block
			@block-ui-style "span6"

				@list "_20_Photo_List"
				@list-header "Photos (optional):"
				@list-optional "true"
				@list-style "List"
				@list-sort "Ascending"
				@list-header-ui-style "bold"

					@photo "Photo"
					@photo-header "Photo:"
					@photo-optional "Yes"
					@photo-resolution "Medium"
					@photo-meta-line-item "20"

					@text "Photo_Description"
					@text-header "Description:"
					@text-optional "Yes"

					@file "File"
					@file-header "File:"
					@file-optional "Yes"

					@text "File_Description"
					@text-header "Description:"
					@text-optional "Yes"

				@end-list

			@end-block

		@end-block

	@end-block

	@finding
	@finding-points "3"
	@finding-select "_20_Comments"
	@finding-score-when "is-not-blank(_20_Choice) and _20_Choice != 'N/A'"
	@finding-meta-Question_Number "20"

		@case
		@case-type "Positive"
		@case-points "3"
		@case-test "not(_20_Choice = 'No')"

		@case
		@case-type "Foundational"
		@case-points "_20_Selected_Points + 0"
		@case-test "_20_Choice = 'No'"

	@end-finding

	@block "_Score_Points_Calc"
	@block-visible-when "call('Core.IsITUser')"

		@text "_20_Selected_Points"
		@text-header "Non-Compliant Awarded Points (Calc Field):"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-blank(_20_Selected_Points), 0, _20_Selected_Points)"
		@text-expression-when "is-blank(_20_Selected_Points)"

	@end-block

@end-block
