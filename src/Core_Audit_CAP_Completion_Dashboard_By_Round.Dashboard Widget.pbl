﻿@title " "
@display "No"


@queue "CAP_Completed_DEBUG"
    @queue-header "^^Completed CAP Debug Queue|Completed CAP Debug Queue^^"
    @queue-test "false() and call('Core.IsITUser')"
    @queue-activator "^^Reporting|Reporting^^ | ^^Operations|Operations^^ | ^^Details|Details^^"
    @queue-sequence "300"
    @queue-query "Combined"
    @queue-sort "Begin_Date ASC"

    @column "Name"
        @column-header "Period"

    @column "Begin_Date"
        @column-header "Begin Date"
        @column-type "Date"

    @column "End_Date"
        @column-header "End Date"
        @column-type "Date"

    @column "Total_CAPs"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Open_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Compliant_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Overdue_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Completed_Overdue_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Open_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Compliant_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Overdue_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Completed_Overdue_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @end-queue
    
    ----
    
    @comment
    Chart - All CAPs
    @end-comment
    
    @queue "CAP_Completion_By_Round"
    @queue-header "^^Corrective Action Plan Completion (By Round)|Corrective Action Plan Completion (By Period)^^"
    @@queue-test "call('Core.CheckPermission', 'OMX_Dashboard') and (true() or not(page-filter-exists()) or contains(page-filter('Core_Audit_Service_Filter.List.Dashboard_Widgets'), ';CAPCOMP;'))"
    @queue-query "Combined"
    @queue-sort "Begin_Date ASC"
    @queue-show-controls "No"
    
    @column "Name"
        @column-header "Period"

    @column "Begin_Date"
        @column-header "Begin Date"
        @column-type "Date"

    @column "End_Date"
        @column-header "End Date"
        @column-type "Date"

    @column "Total_CAPs"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Open_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Compliant_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Overdue_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Completed_Overdue_CAP"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Open_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Compliant_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Overdue_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"
    
    @column "Completed_Overdue_CAP_Percentage"
        @column-type "Number"
        @column-format "{0:0}"

@plugin
<div id="CAPCOMP_By_Round_Chart" style="height: 390px">Loading chart</div>

<div id="CAPCOMP_By_Round_Chart_Button_Panel" class="pull-right" style="border:1px solid #eee;border-radius:4px;margin-top:-10px;z-index:1100;position:relative;">
    <button class="primary margin-tb-none btn btn-flat hide" id="DataButton1">
        <i id="icon1" class="primary medium icon-arrow-left"></i>
    </button>
    <button class="primary margin-tb-none btn btn-flat" id="DataButton2">
        <i id="icon2" class="primary medium icon-arrow-right"></i>
    </button>
</div>

<script type="text/javascript">
$(document).ready(function() {
Highcharts.setOptions({colors: ['#94201C', '#FA9F1E', '#82B941', '#1D5CAA']});
var chart_1; //Chart object that will contain your default 1st chart
var chart_2; //Chart object that will contain your 2nd chart
var chart_div = "CAPCOMP_By_Round_Chart"; //Div ID that contains chart (must be same as above)

var chart_1CAPCOMP = {
      chart: {         
        renderTo: chart_div,
		defaultSeriesType: 'column'
		},
                lang: {
                        exportButtonTitle: "^^Dashboard_Export_Image_Title|Export Image^^"
                },     
		title: {         
			text:"^^CAP_Completion_Title|Corrective Action Plan Completion (By Period)^^",
                        //y: 35,
                        //margin: 10,
                       style: {
                fontSize: '16px'
			}
		},     
		xAxis: {
			lineWidth: 1,
			lineColor: '#0D8ECF',
			gridLineWidth: 0,
            minorGridLineWidth: 0,
			categories: [<REPEAT>'{{Name}}',</REPEAT>null].slice(0,-1)		
		},
		yAxis: {
			lineWidth: 1,
			lineColor: '#0D8ECF',
			gridLineWidth: 0,
            minorGridLineWidth: 0,
            min: 0,
            max: 100,			
			title: {            
			text: "^^CAP_Completion_YAxis_Label_PCT|Percentage (%)^^"         
			}
			},      
			legend: {
                layout: 'horizontal',
                align: 'center',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+ point.series.name +': '+
                    point.y.toFixed(2) +'%';
                });
                
                return s;
            },
            shared: true
        },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                        textOutline: false
                        },
         formatter: function() { 
        if (this.y != 0) { 
          return null; 
        } else { 
          return null; 
                  } 
                } 
 
            }
                }
            },
            series: [{
                name: "^^CAP_Overdue|Overdue^^",
                data: [<REPEAT>{{Overdue_CAP_Percentage}},</REPEAT>null].slice(0,-1)
            }, {
                name: "^^CAP_Completed_Overdue|Completed Overdue^^",
                data: [<REPEAT>{{Completed_Overdue_CAP_Percentage}},</REPEAT>null].slice(0,-1)
            }, {
                name: "^^CAP_Ontime_Completion|On-time Completion^^",
                data: [<REPEAT>{{Compliant_CAP_Percentage}},</REPEAT>null].slice(0,-1)

            }, {
                name: "^^CAP_Open|Open^^",
                data: [<REPEAT>{{Open_CAP_Percentage}},</REPEAT>null].slice(0,-1)
            }],
		exporting: {
                    enabled: true,
                    width: 1000000,
                    filename: "^^CAP_Completion_Title|Corrective Action Plan Completion (By Period)^^",
                }
};
	
var chart_2CAPCOMP = {
      chart: {         
		renderTo: chart_div,
		defaultSeriesType: 'column'
		},     
		title: {         
			text:"^^CAP_Completion_Title_Counts|Corrective Action Plan Completion (By Period)^^",
                        //y: 35,
                        //margin: 10,
                       style: {
                fontSize: '16px'
			}
		},     
		xAxis: {
			lineWidth: 1,
			lineColor: '#0D8ECF',
			gridLineWidth: 0,
            minorGridLineWidth: 0,
			categories: [<REPEAT>'{{Name}}',</REPEAT>null].slice(0,-1)    	
		},
			yAxis: {
				lineWidth: 1,
				lineColor: '#0D8ECF',
				gridLineWidth: 0,
                minorGridLineWidth: 0,
                min: 0,			
				title: {            
				text: "^^CAP_Completion_YAxis_Label_Count|Count^^"         
				},
                   stackLabels: {
                    style: {
                    color: 'black'
                          },
                      enabled: false,
                   formatter: function() {
                     return '<b>' + this.y + '</b>';
                }
            }


			},      
			legend: {
                layout: 'horizontal',
                align: 'center',
                floating: false,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+ point.series.name +': '+
                        point.y;
                });
                
                return s;
            },
            shared: true
        },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                        textOutline: false
                        },
         formatter: function() { 
        if (this.y != 0) { 
          return this.y ; 
        } else { 
          return null; 
                  } 
                } 
 
            }
                }
            },
            series: [{
                name: "^^CAP_Overdue|Overdue^^",
                data: [<REPEAT>{{Overdue_CAP}},</REPEAT>null].slice(0,-1)
            }, {
                name: "^^CAP_Completed_Overdue|Completed Overdue^^",
                data: [<REPEAT>{{Completed_Overdue_CAP}},</REPEAT>null].slice(0,-1)
            }, {
                name: "^^CAP_Ontime_Completion|On-time Completion^^",
                data: [<REPEAT>{{Compliant_CAP}},</REPEAT>null].slice(0,-1)
            }, {
                name: "^^CAP_Open|Open^^",
                data: [<REPEAT>{{Open_CAP}},</REPEAT>null].slice(0,-1)
            }],
		exporting: {
            enabled: true,
            width: 1000000,
            filename: "^^CAP_Completion_Title|Corrective Action Plan Completion (By Period)^^",
        }
};

// Toggle Action
    
    $("button[id*='DataButton']").click(function(event) {
        button1classes = document.getElementById("DataButton1").classList,
        button2classes = document.getElementById("DataButton2").classList,
        currel = event.target.id || $(this).attr('id');

          if (currel == "DataButton2" || currel == "icon2") {
          //alert(currel);
            chart_2 = new Highcharts.Chart(chart_2CAPCOMP);
            chart_2.redraw();
            $("button[id = 'DataButton1']").removeClass('hide').addClass('show');
            $("button[id = 'DataButton2']").removeClass('show').addClass('hide');
        } 
        
          else if (currel == "DataButton1" || currel == "icon1") {
          //alert(currel);
            chart_1 = new Highcharts.Chart(chart_1CAPCOMP);
            chart_1.redraw();
            $("button[id = 'DataButton1']").removeClass('show').addClass('hide');
            $("button[id = 'DataButton2']").removeClass('hide').addClass('show');
        }
        
    });
    
    chart_1 = new Highcharts.Chart(chart_1CAPCOMP);
    
});
</script>
@end-plugin
@end-queue