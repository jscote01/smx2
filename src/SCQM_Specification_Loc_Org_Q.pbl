﻿@app "SCQM_Specification_Loc_Org_Q"

@language "all"

@analyze

    @query "Locations"
        @query-queue "Core_Organization_Location.Master_Q"
        @query-queue-datafirewall "No"
    
        @column "LID"
            @column-source "StoreId"
        @column "Core_Organization"
        @column "Location_Name"        
        
    @end-query
    
    @query "DFW"
        @query-queue "Data_Firewall.List"
        @query-filter "[Type] = 'Include' AND (isNull(Start_Date,getdate()) <= getdate() and isNull(End_Date, getdate()) >= getdate() and Status = 'Active') "    
    
        @column "User_SID"
            @column-source "User"        
        @column "Access_Tag_Core_Organization"
        @column "Access_Tag_Core_Organization_Location"

    @end-query
 
     @query "Orgs"
         @query-queue "Core_Organization.Master_Q"
         @query-queue-datafirewall "No"
    
        @column "OID"
            @column-source "StoreId"
        @column "Organization_Name"
        
    @end-query  
    
    @query "Combined_Loc"
        @query-distinct-values "Yes"        
        @subquery "Locations"    
        @subquery "DFW"
        @subquery-join "(DFW.Access_Tag_Core_Organization_Location = Locations.LID or DFW.Access_Tag_Core_Organization_Location is NULL) and (DFW.Access_Tag_Core_Organization = Locations.Core_Organization or DFW.Access_Tag_Core_Organization is NULL)"    
        
        @column "User_S"
            @column-source "DFW.User_SID"
        
        @column "Core_Organization"
            @column-source "Locations.Core_Organization"
        
        @column "Location_Name"
            @column-source "Locations.Location_Name"

    @end-query
   
    @query "Combined_Org"
        @query-distinct-values "Yes"    
        @subquery "Orgs"    
        @subquery "DFW"
        @subquery-join "DFW.Access_Tag_Core_Organization = Orgs.OID or (DFW.Access_Tag_Core_Organization is NULL and DFW.Access_Tag_Core_Organization_Location is NULL)"    
        
        @column "User_S"
            @column-source "DFW.User_SID"
        
        @column "Core_Organization"
            @column-source "Orgs.OID" 
        
        @column "Location_Name"
            @column-source "Orgs.Organization_Name"
            
    @end-query

    @query "Grouped"
        @query-distinct-values "Yes" 
        @query-type "Union"
        @subquery "Combined_Loc"    
        @subquery "Combined_Org"
    
        @column "User_S"
        @column "Core_Organization"
       
    @end-query
    
    @query "Combined"
        @query-distinct-values "Yes" 
        @subquery "Grouped"    
       
        @column "User_S"
        @column "Core_Organization"
  
    @end-query 
    
    @query "Specifications"
        @query-queue "SCQM_Specification.Master_Q"
         
        @column "SID"
            @column-source "StoreId"       
        @column "Specification_Name"        
        @column "Core_Organization"
        @column "Specification_Type"
        @column "Product_Hierarchy_1"
        @column "Specification_Concepts"
        @column "Specification_Number"
        @column "Spec_Creator_Name"
        @column "Created"
        @column "Last_Modified_Date"
        @column "Final_Approval_Date"        
        @column "Specification_Author_Name"
        @column "Status_Name"
        @column "Status_Code"

    @end-query 
    
    @query "Spec_Combined"
        @query-store-id "SID"    
        @subquery "Combined"    
        @subquery "Specifications"
        @subquery-join "Combined.Core_Organization = Specifications.Core_Organization"
   
        @column "SID"
            @column-source "Specifications.SID" 
        @column "User_S"
            @column-source "Combined.User_S"   
        @column "Product_Name"  
            @column-source "Specifications.Specification_Name"
        @column "Core_Organization"
            @column-source "Combined.Core_Organization"   
        @column "Type"
            @column-source "Specifications.Specification_Type"
        @column "Product_Category"
            @column-source "Specifications.Product_Hierarchy_1"
        @column "Concepts"
            @column-source "Specifications.Specification_Concepts"
        @column "Specification_Number"
            @column-source "Specifications.Specification_Number"
        @column "Spec_Creator_Name"
            @column-source "Specifications.Spec_Creator_Name"
        @column "Created"
            @column-source "Specifications.Created"
        @column "Last_Modified_Date"
            @column-source "Specifications.Last_Modified_Date"
        @column "Final_Approval_Date"
            @column-source "Specifications.Final_Approval_Date"
        @column "Specification_Author_Name"
            @column-source "Specifications.Specification_Author_Name"
        @column "Status_Name" 
            @column-source "Specifications.Status_Name"
        @column "Status_Code" 
            @column-source "Specifications.Status_Code"
        
   @end-query

@end-analyze



@queue "All"
@queue-query "Spec_Combined"
@queue-key "SID"
@queue-activator "^^Internal_Administration_Menu|Admin^^ | ^^Specification|Specification^^ | ^^Supplier Specs|Supplier Specs^^"
@queue-header "^^Supplier Specs|Supplier Specs^^"
@queue-test "call('Core.IsITUser')"
    
    @action
        @action-name "Navigate"
        @action-text "Product_Name | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "SID"
        @column-hidden "Yes"    
    @column "User_S"
        @column-hidden "Yes" 
    @column "Core_Organization"
        @column-hidden "Yes" 
    @column "Product_Name"
        @column-header "^^Product Name|Product Name^^"
    @column "Type"
        @column-header "^^Specification Type|Specification Type^^"
    @column "Product_Category"
        @column-header "^^Product Category|Product Category^^"    
    @column "Concepts"
        @column-header "^^Concepts|Concepts^^"    
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"    
    @column "Spec_Creator_Name"
        @column-header "^^Creator|Creator^^"
    @column "Created"
        @column-header "^^Created|Created^^"
    @column "Last_Modified_Date"
        @column-header "^^Last Modified Date|Last Modified Date^^"    
    @column "Specification_Author_Name"
        @column-header "^^Author|Author^^"
    @column "Status_Name"
        @column-header "^^Status|Status^^"
    @column "Status_Code"
        @column-hidden "Yes"

        
@end-queue


@queue "InProgess_User"
@queue-query "Spec_Combined"
@queue-key "SID"
@queue-header "^^In Progress Specifications|In Progress Specifications^^"
@queue-activator "^^Products|Products^^ | ^^Specifications|Specifications^^"
@queue-sort "Last_Modified_Date DESC"
@queue-test "(boolean(call('SCQM.SPEC.IsOrg')) or boolean(call('SCQM.SPEC.IsOrgLoc'))) and call('Core.CheckCapability', 'SMX.Specification')"
@queue-filter "Status_Code IN ('SPC_Pre_Draft', 'SPC_Draft', 'SPC_Pending_Nomenclature_Approval', 'SPC_Draft_Finalized', 'SPC_Pending_Final_Approval') AND User_S = {{user-id()}} AND (Product_Category = {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} IS NULL) AND (Type = {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} IS NULL) AND (Concepts LIKE concat('%',{{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}},'%') OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} IS NULL)"
    
    @action
        @action-name "Navigate"
        @action-text "Product_Name | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "SID"
        @column-hidden "Yes"    
    @column "User_S"
        @column-hidden "Yes" 
    @column "Core_Organization"
        @column-hidden "Yes" 
    @column "Product_Name"
        @column-header "^^Product Name|Product Name^^"
    @column "Type"
        @column-header "^^Specification Type|Specification Type^^"
    @column "Product_Category"
        @column-header "^^Product Category|Product Category^^"    
    @column "Concepts"
        @column-header "^^Concepts|Concepts^^"    
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"    
    @column "Spec_Creator_Name"
        @column-header "^^Creator|Creator^^"
    @column "Created"
        @column-header "^^Created|Created^^"
    @column "Last_Modified_Date"
        @column-header "^^Last Modified Date|Last Modified Date^^"    
    @column "Specification_Author_Name"
        @column-header "^^Author|Author^^"
    @column "Status_Name"
        @column-header "^^Status|Status^^"
    @column "Status_Code"
        @column-hidden "Yes"        
        
@end-queue

@queue "Approved_User"
@queue-query "Spec_Combined"
@queue-key "SID"
@queue-header "^^Approved Specifications|Approved Specifications^^"
@queue-activator "^^Products|Products^^ | ^^Specifications|Specifications^^"
@queue-sort "Final_Approval_Date DESC"
@queue-filter "User_S = {{user-id()}} AND Status_Code = 'SPC_Finalized' AND (Product_Category = {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} IS NULL) AND (Type = {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} IS NULL) AND (Concepts LIKE concat('%',{{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}},'%') OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} IS NULL)"
@queue-test "(boolean(call('SCQM.SPEC.IsOrg')) or boolean(call('SCQM.SPEC.IsOrgLoc'))) and call('Core.CheckCapability', 'SMX.Specification')"
    
    @action
        @action-name "Navigate"
        @action-text "Product_Name | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "SID"
        @column-hidden "Yes"    
    @column "User_S"
        @column-hidden "Yes" 
    @column "Core_Organization"
        @column-hidden "Yes" 
    @column "Product_Name"
        @column-header "^^Product Name|Product Name^^"
    @column "Type"
        @column-header "^^Specification Type|Specification Type^^"
    @column "Product_Category"
        @column-header "^^Product Category|Product Category^^"    
    @column "Concepts"
        @column-header "^^Concepts|Concepts^^"    
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"    
    @column "Spec_Creator_Name"
        @column-header "^^Creator|Creator^^"
    @column "Created"
        @column-header "^^Created|Created^^"
    @column "Final_Approval_Date"
        @column-header "^^Issue Date|Issue Date^^"    
    @column "Specification_Author_Name"
        @column-header "^^Author|Author^^"
    @column "Status_Name"
        @column-header "^^Status|Status^^"
    @column "Status_Code"
        @column-hidden "Yes"        
        
@end-queue

@queue "Pending_Archive_User"
@queue-query "Spec_Combined"
@queue-key "SID"
@queue-header "^^Pending Archive Specifications|Pending Archive Specifications^^"
@queue-activator "^^Products|Products^^ | ^^Specifications|Specifications^^"
@queue-sort "Final_Approval_Date DESC"
@queue-filter "User_S = {{user-id()}} AND Status_Code = 'SPC_Pending_Archive' AND (Product_Category = {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Product_Category.Product_Category_Spec_Filter.Name')}} IS NULL) AND (Type = {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Spec_Type.Spec_Type_Filter.Name')}} IS NULL) AND (Concepts LIKE concat('%',{{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}},'%') OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} = 'All' OR {{page-filter('SCQM_Specification_Filter_Concept.Concept_Filter.Concept_Name')}} IS NULL)"
@queue-test "(boolean(call('SCQM.SPEC.IsOrg')) or boolean(call('SCQM.SPEC.IsOrgLoc'))) and call('Core.CheckCapability', 'SMX.Specification')"
    
    @action
        @action-name "Navigate"
        @action-text "Product_Name | ^^OPEN|OPEN^^"
        @action-container "Column"
    
    @column "SID"
        @column-hidden "Yes"    
    @column "User_S"
        @column-hidden "Yes" 
    @column "Core_Organization"
        @column-hidden "Yes" 
    @column "Product_Name"
        @column-header "^^Product Name|Product Name^^"
    @column "Type"
        @column-header "^^Specification Type|Specification Type^^"
    @column "Product_Category"
        @column-header "^^Product Category|Product Category^^"    
    @column "Concepts"
        @column-header "^^Concepts|Concepts^^"    
    @column "Specification_Number"
        @column-header "^^Specification Number|Specification Number^^"    
    @column "Spec_Creator_Name"
        @column-header "^^Creator|Creator^^"
    @column "Created"
        @column-header "^^Created|Created^^"
    @column "Final_Approval_Date"
        @column-header "^^Issue Date|Issue Date^^"    
    @column "Specification_Author_Name"
        @column-header "^^Author|Author^^"
    @column "Status_Name"
        @column-header "^^Status|Status^^"
    @column "Status_Code"
        @column-hidden "Yes"        
        
@end-queue