﻿@display "No"

@comment
CUSTOMER-SPECIFIC FUNCTIONS USED ON THE HEADER AND TOP PAGE - MAKE SURE TO CHANGE 'SCQM' IN THE FUNCTION NAME TO SOMETHING APPROPRIATE FOR THE CUSTOMER
@end-comment


@comment
Return true if header has been completed
@end-comment
@function "SCQM.Spec_WB.IsHeaderCompleted"
boolean-from-string(/store/*[1]/Specification_Workbook/tracking/topic[@id = 'SCQM_Specification_Workbook.Header']/@completed = 'true')
@end-function


@comment
Does the current user have access?
EX: call('SCQM.Spec_WB.CurrentUserHasAccess')
@end-comment
@function "SCQM.Spec_WB.CurrentUserHasAccess"
boolean-from-string(
    store(find-store-id('Core_Task_Assignment_Preview_Admin.Master_Q', concat('ContextId = ', char(39), store-id(), char(39)), 'ContextId'))/*[1]/Assignment_Preview/item/Assigned_User/@refId = user-id() or 
    /store/SCQM_Specification/Specification_Workbook/Specification_Author/@refId = user-id() or 
    /store/SCQM_Specification/Collaboration_Author/@refId = user-id() or 
    /store/SCQM_Specification/Specification_Creator/@refId = user-id()
)
@end-function


@comment
Determine whether a topic in the workbook is meant to be read-only. Used in the @workbook-topic-read-only-when tag.
Header is editable while in Pre-Draft and Draft mode, or if in Pending Nomenclature Review mode and you are the assigned reviewer for a Vendor-authored Spec
Buttons_Banner is always editable
All other topics are editable except when the Verify_Status or Return_Status values are set
  #returns Boolean True when the topic is to be read-only
@end-comment
@function "SCQM.Spec_WB.IsTopicReadOnly"
boolean-from-string(
    (
        topic-id() = 'SCQM_Specification_Workbook.Header'
        and is-not-blank(/store/*[1]/Status/Status_Code) 
        and (
            not(separated-values-contain('SPC_Pre_Draft, SPC_Draft', /store/*[1]/Status/Status_Code))
            or (
                /store/*[1]/Status/Status_Code = 'SPC_Pending_Nomenclature_Approval'
                and not((call('SCQM.SPEC.IsApprover', store-id(), 'Nomenclature Approver', user-id()) or call('Core.IsITUser')))
            )
        )
    ) 
    or 
    (
        not(contains(topic-id(), 'SCQM_Specification_Workbook.Buttons_Banner'))
        and (
            is-not-blank(Verify_Status) 
            or is-not-blank(Return_Status) 
            or is-not-blank(Discontinue_Status)
        )
    )
)
@end-function


@comment
Determine whether the Product Name field is editable outside of Pre-Draft and Draft
Always editable in Pre-Draft
Editable in Pending Nomenclature Review by the assigned approver (or IT)
  #returns Boolean True when editable, False to show as plain label
@end-comment
@function "SCQM.Spec_WB.IsSpecificationNameEditable"
boolean-from-string(
    /store/*[1]/Status/Status_Code = 'SPC_Pending_Nomenclature_Approval' 
    and (
        call('SCQM.SPEC.IsApprover', store-id(), 'Nomenclature Approver', user-id()) 
        or call('Core.IsITUser')
    )
)
@end-function


@comment
Determine whether the Regulatory Name field is editable outside of Pre-Draft and Draft
Always editable in Pre-Draft
Editable in Draft by the Supplier Author when the Spec is vendor-authored
Editable in Pending Nomenclature Review by the assigned approver (or IT)
  #returns Boolean True when editable, False to show as plain label
@end-comment
@function "SCQM.Spec_WB.IsRegulatoryNameEditable"
boolean-from-string(
    /store/*[1]/Status/Status_Code = 'SPC_Pending_Nomenclature_Approval' 
    and (
        call('SCQM.SPEC.IsApprover', store-id(), 'Nomenclature Approver', user-id()) 
        or call('Core.IsITUser')
    )
)
@end-function


@comment
Determine if the current user can view the Formula Ingredients section
  #returns Boolean True when the current user can view the Formula Ingredients section
@end-comment
@function "SCQM.Spec_WB.IsFormulaOwner"
boolean-from-string(
    call('SCQM.SPEC.SpecOwnerisCompany') or 
    call('SCQM.SPEC.SpecOwnerisCompanyAndVendor') or 
    (
        call('SCQM.SPEC.SpecOwnerisVendor') 
        and 
        queue-count('Core_User_Org_and_Loc_Access.All_Q', concat('Is_External_User is not null and Core_Organization = ', char(39), Core_Organization/@refId, char(39), ' and UID = ', char(39), user-id(), char(39))) > 0
    )
)
@end-function