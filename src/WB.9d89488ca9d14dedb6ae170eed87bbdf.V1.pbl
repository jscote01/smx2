﻿@topic
@topic-title "Yum - Specification Template MAIN"

@text "Text_1"
@text-header "Spec Name"

@text "Text_2"
@text-header "Spec Number"
@text-type "Number"

@text "Text_3"
@text-header "Issue Number"

@choice "Choice_1"
@choice-style "DropDownList"
@choice-header "Status"
@choice-default "Draft"

	@option
	@option-text "Pre-Draft"

	@option
	@option-text "Draft"

	@option
	@option-text "Draft Pending Nomenclature"

	@option
	@option-text "Draft Finalized"

	@option
	@option-text "Finalized"

@end-choice

@text "Text_4"
@text-header "Effective Date"
@text-type "Date"

@checkbox "Checkbox_1"
@checkbox-header "Inactive"
@checkbox-optional "Yes"

@block "1"
@block-visible-when "Checkbox_1 = 'true'"

	@text "Text_8"
	@text-header "Reason for De-Activation?"

@end-block

@text "Text_5"
@text-header "Supercedes"
@text-optional "Yes"

@text "Text_6"
@text-header "Global Item Number"

@lookup "Lookup_1"
@lookup-header "Author"
@lookup-queue "User.Regular_List"

@choice "Approved_Concepts"
@choice-style "BulkList[Mode: Snapshot]"
@choice-header "^^Approved Concepts|Approved Concepts^^:"
@choice-columns "6"
@choice-postback "Yes"
@choice-ui-style "form-horizontal"

	@options
	@options-queue "Core_Concept.Active_Lookup_Q"
	@options-value "Concept_ID"
	@options-text "Concept_Name"

@end-choice

@choice "Regions"
@choice-style "BulkList[Mode: Snapshot]"
@choice-header "^^Approved Regions|Approved Regions^^:"
@choice-columns "3"
@choice-postback "Yes"
@choice-ui-style "form-horizontal"

	@options
	@options-queue "Core_Admin_Region.Active_Lookup_Q"
	@options-queue-filter "Region_Name != 'Global'"
	@options-value "StoreId"
	@options-text "Region_Name"

@end-choice

@block
@block-visible-when "is-not-blank(Regions)"

	@choice "Countries"
	@choice-style "BulkList[Mode: Snapshot]"
	@choice-header "^^Approved Countries|Approved Countries^^:"
	@choice-columns "3"
	@choice-postback "Yes"
	@choice-ui-style "form-horizontal"

		@options
		@options-queue "Core_Admin_Country_Region.Country_Region_Lookup_Active_Q"
		@options-queue-filter "{{filter:queue-selection-filter(Regions, 'Region')}}"
		@options-value "StoreId"
		@options-text "Country_Name"

	@end-choice

@end-block

@choice "Product_Hierarchy_1"
@choice-style "DropDownLookup"
@choice-header "{{localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 1))}}:"
@choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_1"
@choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
@choice-field "Name"
@choice-ui-style "width-auto"

@block
@block-visible-when "false()"

	@text "Specification_Type"
	@text-header "Specification Type (Calc Field)"
	@text-expression "if(Product_Hierarchy_1/Name='Food', 'F', 'N')"
	@text-expression-when "is-blank(Specification_Type) or is-changed(Product_Hierarchy_1)"

@end-block

@block
@block-visible-when "is-not-blank(Product_Hierarchy_1) and call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 2)"

	@choice "Product_Hierarchy_2"
	@choice-style "DropDownLookup"
	@choice-header "{{localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 2))}}:"
	@choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_2"
	@choice-filter "Parent_Category_Name_1_Level = {{Product_Hierarchy_1/Name}}"
	@choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
	@choice-field "Name"
	@choice-ui-style "width-auto"

@end-block

@block
@block-visible-when "is-not-blank(Product_Hierarchy_2) and call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 3)"

	@choice "Product_Hierarchy_3"
	@choice-style "DropDownLookup"
	@choice-header "{{localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 3))}}:"
	@choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_3"
	@choice-filter "Parent_Category_Name_1_Level = {{Product_Hierarchy_2/Name}}"
	@choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
	@choice-field "Name"
	@choice-ui-style "width-auto"

@end-block

@block
@block-visible-when "is-not-blank(Product_Hierarchy_3) and call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 4)"

	@choice "Product_Hierarchy_4"
	@choice-style "DropDownLookup"
	@choice-header "{{localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 4))}}:"
	@choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_4"
	@choice-filter "Parent_Category_Name_1_Level = {{Product_Hierarchy_3/Name}}"
	@choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
	@choice-field "Name"
	@choice-ui-style "width-auto"

@end-block

@block
@block-visible-when "is-not-blank(Product_Hierarchy_4) and call('SCQM.Product_Hierarchy.ProductHierarchyLevelExists', 5)"

	@choice "Product_Hierarchy_5"
	@choice-style "DropDownLookup"
	@choice-header "{{localize-token(call('SCQM.Product_Hierarchy.GetProductHierarchyLevelName', 5))}}:"
	@choice-queue "SCQM_Admin_Product_Categories.Active_Lookup_Q_Level_5"
	@choice-filter "Parent_Category_Name_1_Level = {{Product_Hierarchy_4/Name}}"
	@choice-select "SCQM_Admin_Product_Categories/Name | SCQM_Admin_Product_Categories/Code"
	@choice-field "Name"
	@choice-ui-style "width-auto"

@end-block

@text "Text_18"
@text-header "Reason for Change"
@text-columns "40"
@text-rows "2"

@signature "Signature_1"
@signature-header "Signature"


@text "Total_Shelf_Life"
@text-header "Total Shelf Life (days)"
@text-type "Integer"

@text "Life_To_DC"
@text-header "Shelf Life to DC (days)"
@text-type "Integer"

@text "Shelf_Life_Store"
@text-header "Shelf Life to In Store (days)"


@text "Finished_Product_Properties"
@text-header "Notes"
@text-columns "40"
@text-rows "4"


@text "General_Description"
@text-header "General Description"
@text-columns "40"
@text-rows "3"

@text "Ingredient_Statement"
@text-header "Ingredient Statement"
@text-columns "40"
@text-rows "3"

@text "Ingredient_Restriction"
@text-header "Ingredient Restriction"
@text-columns "40"
@text-rows "3"

@text "Declaration"
@text-header "Declaration"
@text-columns "40"
@text-rows "3"


@list "List_1"
@list-header "Micro"
@list-style "Grid"

	@text "Spec_Limit"
	@text-header "Spec Limits"

	@text "Method"
	@text-header "Method"

	@text "Test_Used"
	@text-header "Test Used"

	@choice "KPI"
	@choice-style "DropDownList"
	@choice-header "Appear on KPI?"

		@option
		@option-text "Yes"

		@option
		@option-text "No"

	@end-choice

	@choice "Cutting"
	@choice-style "DropDownList"
	@choice-header "Appear on Cutting?"

		@option
		@option-text "Yes"

		@option
		@option-text "No"

	@end-choice

@end-list


@list "List_2"
@list-header "Chemical"
@list-style "Grid"

	@text "Spec_Limit"
	@text-header "Spec Limits"

	@text "Method"
	@text-header "Method"

	@text "Test_Used"
	@text-header "Test Used"

	@text "KPI"
	@text-header "Appear on KPI?"

	@text "Cutting"
	@text-header "Appear on Cutting?"

@end-list

@list "Chemical_2"
@list-header "Chemical 2"

	@text "Text_1"
	@text-header "Description"

	@file "File_1"
	@file-header "Attach Chem Sheet"

@end-list


@list "Physical"
@list-header "Physical"
@list-style "Grid"

	@text "Spec_Limit"
	@text-header "Spec Limits"

	@text "Method"
	@text-header "Method"

	@text "Test_Used"
	@text-header "Test Used"

	@text "KPI"
	@text-header "Appear on KPI?"

	@text "Cutting"
	@text-header "Appear on Cutting?"

@end-list


@list "Sens_Prop"
@list-header "Sensory"
@list-style "Grid"

	@text "Spec_Limit"
	@text-header "Spec Limits"

	@text "Method"
	@text-header "Method"

	@text "Test_Used"
	@text-header "Test Used"

	@text "KPI"
	@text-header "Appear on KPI?"

	@text "Cutting"
	@text-header "Appear on Cutting?"

@end-list


@title "^^Packaging|Packaging^^"
    @title-ui-style "primary large margin-tb-mini"

@mode "Other"

@block
@block-ui-style "well well-mini well-info-light inverse"

	@icon "icon-info-sign info medium"
    {label(margin-lr-mini medium): '^^Spec_Packaging_Note|Specification Note: When you are inputting the packaging, add them in order from closest (directly in contact) to the food to furthest from the food^^.'}
@end-block

@newline "Medium"

@block
@block-ui-style "form-horizontal"

	@choice "System_Of_Measurement"
	@choice-style "RadioButtonList"
	@choice-header "^^System of Measurement|System of Measurement^^:"
	@choice-columns "2"
	@choice-default-expression "'English'"

		@option
		@option-value "English"
		@option-text "^^English|English^^"

		@option
		@option-value "Metric"
		@option-text "^^Metric|Metric^^"

	@end-choice

@end-block

@list "Packaging"
@list-header "^^Layers of Packaging|Layers of Packaging^^:"
@list-allow-order "No"
@list-header-ui-style "medium info block"

	@block
	@block-ui-style "clear span6"

		@choice "Package_Type"
		@choice-style "DropDownLookup"
		@choice-header "^^Type of Package|Type of Package^^:"
		@choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
		@choice-filter "Dropdown_Group_ID = 'Package Type' and Dropdown_List_Dropdown_Name = 'Package Type'"
		@choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
		@choice-field "Item_Name"
		@choice-ui-style "width-auto"

	@end-block

	@block
	@block-visible-when "Package_Type/Item_Name = 'Other'"
	@block-ui-style "span6"

		@text "Package_Type_Other"
		@text-header "^^Please Explain|Please Explain^^:"
		@text-rows "5"

	@end-block

	@block
	@block-ui-style "clear span6"

		@choice "Package_Material"
		@choice-style "DropDownLookup"
		@choice-header "^^Material|Material^^:"
		@choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
		@choice-filter "Dropdown_Group_ID = 'Package Material' and Dropdown_List_Dropdown_Name = 'Package Material'"
		@choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
		@choice-field "Item_Name"
		@choice-ui-style "width-auto"

	@end-block

	@block
	@block-visible-when "Package_Material/Item_Name = 'Other'"
	@block-ui-style "span6"

		@text "Package_Material_Other"
		@text-header "^^Please Explain|Please Explain^^:"
		@text-rows "5"

	@end-block

	@block
	@block-ui-style "clear span6"

		@choice "Package_Closure"
		@choice-style "DropDownLookup"
		@choice-header "^^Enclosure|Enclosure^^:"
		@choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
		@choice-filter "Dropdown_Group_ID = 'Package Closure' and Dropdown_List_Dropdown_Name = 'Package Closure'"
		@choice-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"
		@choice-field "Item_Name"
		@choice-ui-style "width-auto"

	@end-block

	@block
	@block-visible-when "Package_Closure/Item_Name = 'Other'"
	@block-ui-style "span6"

		@text "Package_Closure_Other"
		@text-header "^^Please Explain|Please Explain^^:"
		@text-rows "5"

	@end-block

	@block
	@block-ui-style "clear span6"

		@choice "Tamper_Evident"
		@choice-style "RadioButtonList"
		@choice-header "^^Is Tamper Evident?|Is Tamper Evident?^^"
		@choice-columns "2"

			@option
			@option-value "Yes"
			@option-text "^^Yes|Yes^^"

			@option
			@option-value "No"
			@option-text "^^No|No^^"

		@end-choice

	@end-block

	@newline "Small"

	@list "Photos_List"
	@list-header "^^Photos|Photos^^:"
	@list-optional "Yes"
	@list-allow-order "No"
	@list-style "Grid"
	@list-sort "Ascending"

		@file "Photo_File"
		@file-optional "Yes"

		@text "Photo_Description"
		@text-header "^^Photo Description|Photo Description^^:"
		@text-rows "5"
		@text-optional "Yes"

	@end-list

	@newline "Small"

@end-list

@block
@block-ui-style "form-horizontal"

	@newline "Medium"
    
    {label(medium info block): '^^Weight Information Per Each Item|Weight Information Per Each Item^^'}
    
	@newline "Small"

	@block
	@block-visible-when "System_Of_Measurement = 'English' or is-blank(System_Of_Measurement)"
	@block-ui-style "clear span6"
        
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}{label(required-label): '*'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|-
        {label(small bold block): '^^Ounces|Ounces^^'}
|
		@text "Serving_Target_Weight"
		@text-header "^^Target Weight|Target Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-validation-test "(is-blank(Serving_Min_Weight) or Serving_Target_Weight >= Serving_Min_Weight) and Serving_Target_Weight <= Serving_Max_Weight"
		@text-validation-error "^^Target_Validation_Message|Target must be between Min and Max^^"
		@text-header-ui-style "hide"
|
		@text "Serving_Min_Weight"
		@text-header "^^Min Weight|Min Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-validation-test "is-blank(Serving_Min_Weight) or (Serving_Min_Weight <= Serving_Target_Weight and Serving_Min_Weight <= Serving_Max_Weight)"
		@text-validation-error "^^Min_Validation_Message|Min must be less than or equal to Target^^"
		@text-header-ui-style "hide"
|
		@text "Serving_Max_Weight"
		@text-header "^^Max Weight|Max Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-validation-test "Serving_Max_Weight >= Serving_Target_Weight and (is-blank(Serving_Min_Weight) or Serving_Max_Weight >= Serving_Min_Weight)"
		@text-validation-error "^^Max_Validation_Message|Max must be greater than or equal to Target^^"
		@text-header-ui-style "hide"
|
|-
        {label(small bold block): '^^Grams|Grams^^'}
|
		@text "Serving_Target_Weight_G"
		@text-header "^^Target Weight|Target Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Target_Weight),call('Core.ConvertFrom', 'oz','g', coalesce(Serving_Target_Weight, 0)),0)"
		@text-expression-when "is-changed(Serving_Target_Weight)"
		@text-header-ui-style "hide"
|
		@text "Serving_Min_Weight_G"
		@text-header "^^Min Weight|Min Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Min_Weight), call('Core.ConvertFrom', 'oz', 'g', coalesce(Serving_Min_Weight, 0)), 0)"
		@text-expression-when "is-changed(Serving_Min_Weight)"
		@text-header-ui-style "hide"
|
		@text "Serving_Max_Weight_G"
		@text-header "^^Max Weight|Max Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Max_Weight), call('Core.ConvertFrom', 'oz', 'g', coalesce(Serving_Max_Weight, 0)), 0)"
		@text-expression-when "is-changed(Serving_Max_Weight)"
		@text-header-ui-style "hide"
|}
	@end-block

	@block
	@block-visible-when "System_Of_Measurement = 'Metric'"
	@block-ui-style "clear span6"
        
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}{label(required-label): '*'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|- 
        {label(small bold block): '^^Grams|Grams^^'}
|
		@text "Serving_Target_Weight_G"
		@text-header "^^Target Weight|Target Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-validation-test "(is-blank(Serving_Min_Weight_G) or Serving_Target_Weight_G >= Serving_Min_Weight_G) and Serving_Target_Weight_G <= Serving_Max_Weight_G"
		@text-validation-error "^^Target_Validation_Message|Target must be between Min and Max^^"
		@text-header-ui-style "hide"
|
		@text "Serving_Min_Weight_G"
		@text-header "^^Min Weight|Min Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-validation-test "is-blank(Serving_Min_Weight_G) or (Serving_Min_Weight_G <= Serving_Target_Weight_G and Serving_Min_Weight_G <= Serving_Max_Weight_G)"
		@text-validation-error "^^Min_Validation_Message|Min must be less than or equal to Target^^"
		@text-header-ui-style "hide"
|
		@text "Serving_Max_Weight_G"
		@text-header "^^Max Weight|Max Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-validation-test "Serving_Max_Weight_G >= Serving_Target_Weight_G and (is-blank(Serving_Min_Weight_G) or Serving_Max_Weight_G >= Serving_Min_Weight_G)"
		@text-validation-error "^^Max_Validation_Message|Max must be greater than or equal to Target^^"
		@text-header-ui-style "hide"
|
|-
        {label(small bold block): '^^Ounces|Ounces^^'}
|
		@text "Serving_Target_Weight"
		@text-header "^^Target Weight|Target Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Target_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Target_Weight_G, 0)), 0)"
		@text-expression-when "is-changed(Serving_Target_Weight_G)"
		@text-header-ui-style "hide"
|
		@text "Serving_Min_Weight"
		@text-header "^^Min Weight|Min Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-optional "Yes"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Min_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Min_Weight_G, 0)), 0)"
		@text-expression-when "is-changed(Serving_Min_Weight_G)"
		@text-header-ui-style "hide"
|
		@text "Serving_Max_Weight"
		@text-header "^^Max Weight|Max Weight^^:"
		@text-type "Number"
		@text-columns "20"
		@text-read-only-when "true()"
		@text-expression "if(is-not-blank(Serving_Max_Weight_G), call('Core.ConvertFrom', 'g', 'oz', coalesce(Serving_Max_Weight_G, 0)), 0)"
		@text-expression-when "is-changed(Serving_Max_Weight_G)"
		@text-header-ui-style "hide"
|}
	@end-block

	@newline "Medium"
    
    {label(medium info block): '^^General Packaging Information|General Packaging Information^^'}
    
	@newline "Small"

	@block
	@block-ui-style "clear span6"

		@text "Serving_Portions_Inner"
		@text-header "^^Each Items per Inner Package|Each Items per Inner Package^^:"
		@text-type "Integer"

		@text "Inner_Pkg_Per_Box"
		@text-header "^^Inner Packages per Box|Inner Packages per Box^^:"
		@text-footer "^^If the Inner Package is the Box, enter '1'|If the Inner Package is the Box, enter '1'^^"
		@text-type "Integer"

		@block
		@block-visible-when "is-not-blank(Serving_Portions_Inner) and is-not-blank(Inner_Pkg_Per_Box)"

			@label "Serving_Portions_Per_Box"
			@label-header "^^Each Items per Box|Each Items per Box^^:"

			@block
			@block-visible-when "false()"

				@text "Serving_Portions_Per_Box"
				@text-header "^^Each Items per Box|Each Items per Box^^ (^^CALC|CALC^^):"
				@text-expression "if(is-not-blank(Serving_Portions_Inner) and is-not-blank(Inner_Pkg_Per_Box), Serving_Portions_Inner * Inner_Pkg_Per_Box, Serving_Portions_Per_Box)"
				@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box)"

			@end-block

			@block
			@block-visible-when "is-not-blank(Serving_Target_Weight) and (System_Of_Measurement = 'English' or is-blank(System_Of_Measurement))"

				@newline "Medium"
                
                {label(medium info block): '^^Net Weight Per Box|Net Weight Per Box^^'}
                
				@newline "Small"
    
{|class="table table-naked"
|- 
|
|               {label(small bold): '^^Target Weight|Target Weight^^'}
|               {label(small bold): '^^Min Weight|Min Weight^^'}
|               {label(small bold): '^^Max Weight|Max Weight^^'}
|-
                {label(small bold): '^^Pounds|Pounds^^'}
|
				@label "Target_Weight_Per_Box"
				@label-header "^^Target Weight|Target Weight^^:"
				@label-header-ui-style "hide"

				@block
				@block-visible-when "is-not-blank(Serving_Min_Weight)"
|
					@label "Min_Weight_Per_Box"
					@label-header "^^Min Weight|Min Weight^^:"
					@label-header-ui-style "hide"

				@end-block

				@block
				@block-visible-when "is-not-blank(Serving_Max_Weight)"
|
					@label "Max_Weight_Per_Box"
					@label-header "^^Max Weight|Max Weight^^:"
					@label-header-ui-style "hide"

				@end-block
|
|-
                {label(small bold): '^^Kilograms|Kilograms^^'}
|
				@label "Target_Weight_Per_Box_Kg"
				@label-header "^^Target Weight|Target Weight^^:"
				@label-header-ui-style "hide"

				@block
				@block-visible-when "is-not-blank(Serving_Min_Weight)"
|
					@label "Min_Weight_Per_Box_Kg"
					@label-header "^^Min Weight|Min Weight^^:"
					@label-header-ui-style "hide"

				@end-block

				@block
				@block-visible-when "is-not-blank(Serving_Max_Weight)"
|
					@label "Max_Weight_Per_Box_Kg"
					@label-header "^^Max Weight|Max Weight^^:"
					@label-header-ui-style "hide"

				@end-block
|}
			@end-block

		@end-block

		@block
		@block-visible-when "is-not-blank(Serving_Target_Weight_G) and System_Of_Measurement = 'Metric'"

			@newline "Medium"
            
            {label(medium info block): '^^Net Weight Per Box|Net Weight Per Box^^'}
            
			@newline "Small"
    
{|class="table table-naked"
|- 
|
|       {label(small bold): '^^Target Weight|Target Weight^^'}
|       {label(small bold): '^^Min Weight|Min Weight^^'}
|       {label(small bold): '^^Max Weight|Max Weight^^'}
|- 
        {label(small bold  block): '^^Kilograms|Kilograms^^'}
|
			@label "Target_Weight_Per_Box_Kg"
			@label-header "^^Target Weight|Target Weight^^:"
			@label-header-ui-style "hide"

			@block
			@block-visible-when "is-not-blank(Serving_Min_Weight_G)"
|
				@label "Min_Weight_Per_Box_Kg"
				@label-header "^^Min Weight|Min Weight^^:"
				@label-header-ui-style "hide"

			@end-block
|
			@label "Max_Weight_Per_Box_Kg"
			@label-header "^^Max Weight|Max Weight^^:"
			@label-header-ui-style "hide"
|
|-
        {label(small bold): '^^Pounds|Pounds^^'}
|
			@label "format(Target_Weight_Per_Box,'Number', '{0:N2}')"
			@label-header "^^Target Weight|Target Weight^^:"
			@label-header-ui-style "hide"

			@block
			@block-visible-when "is-not-blank(Serving_Min_Weight)"
|
				@label "format(Min_Weight_Per_Box,'Number', '{0:N2}')"
				@label-header "^^Min Weight|Min Weight^^:"
				@label-header-ui-style "hide"

			@end-block
|
			@label "format(Max_Weight_Per_Box,'Number', '{0:N2}')"
			@label-header "^^Max Weight|Max Weight^^:"
			@label-header-ui-style "hide"
|}

		@end-block

		@block
		@block-visible-when "false()"

			@text "Target_Weight_Per_Box"
			@text-header "^^Net Target Weight per Box|Net Target Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Target_Weight), Serving_Portions_Per_Box * Serving_Target_Weight div 16, Target_Weight_Per_Box)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight) or is-changed(Serving_Target_Weight_G)"

			@text "Min_Weight_Per_Box"
			@text-header "^^Net Min Weight per Box|Net Min Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-optional "Yes"
			@text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Min_Weight),Serving_Portions_Per_Box * Serving_Min_Weight div 16, 0)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Min_Weight) or is-changed(Serving_Min_Weight_G)"

			@text "Max_Weight_Per_Box"
			@text-header "^^Net Max Weight per Box|Net Max Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-optional "Yes"
			@text-expression "if(is-not-blank(Serving_Portions_Per_Box) and is-not-blank(Serving_Max_Weight),Serving_Portions_Per_Box * Serving_Max_Weight div 16, 0)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Max_Weight)or is-changed(Serving_Max_Weight_G)"

			@text "Target_Weight_Per_Box_Kg"
			@text-header "^^Net Target Weight per Box|Net Target Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-expression "if(is-not-blank(Target_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Target_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight) or is-changed(Serving_Target_Weight_G)"

			@text "Min_Weight_Per_Box_Kg"
			@text-header "^^Net Min Weight per Box|Net Min Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-optional "Yes"
			@text-expression "if(is-not-blank(Min_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Min_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Min_Weight) or  is-changed(Serving_Min_Weight_G)"

			@text "Max_Weight_Per_Box_Kg"
			@text-header "^^Net Max Weight per Box|Net Max Weight per Box^^ (^^CALC|CALC^^):"
			@text-type "Number"
			@text-optional "Yes"
			@text-expression "if(is-not-blank(Max_Weight_Per_Box), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Max_Weight_Per_Box, 0)), 'Number', '{0:N2}'), 0)"
			@text-expression-when "is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Max_Weight) or is-changed(Serving_Max_Weight_G)"

		@end-block

	@end-block

	@block
	@block-ui-style "span6"

		@text "Boxes_Per_Case"
		@text-header "^^Boxes Per Case|Boxes Per Case^^:"
		@text-type "Integer"

		@block
		@block-visible-when "is-not-blank(Boxes_Per_Case) and is-not-blank(Target_Weight_Per_Box)"

			@label "format(Case_Weight, 'Number', '{0:N2}')"
			@label-header "^^Case Weight|Case Weight^^ (^^lbs|lbs^^):"

			@label "Case_Weight_Kg"
			@label-header "^^Case Weight|Case Weight^^ (^^kg|kg^^):"

			@block
			@block-visible-when "false()"

				@text "Case_Weight"
				@text-header "^^Case Weight|Case Weight^^ (^^CALC|CALC^^):"
				@text-type "Number"
				@text-expression "if(is-not-blank(Boxes_Per_Case) and is-not-blank(Target_Weight_Per_Box), Boxes_Per_Case * Target_Weight_Per_Box, Case_Weight)"
				@text-expression-when "is-changed(Boxes_Per_Case) or is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight)"

				@text "Case_Weight_Kg"
				@text-header "^^Case Weight|Case Weight^^ (^^CALC|CALC^^):"
				@text-type "Number"
				@text-expression "if(is-not-blank(Case_Weight), format(call('Core.ConvertFrom', 'lbs', 'kg', coalesce(Case_Weight, 0)), 'Number', '{0:N2}'), 0)"
				@text-expression-when "is-changed(Boxes_Per_Case) or is-changed(Serving_Portions_Inner) or is-changed(Inner_Pkg_Per_Box) or is-changed(Serving_Target_Weight)"

			@end-block

		@end-block

	@end-block

@end-block


