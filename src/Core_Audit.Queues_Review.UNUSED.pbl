﻿@title "Review Queues"
@display "No"

@comment
This queue displays a list of all audits ready for review.

@end-comment

@queue "Review_Open_Q"
@queue-header "Reviews Open / In-Progress (All Times in ET)"
@queue-sort "Review_Due_DateTime asc"
@queue-sequence "405"
@queue-test "not(is-offline()) and (call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Review_Open_Q'))"
@queue-filter "Audit_Completion_Date IS NOT NULL AND (Review_Status = 'Open' or Review_Status = 'Checked Out' or Review_Status = 'Checked In') AND Status NOT IN ('Void')"
@queue-activator "^^All Audits|All Audits^^ | ^^Report Review|Report Review^^"
@queue-allow-new "No"


@column "Account_Name"

@column "Service_Name"

@column "Audit_Type"

@column "Audit_Score_Calc"

@column "Audit_Rating"

@column "Auditor_Full_Name"

@column "Audit_Completion_Date_EST"

@column "Offline_Sync_Up_Date_Time_EST"

@column "Review_Due_DateTime"
@column-ui-style "{{if(date-greater-than(now(0), Review_Due_DateTime_UTC), 'bcolor-danger-light', if(date-greater-than(date-add(now(0), '2 hours'), Review_Due_DateTime_UTC), 'bg-yellow', ''))}}"

@column "Review_Status"

@column "Review_Notes"

@column "Review_Checked_Out_By"

@column "Review_Checked_Out_Time"

@column "Review_Checked_In_Time"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Audit_Report"

@column "Workorder_Number"

@end-queue


@comment
This queue displays a list of all audits ready for review.

@end-comment

@queue "Review_Approved_Q"
@queue-header "Reviews Approved (All Times in ET)"
@queue-sort "Review_Due_DateTime desc"
queue-sort "Review_Approved_DateTime desc"
@queue-sequence "405"
@queue-test "not(is-offline()) and (call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Review_Approved_Q'))"
@queue-filter "Status = 'Closed' and (Review_Status = 'Approved')"
@queue-activator "^^All Audits|All Audits^^ | ^^Report Review|Report Review^^"
@queue-allow-new "No"

queue-row-ui-style "{{if(date-greater-than(Review_Approved_DateTime, Review_Due_DateTime), 'bcolor-danger-light', '')}}"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Type"

@column "Audit_Score_Calc"

@column "Audit_Rating"

@column "Auditor_Full_Name"

@column "Audit_Completion_Date_EST"

@column "Offline_Sync_Up_Date_Time_EST"

@column "Review_Due_DateTime"

@column "Review_Approved_DateTime"

@column "Review_Turn_Around_Time"

@column "Review_From_Checkout_Turn_Around_Time"

@column "Review_Checked_Out_Time"

@column "Review_Checked_In_Time"

@column "Review_Overdue"

@column "Modification_Reason"

@column "Review_Approved_By"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Audit_Report"

@column "Workorder_Number"

@column "Audit"
@column-header "Store ID"

@end-queue

----
@comment
This queue displays a list of all audits needing a review that are within 2 hours of the due date/time.

@end-comment

@queue "Review_Open_Two_Hours_Q"
@queue-header "Reviews Open / In-Progress - Send Reminder Notification"
@queue-sequence "407"
@queue-test "call('Core.IsITUser')"
@queue-filter "Status = 'Closed' and (Review_Status = 'Open' or Review_Status = 'Checked Out' or Review_Status = 'Checked In') and Review_Warning_Notification_Status is NULL and GETUTCDATE() >= DATEADD(HOUR, -2, Review_Due_DateTime_UTC)"
@queue-activator "^^All Audits|All Audits^^ | ^^Report Review|Report Review^^"
@queue-allow-new "No"

@column "Account_Name"

@column "Service_Name"

@column "Audit_Type"

@column "Audit_Score_Calc"

@column "Audit_Rating"

@column "Audit_Completion_Date"

@column "Review_Due_DateTime"

@column "Review_Status"

@column "Review_Checked_Out_By"

@column "Review_Checked_Out_Time"

@column "Review_Checked_In_Time"

@column "Audit_Location_Name"

@column "City"

@column "State"

@column "Review_Warning_Notification_Status"

@column "Audit_Report"

@end-queue