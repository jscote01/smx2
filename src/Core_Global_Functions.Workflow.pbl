﻿@display "No"
@print "No"


@function "Core.IsUserEligibleForRole"
@parameter "$User"
@parameter "$Role"
@parameter "$MetaData1"
@parameter "$MetaData2"
@parameter "$MetaData3"
@parameter "$MetaData4"
@parameter "$MetaData5"
boolean(
    queue-count('Core_Task_Assignment_Workflow_Assignments.All_Q', concat(
        'Role = ', char(39), $Role, char(39),
        ' AND Meta_Data_1 = ', char(39), call('Core.GetValueOrDefault', $MetaData1, 'NA'), char(39), 
        ' AND Meta_Data_2 = ', char(39), call('Core.GetValueOrDefault', $MetaData2, 'NA'), char(39), 
        ' AND Meta_Data_3 = ', char(39), call('Core.GetValueOrDefault', $MetaData3, 'NA'), char(39), 
        ' AND Meta_Data_4 = ', char(39), call('Core.GetValueOrDefault', $MetaData4, 'NA'), char(39), 
        ' AND Meta_Data_5 = ', char(39), call('Core.GetValueOrDefault', $MetaData5, 'NA'), char(39), 
        ' AND Assignee = ', char(39), $User, char(39), ' AND Status_Code = ', char(39), 'Active', char(39)))
> 0)
@end-function

@function "Core.IsUserEligibleForLocalRole"
@parameter "$User"
@parameter "$EntityId"
boolean(
    queue-count('Core_User_Organization_Access.All', concat('User = ', char(39), $User, char(39), ' AND Core_Organization = ', char(39), $EntityId, char(39))) > 0 or
    queue-count('Core_User_Location_Access.All', concat('User = ', char(39), $User, char(39), ' AND Core_Organization_Location = ', char(39), $EntityId, char(39))) > 0
    )
@end-function



@function "Core.GetPrimaryUserForRole"
@parameter "$Role"
@parameter "$MetaData1"
@parameter "$MetaData2"
@parameter "$MetaData3"
@parameter "$MetaData4"
@parameter "$MetaData5"
queue('Core_Task_Assignment_Workflow_Assignments.All_Q', concat(
    'Role = ', char(39), $Role, char(39), 
    ' AND Meta_Data_1 = ', char(39), call('Core.GetValueOrDefault', $MetaData1, 'NA'), char(39), 
    ' AND Meta_Data_2 = ', char(39), call('Core.GetValueOrDefault', $MetaData2, 'NA'), char(39), 
    ' AND Meta_Data_3 = ', char(39), call('Core.GetValueOrDefault', $MetaData3, 'NA'), char(39), 
    ' AND Meta_Data_4 = ', char(39), call('Core.GetValueOrDefault', $MetaData4, 'NA'), char(39), 
    ' AND Meta_Data_5 = ', char(39), call('Core.GetValueOrDefault', $MetaData5, 'NA'), char(39), 
    ' AND Status_Code = ', char(39), 'Active', char(39), ' AND Primary = ', char(39), 'Yes', char(39)), 'Role', 1, 1, '', false())/item[1]/Assignee
@end-function

@function "Core.GetEligibleUserForRole"
@parameter "$Role"
@parameter "$MetaData1"
@parameter "$MetaData2"
@parameter "$MetaData3"
@parameter "$MetaData4"
@parameter "$MetaData5"
queue('Core_Task_Assignment_Workflow_Assignments.All_Q', concat(
    'Role = ', char(39), $Role, char(39), 
    ' AND Meta_Data_1 = ', char(39), call('Core.GetValueOrDefault', $MetaData1, 'NA'), char(39), 
    ' AND Meta_Data_2 = ', char(39), call('Core.GetValueOrDefault', $MetaData2, 'NA'), char(39), 
    ' AND Meta_Data_3 = ', char(39), call('Core.GetValueOrDefault', $MetaData3, 'NA'), char(39), 
    ' AND Meta_Data_4 = ', char(39), call('Core.GetValueOrDefault', $MetaData4, 'NA'), char(39), 
    ' AND Meta_Data_5 = ', char(39), call('Core.GetValueOrDefault', $MetaData5, 'NA'), char(39), 
    ' AND Status_Code = ', char(39), 'Active', char(39), ' AND Primary IS NULL'), 'Role', 1, 1, '', false())/item[1]/Assignee
@end-function

@function "Core.IsRoleLocal"
@parameter "$RoleId"
    boolean(store($RoleId)/Core_Workflow_Role/Is_Local_Workflow_Role = 'true')
@end-function

@function "Core.GetLocalRoleType"
@parameter "$RoleId"
    store($RoleId)/Core_Workflow_Role/Local_Role_Type
@end-function

@function "Core.GetPrimaryOrLocalUserForRole"
@parameter "$Role"
@parameter "$MetaData1"
@parameter "$MetaData2"
@parameter "$MetaData3"
@parameter "$MetaData4"
@parameter "$MetaData5"
@parameter "$OrganizationId"
@parameter "$LocationId"
if(call('Core.IsRoleLocal', $Role),
    if(store($Role)/Core_Workflow_Role/Local_Role_Type = 'Organization',
        call('Core.GetOrgPrimaryContact', $OrganizationId)/StoreId,
        call('Core.GetLocPrimaryContact', $LocationId)/StoreId),
    call('Core.GetPrimaryUserForRole', $Role, $MetaData1, $MetaData2, $MetaData3, $MetaData4, $MetaData5))
@end-function


@function "Core.GetTaskAssignment"
@parameter "$Context"
@parameter "$AdditionalContext"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status IN (', char(39), call('Core.GetStatusStoreId', 'AS_Open', 'Assignment States'), char(39), ',', char(39), call('Core.GetStatusStoreId', 'AS_Pending', 'Assignment States'), char(39) ,' ) AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ') ', 
                if(is-not-blank($AdditionalContext), 
                    concat(' AND Additional_Context = ', char(39), $AdditionalContext, char(39)), ''
                ))
            , 'Assigned_Date DESC', 1, 1, '', false())/item[1]
@end-function


@function "Core.GetOpenTaskAssignee"
@parameter "$Context"
@parameter "$AddContext"
@comment
This function will find an open task for the context and additional context and return the Assignee store id
@end-comment
queue('Core_Task_Assignment.Open_Lookup_Q', concat('Context = ', char(39), $Context, char(39), ' AND Additional_Context = ', char(39), $AddContext, char(39)), 'Assigned_Date DESC', 0, 0, 'Assignee', false())/item[1]/Assignee
@end-function

@function "Core.GetAssigneeOfTask"
@parameter "$TaskId"
queue('Core_Task_Assignment.Master_Q', concat('StoreId = ', char(39), $TaskId, char(39)))/item[1]/Assignee
@end-function



@function "Core.CurrentUserHasOpenTask"
@parameter "$Context"
boolean( count(call('Core.GetOpenTaskAssignmentByUserForContext', $Context, user-id())/item) > 0 )
@end-function

@function "Core.CurrentContextHasOpenTaskForOtherUsers"
@parameter "$Context"
boolean( count(call('Core.GetOpenTaskAssignmentByContextForOtherUsers', $Context, user-id())/item) > 0 )
@end-function


@function "Core.GetModuleOfApp"
@parameter "$AppName"
queue('Core_Admin_Module_Apps.All_Q', concat('App_Name = ', char(39), $AppName, char(39)), 'Core_Admin_Module', -1, -1, 'Core_Admin_Module', false())/item[1]/Core_Admin_Module
@end-function

@function "Core.ModuleSupportPreview"
@parameter "$Module"
if(is-blank(store(call('Core.GetModuleStoreIdByModuleId', $Module), '/store/*[1]/Support_Preview')), false(),boolean-from-string(store(call('Core.GetModuleStoreIdByModuleId', $Module), 			'/store/*[1]/Support_Preview')))
@end-function



@function "Core.GetTaskAssignmentByStatus"
@parameter "$Context"
@parameter "$AdditionalContext"
@parameter "$Status"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status = ', char(39), call('Core.GetStatusStoreId', $Status, 'Assignment States'), char(39), ' AND Context = ', char(39), $Context, char(39), 
                if(is-not-blank($AdditionalContext), 
                    concat(' AND Additional_Context = ', char(39), $AdditionalContext, char(39)), ''
                ))
            , 'Assigned_Date DESC', 1, 1, '', false())/item[1]
@end-function


@function "Core.GetOpenTaskAssignmentByUserForContext"
@parameter "$Context"
@parameter "$UserId"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status = ', char(39), call('Core.GetStatusStoreId', 'AS_Open', 'Assignment States'), char(39), ' AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ') AND Assignee = ', char(39), $UserId, char(39))
                
            , 'Assigned_Date DESC', 0, 0, ' ', false())
@end-function


@function "Core.GetOpenTaskAssignmentForContext"
@parameter "$Context"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status = ', char(39), call('Core.GetStatusStoreId', 'AS_Open', 'Assignment States'), char(39), ' AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ')')
                
            , 'Assigned_Date DESC', 0, 0, ' ', false())
@end-function

@function "Core.GetOpenOrPendingTaskAssignmentForContext"
@parameter "$Context"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status_Code IN (', char(39), 'AS_Open', char(39), ',',char(39), 'AS_Pending', char(39), ') AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ')')
                
            , 'Assigned_Date DESC', 0, 0, ' ', false())
@end-function

@function "Core.GetOpenTaskAssignmentByContextForOtherUsers"
@parameter "$Context"
@parameter "$UserId"
queue('Core_Task_Assignment.Master_Q', 
            concat('Status = ', char(39), call('Core.GetStatusStoreId', 'AS_Open', 'Assignment States'), char(39), ' AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ')  AND Assignee != ', char(39), $UserId, char(39))
                
            , 'Assigned_Date DESC', 0, 0, ' ', false())
@end-function

@function "Core.GetOverridableTaskAssignmentsForContextAndRole"
@parameter "$Context"
@parameter "$Role"
queue('Core_Task_Assignment.Master_Q', 
            concat('((Status = ', char(39), call('Core.GetStatusStoreId', 'AS_Open', 'Assignment States'), char(39), ' OR Status = ', char(39), call('Core.GetStatusStoreId', 'AS_Pending', 'Assignment States'), char(39),
            ' ) AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ')) AND Additional_Context = ', char(39), $Role, char(39))
                
            , 'Assigned_Date DESC', 0, 0, '', false())
@end-function


@function "Core.GetTaskAssignmentOverrideForContextAndAdditionalContext"
@parameter "$Context"
@parameter "$AdditionalContext"
queue('Core_Task_Assignment_Override.Master_Q', 
            concat('Context = ', char(39), $Context, char(39), ' AND Additional_Context = ', char(39), $AdditionalContext ,char(39), ' AND Status_Code = ', char(39), 'Active',char(39))
            , 'Override_Date', 1, 1, '', false())/item[1]
@end-function

@function "Core.GetTaskAssignmentOverrideForContext"
@parameter "$Context"
queue('Core_Task_Assignment_Override.Master_Q', 
            concat('Context = ', char(39), $Context, char(39), ' AND Status_Code = ', char(39), 'Active',char(39))
            , 'Override_Date', -1, -1, '', false())
@end-function

@function "Core.GetTaskAssignmentOverrideForContextAndAssignee"
@parameter "$Context"
@parameter "$Assignee"
queue('Core_Task_Assignment_Override.Master_Q', 
            concat('Context = ', char(39), $Context, char(39), ' AND Assignee = ', char(39), $Assignee ,char(39), ' AND Status_Code = ', char(39), 'Active',char(39))
            , 'Override_Date', -1, -1, '', false())
@end-function


@function "Core.GetLatestTaskAssignmentsForContext"
@parameter "$Context"
queue('Core_Task_Assignment_Analyze.Ranked_Q', concat('Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39)), 'StoreId', -1, -1, '', false())
@end-function


@function "Core.GetLatestTaskAssignmentsForContextAndAdditionalContext"
@parameter "$Context"
@parameter "$AdditionalContext"
queue('Core_Task_Assignment_Analyze.Ranked_Q', concat('Additional_Context = ', char(39), $AdditionalContext, char(39), ' AND (Context = ', char(39), $Context, char(39), ' OR Parent_Context = ', char(39), $Context, char(39), ')'), 'StoreId', -1, -1, '', false())
@end-function


@function "Core.GetTaskAssignmentStatusCode"
@parameter "$AssignmentId"
store($AssignmentId, '/store/Core_Task_Assignment/Status/Status_Code')
@end-function

@function "Core.GetTaskAssignmentExpirationDate"
@parameter "$AssignmentId"
store($AssignmentId, '/store/Core_Task_Assignment/Expiration_Date')
@end-function

@function "Core.GetTaskAssignmentIncompletionDate"
@parameter "$AssignmentId"
store($AssignmentId, '/store/Core_Task_Assignment/Incompletion_Date')
@end-function

@function "Core.GetTaskAssignmentActivationDate"
@parameter "$AssignmentId"
store($AssignmentId, '/store/Core_Task_Assignment/Activation_Date')
@end-function


@function "Core.GetTaskAssigneeOrPrimary"
@parameter "$Context"
@parameter "$Role"
@parameter "$MetaData1"
@parameter "$MetaData2"
@parameter "$MetaData3"
@parameter "$MetaData4"
@parameter "$MetaData5"
if(is-not-blank(call('Core.GetTaskAssignmentByStatus', $Context, $Role, 'AS_Completed')), 
    call('Core.GetTaskAssignmentByStatus', $Context, $Role, 'AS_Completed')/Assignee, 
    if(is-not-blank(call('Core.GetTaskAssignment', $Context, $Role)), 
        call('Core.GetTaskAssignment', $Context, $Role)/Assignee, 
        call('Core.GetPrimaryOrLocalUserForRole', $Role, $MetaData1, $MetaData2, $MetaData3, $MetaData4, $MetaData5, '', '')))
@end-function

@function "Core.GetMetaDataValuePath"
@parameter "$Account"
@parameter "$Module"
@parameter "$MetaDataNumber"
store(queue('Core_Configuration.Master_Q', concat('Core_Account = ', char(39), $Account, char(39)))/item[1]/StoreId)/Core_Configuration/Module_Meta_Data_Configuration/item[Module/@refId = $Module]/*[name(.) = concat('Meta_Data_', $MetaDataNumber)]/Meta_Data_Display_Value_Path
@end-function

@function "Core.GetActiveRoles"
@parameter "$Account"
@parameter "$Module"
queue('Core_Workflow_Role.Active_Lookup_Q', concat('Module = ', char(39), $Module, char(39), ' AND (Core_Account IS NULL OR Core_Account = ', char(39), $Account, char(39), ')'), 'Sequence ASC', -1, -1, 'Workflow_Role_ID;Workflow_Role_Name;Workflow_Role_Applies_To', true())
@end-function


@function "Core.GetRoleIdByName"
@parameter "$RoleName"
@comment
Get the StoreId of the Role whose name is given
  #param String $RoleName Name of the Role to find
  #returns StoreId
@end-comment
find-store-id(
    'Core_Workflow_Role.Master_Q', 
    concat('Workflow_Role_Name = ', char(39), $RoleName, char(39)), 
    'Workflow_Role_Name'
)
@end-function


@function "Core.GetRoleIdByID"
@parameter "$RoleID"
@comment
Get the StoreId of the Role whose ID is given
  #param String $RoleID ID of the Role to find
  #returns StoreId
@end-comment
find-store-id(
    'Core_Workflow_Role.Master_Q', 
    concat('Workflow_Role_ID = ', char(39), $RoleID, char(39)), 
    'Workflow_Role_ID'
)
@end-function


@function "Core.GetRoleNameByID"
@parameter "$RoleID"
@comment
Get the Workflow_Role_Name field of the Role whose ID is given
  #param String $RoleID ID of the Role to find
  #returns String
@end-comment
queue('Core_Workflow_Role.Master_Q', concat('Workflow_Role_ID = ', char(39), $RoleID, char(39)), ' ', 0, 0, 'Workflow_Role_Name', true())/item[1]/Workflow_Role_Name
@end-function


@function "Core.GetModuleStoreIdByModuleId"
@parameter "$Module"
queue('Core_Admin_Module.Master_Q', concat('Module_Id = ', char(39), $Module, char(39)))/item[1]/StoreId
@end-function


@function "Core.GetModuleStoreIdByModuleName"
@parameter "$Module"
queue('Core_Admin_Module.Master_Q', concat('Module_Name = ', char(39), $Module, char(39)))/item[1]/StoreId
@end-function


@function "Core.GetDefaultApproverByRoleID"
@parameter "$Role"
queue('Core_Workflow_Role_Default_Approver.Master_Q', concat('Core_Workflow_Role = ', char(39), call('Core.GetRoleIdByID', $Role), char(39)), 'Workflow_Role_ID', -1, -1, 'Default_Approver', true())/item[1]/Default_Approver
@end-function

@function "Core.GetDefaultApproverByRole"
@parameter "$Role"
queue('Core_Workflow_Role_Default_Approver.Master_Q', concat('Core_Workflow_Role = ', char(39), $Role, char(39)), 'Workflow_Role_ID', -1, -1, 'Default_Approver', true())/item[1]/Default_Approver
@end-function


@function "Core.GetWorkbookRefId"
@parameter "$WorkbookName"
queue('Extension.List', concat('name = ', char(39), $WorkbookName, char(39)), 'name', -1, -1, 'name', false())/item[1]/StoreId
@end-function

@function "Core.GetAuditFormVersionWorkbook"
@parameter "$AuditFormVersionRefId"
queue('Core_Audit_Form_Version.Master_Q', concat('StoreId = ', char(39), $AuditFormVersionRefId, char(39)), 'Audit_Workbook', -1, -1, 'Audit_Workbook', false())/item[1]/Audit_Workbook
@end-function

@function "Core.GetQuestionLibraryWorkbook"
'Core_Audit_Question_Details'
@end-function

@function "Core.GetTaskDueDate"
@parameter "$TokenId"
@comment
Get the due date for a task based on a token id. This assumes an application that matches the token Id to an xpath expression to calculate the due date
@end-comment
now()
@end-function

@function "Core.TaskListHTML"
concat('<ul>', join-format(call('Core.GetOpenTaskAssignmentByUserForContext', store-id(), user-id())/item, replace('<li>{{Summary} }</li>', ' }', '}'), ' '),'</ul>')
@end-function

@function "Core.OpenTaskListForOtherUsersHTML"
concat('<ul>', join-format(call('Core.GetOpenTaskAssignmentByContextForOtherUsers', store-id(), user-id())/item, concat('<li>', replace('{{Action_Item_Text} }', ' }', '}'), ' <b>(Assigned to: ', replace('{{Assignee_Informal_Full_Name} }', ' }', '}'), ')</b>', '</li>'), ' '),'</ul>')
@end-function

@function "Core.GetCompletedTaskAssignee"
@parameter "$Context"
@parameter "$RoleName"
queue('Core_Task_Assignment.Master_Q', concat('Status_Code = ', char(39),'AS_Completed',char(39), ' AND (Context = ', char(39),$Context,char(39), ' OR Parent_Context = ', char(39),$Context,char(39), ') AND Additional_Context = ', char(39), coalesce(call('Core.GetRoleIdByName',$RoleName), call('Core.GetRoleIdByID',$RoleName)), char(39)), 'Assigned_Date DESC', 0, 0, 'Assignee', false())/item[1]/Assignee
@end-function

@function "Core.TaskOverrideSQLFilter"
concat('(Context = ', char(123),char(123),'store-id()',char(125),char(125), ' OR Parent_Context = ', char(123),char(123),'store-id()',char(125),char(125), ') AND ISNULL(User,-1) <> ', char(123),char(123),'user-id()',char(125),char(125))
@end-function