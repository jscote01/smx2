﻿@display "No"
@print "No"


@execute
@execute-when "is-not-blank(New_Worksheet) and is-blank(New_Worksheet/Core_Organization) and call('Core.GetCurrentUserOrganizationCount') = 1"
    @update
    @update-target-select "$Single_Org_StoreId"
    @update-source-select "queue('Core_Organization.Active_Lookup_Q', '1=1')/item[1]/StoreId"
    
    @update
    @update-target-select "New_Worksheet/Core_Organization"
    @update-source-select "$Single_Org_StoreId"
    @update-source-type "Lookup"

    @update
    @update-target-select "Organization"
    @update-source-select "$Single_Org_StoreId"
    @update-source-type "Lookup"
@end-execute

@execute
@execute-when "call('CoreEM.IsCreateUser') and is-blank(New_Worksheet) and call('CoreEM.IsPendingSubmission')"
    @invoke
    @invoke-execute "Core_Entity_Management.SingleLocationUserSetup"
    @invoke-when "call('CoreEM.IsCreateUser') and is-blank(New_Worksheet/Organization_Locations) and call('Core.GetCurrentUserLocationCount') = 1"
        
    @update
    @update-target-select "New_Worksheet/Status"
    @update-source-select "call('Core.GetStatusStoreId', 'Active', 'Active States')"
    @update-source-type "Lookup"
    @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"        
        
    @update
        @update-target-select "$none"
        @update-source-select "''"
        @update-run-views "New_Worksheet"   
@end-execute

@comment
(1) Copy existing values if UPDATING
@end-comment
@execute
@execute-when "call('CoreEM.IsUpdateUser') and call('CoreEM.IsPendingSubmission') and is-blank(New_Worksheet) and is-not-blank(Contact)"
    @update
    @update-target-select "New_Worksheet"
    @update-source-select "store(Contact/@refId)/Core_Contact"
    @update-source-type "Node"
    
    @update
    @update-target-select "New_Worksheet/Associated_To_Organization"
    @update-source-select "if(count(New_Worksheet/Organizations/value) > 0, 'Yes', 'No')"
    
    @update
    @update-target-select "New_Worksheet/Associated_To_Organization_Location"
    @update-source-select "if(count(New_Worksheet/Organization_Locations/value) > 0, 'Yes', 'No')"
    
    @update
        @update-target-select "$none"
        @update-source-select "''"
        @update-run-views "New_Worksheet"
@end-execute

@execute
@execute-when "call('CoreEM.IsDeleteUser') and call('CoreEM.IsPendingApproval') and is-blank(New_Worksheet) and is-not-blank(Contact)"
    @update
    @update-target-select "New_Worksheet"
    @update-source-select "store(Contact/@refId)/Core_Contact"
    @update-source-type "Node"
    
    @update
    @update-target-select "New_Worksheet/Associated_To_Organization"
    @update-source-select "if(count(New_Worksheet/Organizations/value) > 0, 'Yes', 'No')"
    
    @update
    @update-target-select "New_Worksheet/Associated_To_Organization_Location"
    @update-source-select "if(count(New_Worksheet/Organization_Locations/value) > 0, 'Yes', 'No')"
    
    @update
    @update-target-select "New_Worksheet/Status"
    @update-source-select "call('Core.GetDeletedStatusStoreId', 'Active States')"
    @update-source-type "Lookup"
    @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"
    
    @update
        @update-target-select "$none"
        @update-source-select "''"
        @update-run-views "New_Worksheet"   
@end-execute

@execute
@execute-when "EM_Request_Type/Entity_Type = 'User' and is-not-blank(Organization) and is-blank(New_Worksheet/Core_Organization)"
    @update
    @update-target-select "New_Worksheet/Core_Organization"
    @update-source-select "Organization/@refId"
    @update-source-type "Lookup"
    @update-source-lookup-select "Core_Organization/Organization_Name | Core_Organization/Organization_ID | Core_Organization/Business_Type"
@end-execute

@execute
@execute-when "((call('CoreEM.IsUpdateUser') and call('CoreEM.IsPendingSubmission')) or (call('CoreEM.IsDeleteUser') and call('CoreEM.IsPendingApproval'))) and is-blank(Old_Worksheet) and is-not-blank(New_Worksheet)"
    @update
    @update-target-select "Old_Worksheet"
    @update-source-select "New_Worksheet"
    @update-source-type "Node"  
@end-execute



@comment
(2) Complete UPDATE on "APPROVED"
@end-comment
@execute
    @execute-when "call('CoreEM.IsUpdateUser') and Status/Status_Code = 'EM_Approved' and is-blank(Updated_Contact) and not(call('CoreEM.IsUserType'))"

    @update
    @update-target-select "$EMUC_Vars"
    @update-source-type "Node"
    
    @update
    @update-target-select "$EMUC_Vars/ContactStoreId"
    @update-source-select "/store/*[1]/Contact/@refId"
    
    @update
    @update-target-select "$EMUC_Vars/ContactWorksheet"
    @update-source-select "New_Worksheet"
    @update-source-type "Node"
    @update
    @update-target-select "$EMUC_Vars/ContactWorksheet/User_Worksheet"
    @update-source-type "Node"
    @update
    @update-target-select "$EMUC_Vars/ContactWorksheet/*[starts-with(local-name(), 'Profile_')]"
    @update-source-type "Node"
    @update
    @update-target-select "$EMUC_Vars/ContactWorksheet/Organizations"
    @update-source-select "''"
    
    @invoke
    @invoke-execute "Core_Contact.Update"
    @invoke-parameter "StoreId"
    @invoke-parameter-expression "$EMUC_Vars/ContactStoreId"
    @invoke-parameter-direction "In Out"
    @invoke-parameter "ContactNodes"
    @invoke-parameter-expression "$EMUC_Vars/ContactWorksheet"
    
    @invoke
    @invoke-execute "Core_Entity_Management.HandleRelatedUser"
    @invoke-when "is-blank(Updated_User)"
    @invoke-parameter "ContactStoreId"
    @invoke-parameter-expression "$EMUC_Vars/ContactStoreId"
    @invoke-parameter "WorksheetNodes"
    @invoke-parameter-expression "$EMUC_Vars/ContactWorksheet"
    @invoke-parameter "UserId"
    @invoke-parameter-expression "$EMUC_Vars/UserId"
    @invoke-parameter-direction "Out"
    
    @invoke
    @invoke-execute "Core_Contact.AddOrganization"
    @invoke-when "is-not-blank($EMUC_Vars/ContactWorksheet/Core_Organization) and $EMUC_Vars/ContactWorksheet/Associated_To_Organization = 'Yes'"
    @invoke-store-id "concat('Core_Contact.', $EMUC_Vars/ContactStoreId)"
    @invoke-parameter "Organization_ID"
    @invoke-parameter-expression "$EMUC_Vars/ContactWorksheet/Core_Organization/Organization_ID"
    
    @invoke
    @invoke-execute "Core_Contact.RemoveOrganization"
    @invoke-when "is-not-blank($EMUC_Vars/ContactWorksheet/Core_Organization) and $EMUC_Vars/ContactWorksheet/Associated_To_Organization != 'Yes'"
    @invoke-store-id "concat('Core_Contact.', $EMUC_Vars/ContactStoreId)"
    @invoke-parameter "Organization_ID"
    @invoke-parameter-expression "$EMUC_Vars/ContactWorksheet/Core_Organization/Organization_ID"
    
    @invoke
    @invoke-execute "Core_Entity_Management.AddNewLocations"
    @invoke-when "$EMUC_Vars/ContactWorksheet/Associated_To_Organization_Location = 'Yes'"
    @invoke-parameter "ContactStoreId"
    @invoke-parameter-expression "$EMUC_Vars/ContactStoreId"
    @invoke-parameter "UserStoreId"
    @invoke-parameter-expression "$EMUC_Vars/UserId"

    @invoke
    @invoke-execute "Core_Entity_Management.RemoveOldLocations"
    @invoke-parameter "ContactStoreId"
    @invoke-parameter-expression "$EMUC_Vars/ContactStoreId"
    @invoke-parameter "UserStoreId"
    @invoke-parameter-expression "$EMUC_Vars/UserId"
    
    @update
    @update-store-id "concat('Core_Contact.', $EMUC_Vars/ContactStoreId)"
    @update-target-select "/store/Core_Contact/Update_Contacts"
    @update-source-select "'Yes'"
    @update-run-workbook "Yes"

    @update
    @update-target-select "Updated_Contact"
    @update-source-select "now(0)"
@end-execute


@comment
(3) Complete CREATE on "APPROVED"
@end-comment
@execute
    @execute-when "call('CoreEM.IsCreateUser') and Status/Status_Code = 'EM_Approved' and not(call('CoreEM.IsUserType'))"


    @invoke
        @invoke-execute "User.BuildUserNodesetFromContactNodeset"
        @invoke-when "New_Worksheet/Licensed_User = 'Yes' and is-blank(Created_User)"
        @invoke-parameter "ContactNodes"
            @invoke-parameter-expression "New_Worksheet"
        @invoke-parameter "ProfileList"
            @invoke-parameter-expression "join(/store/Core_Entity_Management/EM_Request_Type/Profiles/value, ',')"
        @invoke-parameter "FirewallList"
            @invoke-parameter-expression "New_Worksheet/DFW_List"
        @invoke-parameter "UserNodes"
            @invoke-parameter-expression "New_Worksheet/User_Worksheet"
            @invoke-parameter-direction "Out"
    
    @invoke
        @invoke-execute "Core_Entity_Management.CreateUserAndFirewalls"
        @invoke-when "New_Worksheet/Licensed_User = 'Yes' and is-blank(Created_User)"
        @invoke-parameter "WorksheetNodes"
            @invoke-parameter-expression "New_Worksheet/User_Worksheet"
        @invoke-parameter "UserStoreId"
            @invoke-parameter-expression "$User_StoreID"
            @invoke-parameter-direction "Out"
    
    @update
        @update-target-select "New_Worksheet/User"
        @update-source-select "$User_StoreID"
        @update-source-type "Lookup"
        @update-source-lookup-select "User/User_Name | User/Informal_Full_Name"

    @invoke
        @invoke-execute "Core_Entity_Management.CreateContact"
        @invoke-when "is-blank(Created_Contact)"
        @invoke-parameter "WorksheetNodes"
            @invoke-parameter-expression "New_Worksheet"
        @invoke-parameter "ContactStoreId"
            @invoke-parameter-expression "$Contact_StoreID"
            @invoke-parameter-direction "Out"
@end-execute


@comment
(4) Complete DELETE on "APPROVED"
@end-comment
@execute
    @execute-when "call('CoreEM.IsDeleteUser') and Status/Status_Code = 'EM_Approved' and is-blank(Deleted_Contact)"
    
    @invoke
        @invoke-execute "Core_Contact.Delete"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "Contact/@refId"
    
    @invoke
        @invoke-execute "Core_Entity_Management.DeleteUser"
        @invoke-when "is-not-blank(New_Worksheet/User/@refId)"
        @invoke-parameter "UserId"
            @invoke-parameter-expression "New_Worksheet/User/@refId"
    
    @update
        @update-target-select "Deleted_Contact"
        @update-source-select "now(0)"
@end-execute




@execute "CreateContact"
    @update
    @update-target-select "$Parameter:WorksheetNodes/Status"
    @update-source-select "call('Core.GetStatusStoreId', 'Active', 'Active States')"
    @update-source-type "Lookup"
    @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"     
    @update
    @update-target-select "$Parameter:WorksheetNodes/Contact_Type"
    @update-source-select "''"
    @update
    @update-target-select "$Parameter:WorksheetNodes/User_Worksheet"
    @update-source-type "Node"
    @update
    @update-target-select "$Parameter:WorksheetNodes/*[starts-with(local-name(), 'Profile_')]"
    @update-source-type "Node"
    
    @invoke
    @invoke-execute "Core_Contact.Create"
    @invoke-parameter "ContactNodes"
    @invoke-parameter-expression "$Parameter:WorksheetNodes"
    @invoke-parameter "NewStoreId"
    @invoke-parameter-expression "$Parameter:ContactStoreId"
    @invoke-parameter-direction "Out"
    
    @invoke
    @invoke-execute "Core_Contact.AddOrganization"
    @invoke-when "is-not-blank($Parameter:WorksheetNodes/Core_Organization) and $Parameter:WorksheetNodes/Associated_To_Organization = 'Yes'"
    @invoke-store-id "concat('Core_Contact.', $Parameter:ContactStoreId)"
    @invoke-parameter "Organization_ID"
    @invoke-parameter-expression "$Parameter:WorksheetNodes/Core_Organization/Organization_ID"
    
    @invoke
    @invoke-execute "Core_Data_Firewall.CreateOrganizationFirewall"
    @invoke-when "is-not-blank($Parameter:WorksheetNodes/User) and is-not-blank($Parameter:WorksheetNodes/Core_Organization) and $Parameter:WorksheetNodes/Associated_To_Organization = 'Yes'"
    @invoke-parameter "UserId"
    @invoke-parameter-expression "$Parameter:WorksheetNodes/User/@refId"
    @invoke-parameter "OrganizationId"
    @invoke-parameter-expression "$Parameter:WorksheetNodes/Core_Organization/@refId"
    
    @invoke
    @invoke-execute "Core_Entity_Management.AddNewLocations"
    @invoke-when "is-not-blank($Parameter:WorksheetNodes/Organization_Locations) and $Parameter:WorksheetNodes/Associated_To_Organization_Location = 'Yes'"
    @invoke-parameter "ContactStoreId"
    @invoke-parameter-expression "$Parameter:ContactStoreId"
    @invoke-parameter "UserStoreId"
    @invoke-parameter-expression "$Parameter:WorksheetNodes/User/@refId"
    
    @update
    @update-store-id "concat('Core_Contact.', $Parameter:ContactStoreId)"
    @update-target-select "/store/Core_Contact/Update_Contacts"
    @update-source-select "'Yes'"
    @update-run-workbook "Yes"

    @update
    @update-target-select "Newly_Created_StoreId"
    @update-source-select "$Parameter:ContactStoreId"
    @update
    @update-target-select "Created_Contact"
    @update-source-select "now(0)"
@end-execute

@execute "AddNewLocations"
@execute-select "New_Worksheet/Organization_Locations/value[is-not-blank(.)]"
@execute-select-key "."
    @update
    @update-target-select "$NotInOldWorksheet"
    @update-source-select "if(is-blank(/store/Core_Entity_Management/Old_Worksheet/Organization_Locations[value = execute-select-context()]), 1, 0)"

    @invoke
    @invoke-execute "Core_Contact.AddLocation"
    @invoke-when "$NotInOldWorksheet = 1"
    @invoke-store-id "concat('Core_Contact.', $Parameter:ContactStoreId)"
    @invoke-parameter "Location_ID"
    @invoke-parameter-expression "store(execute-select-context())/Core_Organization_Location/Location_ID"
    
    @invoke
    @invoke-execute "Core_Data_Firewall.CreateLocationFirewall"
    @invoke-when "$NotInOldWorksheet = 1 and call('CoreEM.IsCoreUserRequest')"
    @invoke-parameter "LocationId"
    @invoke-parameter-expression "execute-select-context()"
    @invoke-parameter "UserId"
    @invoke-parameter-expression "$Parameter:UserStoreId"
@end-execute

@execute "RemoveOldLocations"
@execute-select "Old_Worksheet/Organization_Locations/value[is-not-blank(.)]"
@execute-select-key "."
    @update
        @update-target-select "$Local:NotInNewWorksheet"
        @update-source-select "if(/store/Core_Entity_Management/New_Worksheet/Associated_To_Organization_Location != 'Yes' or is-blank(/store/Core_Entity_Management/New_Worksheet/Organization_Locations[value = execute-select-context()]), 1, 0)"

    @invoke
        @invoke-execute "Core_Contact.RemoveLocation"
        @invoke-when "$Local:NotInNewWorksheet = 1"
        @invoke-store-id "concat('Core_Contact.', $Parameter:ContactStoreId)"
        @invoke-parameter "Location_ID"
            @invoke-parameter-expression "store(execute-select-context())/Core_Organization_Location/Location_ID"

    @invoke
        @invoke-execute "Core_Data_Firewall.RevokeLocationFirewall"
        @invoke-when "$Local:NotInNewWorksheet = 1 and call('CoreEM.IsCoreUserRequest')"
        @invoke-parameter "LocationId"
            @invoke-parameter-expression "execute-select-context()"
        @invoke-parameter "UserId"
            @invoke-parameter-expression "$Parameter:UserStoreId"
@end-execute

@execute "SingleLocationUserSetup"
    @update
        @update-target-select "$Local:Current_User_Location_StoreId"
        @update-source-select "queue('Data_Firewall.Active_Lookup_Q',call('Core_Data_Firewall.AllLocsFilter',user-id()))/item[1]/Access_Tag_Core_Organization_Location"

    @update
        @update-target-select "$Local:Current_User_Location"
        @update-source-select "queue('Core_Organization_Location.Master_Q', 'StoreId = {{$Local:Current_User_Location_StoreId}}')/item[1]"
        @update-source-type "Node"

    @if "is-not-blank($Local:Current_User_Location/Core_Organization_ID)"
        @update
            @update-target-select "Organization"
            @update-source-select "$Local:Current_User_Location/Core_Organization"
            @update-source-type "Lookup"
    @end-if
    
    @if "is-not-blank($Local:Current_User_Location_StoreId)"
        @update
            @update-target-select "Organization_Location"
            @update-source-select "$Local:Current_User_Location_StoreId"
            @update-source-type "Lookup"
    @end-if

    @update
        @update-target-select "New_Worksheet/Organization_Locations"
        @update-source-select "''"

    @update
        @update-target-select "New_Worksheet/Organization_Locations/@all"
        @update-source-select "'false'"

    @update
        @update-target-select "$Local:NewNode/value"
        @update-source-select "$Local:Current_User_Location_StoreId"

    @update
        @update-target-select "$Local:NewNode/value/@text"
        @update-source-select "$Local:Current_User_Location/Organization_Dash_Location"

    @update
        @update-target-select "New_Worksheet/Organization_Locations"
        @update-source-select "$Local:NewNode/value"
        @update-source-type "Subnodes[Append: Yes]"
        @update-run-workbook "Yes"
@end-execute