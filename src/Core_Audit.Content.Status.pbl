﻿@mode "Information"
@title "^^Audit Status|Audit Status^^"
    @title-icon "icon-list-ol"
----
@comment
BUTTONS
@end-comment

@block
    @block-visible-when "is-blank(Status)"
    
    @button "Status"
        @button-value "Assigned"
        @button-text "^^Set Status to Assigned|Set Status to Assigned^^"
        @button-ui-style "btn-inverse"
        @button-show-value "No"
        @button-action "SaveAndValidate"
    
    @button "Status"
        @button-value "Scheduled"
        @button-text "^^Set Status to Scheduled|Set Status to Scheduled^^"
        @button-ui-style "btn-inverse"
        @button-show-value "No"
        @button-action "SaveAndValidate"
@end-block

@block
    @block-visible-when "Status = 'Assigned'"
    
    @button "Status"
        @button-value "Scheduled"
        @button-text "^^Change Status to Scheduled|Change Status to Scheduled^^"
        @button-action "SaveAndReload"
        @button-show-value "No"
        @button-ui-style "btn-inverse"
@end-block

@block
    @block-visible-when "Status = 'Scheduled' "
    @block-navigate "_Next"
@comment
    @button "Preview_Audit"
        @button-value "Approved"
        @button-text "^^Preview Audit Form|Preview Audit Form^^"
        @button-show-value "No"
        @button-optional "Yes"
@end-comment
@end-block

@comment
IN-PROGRESS
8/1/2014 - cmcdonald
Temporarily remove restrictins on conducting audit online for QMS
added not(is-offline()) or...
@end-comment

@block
@block-visible-when "Status = 'Scheduled' and 
(Auditor = user-id() or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or (Core_Audit_Service/Require_Review = 'true' and (call('Core.CheckPermission', 'Core_Audit.Audit_Review_Manage_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Review_View')))) and 
(is-offline() or call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View')) and 
date-equal(Audit_Scheduled_Date, date-of(now())) and is-blank(Linked_Workorder_Activity_ID)"
    @button "Status"
        @button-value "In-Progress"
        @button-text "^^Start Audit|Start Audit^^"
        @button-show-value "No"
        @button-action "SaveAndReload"
        @button-ui-style "btn-success"
        @button-icon "icon-ok"
        @button-align "Left"
@end-block

@block
@block-visible-when "Status = 'Scheduled' and 
(Auditor = user-id() or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or (Core_Audit_Service/Require_Review = 'true' and (call('Core.CheckPermission', 'Core_Audit.Audit_Review_Manage_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Review_View')))) and 
(is-offline() or call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View')) and 
date-equal(Audit_Scheduled_Date, date-of(now())) and is-not-blank(Linked_Workorder_Activity_ID)"
    @button "Status"
        @button-value "In-Progress"
        @button-text "^^Start Audit|Start Audit^^"
        @button-confirm "^^Linked Audit Message|This is a linked audit. Make sure you click the 'Open Linked Audit' button inside the audit form. Click Okay to continue.^^"
        @button-show-value "No"
        @button-action "SaveAndReload"
        @button-ui-style "btn-success"
        @button-icon "icon-ok"
        @button-align "Left"
@end-block


@block
@block-visible-when "Status = 'Scheduled' and 
(Auditor = user-id() or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or (Core_Audit_Service/Require_Review = 'true' and (call('Core.CheckPermission', 'Core_Audit.Audit_Review_Manage_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Review_View')))) and 
(is-offline() or call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View')) and 
not(date-equal(Audit_Scheduled_Date, date-of(now())))"
    @button "Status"
        @button-value "In-Progress"
        @button-text "^^Start Audit|Start Audit^^"
        @button-confirm "^^The Scheduled Date is not today|The Scheduled Date for this audit is not today. Please verify the address. Do you still want to start the audit^^?"
        @button-show-value "No"
        @button-action "SaveAndReload"
        @button-ui-style "btn-success"
        @button-icon "icon-ok"
        @button-align "Left"
@end-block

@comment
STATUS CHANGES FOR ADMIN/TESTING PURPOSES
@end-comment

@block
@block-visible-when "call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Support_View') or (Core_Audit_Service/Require_Review = 'true' and (call('Core.CheckPermission', 'Core_Audit.Audit_Review_Manage_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Review_View')))"
    @block
    @block-visible-when "Status = 'Closed' or Status = 'Refused Entry' or is-not-blank(Modify_Closed_Audit)"
        @checkbox "Modify_Closed_Audit"
        @checkbox-header "^^Modify Closed Audit|Modify Closed Audit^^?"
        @checkbox-expression "If(Status='Closed' and (is-changed('Status') or is-changed('Status_Check') or is-not-blank(Status_Change_Event)), '', Modify_Closed_Audit)"
        @checkbox-read-only-when "Status = 'Assigned' or Status = 'Scheduled' or Status = 'In-Progress' or Status = 'Completed' or (Review_Status = 'Checked Out' and Review_Checked_Out_ID != user-id())"
        @block
        @block-visible-when "Review_Status = 'Checked Out' and Review_Checked_Out_ID != user-id()"
        @label "Review_Checked_Out_By"
        @label-header "^^Checked Out By|Checked Out By^^:"
        @label-ui-style "bcolor-danger-light"
        @end-block
    @end-block

    @block
    @block-visible-when "is-blank(Status) or Status = 'Assigned' or Status = 'Scheduled' or Status = 'In-Progress' or Status = 'Completed' or Status = 'Void' or Status = 'Refused Entry' or is-not-blank(Modify_Closed_Audit)"
    
        @choice "Status"
        @choice-style "DropDownList"
        @choice-header "^^Status changes for Admin Purposes|Status changes for Admin Purposes^^:"
	@option
    	    @option-text "^^Assigned|Assigned^^"
	    @option-value "Assigned"
	@option
    	    @option-text "^^Scheduled|Scheduled^^"
	    @option-value "Scheduled"
	@option
    	    @option-text "^^In-Progress|In-Progress^^"
	    @option-value "In-Progress"
	@option
    	    @option-text "^^Completed|Completed^^"
	    @option-value "Completed"
	@option
    	    @option-text "^^Closed|Closed^^"
	    @option-value "Closed"
	@option
    	    @option-text "^^Void|Void^^"
	    @option-value "Void"
	@end-choice
        
        @block
        @block-visible-when "is-not-blank(Modify_Closed_Audit) and (call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View'))"

            @list "Modified_Closed_Audit_List"
                @list-style "Discussion"
                @list-add-label "^^Add New|Add New^^ ^^Reason|Reason^^"
                @list-header "^^Reasons for Modifying Closed Audits|Reasons for Modifying Closed Audits^^"
                @list-header-ui-style "default primary"
                @list-theme "ColoredButtons"
                
                @text "Closed_Audit_Modification_Reason"
                @text-header "^^Reason|Reason^^:"
                @text-rows "4"
                
                @text "Closed_Audit_Modification_User"
                @text-header "^^User|User^^:"
                @text-read-only-when "true()"
                @text-expression "If(is-blank(Closed_Audit_Modification_User), user()/User_Name, Closed_Audit_Modification_User)"
                
                @text "Closed_Audit_Modification_Date"
                @text-header "^^Date|Date^^:"
                @text-type "DateTime"
                @text-read-only-when "true()"
                @text-expression "If(is-blank(Closed_Audit_Modification_Date), now(), Closed_Audit_Modification_Date)"
            
            @end-list
            @newline "Small"
        
        @end-block
    @end-block
@end-block

@comment
----
Show Modification reasons read-only when they exist and user's not actively adding new reasons
@end-comment
@block
@block-visible-when "(Status = 'Closed' or Status = 'Refused Entry') and is-blank(Modify_Closed_Audit) and is-not-blank(Modified_Closed_Audit_List/item[1]/Closed_Audit_Modification_Reason)"

    @list "Modified_Closed_Audit_List"
        @list-read-only-when "true()"
        @list-style "Discussion"
        @list-header "^^Reasons for Modifying Closed Audits|Reasons for Modifying Closed Audits^^"
        
        @text "Closed_Audit_Modification_Reason"
        @text-header "^^Reason|Reason^^:"
        @text-rows "4"
        @text-read-only-when "true()"
        
        @text "Closed_Audit_Modification_User"
        @text-header "^^User|User^^:"
        @text-read-only-when "true()"
        
        @text "Closed_Audit_Modification_Date"
        @text-header "^^Date|Date^^:"
        @text-type "DateTime"
        @text-read-only-when "true()"
    
    @end-list

@end-block

@block
@block-visible-when "Status = 'Closed' and (call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Support_View'))"
    @checkbox "Re_Export"
    @checkbox-header "^^Manually mark audit for re-export|Manually mark audit for re-export^^?"
    @checkbox-footer "^^Re_Export_Footer|Re-export is automatic if audit is modified^^."
    
    @block
    @block-visible-when "is-not-blank(Re_Export_Date)"
    Re-export requested on {{Re_Export_Date}}.
    @end-block
@end-block

@label "Status"
    @label-ui-style "large bold"
    @label-header "^^Current Status|Current Status^^:"

@block
@block-visible-when "Status = 'In-Progress' or Status = 'Completed' or Status = 'Closed' or Status = 'Refused Entry' or (Auditor = user-id() or call('Core.CheckPermission', 'Core_Audit.Audit_Read_Only_View'))"
    @label "Audit_Scheduled_Date"
        @label-header "^^Scheduled Date|Scheduled Date^^:"
@end-block

@block
@block-visible-when "(Status = 'Assigned' or Status = 'Scheduled') and (call('Core.IsITUser') or call('Core.CheckPermission', 'Core_Audit.Audit_Edit_Closed_View') or call('Core.CheckPermission', 'Core_Audit.Audit_Support_View'))"
    @text "Audit_Scheduled_Date"
    @text-header "^^Scheduled Date|Scheduled Date^^:"
    @text-type "Date"
    @text-validation-test "(Status = 'Scheduled' and is-not-blank(Audit_Scheduled_Date)) or Status = 'Assigned'"
    @text-validation-error "^^Scheduled Date Validation|Scheduled Date must be entered for audits in status of 'Scheduled'.^^"
@end-block

@block
@block-visible-when "Status = 'In-Progress' or Status = 'Completed' or Status = 'Closed' or Status = 'Refused Entry' "
    @text "Audit_Start_Date"
    @text-header "^^Start Date/Time|Start Date/Time^^:"
    @text-expression "If(Status='In-Progress' and (is-changed('Status') or is-not-blank(Status_Change_Event)) and is-blank(Audit_Start_Date), now(), Audit_Start_Date)"
    @text-type "DateTime"
@end-block

@block
@block-visible-when "is-changed('Audit_Start_Date')"
    @text "Audit_Start_Date_Change_Reason"
    @text-header "^^Start_Date_Change_Reason|Please explain why you are changing the start time of the audit^^:"
    @text-rows "3"
@end-block

@block
@block-visible-when "Status = 'Closed' or Status = 'Refused Entry'"
    @text "Audit_Completion_Date"
    @text-header "^^Completion Date/Time|Completion Date/Time^^:"
    @text-type "DateTime"
    
    @label "time-zone-of(Audit_Completion_Date)"
    @label-header "^^Time Zone|Time Zone^^:"

@end-block

@block
@block-visible-when "Status = 'Scheduled' "
    @link "_Next"
    @link-text "^^Preview Audit|Preview Audit^^"
    @link-align "Left"
    @link-ui-style "btn btn-medium btn-primary-light controls"
    @link-icon "icon-file"
@end-block