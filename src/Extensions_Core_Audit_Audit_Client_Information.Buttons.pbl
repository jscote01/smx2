﻿@title ""
@title-ui-style "hide"
@mode "Audit_Client_Information"

    @button "Back"
        @button-value "Back"
        @button-text "^^Back|Back^^"
        @button-optional "Yes"
        @button-action "back"
        @button-ui-style "pull-right margin-lr-sm"
        
    @block
    @block-visible-when "is-not-blank(Status) and (call('Core.CheckPermission', 'Core_Audit.Create') or call('Core.IsITUser'))"
         @button "Save"
            @button-value "Save"
            @button-text "^^Save|Save^^"
            @button-optional "Yes"
            @button-ui-style "pull-right margin-lr-sm"
            @button-action "Validate, Save, Reload" 
    @end-block

    @block
    @block-visible-when "is-blank(Status) and (call('Core.CheckPermission', 'Core_Audit.Create') or call('Core.IsITUser'))"
     
        @button "Status_Change"
            @button-value "Scheduled"
            @button-text "^^Set_Status_to_Scheduled|Set Status to Scheduled^^"
            @button-ui-style "btn-inverse pull-right margin-lr-sm"
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-optional "Yes"       

    @end-block

    @block
    @block-visible-when "Status = 'Assigned' and (call('Core.CheckPermission', 'Core_Audit.Create') or call('Core.IsITUser'))"
        @button "Status_Change"
            @button-value "Scheduled"
            @button-text "^^Change_Status_to_Scheduled|Change Status to Scheduled^^"
            @button-action "Validate, Save, Reload"
            @button-show-value "No"
            @button-ui-style "btn-inverse pull-right margin-lr-sm"
            @button-optional "Yes"
    @end-block

    @block
    @block-visible-when "Status = 'Scheduled' and (Assignments/item/Auditor/@refId = user-id() or call('Core.IsITUser'))"
        @button "Status_Change"
            @button-value "In-Progress"
            @button-text "^^Start_Audit|Start Audit^^"
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-ui-style "btn-success pull-right margin-lr-sm"
            @button-align "Right"
            @button-optional "Yes"
    @end-block

    @block
    @block-visible-when "Status = 'In-Progress' and is-complete(Questionnaire) and (Assignments/item/Auditor/@refId = user-id() or call('Core.IsITUser')) and (count(Assignments/item[Auditor/@refId = user-id()]/Auditor_Submitted) = 0) or (count(Assignments/item[Auditor/@refId != user-id()]/Auditor_Submitted) > (Assignments/item[last()]/@index - 1)) and not(is-offline())"
     @button "Auditor_Submission"
            @button-value "Submit"
            @button-text "^^Submit_Audit|Submit Audit^^"
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-ui-style "btn-success pull-right margin-lr-sm"
            @button-align "Left"
            @button-optional "Yes"
    @end-block

    @block
    @block-visible-when "Status = 'In-Progress' and is-complete(Questionnaire) and (Assignments/item/Auditor/@refId = user-id() or call('Core.IsITUser')) and (count(Assignments/item[Auditor/@refId != user-id()]/Auditor_Submitted) = (Assignments/item[last()]/@index - 1)) and not(is-offline())"
     @button "Status_Change"
            @button-value "Completed"
            @button-text "^^Complete_Audit|Complete Audit^^"
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-ui-style "btn-success pull-right margin-lr-sm"
            @button-align "Left"
            @button-optional "Yes"
    @end-block

    @block
    @block-visible-when "Status = 'In-Progress' and is-complete(Questionnaire) and (Assignments/item/Auditor/@refId = user-id() or call('Core.IsITUser')) and is-offline()"
     @button "Status_Change"
            @button-value "Completed"
            @button-text "^^Complete_Audit|Complete Audit^^"
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-ui-style "btn-success pull-right margin-lr-sm"
            @button-align "Left"
            @button-optional "Yes"
    @end-block

    @block
    @block-visible-when "Status = 'Completed' and is-complete(Questionnaire) and (Assignments/item/Auditor/@refId = user-id() or call('Core.IsITUser')) and boolean-from-string(call('Core.Audit.ReviewsComplete'))"
     @button "Status_Change"
            @button-value "Closed"
            @button-text "^^Close_Audit|Close Audit^^"       
            @button-show-value "No"
            @button-action "Validate, Save, Reload"
            @button-ui-style "btn-success pull-right margin-lr-sm"
            @button-align "Left"
            @button-optional "Yes"
    @end-block