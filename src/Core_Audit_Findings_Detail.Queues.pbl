﻿@display "No"
@print "No"


@comment
Standard Queue for Single Service
@end-comment

@queue "Observation_Details_Standard_Q"
    @queue-description "{{if(language() = 'en',store(find-store-id('Permissions_Queues.Master_Q', concat('Queue_View_ID = ', char(39),'Observation_Details_Standard_Q',char(39))))/Permissions_Queues/Description, localize-token('gFindingDetails'))}}"
    @queue-header "^^Finding Details|Finding Details^^"
    @queue-sort "Audit_Completion_Date DESC"
    @queue-sequence "150"
    @queue-allow-new "No"
    @queue-test "call('Core.CheckPermission', 'Observation_Details_Standard_Q')"
    @queue-filter "(Finding_Detail_Status IS NULL OR Finding_Detail_Status != 'Deleted') AND Type != 'Information'"
    @queue-activator "^^Reports|Reports^^ | ^^Finding Details|Finding Details^^"
    
    @action
        @action-name "QuickReport"
        @action-argument "Observations_Export_Standard"
        @action-text "^^ExportPDF|Report^^"
        @action-description "^^ExportPDFDESC|View Queue as Adobe PDF document. Requires a compatible reader^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToSpreadsheet"
        @action-argument "Observations_Export_Standard"
        @action-text "^^ExportToSpreadsheet|Download Refreshable Web Query^^"
        @action-description "^^ExportToSpreadsheetDESC|items with a spreadsheet application^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToCsv"
        @action-argument "Observations_Export_Standard"
        @action-text "^^ExportToCSV|Export to Spreadsheet (CSV)^^"
        @action-description "^^ExportToCSVDESC|Download items in CSV format^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToRss"
        @action-argument "Observations_Export_Standard"
        @action-text "^^ExportToRss|View RSS Feed^^"
        @action-description "^^ExportToRssDESC|Syndicate items with an RSS reader^^."
        @action-container "ViewTitle"
    
    @column "LocationName"
        @column-header "^^LocationID|Location^^"
        @column-search "Yes"
        @column-navigate-text "^^OPEN|OPEN^^"
    
    @column "Services"
        @column-header "^^ServiceTitle|Service Name^^"
    
    @column "Audit_Type_Display"
        @column-header "^^Audit Type|Audit Type^^"
    
    @column "Round_Title"
        @column-header "^^Round|Round^^"
        @column-search "Auto"
    
    @column "Audit_Completion_Date"
        @column-header "^^AuditCompletionDate|Completion Date^^"
        @column-type "Date"
    
    @column "Audit_Completion_Time"
        @column-header "^^AuditCompletionTime|Completion Time^^"
        @column-type "Time"
    
    @column "Type"
        @column-header "^^Risk|Risk^^"
    
    @column "Tag"
        @column-header "^^Department|Department^^"
        @column-search "^^Search_Department|No^^"
        @column-hidden "^^Hide_Department|No^^"
        
    @column "Repeats"
        @column-header "^^Repeats|Repeats^^"
        @column-search "No"
    
    @column "Topic2_Number"
        @column-header "^^Item|Item^^"
    
    @column "Topic2"
        @column-header "^^Standard|Standard^^"
    
    @column "Note"
        @column-header "^^Observation|Observation^^"
    
    @column "Points"
        @column-header "^^Points|Deducted^^"
        @column-type "Integer"
    
    @column "Audit_Rating"
        @column-header "^^Rating|Rating^^"
        @column-hidden "^^Hide_Rating|No^^"
        @column-search "^^Search_Rating|Auto^^"
    
    @column "Audit_Score_Calc"
        @column-header "^^Score|Score^^"
        @column-type "Number"
        @column-format "{0:N1}"
        @column-aggregate "Average"
        @column-search "^^Search_Score|Auto^^"
        @column-hidden "^^Hide_Score|No^^"
        
    @column "City"
        @column-header "^^City|City^^"
     
    @column "State"
        @column-header "^^State_Province|State/Prov^^"
     
    @column "Zip_Code"
        @column-header "^^Zip_Code|Zip^^"
    
    @column "Keyword_Name1"
        @column-hidden "Yes"
    
    @column "Keyword_Value1"
        @column-header "^^Keyword1|Keyword 1^^"
        @column-search "^^Search_Keyword1|No^^"
        @column-hidden "^^Hide_Keyword1|Yes^^"
    
    @column "Keyword_Name2"
        @column-hidden "Yes"
    
    @column "Keyword_Value2"
        @column-header "^^Keyword2|Keyword 2^^"
        @column-search "^^Search_Keyword2|No^^"
        @column-hidden "^^Hide_Keyword2|Yes^^"
    
    @column "Keyword_Name3"
        @column-hidden "Yes"
    
    @column "Keyword_Value3"
        @column-header "^^Keyword3|Keyword 3^^"
        @column-search "^^Search_Keyword3|No^^"
        @column-hidden "^^Hide_Keyword3|Yes^^"
    
    @column "Keyword_Name4"
        @column-hidden "Yes"
    
    @column "Keyword_Value4"
        @column-header "^^Keyword4|Keyword 4^^"
        @column-search "^^Search_Keyword4|No^^"
        @column-hidden "^^Hide_Keyword4|Yes^^"
    
    @column "Keyword_Name5"
        @column-hidden "Yes"
    
    @column "Keyword_Value5"
        @column-header "^^Keyword5|Keyword 5^^"
        @column-search "^^Search_Keyword5|No^^"
        @column-hidden "^^Hide_Keyword5|Yes^^"
    
     
    @column "Photo_Name"
        @column-header "^^Photo_Name|Photo Name^^"
        @column-hidden "^^Hide_Photography|Yes^^"
        @column-search "^^Search_Photography|Yes^^"
    
    @column "Photo_Link"
        @column-header "^^Photo|Photo^^"
        @column-type "Binary"
        @column-hidden "^^Hide_Photography|Yes^^"
        @column-search "^^Search_Photography|Yes^^"
    
    @column "AuditReport"
        @column-header "^^AuditReport|PDF Report^^"
        @column-type "Binary"
        @column-search "No"
        @column-hidden "No"

@end-queue


@queue "Observation_Details_Filtered_Q"
    @queue-description "{{if(language() = 'en',store(find-store-id('Permissions_Queues.Master_Q', concat('Queue_View_ID = ', char(39),'Observation_Details_Filtered_Q',char(39))))/Permissions_Queues/Description, localize-token('gFindingDetails'))}}"
    @queue-header "^^Finding Details|Finding Details^^"
    @queue-sort "Audit_Completion_Date DESC"
    @queue-sequence "150"
    @queue-allow-new "No"
    @queue-test "call('Core.CheckPermission', 'Observation_Details_Filtered_Q')"
    @queue-filter "Services = {{page-filter('Core_Audit_Findings_Service_Detail.ALL_Findings_Detail_Service_Filter_Q.Service_Name')}} AND Round_Title = {{page-filter('Core_Audit_Findings_Round_Detail.ALL_Findings_Detail_Rounds_Filter_Q.Round_Title')}} AND (Finding_Detail_Status IS NULL OR Finding_Detail_Status != 'Deleted') AND Type != 'Information'"
    @queue-activator "^^Reports|Reports^^ | ^^Finding Details|Finding Details^^"
    
    @action
        @action-name "QuickReport"
        @action-argument "Observations_Export_Filtered"
        @action-text "^^ExportPDF|Report^^"
        @action-description "^^ExportPDFDESC|View Queue as Adobe PDF document. Requires a compatible reader^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToSpreadsheet"
        @action-argument "Observations_Export_Filtered"
        @action-text "^^ExportToSpreadsheet|Download Refreshable Web Query^^"
        @action-description "^^ExportToSpreadsheetDESC|items with a spreadsheet application^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToCsv"
        @action-argument "Observations_Export_Filtered"
        @action-text "^^ExportToCSV|Export to Spreadsheet (CSV)^^"
        @action-description "^^ExportToCSVDESC|Download items in CSV format^^."
        @action-container "ViewTitle"
    
    @action
        @action-name "ExportToRss"
        @action-argument "Observations_Export_Filtered"
        @action-text "^^ExportToRss|View RSS Feed^^"
        @action-description "^^ExportToRssDESC|Syndicate items with an RSS reader^^."
        @action-container "ViewTitle"
    
    @column "LocationName"
        @column-header "^^LocationID|Location^^"
        @column-search "Yes"
        @column-navigate-text "^^OPEN|OPEN^^"
    
    @column "Services"
        @column-header "^^ServiceTitle|Service Name^^"
    
    @column "Audit_Type_Display"
        @column-header "^^Audit Type|Audit Type^^"
    
    @column "Round_Title"
        @column-header "^^Round|Round^^"
        @column-search "Auto"
    
    @column "Audit_Completion_Date"
        @column-header "^^AuditCompletionDate|Completion Date^^"
        @column-type "Date"
    
    @column "Audit_Completion_Time"
        @column-header "^^AuditCompletionTime|Completion Time^^"
        @column-type "Time"
    
    @column "Type"
        @column-header "^^Risk|Risk^^"
    
    @column "Tag"
        @column-header "^^Department|Department^^"
        @column-search "^^Search_Department|No^^"
        @column-hidden "^^Hide_Department|No^^"
        
    @column "Repeats"
        @column-header "^^Repeats|Repeats^^"
        @column-search "No"
    
    @column "Topic2_Number"
        @column-header "^^Item|Item^^"
    
    @column "Topic2"
        @column-header "^^Standard|Standard^^"
    
    @column "Note"
        @column-header "^^Observation|Observation^^"
    
    @column "Points"
        @column-header "^^Points|Deducted^^"
        @column-type "Integer"
    
    @column "Audit_Rating"
        @column-header "^^Rating|Rating^^"
        @column-hidden "^^Hide_Rating|No^^"
        @column-search "^^Search_Rating|Auto^^"
    
    @column "Audit_Score_Calc"
        @column-header "^^Score|Score^^"
        @column-type "Number"
        @column-format "{0:N1}"
        @column-aggregate "Average"
        @column-search "^^Search_Score|Auto^^"
        @column-hidden "Yes"
    
    @column "Audit_Score_Calc_0"
        @column-header "^^Score|Score^^"
        @column-type "Number"
        @column-format "{0:N0}"
        @column-search "No"
        @column-aggregate "Average"
        @column-hidden "%Hide_Calc_0%Yes%Hide_Calc_0%"
    
    @column "Audit_Score_Calc_1"
        @column-header "^^Score|Score^^"
        @column-type "Number"
        @column-format "{0:N1}"
        @column-search "No"
        @column-aggregate "Average"
        @column-hidden "%Hide_Calc_1%Yes%Hide_Calc_1%"
        
    @column "Audit_Score_Calc_2"
        @column-header "^^Score|Score^^"
        @column-type "Number"
        @column-format "{0:N2}"
        @column-search "No"
        @column-aggregate "Average"
        @column-hidden "%Hide_Calc_2%Yes%Hide_Calc_2%"
    
    @column "City"
        @column-header "^^City|City^^"
     
    @column "State"
        @column-header "^^State_Province|State/Prov^^"
     
    @column "Zip_Code"
        @column-header "^^Zip_Code|Zip^^"
    
    @column "Keyword_Name1"
        @column-hidden "Yes"
    
    @column "Keyword_Value1"
        @column-header "^^Keyword1|Keyword 1^^"
        @column-search "^^Search_Keyword1|No^^"
        @column-hidden "^^Hide_Keyword1|Yes^^"
    
    @column "Keyword_Name2"
        @column-hidden "Yes"
    
    @column "Keyword_Value2"
        @column-header "^^Keyword2|Keyword 2^^"
        @column-search "^^Search_Keyword2|No^^"
        @column-hidden "^^Hide_Keyword2|Yes^^"
    
    @column "Keyword_Name3"
        @column-hidden "Yes"
    
    @column "Keyword_Value3"
        @column-header "^^Keyword3|Keyword 3^^"
        @column-search "^^Search_Keyword3|No^^"
        @column-hidden "^^Hide_Keyword3|Yes^^"
    
    @column "Keyword_Name4"
        @column-hidden "Yes"
    
    @column "Keyword_Value4"
        @column-header "^^Keyword4|Keyword 4^^"
        @column-search "^^Search_Keyword4|No^^"
        @column-hidden "^^Hide_Keyword4|Yes^^"
    
    @column "Keyword_Name5"
        @column-hidden "Yes"
    
    @column "Keyword_Value5"
        @column-header "^^Keyword5|Keyword 5^^"
        @column-search "^^Search_Keyword5|No^^"
        @column-hidden "^^Hide_Keyword5|Yes^^"

    @column "Photo_Name"
        @column-header "^^Photo_Name|Photo Name^^"
        @column-hidden "^^Hide_Photography|Yes^^"
        @column-search "^^Search_Photography|Yes^^"
    
    @column "Photo_Link"
        @column-header "^^Photo|Photo^^"
        @column-type "Binary"
        @column-hidden "^^Hide_Photography|Yes^^"
        @column-search "^^Search_Photography|Yes^^"
    
    @column "AuditReport"
        @column-header "^^AuditReport|PDF Report^^"
        @column-type "Binary"
        @column-search "No"
        @column-hidden "No"
@end-queue