﻿@display "No"

@execute
    @execute-when "is-changed(Associated_To_Organization_Location) and Associated_To_Organization_Location != 'Yes'"
    
    @update
        @update-target-select "Organization_Locations"
        @update-source-type "Node"
    
    @update
        @update-target-select "Contact_Type/Organization_Location"
        @update-source-type "Node"
@end-execute

@execute
    @execute-when "is-changed(Associated_To_Organization) and Associated_To_Organization != 'Yes'"
    
    @update
        @update-target-select "Contact_Type/Organization"
        @update-source-type "Node"
@end-execute


@comment
(1) Populate Organization for the associated to Organization Users
(2) For Users with only Facility access, Associated_To_Organization = 'No'
@end-comment

@execute
    @execute-when "is-not-blank(/store/Core_Entity_Management/Organization) and is-blank(Core_Organization) and call('Core.CurrentAssociatedToOrg')"

    @update
        @update-target-select "Core_Organization"
        @update-source-select "/store/Core_Entity_Management/Organization/@refId"
        @update-source-type "Lookup"
@end-execute

@execute
    @execute-when "is-blank(Associated_To_Organization) and call('Core.GetCurrentUserOrganizationCount') = 0"
    
    @update
        @update-target-select "Associated_To_Organization"
        @update-source-select "'No'"
        
    @update
        @update-target-select "Core_Organization"
        @update-source-select "queue('Core_Organization_Location.Active_Lookup_Q', '1=1')/item[1]/Core_Organization"
@end-execute


@comment
Populate Profiles for Contact/User
@end-comment

@execute
    @execute-when "is-changed(Associated_To_Organization_Location) or is-changed(Associated_To_Organization) or (call('Core.GetCurrentUserLocationCount') = 1 and is-blank(Associated_To_Organization_Location)) or is-changed(Licensed_User)"
    
    @update
        @update-target-select "Profile_Custom_Profile_8"
        @update-source-type "Node"  
    
    @update
        @update-target-select "Profile_Custom_Profile_9"
        @update-source-type "Node"
@end-execute

@execute
    @execute-when "Licensed_User = 'Yes' and Associated_To_Organization = 'Yes'"
    
    @invoke
        @invoke-execute "CheckContactProfileNodesForOrg"
@end-execute

@execute
    @execute-when "Licensed_User = 'Yes' and Associated_To_Organization_Location = 'Yes'"

    @invoke
        @invoke-execute "CheckContactProfileNodesForLoc"
@end-execute


@execute "CheckContactProfileNodesForOrg"
    @execute-select "/store/Core_Entity_Management/EM_Request_Type/User_Org_Profile/value"
    @execute-select-key "."

        @invoke
            @invoke-execute "SetCustomProfileNode"
            @invoke-parameter "ProfileName"
            @invoke-parameter-expression "call('Core.GetCustomProfileNameFromPermissionStoreId', execute-select-context())"
@end-execute


@execute "CheckContactProfileNodesForLoc"
    @execute-select "/store/Core_Entity_Management/EM_Request_Type/User_Loc_Profile"
    @execute-select-key "."
    
        @invoke
            @invoke-execute "SetCustomProfileNode"
            @invoke-parameter "ProfileName"
            @invoke-parameter-expression "call('Core.GetCustomProfileNameFromPermissionStoreId', execute-select-context())"
@end-execute



@execute "SetCustomProfileNode"
@comment
params: ProfileName
@end-comment

    @update
        @update-target-select "{{concat('/store/Core_Entity_Management/New_Worksheet/Profile_', replace($Parameter:ProfileName, ' ', '_'))}}"
        @update-source-select "Node"
        
    @update
        @update-target-select "{{concat('/store/Core_Entity_Management/New_Worksheet/Profile_', replace($Parameter:ProfileName, ' ', '_'), '/', replace($Parameter:ProfileName, ' ', '_'))}}"
        @update-source-select "$Parameter:ProfileName"

@end-execute


@comment
Do some custom validation
(1) Check if the user exists (currently or even Inactive, until we get the bug fix)
(2) Make sure a profile is selected for BBI User
(3) If the Organization has no locations, and the Contact for the Change Request does NOT "work directly for the Organization"
@end-comment

@execute
    @execute-when "true()"
    
    @update
        @update-target-select "Invalid_Content"
        @update-source-type "Node"    
        
@end-execute

@execute
    @execute-when "call('CoreEM.ActiveDuplicateUserContact') or call('CoreEM.DuplicateUserContact')"
    
    @update
        @update-target-select "Invalid_Content"
        @update-source-select "now()"
@end-execute

@execute
    @execute-when "call('CoreEM.NoProfileSelected') and call('CoreEM.IsUserType')"
    
    @update
        @update-target-select "Invalid_Content"
        @update-source-select "now()"
@end-execute

@execute
    @execute-when "not(call('Core.OrgHasActiveOrgLocation', Core_Organization/@refId)) and Associated_To_Organization = 'No' and is-not-blank(Associated_To_Organization)"
    
    @update
        @update-target-select "Invalid_Content"
        @update-source-select "now()"
@end-execute

@execute
    @execute-when "is-not-blank(Invalid_Content) and is-not-blank(Verify_Status) and Verify_Status != 'EM_Discarded'"
    
    @update
        @update-target-select "Verify_Status"
        @update-source-type "Node"
    
    @update
        @update-target-select "Invalid_Content"
        @update-source-type "Node"
@end-execute

@execute
    @execute-when "is-not-blank(Verify_Status) and (Verify_Status != /store/Core_Entity_Management/Status/Status_Code)"
    
    @update
        @update-target-select "/store/Core_Entity_Management/Verify_Status"
        @update-source-select "Verify_Status"
        @update-run-views "New_Main_View"    
        @update-publish "Yes"
@end-execute

@execute
    @execute-when "is-not-blank(Verify_Status) and Verify_Status != 'EM_Discarded'"
    
    @update
        @update-target-select "Verify_Status"
        @update-source-type "Node"
@end-execute

[[Shared_Core_Addresses.Executes]]