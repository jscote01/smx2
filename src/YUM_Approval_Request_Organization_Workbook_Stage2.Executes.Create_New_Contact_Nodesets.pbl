﻿@display "No"

@execute "Create_New_Contact_Nodes"
@comment
This will create a nodeset in the SCQM_Approval_Request_Organization 
XML structure should be:
Create_New_Core_Contacts/item (this is a list of Contacts - each Contact to be created should be a separate list item)
    Email *REQUIRED
    First_Name *REQUIRED
    Last_Name *REQUIRED
    Primary_Number *REQUIRED
    Status (lookup) *REQUIRED
    Contact_Type *REQUIRED
        NOTE: This will be in the form:
            Contact_Type
                Organization text="Organization"Organization/Organization
            /Contact_Type
    Licensed_User *REQUIRED
        NOTE: This will be Yes or No; when Yes and the User already exists, it will populate the User field automatically
    Mobile_Number
    Salutation
    Contact_Title
    Physical_Address_Line_1
    Physical_Address_Line_2
    Physical_Address_Line_3
    Physical_City
    Physical_State (lookup or text)
    Physical_Zip
    Physical_Country (lookup)
/Create_New_Core_Contacts/item
@end-comment


    @comment
    MAIN DETAILS
    @end-comment    

    @update
        @update-target-select "$Local:NewContact/Email"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Email"

    @update
        @update-target-select "$Local:NewContact/First_Name"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_First_Name"

    @update
        @update-target-select "$Local:NewContact/Last_Name"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Last_Name"

    @update
        @update-target-select "$Local:NewContact/Primary_Number"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Primary_Number"

    @update
        @update-target-select "$Local:NewContact/Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Active', 'Active States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"

    @update
        @update-target-select "$Local:NewContact/Contact_Type"
        @update-source-select "''"

    @update
        @update-target-select "$Local:NewContact/Contact_Type/Organization"
        @update-source-select "'Organization'"

    @update
        @update-target-select "$Local:NewContact/Contact_Type/Organization/@text"
        @update-source-select "$Local:NewContact/Contact_Type/Organization"


    @comment
    ADDITIONAL DETAILS
    @end-comment    

    @update
        @update-target-select "$Local:NewContact/Mobile_Number"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Mobile_Phone"

    @update
        @update-target-select "$Local:NewContact/Salutation"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Salutation/@refId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Custom_Dropdown_Item/Item_Name | Core_Custom_Dropdown_Item/Item_Value"

    @update
        @update-target-select "$Local:NewContact/Contact_Title"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Title"


    @comment
    PHYSICAL ADDRESS
    @end-comment    

    @update
        @update-target-select "$Local:NewContact/Physical_Address_Line_1"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Address_Line_1"

    @update
        @update-target-select "$Local:NewContact/Physical_Address_Line_2"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Address_Line_2"

    @update
        @update-target-select "$Local:NewContact/Physical_Address_Line_3"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Address_Line_3"

    @update
        @update-target-select "$Local:NewContact/Physical_City"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_City"

    @if "is-blank(/store/*[1]/Org_Workbook_Stage2/Organization_Contact_State/State_Name)"
        @update
            @update-target-select "$Local:NewContact/Physical_State"
            @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_State"
    @end-if

    @if "is-not-blank(/store/*[1]/Org_Workbook_Stage2/Organization_Contact_State/State_Name)"
        @update
            @update-target-select "$Local:NewContact/Physical_State"
            @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_State/@refId"
            @update-source-type "Lookup"
            @update-source-lookup-select "Core_Admin_State_Province/State_Name | Core_Admin_State_Province/State_Code | Core_Admin_State_Province/State_Id"            
    @end-if

    @update
        @update-target-select "$Local:NewContact/Physical_Zip"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Zip"

    @update
        @update-target-select "$Local:NewContact/Physical_Country"
        @update-source-select "/store/*[1]/Org_Workbook_Stage2/Organization_Contact_Country/@refId"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Admin_Country/Country_Name | Core_Admin_Country/Country_Code"        


    @comment
    CUSTOM ATTRIBUTES
    @end-comment    


    @comment
    USER INFORMATION
    @end-comment    

    @update
        @update-target-select "$Local:NewContact/Licensed_User"
        @update-source-select "'Yes'"

    @update
        @update-target-select "$Local:NewContact/Licensed_User/@text"
        @update-source-select "$Local:NewContact/Licensed_User"
    

    @comment
    CREATE ITEM IN Create_New_Core_Contacts
    @end-comment    

    @update
        @update-target-select "/store/SCQM_Approval_Request_Organization/Create_New_Core_Contacts"
        @update-source-select "$Local:NewContact/*"
        @update-source-type "Item"

@end-execute
