﻿@mode "Banner"

@title " "
    @title-ui-style "hide"
    
@comment
==========================================================================
SPECIFICATION MANAGEMENT (CLONE, CHILD, REVISION, VARIATION)
==========================================================================
@end-comment

@comment
Message while Revision or Variation are outstanding
@end-comment
@block
    @block-visible-when "call('SCQM.Specification.ShowRevVarOutstandingMessage')"
    @block-ui-style "clear form-inline well well-mini well-info-light inverse"
    @icon "icon-info-sign info medium"
    @block
        @block-visible-when "call('SCQM.Specifiation.IsRevisionOutstanding', store-id())"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Outstanding_Revision_Message_1|A Revision has been created for this Specification.^^  ^^Outstanding_Revision_Message_2|This record is set to be archived once its replacement has been approved.^^  ^^Outstanding_Revision_Message_3|Use the "Replacement Specification" button to navigate to the replacement record.^^'}
    @end-block    
    @block
        @block-visible-when "not(call('SCQM.Specifiation.IsRevisionOutstanding', store-id()))"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Outstanding_Variation_Message_1|A Variation has been created for this Specification.^^  ^^Outstanding_Revision_Message_2|This record is set to be archived once its replacement has been approved.^^  ^^Outstanding_Revision_Message_3|Use the "Replacement Specification" button to navigate to the replacement record.^^'}
    @end-block    

    @newline "Small"

    @link "http://{{domain()}}{{if(contains(domain(),'local'),':8080','')}}/rql/a/store/{{call('SCQM.Specifiation.SpawnedVarRevStoreId', store-id())}}"
        @link-text "^^Replacement Specification|Replacement Specification^^"
        @link-align "Center"
        @link-ui-style "btn btn-info pull-right"
        @link-href "Yes"

@end-block

@comment
Message once Spec is Archived and has a Revision or Variation
@end-comment
@block
    @block-visible-when "call('SCQM.Specification.ShowArchivedWithRevVarMessage')"
    @block-ui-style "clear form-inline well well-mini well-info-light inverse"
    @icon "icon-info-sign info medium"
    @block
        @block-visible-when "store(call('SCQM.Specifiation.SpawnedVarRevStoreId', store-id()))/*[1]/Spec_Creation_Method = 'Revision'"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Archived_Revision_Message_1|A Revision has been created for this Specification.^^  ^^Archived_Revision_Message_2|This record has been archived.^^  ^^Archived_Revision_Message_3|Use the "Replacement Specification" button to navigate to the replacement record.^^'}
    @end-block    
    @block
        @block-visible-when "store(call('SCQM.Specifiation.SpawnedVarRevStoreId', store-id()))/*[1]/Spec_Creation_Method = 'Variation'"
        {label(medium margin-lr-mini):'^^Please Note|Please Note^^: ^^Archived_Variation_Message_1|A Variation has been created for this Specification.^^  ^^Archived_Revision_Message_2|This record has been archived.^^  ^^Archived_Revision_Message_3|Use the "Replacement Specification" button to navigate to the replacement record.^^'}
    @end-block    

    @newline "Small"

    @link "http://{{domain()}}{{if(contains(domain(),'local'),':8080','')}}/rql/a/store/{{call('SCQM.Specifiation.SpawnedVarRevStoreId', store-id())}}"
        @link-text "^^Replacement Specification|Replacement Specification^^"
        @link-align "Center"
        @link-ui-style "btn btn-info pull-right"
        @link-href "Yes"

@end-block