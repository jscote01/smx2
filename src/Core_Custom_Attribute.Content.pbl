﻿@title " "

@link "Help_Info"
    @link-href "Yes"
    @link-target "Modal"
    @link-text "^^Custom Attribute Guide|Custom Attribute Guide^^"
    @link-ui-style "btn padding-tb-sm padding-lr-sm margin-tb-sm"
    @link-text-ui-style "padding-lr-mini padding-tb-sm medium"
    @link-icon "icon-info-sign"
    @link-icon-ui-style "xx-large"
    @link-align "Left"               

@modal "Help_Info"
    @modal-header "Custom Attribute Field Information"
    @modal-header-ui-style "text-align-left large"
    @modal-close "Header"
    @modal-width "{{if(is-mobile(), '98%', '600px')}}"
<b>Account: </b>The site the Custom Attribute is to be used for.</br></br>
<b>Name: </b>The name of the Custom Attribute.</br></br>
<b>Display_Name: </b>The label that displays next to the input field of a custom attribute.</br></br>
<b>App: </b>The App which the custom attribute will be used in.</br></br>
<b>Condition: </b>An evaluated condition that can be used to further filter custom attributes within an app. For example, within Core_Organization you may want to have a field that is used for Organizations with a particular Business Type. The condition for the custom attribute would then look something like this: /store/Core_Organization/Business_Type/Business_Type_Id = 'Audit Firm' Note: When referencing a field within the app it MUST be fully pathed to ensure you get the field from the correct context.</br></br>
<b>Group: </b>Used as a display filter in combination with the @list control in order to only display pertinent fields.</br></br>
<b>Sequence: </b>Used to order custom attributes.</br></br>
<b>Publish Attribute to Queue: </b>Tells the app that this field should be published. If yes, you must define a Published Sequence number.</br></br>
<b>Published Sequence: </b>This integer will correspond to the Custom_Attribute_# column on the app. For example, a Custom Attribute with a published sequence of 3 will be the column Custom_Attribute_3 in the Apps master queue (assuming the pbl developer prepared the app to work with custom attributes as defined above).</br></br>
<b>Type: </b>The type of value the custom attribute will be. Can be string, date, number, option, or lookup. If option is selected, you have the ability to specify what the options are. Currently this is limited to string values and no text validation occurs. If lookup is selected, you will have the ability to set the queue which the lookup references and the queue filter to use.</br></br>
<b>Queue: </b>The AppName.QueueName of the Queue that the Lookup, DropDownLookup, or Bulklist is linked to.</br></br>
<b>Filter: </b>The SQL Filter to use on the queue defined above.</br></br>
<b>Lookup Select: </b>To be used with Lookups. The Select statement that will determine what values to pull in from the store. i.e. User/* or User/User_Name </br></br>
<b>Lookup Field: </b>To be used with Lookups. The name of the Field that will be dispalyed. Needs to be brought in with the Lookup Select. </br></br>
<b>Value: </b>To be used with BulkList and Dropdown Lookups. The value to be placed in the element body of the custom attribute. Must be a column on the queue used. </br></br>
<b>Text: </b>To be used with Bulklist and Dropdown Lookups. The value to be displayed and placed in the @text attribute. Must be a column on the queue used. </br></br>
<b>Optional When Expression: </b>An evaluated condition used to determine if the Custom Attribute is required or not. Note: When referencing a field within the app it MUST be fully pathed to ensure you get the field from the correct context.</br></br>
<b>Visible When Expression: </b>An evaluated condition used to determine if the Custom Attribute is visible or not. Note: When referencing a field within the app it MUST be fully pathed to ensure you get the field from the correct context.</br></br>
<b>Status: </b>Active or Inactive. Used to turn the custom attribute on or off.</br>
@end-modal

@block
    @block-visible-when "call('Core.IsNotNewStore')"
    
    @button "Update_Stores"
        @button-value "Update"
        @button-text "^^Update Application Stores|Update Application Stores^^"
        @button-optional "Yes"
      
    @button "Update_Tokens"
        @button-value "Update_Tokens"
        @button-text "^^Update Tokens|Update Tokens^^"
        @button-optional "Yes"    
@end-block

@block
@block-visible-when "call('Core.IsNewStore')"

    @button "Update_Tokens"
        @button-value "Update_Tokens"
        @button-text "^^Add Tokens|Add Tokens^^"
        @button-optional "Yes"  

@end-block

[[Shared_Core_Account]]

@lookup "App"
    @lookup-header "^^App|App^^:"
    @lookup-allow-new-when "false()"
    @lookup-allow-navigate-when "call('Core.AllowLookupNavigation','Extension')"
    @lookup-queue "Extension.List"
    @lookup-select "extension/*"
    @lookup-field "name"
    @lookup-updatable-when "false()"
    @lookup-read-only-when "call('Core.IsNotNewStore')"
    @lookup-ui-style "width-auto"    

@text "Name"
    @text-header "^^Name|Name^^:"
    @text-read-only-when "call('Core.IsNotNewStore') and MatchRegex(Name, '^[A-Za-z_][A-Za-z0-9_ ]*$')"
    @text-validation-test "MatchRegex(Name, '^[A-Za-z_][A-Za-z0-9_ ]*$')"
    @text-validation-error "^^Custom_Attribute_Name_Restrictions|The name can only contain alphanumeric characters, spaces or underscores and must not start with a number or a space.^^"

@text "Display_Name"
    @text-header "^^Display Name|Display Name^^:"

@text "Condition"
    @text-header "^^Condition|Condition^^:"
    @text-default-expression "'true()'"
    @text-columns "120"
    @text-rows "3"

@text "Group"
    @text-header "^^Group|Group^^:"

@text "Sequence"
    @text-header "^^Sequence|Sequence^^:"
    @text-type "Integer"

@choice "Published"
    @choice-header "^^Publish Attribute to Queue|Publish Attribute to Queue^^?"
    @choice-columns "2"
	
	@option
		@option-text "^^Yes|Yes^^"
		@option-value "Yes"
	@option
		@option-text "^^No|No^^"
		@option-value "No"
@end-choice

@block
    @block-visible-when "queue-count('Core_Custom_Attribute.Master_Q', 'App = {{App/@refId}} and Published = \'Yes\' and Published_Sequence = {{Published_Sequence}} and StoreId <> {{store-id()}}') > 0"
    {label(large warning margin-lr-mini): "^^Custom_Attribute_Sequence_Warning|Another Custom Attribute for this App is using this sequence number. Duplicating the sequence number will cause inconsistent values to be published!^^"}
@end-block

@block
    @block-visible-when "Published = 'Yes'"

    @choice "Published_Sequence"
        @choice-style "DropDownList"
        @choice-header "^^Published Sequence|Published Sequence^^:"
        
        @choice-option "1"
        @choice-option "2"
        @choice-option "3"
        @choice-option "4"
        @choice-option "5"
        @choice-option "6"
        @choice-option "7"
        @choice-option "8"
        @choice-option "9"
        @choice-option "10"
@end-block


@choice "Type"
    @choice-style "DropDownList"
    @choice-header "^^Type|Type^^:"

	@option
		@option-text "^^Label|Label^^"
		@option-value "Label"
	@option
		@option-text "^^String|String^^"
		@option-value "String"
	@option
		@option-text "^^Number|Number^^"
		@option-value "Number"
	@option
		@option-text "^^Date|Date^^"
		@option-value "Date"
	@option
		@option-text "^^Option|Option^^"
		@option-value "Option"
	@option
		@option-text "^^Lookup|Lookup^^"
		@option-value "Lookup"
	@option
		@option-text "^^DropdownLookup|DropdownLookup^^"
		@option-value "DropdownLookup"
	@option
		@option-text "^^Bulklist|Bulklist^^"
		@option-value "Bulklist"
@end-choice

@block
    @block-visible-when "Type = 'Label'"
    
    @text "Label_Expression"
        @text-header "^^Label Expression|Label Expression^^"
        @text-columns "120"
        @text-rows "3"
@end-block

@block
    @block-visible-when "Type = 'Lookup'"

    @text "Lookup_Queue"
        @text-header "^^Lookup_Queue|Queue^^:"
        @text-columns "120"
        @text-rows "3"
    
    @text "Lookup_Filter"
        @text-header "^^Lookup_Filter|Filter^^:"
        @text-columns "120"
        @text-rows "3"
    
    @text "Lookup_Select"
        @text-header "^^Lookup Select|Lookup Select^^:"
        @text-columns "120"
        @text-rows "3"
    
    @text "Lookup_Field"
        @text-header "^^Lookup Field|Lookup Field^^:"
@end-block

@block
    @block-visible-when "Type = 'Bulklist' or Type = 'DropdownLookup'"

    @text "Bulklist_Queue"
        @text-header "^^Bulklist_Queue|Queue^^:"
        @text-columns "120"
        @text-rows "3"
    
    @text "Bulklist_Filter"
        @text-header "^^Bulklist_Filter|Queue Filter^^:"
        @text-columns "120"
        @text-rows "3"
    
    @text "Bulklist_Value"
        @text-header "^^Bulklist_Value|Value^^:"
    
    @text "Bulklist_Text"
        @text-header "^^Bulklist_Text|Text^^:"
@end-block

@block
    @block-visible-when "Type = 'Option'"
    
    @list "Options"
        @list-sort "Ascending"
        @list-style "List"
        @list-header "^^Options|Options^^:"
        
        @text "Option"
            @text-header "^^Option|Option^^:"
    @end-list
    
    @publish "Options"
        @publish-select "Options/item"
        @publish-when "true()"
        
        @property "ID"
            @property-select "@id"
            @property-is-key "Yes"
        
        @property "Name"
            @property-select "Option"
        
        @property "Custom_Attribute_Name"
            @property-select "/store/Core_Custom_Attribute/Name"
    @end-publish
@end-block


@text "Optional_When"
    @text-header "^^Optional_When|Optional When Expression^^:"
    @text-columns "120"
    @text-rows "3"

@text "Visible_When"
    @text-header "^^Visible_When|Visible When Expression^^:"
    @text-columns "120"
    @text-rows "3"

@text "Tool_Tip"
    @text-header "^^Tool Tip|Tool Tip^^"
    @text-optional "Yes"

[[Shared_Core_Status_Active_Choice]]