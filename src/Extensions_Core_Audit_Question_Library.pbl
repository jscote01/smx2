﻿@title " "
@title-ui-style "hide"
@mode "Main"

@comment
=== Do not forget to put the "mode" modifier on your sub wikis ===
@end-comment

@text "Default_Finding_Note"
    @text-header "^^Default Finding Note|Default Finding Note^^"
    @text-optional "Yes"    
    @text-columns "100"
    @text-rows "3"    

@block
    @block-visible-when "call('Core.IsITUser')"
    
    @text "AWARDED_POINTS"
        @text-header "Awarded Points (Expression)"
        @text-expression "concat('_', QUESTION_ID, '_Selected_Points + 0')"   
        @text-expression-when "(is-blank(AWARDED_POINTS) and is-not-blank(QUESTION_ID)) or is-changed(QUESTION_ID)" 
        @text-optional "Yes" 
        @text-columns "100"

    @checkbox "Allow_NA"
        @checkbox-header "Add NA Option?"
        @checkbox-postback "Yes"
        
        @block
            @block-visible-when "is-changed(Allow_NA)"
            @end-block        
     
    @text "Point_Values"
        @text-header "Point Values:"
        @text-optional "Yes" 
        @text-validation-test "is-blank(Point_Values) or (is-not-blank(Point_Values) and count-items(Point_Values) <= 6) and contains(Point_Values, '0')"
        @text-validation-error "Please enter up to 6 scores in a comma-separated list (i.e. 5,3,1,0)."
        @text-footer "Comma separated list (highest to lowest point value)" 
        
    @text "Options_Count"
        @text-header "Number of Options:"
        @text-optional "Yes"
        
    @text "Section_ID"
        @text-header "Section ID:"
        @text-optional "Yes"
    
    @text "Section_Description"
        @text-header "Section Description:"
        @text-optional "Yes"
        
    @text "Audit_Number"
        @text-header "Audit Number:"
        @text-optional "Yes"
        
    @block
        @block-ui-style "span6"
    
        @list "Point_Values_Configuration"
            @list-sort "Ascending"        
            @list-style "List"
            @list-header "Point Values Configuration (control options):"
            @list-optional "Yes"
            @list-item-summary "concat(ID,' ',Text)" 
            @list-expanded "No"            
            
                @text "ID"
                    @text-header "ID (optional):"
                    @text-optional "Yes"            
                
                @text "Text"
                    @text-header "Text:"
                    @text-optional "Yes"            
                
                @text "Value"
                    @text-header "Value:"
                    @text-optional "Yes"            
        
                @text "Token"
                    @text-header "Token:"
                    @text-optional "Yes"
                    
                @text "Icon"
                    @text-header "Icon:"
                    @text-optional "Yes"
                    
                @text "Image"
                    @text-header "Image:"
                    @text-optional "Yes"
                    
                @text "UI_Style"
                    @text-header "UI Style:"
                    @text-optional "Yes"
                
                @text "Text_UI_Style"
                    @text-header "Test UI Style:"
                    @text-optional "Yes"
                    
                @text "UI_Style_Checked"
                    @text-header "Checked UI Style:"
                    @text-optional "Yes"
                
                @text "UI_Style_Unchecked"
                    @text-header "Unchecked UI Style:"
                    @text-optional "Yes"
                
            @end-list
            
            @comment
            Expand When Condition
            @end-comment    
            
            @text "Expand_When_Condition"
                @text-header "Expand Question When Condition:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-not-blank(_',QUESTION_ID,'_Choice) and _', QUESTION_ID, '_Choice != ', char(39),'N/A',char(39),' and _', QUESTION_ID, '_Choice != ',char(39),FINDING_POINTS,char(39))"
                
            @comment
            Evaluation Notes (Comments)
            @end-comment    
            
            @text "Evaluation_Notes_Expression"
                @text-header "Evaluation Notes Expression (Comments):"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('case(_', QUESTION_ID,'_Choice = ', char(39), '0', char(39), char(44),' ', char(39), Default_Finding_Note, char(39), char(44), char(39), QUESTION_TEXT, char(39), ')')"
                
            @text "Evaluation_Notes_Expression_When"
                @text-header "Evaluation Notes Expression When:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-changed(_', QUESTION_ID,'_Choice) and not(is-changed(_', QUESTION_ID, '_Comments)) and _', QUESTION_ID, '_Choice != ', char(39), 'N/A',char(39))"
                
            @text "Evaluation_Notes_Optional"
                @text-header "Evaluation Notes (Comments) Optional When:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-blank(_', QUESTION_ID, '_Choice) or _', QUESTION_ID, '_Choice [. = ',char(39),'N/A',char(39), ' or . = ', char(39),segment(Point_Values,1,','), char(39), ']')"
            
            @text "Finding_Score_When_Condition"
                @text-header "Finding Scores When:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-not-blank(_', QUESTION_ID, '_Choice) and _', QUESTION_ID, '_Choice != ', char(39),'N/A', char(39))"
                
            @text "Finding_Selection_Expression"
                @text-header "Finding Selection:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('_', QUESTION_ID, '_Comments')"
                
            @text "Positive_Case_Test"
                @text-header "Positive Case Test (Compliant):"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('not(_', QUESTION_ID, '_Choice = ', char(39), '0', char(39), ')', ' and _', QUESTION_ID, '_Choice != ', char(39),'N/A', char(39))"                
                
                @comment
                text-expression "concat('(_', QUESTION_ID, '_Choice = ', char(39), segment(Point_Values,1,','), char(39),')')"
                @end-comment
                
            @text "Negative_Case_Test"
                @text-header "Negative Case Test (Non-Compliant):"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('_', QUESTION_ID, '_Choice = ', char(39), '0', char(39), ' and _', QUESTION_ID, '_Choice != ', char(39),'N/A', char(39))"
                
            @text "CI_Visible_When_Condition"
                @text-header "CI Visible When:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-not-blank(_', QUESTION_ID, '_Choice) and not(_', QUESTION_ID, '_Choice = ', char(39), 'N/A', char(39), ' or _', QUESTION_ID, '_Choice = ', char(39), segment(Point_Values,1,','), char(39), ') and not(_', QUESTION_ID, '_Choice = ', char(39), '0', char(39),')')"
                
            @text "CAP_Visible_When_Condition"
                @text-header "CAP Visible When:"
                @text-optional "Yes"
                @text-columns "100"
                @text-rows "2"
                @text-expression "concat('is-not-blank(_', QUESTION_ID, '_Choice) and (_', QUESTION_ID, '_Choice = ', char(39), '0', char(39),')')"
            
            
        @end-block    
    
@end-block

@block
@block-visible-when "false()"

@execute
    @execute-when "is-not-blank(Point_Values) and coalesce(FINDING_POINTS,'NA') != segment(Point_Values,1,',')"

    @update
        @update-target-select "FINDING_POINTS"
        @update-source-select "segment(Point_Values,1,',')"

@end-execute

@execute
    @execute-when "is-not-blank(Point_Values) and join(Point_Values_Configuration/item[Text != '' and Text != 'N/A']/Text,',') != Point_Values"
    
    @update
        @update-target-select "Point_Values_Configuration"
        @update-source-type "Node"

@end-execute

@execute
    @execute-when "is-not-blank(Point_Values) and count(Point_Values_Configuration/item[Text != '' and Text != 'N/A']) = 0"
    @execute-select "split(Point_Values,',')"
    @execute-select-key "."
        
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration"
        @update-source-select "CreateItem"
        @update-source-type "Item"
    
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/ID"
        @update-source-select "concat('PT',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index)"
    
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/Text"
        @update-source-select "execute-select-context()/."
        
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/Value"
        @update-source-select "execute-select-context()/."
     
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/UI_Style_Checked"
        @update-source-select "concat('btn-medium white ', case(/store/Core_Audit_Question_Library/Options_Count = 6, case(/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 1,'bcolor-success',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 2,'black bg-yellow3', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 3,'bcolor-warning-light', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 4,'bcolor-warning', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 5,'bcolor-danger-light', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 6,'bcolor-danger'), /store/Core_Audit_Question_Library/Options_Count = 5, case(/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 1,'bcolor-success',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 2,'bcolor-warning-light', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 3,'bcolor-warning', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 4,'bcolor-danger-light', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 5,'bcolor-danger'), /store/Core_Audit_Question_Library/Options_Count = 4, case(/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 1,'bcolor-success',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 2,'bcolor-warning', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 3,'bcolor-danger-light', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 4,'bcolor-danger'), /store/Core_Audit_Question_Library/Options_Count = 3, case(/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 1,'bcolor-success',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 2,'bcolor-warning', /store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 3,'bcolor-danger'), /store/Core_Audit_Question_Library/Options_Count = 2, case(/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 1,'bcolor-success',/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/@index = 2,'bcolor-danger')))"
    
    @update
        @update-target-select "/store/Core_Audit_Question_Library/Point_Values_Configuration/item[last()]/UI_Style_Unchecked"
        @update-source-select "'btn-medium white bg-gray3'"

@end-execute

@execute
    @execute-when "is-not-blank(Allow_NA) and count(Point_Values_Configuration/item[Text = 'N/A']) = 0 and count(Point_Values_Configuration/item[Text != '' or ID != '']) > 0"

    @update
        @update-target-select "Point_Values_Configuration"
        @update-source-select "CreateItem"
        @update-source-type "Item"
    
    @update
        @update-target-select "Point_Values_Configuration/item[last()]/ID"
        @update-source-select "'NA'"
    
    @update
        @update-target-select "Point_Values_Configuration/item[last()]/Text"
        @update-source-select "'N/A'"
        
    @update
        @update-target-select "Point_Values_Configuration/item[last()]/Value"
        @update-source-select "'N/A'"
        
    @update
        @update-target-select "Point_Values_Configuration/item[last()]/UI_Style_Checked"
        @update-source-select "'btn-medium white bcolor-inverse'"
        
    @update
        @update-target-select "Point_Values_Configuration/item[last()]/UI_Style_Unchecked"
        @update-source-select "'btn-medium white bg-gray3'"
        

@end-execute

@execute
    @execute-when "is-blank(Allow_NA) and count(Point_Values_Configuration/item[Text = 'N/A']) > 0"

    @update
        @update-target-select "Point_Values_Configuration/item[Text = 'N/A']"
        @update-source-type "Node"

@end-execute 

@execute
    @execute-when "is-not-blank(Point_Values) and (is-blank(Options_Count) or Options_Count != count(Point_Values_Configuration/item))"
    
    @update
        @update-target-select "Options_Count"
        @update-source-select "count(Point_Values_Configuration/item)"
        
@end-execute

@end-block