﻿@display "No"

@comment
===================================================
This wiki contains functions to be used by the implementation team when building queues for APL Subprocesses
===================================================
@end-comment

@comment
Get the queue activator for the queue of APL subprocesses for the Nth subprocess as configured
  #param Integer $SequenceIndex The index of the subprocess in the configuration
  #returns String The activator name to apply for this queue
@end-comment
@function "SCQM.APLGroup.GetSubprocessQueueActivator"
@parameter "$SequenceIndex"
call('SCQM.APL.GetSubprocessName', $SequenceIndex)
@end-function

@comment
Determine whether the subprocess queue should be presented on the activator in the given sequence
If the Nth subprocess is configured, and the configured subprocess is applicable for the APL Group and the subprocess is configured to be processed for each country in the group
  #param Integer $SequenceIndex The index of the subprocess in the configuration
  #param StoreId $APLGroupStoreId The StoreId of the APL Group
  #returns Boolean True if the queue should be displayed
@end-comment
@function "SCQM.APLGroup.GetSubprocessQueueTest"
@parameter "$SequenceIndex"
@parameter "$APLGroupStoreId"
not(call('SCQM.APL.OncePerAPLGroupChecked', $SequenceIndex))
and call('SCQM.APL.SubprocessTabVisible', $APLGroupStoreId, $SequenceIndex)
and call('SCQM.APL.SubprocessQueueVisible', $APLGroupStoreId, $SequenceIndex)
and queue-count('SCQM_Approved_Product_List_Subprocess_Pairing.Master_Q', concat('APL_Group_Store = ', char(39),$APLGroupStoreId,char(39), ' AND Subprocess_Config_Id = ', char(39),call('SCQM.APL.GetSubprocessConfigId', $SequenceIndex),char(39), ' AND Status_Code IN ', char(40), char(39),'APL_SP_Initialized',char(39), char(44), char(39),'APL_SP_Proposed',char(39), char(44), char(39),'APL_SP_Active',char(39), char(41))) > 0
@end-function

@comment
Determine whether the subprocess queue should presented a Create New Action button 
If there are new countries and the configured condition on the subprocess for displaying the new countries is met, show the new button
  #param Integer $SequenceIndex The index of the subprocess in the configuration
  #param StoreId $APLGroupStoreId The StoreId of the APL Group
  #returns Boolean True if the new action button should be displayed
@end-comment
@function "SCQM.APLGroup.ShowNewSubprocessButton"
@parameter "$SequenceIndex"
@parameter "$APL_Group"
boolean(call('SCQM.APL.ShowRequestNewCountriesForSubprocess_ByIndex', $APL_Group, $SequenceIndex))
@end-function