﻿@title "^^Password_Recovery_Title|Password Recovery^^"
    @title-ui-style "primary large"
@description "^^Choose_Password_Recovery_Questions|Choose and answer your password recovery questions^^"
@mode "Password_Recovery"
@newcolumn

@newline "Medium"

@block
    @block-visible-when "false()"
    @block-ui-style "well well-mini well-danger-light medium padding-tb-sm span9"

^^Security_Questions_Disabled_Instructions|This site is currently not using the Forgot Password Feature.  Security Questions have been disabled.^^

@end-block

@newline "Small"

@block
    @block-visible-when "true()"

    @block
        @block-visible-when "/store/@id != user-id() and (is-blank(/store/@masterStoreId) or /store/@masterStoreId != user-id())"
        
        @label 
            @label-header "^^User_Security_Question1|Security Question 1^^:"
            @label-template "{{if(is-blank(Security_Question_1), '^^Security_Ques_Not_Set|NOT SET^^', Security_Question_1)}}"
            @label-ui-style "bold"
        
        @label
            @label-header "^^User_Security_Answer1|Security Answer 1^^:"
            @label-template "{{if(is-blank(Security_Answer_1), '^^Security_Ques_Not_Set|NOT SET^^', Security_Answer_1)}}"
            @label-ui-style "bold"
        
        @label
            @label-header "^^User_Security_Question2|Security Question 2^^:"
            @label-template "{{if(is-blank(Security_Question_2), '^^Security_Ques_Not_Set|NOT SET^^', Security_Question_2)}}"
            @label-ui-style "bold"
        
        @label
            @label-header "^^User_Security_Answer2|Security Answer 2^^:"
            @label-template "{{if(is-blank(Security_Answer_2), '^^Security_Ques_Not_Set|NOT SET^^', Security_Answer_2)}}"
            @label-ui-style "bold"
    @end-block
    
    @block
        @block-visible-when "(/store/@id = user-id() or /store/@masterStoreId = user-id()) or call('Core.IsITUser')"
        
        @choice "Security_Question_1"
            @choice-header "^^Security_Question1|Security Question 1^^"
            @choice-style "DropDownList"
            @choice-optional "Yes"
            
            @option "Childhood_Friend"
                @option-text "^^Childhood_Friend|What is the name of your favorite childhood friend^^?"
                @option-value "^^Childhood_Friend|What is the name of your favorite childhood friend^^?"
            @option "City_Parents_Met"
                @option-text "^^City_Parents_Met|In what city or town did your father and mother meet^^?"
                @option-value "^^City_Parents_Met|In what city or town did your father and mother meet^^?"
            @option "First_School"
                @option-text "^^First_School|What is the name of the first school you attended^^?"
                @option-value "^^First_School|What is the name of the first school you attended^^?"
            @option "Street_Third_Grade"
                @option-text "^^Street_Third_Grade|What street did you live on in third grade^^?"
                @option-value "^^Street_Third_Grade|What street did you live on in third grade^^?"
            @option "First_Car"
                @option-text "^^First_Car|What was the make and model of your first car^^?"
                @option-value "^^First_Car|What was the make and model of your first car^^?"
        @end-choice
        
        @newline
        
        @block
            @block-visible-when "is-not-blank(Security_Question_1)"
            
            @text "Security_Answer_1"
                @text-header "^^Security_Answer1|Answer 1^^:"
                @text-header-ui-style "bold"
                @text-expression "If(is-changed(Security_Question_1), '', Security_Answer_1)"
                @text-optional "Yes"
        @end-block
        
        @newline
        
        @choice "Security_Question_2"
            @choice-header "^^Security_Question2|Security Question 2^^"
            @choice-style "DropDownList"
            @choice-optional "Yes"
            
            @option "First_Boyfriend_Girlfriend"
                @option-text "^^First_Boyfriend_Girlfriend|What is the first and last name of your first boyfriend or girlfriend^^?"
                @option-value "^^First_Boyfriend_Girlfriend|What is the first and last name of your first boyfriend or girlfriend^^?"
            @option "Childhood_Phone_Number"
                @option-text "^^Childhood_Phone_Number|Which phone number do you remember most from your childhood^^?"
                @option-value "^^Childhood_Phone_Number|Which phone number do you remember most from your childhood^^?"
            @option "Childhood_Favorite_Place"
                @option-text "^^Childhood_Favorite_Place|What was your favorite place to visit as a child^^?"
                @option-value "^^Childhood_Favorite_Place|What was your favorite place to visit as a child^^?"
            @option "Favorite_Artist"
                @option-text "^^Favorite_Artist|Who is your favorite actor, musician, or artist^^?"
                @option-value "^^Favorite_Artist|Who is your favorite actor, musician, or artist^^?"
            @option "Childhood_Memorable_Person"
                @option-text "^^Childhood_Memorable_Person|What is the name of a memorable person from your childhood^^?"
                @option-value "^^Childhood_Memorable_Person|What is the name of a memorable person from your childhood^^?"
        @end-choice
    
        @newline
        
        @block
            @block-visible-when "is-not-blank(Security_Question_2)"
            
            @text "Security_Answer_2"
                @text-header "^^Security_Answer2|Answer 2^^:"
                @text-header-ui-style "bold"
                @text-optional "Yes"
        @end-block
    @end-block

    @button "Update_Security_Questions"
        @button-value "1"
        @button-text "^^Update Security Questions|Update Security Questions^^"
        @button-inline "Yes"
        @button-ui-style "btn-success medium"
        @button-optional "Yes"
        @button-accept "Yes"
@end-block