﻿@mode "Signature"
@title "^^Signature|Signature^^"
@title-ui-style "xx-large primary"
@title-icon "icon-pencil"

@newline "Small"

@block
@block-ui-style "well well-primary-light text-center x-large"
@block-visible-when "Status = 'Closed'"
^^The_audit_is_now_closed|The audit is now closed.^^
@end-block

@block
@block-visible-when "Status = 'Completed' and not(is-complete(Questionnaire) and is-not-blank(Manager_First_Name) and is-not-blank(Manager_Last_Name) and ((is-not-blank(No_Signature) and is-not-blank(No_Signature_Reason)) or is-not-blank(Manager_Signature)) and is-not-blank(Specialist_Signature) and is-not-blank(Audit_Report) and ((Active_Parameters/Responses_Reviewed_By = 'Service Provider' and Create_Corrective_Actions = 'Mandatory' and is-not-blank(Typical_Days_Off) and is-not-blank(Remediation_Phone_Number)) or is-blank(Active_Parameters/Responses_Reviewed_By) or Active_Parameters/Responses_Reviewed_By != 'Service Provider' or Create_Corrective_Actions != 'Mandatory'))"

@button "Status"
@button-value "In-Progress"
@button-text "^^Edit_Audit_Form|Edit Audit Form^^"
@button-show-value "No"
@button-action "Save"
@button-optional "Yes"
@button-causes-reload "Yes"
@button-inline "Yes"
@button-ui-style "btn-large btn-primary"

{label(medium bold margin-lr-med):"^^Make_Additional_Changes|If you need to make additional changes to the report, click the 'Edit Audit Form' button.^^"}

@end-block

@newline "Small"

@block
@block-visible-when "Status = 'Completed' and is-complete(Questionnaire)"
@button "Status_Check"
@button-value "Closed"
@button-text "^^Close_Audit|Close Audit^^"
@button-show-value "No"
@button-action "Save"
@button-causes-reload "Yes"
@button-inline "Yes"
@button-ui-style "btn-large btn-primary"

{label(medium bold margin-lr-med):"^^Close_Audit_Msg|Please click the 'Close Audit' button to finish the audit after filling in this page.^^"}

@newline "Medium"

@end-block

@comment
Tactical Sync for offline client.

@end-comment

@block "Tactical_Sync"
@block-visible-when "Status = 'Closed' and is-offline() and number-or-default(substring(version(),1,4),0) >= 6.36"

@block
@block-ui-style "clear form-inline"

@button "Tactical_Sync_Action" 
@button-value "Synchronize" 
@button-text "^^Synchronize_This|Synchronize This Audit^^" 
@button-causes-reload "No"
@button-causes-close "No"
@button-ui-style "btn-warning pull-left margin-lr-med"
@button-optional "Yes"

@label "localize-timestamp(Tactical_Sync_Done_Local_Stamp)"
@label-ui-style "small"
@label-header "Last Tactical Sync on this Audit: "

@end-block

@block
@block-visible-when "false()"
@text "Tactical_Sync_Done_Local_Stamp"
@text-header "Tactical Sync Completed"
@text-type "DateTime"
@end-block

{label(small margin-lr-med):"^^Synchronize_This_Msg|Selecting this option will synchronize this audit only and will not sync any data down to your tablet. To do a full sync, please click on the Synchronize button in the upper right hand corner of your screen.^^"}

@newline "Small"

@alert "Tactical_Sync_Alert" 
@alert-visible-when "is-synchronizing()" 
    {label(danger medium margin-lr-med margin-tb-sm):"^^Syncronization_In_Progress|Synchronization in progress!^^"}
@end-alert 

@monitor "Sync_State_Monitor " 
@monitor-active-when "is-offline() and is-synchronizing()" 
@monitor-polling-interval "5 seconds" 
@monitor-action "Refresh"

@end-block 

@block "Manager_Name_Block"
@block-visible-when "(Status = 'Completed' and is-complete(Questionnaire)) or Status = 'Closed'"

@label "time-zone-of(now())"
@label-header "^^Computer_Time_Zone|Computer's Time Zone^^:"

@label "time-zone-of(Audit_Start_Date)"
@label-header "^^Chosen_Time_Zone|Chosen Time Zone^^:"

@text "Audit_Start_Date"
@text-header "^^Start_Date_Time|Start Date/Time^^:"
@text-type "DateTime"
@text-read-only-when "Status = 'Closed'"

@block
@block-visible-when "is-not-blank(Audit_Start_Date) and is-not-blank(Audit_Start_Date_Original) and Audit_Start_Date != Audit_Start_Date_Original"
@label "if(is-not-blank(Audit_Start_Date) and is-not-blank(Audit_Start_Date_Original) and Audit_Start_Date != Audit_Start_Date_Original, concat('^^Original_Start_Date_Time|Original Start Date/Time^^: ', localize-datetime(Audit_Start_Date_Original), ' (', time-zone-of(Audit_Start_Date_Original), ')'), ' ')"
@label-header " "
@end-block

@text "Audit_Completion_Date"
@text-header "^^Completion_Date_Time|Completion Date/Time^^:"
@text-type "DateTime"
@text-default-expression "date-set(now(), '0 seconds')"
@text-read-only-when "Status = 'Closed'"

@newline "Medium"

{label(medium primary):"^^Time_Zone_Msg|If your time zone is not accurate, change the start and complete date/time of the audit and pick the appropriate time zone.^^"}

@label "if(is-not-blank(Audit_Start_Date) and is-not-blank(Audit_Completion_Date), concat(date-difference(Audit_Start_Date, Audit_Completion_Date, 'hours'), ' ^^hours_and|hour(s) and^^ ', date-difference(Audit_Start_Date, Audit_Completion_Date, 'minutes') - date-difference(Audit_Start_Date, Audit_Completion_Date, 'hours') * 60, ' ^^minutes|minute(s)^^'), '')"
@label-header "^^Duration|Duration^^:"

@block
@block-visible-when "is-blank(No_Signature)"
@signature "Manager_Signature"
@signature-header "^^Manager_Signature|Manager Signature^^:"
@signature-read-only-when "Status = 'Closed'"
@end-block

@checkbox "No_Signature"
@checkbox-header "^^Unable_Managers_Signature|Unable to get manager's signature?^^"
@checkbox-read-only-when "Status = 'Closed'"

@block
@block-visible-when "is-not-blank(No_Signature)"
@text "No_Signature_Reason"
@text-header "^^Unable_Managers_Signature_Reason|Please describe why you were unable to get a manager's signature^^:"
@text-rows "4"
@text-read-only-when "Status = 'Closed'"
@end-block

@text "Manager_First_Name"
@text-header "^^Manager_First_Name|Manager First Name^^:"
@text-read-only-when "Status = 'Closed'"
@text "Manager_Last_Name"
@text-header "^^Manager_Last_Name|Manager Last Name^^:"
@text-read-only-when "Status = 'Closed'"

@comment
block
block-visible-when "is-not-blank(Manager_First_Name) and is-not-blank(Manager_Last_Name)"
@end-comment
@label "Specialist_Full_Name"
@label-header "^^QMS_Name|QMS Name^^:"

@signature "Specialist_Signature"
@signature-header "{Specialist_Full_Name} ^^Signature|Signature^^:"
@signature-read-only-when "Status = 'Closed'"
@comment
end-block
@end-comment

@end-block "Manager_Name_Block"

@block
@block-visible-when "Status = 'Closed'"
@label "Audit_Completion_Date"
@label-header "^^Completion_Date_Time|Completion Date/Time^^:"
@end-block

@comment
 block-visible-when "Active_Parameters/Responses_Reviewed_By = 'Service Provider' and (Create_Corrective_Actions = 'Mandatory' or is-blank(Create_Corrective_Actions))"
@end-comment
@block "Remediation"
@block-visible-when "Active_Parameters/Responses_Reviewed = 'Yes' and Active_Parameters/Responses_Reviewed_By = 'Service Provider' and Create_Corrective_Actions = 'Mandatory'"

@newline "Small"

{label(medium primary):"^^Gather_CAP_Information|Gather Information for CAP Consultation^^:"}

@choice "Typical_Days_Off"
@choice-style "CheckBoxList"
@choice-columns "2"
@choice-header "^^Days_Off|Check typical days off^^:"
@choice-read-only-when "Status = 'Closed'"
@option "Sunday"
@option-text "^^Sunday|Sunday^^"
@option-value "Sunday"
@option "Monday"
@option-text "^^Monday|Monday^^"
@option-value "Monday"
@option "Tuesday"
@option-text "^^Tuesday|Tuesday^^"
@option-value "Tuesday"
@option "Wednesday"
@option-text "^^Wednesday|Wednesday^^"
@option-value "Wednesday"
@option "Thursday"
@option-text "^^Thursday|Thursday^^"
@option-value "Thursday"
@option "Friday"
@option-text "^^Friday|Friday^^"
@option-value "Friday"
@option "Saturday"
@option-text "^^Saturday|Saturday^^"
@option-value "Saturday"
@option "None"
@option-text "^^Noned|None^^"
@option-value "None"
@end-choice

@block
@block-visible-when "false()"
@text "Remediation_Typical_Days_Off"
@text-header "Typical Days Off:"
@text-expression "join(Typical_Days_Off/*, ', ')"
@end-block

@text "Remediation_Phone_Number"
@text-header "^^Consultation_Phone_Number|Consultation Phone Number^^:"
@text-validation-test "Match(Remediation_Phone_Number, 'Phone')"
@text-mask "(999) 999-9999"
@text-validation-error "^^Phone_Number_Validation_Msg|Please enter a valid phone number with a 3-digit area code.^^"
@text-footer "^^No_Cell_Phone|No cell phone numbers^^"
@text-read-only-when "Status = 'Closed'"

@text "Remediation_Phone_Number_Extension"
@text-header "^^Consultation_Phone_Extension|Consultation Phone Extension^^:"
@text-optional "Yes"
@text-read-only-when "Status = 'Closed'"

@block
@block-visible-when "false()"
@text "Consultation_Input_Count"
@text-header "Consultation Input Count:"
@text-type "Integer"
@text-expression "count(Active_Parameters/Audit_Data_For_Consultation/*)"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 0"
{{Active_Parameters/Audit_Data_For_Consultation/item[1]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_1"
@text-read-only-when "Status = 'Closed'"

@block
@block-visible-when "Consultation_Input_Count > 1"
{{Active_Parameters/Audit_Data_For_Consultation/item[2]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_2"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 2"
{{Active_Parameters/Audit_Data_For_Consultation/item[3]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_3"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 3"
{{Active_Parameters/Audit_Data_For_Consultation/item[4]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_4"
@text-read-only-when "Status = 'Closed'"

@end-block

@block
@block-visible-when "Consultation_Input_Count > 4"
{{Active_Parameters/Audit_Data_For_Consultation/item[5]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_5"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 5"
{{Active_Parameters/Audit_Data_For_Consultation/item[6]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_6"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 6"
{{Active_Parameters/Audit_Data_For_Consultation/item[7]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_7"
@text-read-only-when "Status = 'Closed'"
@end-block

@block
@block-visible-when "Consultation_Input_Count > 7"
{{Active_Parameters/Audit_Data_For_Consultation/item[8]/Consultation_Audit_Data_Name}}:
@text "Consultation_Data_8"
@text-read-only-when "Status = 'Closed'"
@end-block
@end-block

@end-block "Remediation"

@newline "Small"

@block
@block-visible-when "Status = 'Completed' and is-complete(Questionnaire)"
@button "Status_Check"
@button-value "Closed"
@button-text "^^Close_Audit|Close Audit^^"
@button-show-value "No"
@button-action "Save"
@button-causes-reload "Yes"
@button-inline "Yes"
@button-ui-style "btn-large btn-primary"

@newline "Medium"

@end-block