﻿@display "No"

@comment
================================================================================
Functions related to Identifying lists of APL-approved Countries, Regions,
APLs, and Categories by Facility
================================================================================
@end-comment

@function "SCQM.OrgLoc.GetApprovedCountriesList"
@comment
Get a list of all countries in which the given facility has been approved or Conditionally Approved
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved countries
  #returns String Comma-delimited list of country names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Countries.APL_OrgLocs_Distinct_Approved_Countries', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Country_Name', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Countries.APL_OrgLocs_Distinct_Approved_Countries', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Country_Name', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Country_Name,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedRegionsList"
@comment
Get a list of all regions in which the given facility has been approved or Conditionally Approved
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved regions
  #returns String Comma-delimited list of country names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Regions.APL_OrgLocs_Distinct_Approved_Regions', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Region_Code', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Regions.APL_OrgLocs_Distinct_Approved_Regions', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Region_Code', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Region_Code,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedAPLList"
@comment
Get a list of all APL IDs (group Ids) in which the given facility has been approved or Conditionally Approved
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved APL IDs
  #returns String Comma-delimited list of country names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_APLs.APL_OrgLocs_Distinct_Approved_APLs', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'APL_Id', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_APLs.APL_OrgLocs_Distinct_Approved_APLs', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'APL_Id', 
            -1, 
            -1, 
            '', 
            false()
        )/item/APL_Id,
        ', '
    )
)
@end-function

@comment
================================================================================
Lists of categories at each possible hierachical level, as configured for the site
================================================================================
@end-comment

@function "SCQM.OrgLoc.GetApprovedCategory1List"
@comment
Get a list of all Categories (at level 1) of products for which the given facility has been approved or Conditionally Approved to produce
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved categories
  #returns String Comma-delimited list of category names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Category1.APL_OrgLocs_Distinct_Approved_Category1', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Product_Hierarchy_1', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Category1.APL_OrgLocs_Distinct_Approved_Category1', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Product_Hierarchy_1', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Product_Hierarchy_1,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedCategory2List"
@comment
Get a list of all Categories (at level 2) of products for which the given facility has been approved or Conditionally Approved to produce
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved categories
  #returns String Comma-delimited list of category names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Category2.APL_OrgLocs_Distinct_Approved_Category2', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Product_Hierarchy_2', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Category2.APL_OrgLocs_Distinct_Approved_Category2', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Product_Hierarchy_2', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Product_Hierarchy_2,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedCategory3List"
@comment
Get a list of all Categories (at level 3) of products for which the given facility has been approved or Conditionally Approved to produce
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved categories
  #returns String Comma-delimited list of category names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Category3.APL_OrgLocs_Distinct_Approved_Category3', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Product_Hierarchy_3', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Category3.APL_OrgLocs_Distinct_Approved_Category3', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Product_Hierarchy_3', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Product_Hierarchy_3,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedCategory4List"
@comment
Get a list of all Categories (at level 4) of products for which the given facility has been approved or Conditionally Approved to produce
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved categories
  #returns String Comma-delimited list of category names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Category4.APL_OrgLocs_Distinct_Approved_Category4', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Product_Hierarchy_4', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Category4.APL_OrgLocs_Distinct_Approved_Category4', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Product_Hierarchy_4', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Product_Hierarchy_4,
        ', '
    )
)
@end-function

@function "SCQM.OrgLoc.GetApprovedCategory5List"
@comment
Get a list of all Categories (at level 5) of products for which the given facility has been approved or Conditionally Approved to produce
  #param StoreId $OrgLocStoreId StoreId of the Location for which we want approved categories
  #returns String Comma-delimited list of category names
@end-comment
@parameter "$OrgLocStoreId"
if(
    queue-count(
        'SCQM_APL_OrgLoc_Category5.APL_OrgLocs_Distinct_Approved_Category5', 
        concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
        'Product_Hierarchy_5', 
        -1, 
        -1, 
        '', 
        false()
    ) > 3,
    'Multiple',
    join(
        queue(
            'SCQM_APL_OrgLoc_Category5.APL_OrgLocs_Distinct_Approved_Category5', 
            concat('ORGLOC_STORE_ID = ', char(39), $OrgLocStoreId, char(39)), 
            'Product_Hierarchy_5', 
            -1, 
            -1, 
            '', 
            false()
        )/item/Product_Hierarchy_5,
        ', '
    )
)
@end-function