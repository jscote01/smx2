﻿@topic
@topic-title ""

@monitor "Processing_Monitor"
@monitor-active-when "is-not-blank(/store/*[1]/Processing) or call('MinimumProcessingTimeEnforced')"
@monitor-polling-interval "1 second"
@monitor-action "Reload"

@comment
============================================================
Processing Message
============================================================
@end-comment
@block
@block-visible-when "(is-not-blank(/store/*[1]/Processing) or call('MinimumProcessingTimeEnforced'))"
@block-ui-style "well well-mini well-info-light inverse"

    @icon "icon-info-sign info medium"
    {label(margin-lr-mini medium): '^^Monitor_message|The system is processing your request. You may wait or navigate away at any time^^.'}

@end-block

@comment
============================================================
Rejection message
============================================================
@end-comment
@block
@block-visible-when "../Status/Status_Code = 'Rejected' and ../Stage = '3' and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"
@block-ui-style "well well-info-light well-mini inverse"
    @icon "icon-info-sign info medium"
    {label(medium margin-lr-mini):'^^The worksheet was rejected with the following reason|The worksheet was rejected with the following reason^^: '}
    @newline "Small"
    {label(default):/store/*[1]/Return_Reject_Reasons/item[last()]/Reason}
@end-block


@comment
============================================================
Return message
============================================================
@end-comment
@block
@block-visible-when "(/store/*[1]/Status/Status_Code = 'Requested' or /store/*[1]/Status/Status_Code = 'Submitted') and is-not-blank(call('SCQM.AR.GetActionLogEntry', 'Stage 3 Org Workbook Returned', store-id())) and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"
@block-ui-style "clear {{if(/store/*[1]/Status/Status_Code = 'Submitted' and call('SCQM.AR.IsStage3OrgReviewer', /store/*[1]/SCQM_Approval_Request/@refId), 'span8', if(call('SCQM.AR.UserCanResendOrg', /store/*[1]/SCQM_Approval_Request/@refId, store-id()), 'span8', ''))}} well {{if(/store/*[1]/Status/Status_Code = 'Submitted', 'well-info-light', 'well-danger-light')}} well-mini inverse"
    @icon "{{if(/store/*[1]/Status/Status_Code = 'Submitted', 'icon-info-sign info', 'icon-warning-sign danger')}} medium"
    {label(medium margin-lr-mini):'^^The worksheet was returned with the following reason|The worksheet was returned with the following reason^^:'}
    @newline "Small"
    {label(default):/store/*[1]/Return_Reject_Reasons/item[last()]/Reason}
@end-block

@comment
============================================================
Approved message
============================================================
@end-comment
@block
@block-visible-when "../Status/Status_Code = 'Approved' and ../Stage = '3' and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"
@block-ui-style "well well-info-light well-mini inverse"
    @icon "icon-info-sign info medium"
    {label(medium margin-lr-mini):concat('^^The worksheet was approved on|The worksheet was approved on^^ ', localize-timestamp(/store/*[1]/Action_Log/item[Action = 'Stage 3 Org Workbook Approved'][last()]/Time), '.')}
@end-block

@comment
============================================================
Message to submitter after submission.
============================================================
@end-comment
@block
@block-visible-when "../Status/Status_Code = 'Submitted' and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced')) and user()/User_Name = 'guest.access'"
@block-ui-style "clear span8 well well-info-light well-mini inverse"
    @icon "icon-info-sign info medium"
    {label(medium margin-lr-mini):'^^Thank you for your submission|Thank you for your submission^^.'}
@end-block

@comment
============================================================
Re-Send Worksheet
============================================================
@end-comment
@block
@block-visible-when "call('SCQM.AR.UserCanResendOrg', /store/*[1]/SCQM_Approval_Request/@refId, store-id()) and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"
@block-ui-style "{{if(((/store/*[1]/Status/Status_Code = 'Requested' or /store/*[1]/Status/Status_Code = 'Submitted') and is-blank(Action))  and is-not-blank(call('SCQM.AR.GetActionLogEntry', 'Stage 3 Org Workbook Returned', store-id())), 'span4', '')}} align-right"

    @button "Resend"
    @button-value "Yes"
    @button-text "^^Resend Worksheet|Resend Worksheet^^"
    @button-optional "Yes"
    @button-inline "Yes"
    @button-ui-style "btn-inverse"        

    @newline        
    {label(default):concat('^^Requested|Requested^^: ', localize-timestamp(/store/*[1]/Created))}
    
    @block
    @block-visible-when "is-not-blank(Stage3_Org_Resent_Timestamp)"
    @newline    
    {label(default):concat('^^Last Sent|Last Sent^^: ', localize-timestamp(Stage3_Org_Resent_Timestamp))}
    @end-block    

@end-block

@comment
============================================================
Submission buttons
NOTE: All buttons are customizable.  Any button that requires processing should be named 'Action'.
============================================================
@end-comment
@block
@block-visible-when "../Status/Status_Code = 'Requested' and ../Stage = '3' and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"

    @button "Save"
    @button-value "Save"
    @button-text "^^Save|Save^^"
    @button-optional "Yes"
    @button-inline "Yes"
    @button-ui-style "pull-right margin-lr-mini"
    @button-action "Save"
    
    @button "Guide_Me"
    @button-text "^^Guide Me|Guide Me^^"
    @button-value "Guide Me"
    @button-ui-style "btn-info pull-right margin-lr-mini"
    @button-align "Left"
    @button-optional "Yes"
    @button-action "guideMe"
    
    @block
    @block-visible-when "count(tracking/topic[@completed = 'false' and (not(@visible) or @visible = 'true')]) = 0"
        @button "Action"
        @button-value "Submit"
        @button-text "^^Submit|Submit^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-success pull-right margin-lr-mini"
        @button-action "Save,Reload"
    @end-block

@end-block

@comment
============================================================
Approval buttons
NOTE: All buttons are customizable.  Any button that requires processing should be named 'Action'.
============================================================
@end-comment
@block
@block-visible-when "../Status/Status_Code = 'Submitted' and is-blank(/store/*[1]/Processing) and not(call('MinimumProcessingTimeEnforced'))"


    @block
    @block-visible-when "is-blank(Action)"
    
        @button "Action"
        @button-value "PreReject"
        @button-text "^^Reject|Reject^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-danger margin-lr-mini pull-right"

        @button "Action"
        @button-value "PreReturn"
        @button-text "^^Return|Return^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-danger margin-lr-mini pull-right"

        @button "Action"
        @button-value "Approve"
        @button-text "^^Approve|Approve^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-success margin-lr-mini pull-right"
    
    @end-block

    @block
    @block-visible-when "is-not-blank(Action) and Action != 'Approve'"
    
        @link "False_Button"
        @link-text "^^Reject|Reject^^"
        @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"

        @link "False_Button"
        @link-text "^^Return|Return^^"
        @link-ui-style "btn btn-danger margin-lr-mini pull-right disabled white"

        @link "False_Button"
        @link-text "^^Approve|Approve^^"
        @link-ui-style "btn btn-success margin-lr-mini pull-right disabled white"
    
    @end-block

    @newline

    @block "Return"
    @block-visible-when "Action = 'PreReturn' or Action = 'Returned'"
    @block-ui-style "form-horizontal pull-right"
    
        @text "Return_or_Reject_Reason"
        @text-header "^^Reason for Return|Reason for Return^^:"
        @text-rows "3"
        @text-validation-test "is-not-blank(Return_or_Reject_Reason) or Action = 'PreReturn'"
        @text-validation-error "^^A return reason is required|A return reason is required^^"
        
        @button "Action"
        @button-value "Cancel"
        @button-text "^^Cancel|Cancel^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-warning margin-lr-mini pull-right"
        @button-action "Save,Validate"

        @button "Action"
        @button-value "Returned"
        @button-text "^^Return|Return^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-danger margin-lr-mini pull-right"
        @button-action "Save,Validate,Reload"
        
        @newline
        
        {label(small pull-right):'^^This will return the process to the submitter|This will return the process to the submitter^^'}
    
    @end-block

    @block "Reject"
    @block-visible-when "Action = 'PreReject' or Action = 'Rejected'"
    @block-ui-style "form-horizontal pull-right"
    
        @text "Return_or_Reject_Reason"
        @text-header "^^Reason for Rejection|Reason for Rejection^^:"
        @text-rows "3"
        @text-validation-test "is-not-blank(Return_or_Reject_Reason) or Action = 'PreReject'"
        @text-validation-error "^^A rejection reason is required|A rejection reason is required^^"
        
        @button "Action"
        @button-value "Cancel"
        @button-text "^^Cancel|Cancel^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-warning margin-lr-mini pull-right"
        @button-action "Save,Validate"
        
        @button "Action"
        @button-value "Rejected"
        @button-text "^^Reject|Reject^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-danger margin-lr-mini pull-right"
        @button-action "Save,Validate"
        
        @newline
        
        {label(small pull-right):'^^This will end the process|This will end the process^^'}
    
    @end-block

@end-block


@comment
============================================================
Button executes
============================================================
@end-comment

@execute
@execute-when "(Action[.='Approve' or .='Submit'] or (Action[.='Returned' or .='Rejected'] and is-not-blank(Return_or_Reject_Reason))) and is-blank(/store/*[1]/Processing)"
    @update
    @update-target-select "/store/*[1]/Processing"
	@update-source-select "now(0)"

    @update
	@update-target-select "/store/*[1]/Processing_Timestamp"
	@update-source-select "now(0)"
@end-execute   

@execute
@execute-when "Action = 'Cancel'"

    @update
    @update-target-select "Return_or_Reject_Reason"
    @update-source-type "Node"
    
    @update
    @update-target-select "Action"
    @update-source-type "Node"

@end-execute

@execute
@execute-when "Action = 'Submit'"
    
    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Set_Status"
    @invoke-store-id "store-id()"
        @invoke-parameter "StatusCode"
        @invoke-parameter-expression "'Submitted'"
        @invoke-parameter "StatusGroup"
        @invoke-parameter-expression "'Approval States'"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Clear_Processing_Flag"

@end-execute

@execute
@execute-when "Action = 'Approve'"
    
    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Set_Status"
    @invoke-store-id "store-id()"
        @invoke-parameter "StatusCode"
        @invoke-parameter-expression "'Approved'"
        @invoke-parameter "StatusGroup"
        @invoke-parameter-expression "'Approval States'"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Clear_Processing_Flag"

@end-execute

@execute
@execute-when "Action = 'Rejected'"
    
    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Set_Status"
    @invoke-store-id "store-id()"
        @invoke-parameter "StatusCode"
        @invoke-parameter-expression "'Rejected'"
        @invoke-parameter "StatusGroup"
        @invoke-parameter-expression "'Approval States'"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Clear_Processing_Flag"

@end-execute

@execute
@execute-when "Action = 'Returned'"
    
    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Set_Status"
    @invoke-store-id "store-id()"
        @invoke-parameter "StatusCode"
        @invoke-parameter-expression "'Returned'"
        @invoke-parameter "StatusGroup"
        @invoke-parameter-expression "'Approval States'"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Clear_Processing_Flag"

@end-execute

@comment
Re-send worksheet to submitter
- Re-send notifications (same as when worksheet first created)
- Update last resend date/time
- clear the resend button
@end-comment
@execute
@execute-when "is-not-blank(Resend)"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Send_Notifications"
    @invoke-parameter "EmailToSend"
    @invoke-parameter-expression "'Stage3_Creation'"
    
    @update
    @update-target-select "Stage3_Org_Resent_Timestamp"
    @update-source-select "now(0)"
            
    @update
    @update-target-select "Resend"
    @update-source-type "Node"

    @invoke
        @invoke-execute "Core_Event_Log.RecordEvent"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "store-id()"
            @invoke-parameter-direction "In"
        @invoke-parameter "MappingID"
            @invoke-parameter-expression "'AR.Event.Stage3_Org_Worksheet_Resent'"
            @invoke-parameter-direction "In"

@end-execute


@comment
============================================================
PUBLISHER REQUIRED FOR WORKFLOW
NOTE: This must remain in place for the workflow engine to 
function correctly.
============================================================
@end-comment
@execute
@execute-when "Action[.='Submit' or .='Approve' or .='Rejected' or .='Returned']"

    @update
    @update-store-id "store-id()"
    @update-target-select "/store/*[1]/Org_Workbook_Stage3_Action"
    @update-source-select "coalesce(Action, now(0))"

    @invoke
    @invoke-execute "SCQM_Approval_Request_Executes.Clear_Org_Workbook_Action_Flags"    
    
@end-execute