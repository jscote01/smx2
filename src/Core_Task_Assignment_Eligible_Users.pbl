﻿@app "Core_Task_Assignment_Eligible_Users"

@language "all"

@display "No"

@@  This analyze queue will return a list of users which can have tasks assigned to them via WRM that the current User is eligible to take ownership of.
@@  This is used on the Action Item screen for the Assigned To dropdown lookup when Internal Users is selected

@analyze

    @query "Tasks"
    @query-queue "Core_Task_Assignment.Master_Q"
    @query-filter "Status_Code = 'AS_Open' and (Is_Local_Workflow_Role != 'true' or Is_Local_Workflow_Role IS NULL)"
    @query-store-id "TID"
    
        @column "Assignee"     
        @column "Context_Module_Id"
        @column "Context_Module_Name"
        @column "Additional_Context"    
        @column "Meta_Data_1"  
        @column "Meta_Data_2"
        @column "Meta_Data_3"
        @column "Meta_Data_4"
        @column "Meta_Data_5"
        @column "Is_Local_Workflow_Role"

    @end-query

    @query "My_Tasks"
        @query-queue "Core_Task_Assignment.Master_Q"
        @query-filter "Status_Code = 'AS_Open' and Assignee = {{user-id()}}"

        @column "Assignee"  
        @column "Context_Module_Id"
        @column "Context_Module_Name"

    
    @end-query


    @query "Assignments"
    @query-queue "Core_Workflow_Assignment.Master_Q"
    @query-filter "Status_Code = 'Active' and Assignee = {{user-id()}}"
    
        @column "Role"
        @column "Meta_Data_1"
        @column "Meta_Data_2"
        @column "Meta_Data_3"
        @column "Meta_Data_4"
        @column "Meta_Data_5"
        @column "Assignee"
        @column "Primary"
    
    @end-query



    @query "Users"
    @query-queue "User.Master_Q"
    
        @column "UID"
            @column-source "StoreId"
        
        @column "Informal_Full_Name"    
    
    @end-query



    @query "Combined_1"
    @query-store-id "Assignee"
    @subquery "Tasks"
    @subquery "Assignments"
    @subquery-join "Tasks.Additional_Context = Assignments.Role AND (Tasks.Meta_Data_1 = Assignments.Meta_Data_1 or Assignments.Meta_Data_1 = 'NA') AND (Tasks.Meta_Data_2 = Assignments.Meta_Data_2 or Assignments.Meta_Data_2 = 'NA') AND (Tasks.Meta_Data_3 = Assignments.Meta_Data_3 or Assignments.Meta_Data_3 = 'NA') AND (Tasks.Meta_Data_4 = Assignments.Meta_Data_4 or Assignments.Meta_Data_4 = 'NA') AND (Tasks.Meta_Data_5 = Assignments.Meta_Data_5 or Assignments.Meta_Data_5 = 'NA')"
    @subquery-join-type "Inner"
    
        @column "Assignee"  
            @column-source "Tasks.Assignee"    
        @column "Context_Module_Id"
            @column-source "Tasks.Context_Module_Id" 
        @column "Context_Module_Name"
            @column-source "Tasks.Context_Module_Name" 

    @end-query
    
    
    
    @query "Combined"
        @query-type "Union"
        @query-distinct-values "Yes"
        @subquery "Combined_1"
        @subquery "My_Tasks"
    
    @end-query    


    @query "Combined_User_Details"
    @query-store-id "User"
    @query-distinct-values "Yes"
    @subquery "Combined"
    @subquery "Users"
    @subquery-join "Combined.Assignee = Users.UID"
    @subquery-join-type "Left"
    
        @column "User"  
            @column-source "Combined.Assignee"   
        @column "User_Informal_Full_Name"
            @column-source "Users.Informal_Full_Name"    

    @end-query
    
    

    @query "Anyone_Row"
        @query-store-id "User"
    
        @column "User"
        @column "User_Informal_Full_Name"
    
        @row "Anyone|^^Anyone|Anyone^^"
    
    @end-query
    
    
    
    @query "End_All"
        @query-store-id "User"
        @query-type "Union"
        @subquery "Anyone_Row"
        @subquery "Combined_User_Details"
    
        @column "User"
        @column "User_Informal_Full_Name"
        
    @end-query
    

@end-analyze


@queue "All_Q"
    @queue-query "End_All"
    @queue-key "User"   
  
    @column "User_Informal_Full_Name"
        @column-header "^^Assignee|Assignee^^"    
        
    @column "User"
        @column-hidden "Yes"    

@end-queue