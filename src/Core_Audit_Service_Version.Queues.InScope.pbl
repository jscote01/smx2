﻿@display "No"


@comment
QUEUE TO DISPLAY IN SCOPE OF Account - Draft and Committed
@end-comment

@queue "Account_Active_Q"
@queue-header "^^Service Versions|Service Versions^^"
@queue-sort "Core_Audit_Service, Service_Version_Name, Version_Number DESC"
@queue-activator "^^Audit Applications|Audit Applications^^ | ^^Service Versions|Service Versions^^"
@queue-test "call('Core.CheckPermission', 'Core_Admin_Service_Version.Account_Active_Q') and call('Core.CheckCapability', 'CORE.Audit')" 
@queue-filter "Status_Code <> 'Archived' and Status_Code <> 'Deleted'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-allow-navigate "No"
@queue-scope "Core_Account"

    @action
        @action-name "NewAndExecute"
        @action-argument "Core_Audit_Service_Version"
        @action-text "^^Create new|Create new^^ ^^Service Version|Service Version^^"
        @action-container "Sidebar"
        @action-argument "FromInScope"
            @action-argument-value "Yes"
        @action-argument "IsNew"
            @action-argument-value "Yes"

    @action
        @action-name "Navigate"
        @action-text "Version_Notes | ^^OPEN|OPEN^^"
        @action-container "Column"
        
    @column "Core_Audit_Service"    

    @column "Version_Notes"

    @column "Version_Number"
    
    @column "Core_Organization"    
    
    @column "Created_Date"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue


@comment
QUEUE TO DISPLAY IN SCOPE OF Organization - Archived
@end-comment
@queue "Account_Archived_Q"
@queue-header "^^Archived Service Versions|Archived Service Versions^^"
@queue-sort "Core_Audit_Service, Service_Version_Name, Version_Number DESC"
@queue-activator "^^Audit Applications|Audit Applications^^ | ^^Service Versions|Service Versions^^"
@queue-test "call('Core.CheckPermission', 'Core_Audit_Service_Version.Account_Archived_Q') and call('Core.CheckCapability', 'CORE.Audit')" 
@queue-filter "Status_Code = 'Archived' and Status_Code <> 'Deleted'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-scope "Core_Account"
@queue-allow-navigate "No"

    @action
        @action-name "Navigate"
        @action-text "Version_Notes | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "Core_Audit_Service"

    @column "Service_Version_Name"

    @column "Version_Notes"
    
    @column "Core_Organization"  
    
    @column "Created_Date"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue

@comment
QUEUE TO DISPLAY IN SCOPE OF Organization - Draft and Committed
@end-comment

@queue "Organization_Active_Q"
@queue-header "^^Service Versions|Service Versions^^"
@queue-sort "Core_Audit_Service, Service_Version_Name, Version_Number DESC"
@queue-activator "^^Audit Applications|Audit Applications^^ | ^^Service Versions|Service Versions^^"
@queue-test "call('Core.CheckPermission', 'Core_Admin_Service_Version.Organization_Active_Q') and call('Core.CheckCapability', 'CORE.Audit')" 
@queue-filter "Status_Code <> 'Archived' and Status_Code <> 'Deleted'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-allow-navigate "No"
@queue-scope "Core_Organization"

    @action
        @action-name "NewAndExecute"
        @action-argument "Core_Audit_Service_Version"
        @action-text "^^Create new|Create new^^ ^^Service Version|Service Version^^"
        @action-container "Sidebar"
        @action-argument "FromInScope"
            @action-argument-value "Yes"
        @action-argument "IsNew"
            @action-argument-value "Yes"

    @action
        @action-name "Navigate"
        @action-text "Version_Notes | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "Core_Audit_Service"

    @column "Version_Notes"

    @column "Version_Number"
    
    @column "Created_Date"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue


@comment
QUEUE TO DISPLAY IN SCOPE OF Organization - Archived
@end-comment
@queue "Organization_Archived_Q"
@queue-header "^^Archived Service Versions|Archived Service Versions^^"
@queue-sort "Core_Audit_Service, Service_Version_Name, Version_Number DESC"
@queue-activator "^^Audit Applications|Audit Applications^^ | ^^Service Versions|Service Versions^^"
@queue-test "call('Core.CheckPermission', 'Core_Audit_Service_Version.Organization_Archived_Q') and call('Core.CheckCapability', 'CORE.Audit')" 
@queue-filter "Status_Code = 'Archived' and Status_Code <> 'Deleted'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-scope "Core_Organization"
@queue-allow-navigate "No"

    @action
        @action-name "Navigate"
        @action-text "Service_Version_Name | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "Core_Audit_Service"

    @column "Service_Version_Name"

    @column "Version_Number"
    
    @column "Created_Date"
    
    @column "Version_Notes"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue


@comment
QUEUE TO DISPLAY IN SCOPE OF SERVICE - Draft and Committed
@end-comment

@queue "Service_Active_Q"
@queue-header "^^Form Versions|Form Versions^^"
@queue-description "This queue displays any Service Versions that have been defined as belonging to a particular Service. Some Service Versions are not attached to a particular Service or Account and can be seen in the 'Admin | Service Versions' menu location."
@queue-sort "Version_Number DESC"
@queue-activator "^^Summary|Summary^^"
@queue-filter "Status_Code <> 'Archived' and Status_Code <> 'Deleted'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-allow-navigate "No"
@queue-scope "Core_Audit_Service"

    @action
        @action-name "NewAndExecute"
        @action-argument "Core_Audit_Service_Version"
        @action-text "^^Create new|Create new^^ ^^Form Version|Form Version^^"
        @action-container "Sidebar"
        @action-argument "IsNew"
            @action-argument-value "Yes"
        @action-argument "FromInScope"
            @action-argument-value "Yes"


    @action
        @action-name "Navigate"
        @action-text "Version_Notes | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "Version_Notes"

    @column "Version_Number"
    
    @column "Created_Date"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue


@comment
QUEUE TO DISPLAY IN SCOPE OF SERVICE - Archived
@end-comment

@queue "Service_Archived_Q"
@queue-header "^^Archived Form Versions|Archived Form Versions^^"
@queue-activator "^^Summary|Summary^^"
@queue-filter "Status_Code = 'Archived'"
@queue-sequence "111"
@queue-allow-new "No"
@queue-scope "Core_Audit_Service"
@queue-allow-navigate "No"

    @action
        @action-name "Navigate"
        @action-text "Version_Notes | ^^OPEN|OPEN^^"
        @action-container "Column"

    @column "Version_Notes"

    @column "Version_Number"
    
    @column "Created_Date"
    
    @column "Report_Template_Used"

    @column "Status"
@end-queue