﻿@display "No"

@execute "GenerateSentences"
@comment
Used to evaluate a series of expressions against the given context store.

invoke-parameter "Mapping_ID"
    invoke-parameter-expression "Mapping ID in Core_Token_Mapping"
invoke-parameter "ContextStoreId"
    invoke-parameter-expression "Store Id Expected"
invoke-parameter "SentenceList"
    invoke-parameter-expression "Xpath to record Sentence List"
    invoke-parameter-direction "OUT"
@end-comment

    @comment
    Capture the Core_Token_Mapping information to local variable
    @end-comment    
    @update
        @update-target-select "$Parameter:_locals/Mapping"
        @update-source-select "store(queue('Core_Token_Mapping.Site_Admin_Q', concat('Mapping_ID = ', char(39), $Parameter:Mapping_ID, char(39)))/item[1]/StoreId)/Core_Token_Mapping"
        @update-source-type "Node"
    
    @comment
    Capture the pre-save value of the context store to a global variable $Previous_Store when required by the Token Mapping
    @end-comment    
    @invoke
        @invoke-execute "StorePreviousValues"
        @invoke-when "$Parameter:_locals/Mapping/Access_Previous_Version = 'true'"
        @invoke-parameter "ContextStoreId"
            @invoke-parameter-expression "$Parameter:ContextStoreId"

    @comment
    Publish the context store when required by the Token Mapping
    @end-comment    
    @invoke
        @invoke-execute "SaveContextStore"
        @invoke-store-id "$Parameter:ContextStoreId"
        @invoke-when "$Parameter:_locals/Mapping/Run_Workbook_For_Context = 'true'"

    @comment
    Prepare a global variable $Sentences to be used in the execute-select loop to store all Token Mapping sentences as list items as they are created
    @end-comment    
    @update
        @update-target-select "$Sentences"
        @update-source-select "''"

    @comment
    Invoke the sentence generation execute-select loop for each expression in the Token Mapping.  Record to the global variable $Sentences
    @end-comment    
    @invoke
        @invoke-execute "GenerateSentenceTexts"
        @invoke-parameter "ContextStoreId"
            @invoke-parameter-expression "$Parameter:ContextStoreId"
        @invoke-parameter "TextExpressions"
            @invoke-parameter-expression "$Parameter:_locals/Mapping/Text_Expressions"

    @comment
    Record the full list of sentences generated to the OUT parameter SentenceList
    @end-comment    
    @update
        @update-target-select "$Parameter:SentenceList"
        @update-source-select "$Sentences"
        @update-source-type "Node"

    @comment
    Ensure the global variable $Previous_Store is cleared when finished
    @end-comment    
    @update
        @update-target-select "$Previous_Store"
        @update-source-type "Node"

    @comment
    Ensure the global variable $Sentences is cleared when finished
    @end-comment    
    @update
        @update-target-select "$Sentences"
        @update-source-type "Node"

@end-execute


@execute "GenerateSentenceTexts"
    @execute-select "$Parameter:TextExpressions/item"
    @execute-select-key "@id"

    @comment
    Loop through the Token Mapping expressions and capture the Field_Name and the evaluated Sentence for each expression
    Record to the global variable $Sentences as a list item
    @end-comment    

    @update
        @update-target-select "$Parameter:_locals/Field_Name"
        @update-source-select "execute-select-context()/Field_Name"

    @update
        @update-target-select "$Parameter:_locals/Sentence"
        @update-source-select "if(execute-select-context()/Use_Token = 'true', call('Core.EvalToken', execute-select-context()/Text_Expression/Text_Token, $Parameter:ContextStoreId), eval(execute-select-context()/Plain_Text_Expression, store($Parameter:ContextStoreId)/*[1]))"

    @update
        @update-target-select "$Sentences"
        @update-source-select "$Parameter:_locals/Field_Name | $Parameter:_locals/Sentence"
        @update-source-type "Item"        

@end-execute


@execute "GenerateSentenceTextsInCurrentContext"
    @execute-select "$Parameter:TextExpressions/item"
    @execute-select-key "@id"

    @comment
    Loop through the Token Mapping expressions and capture the Field_Name and the evaluated Sentence for each expression
    Record to the global variable $Sentences as a list item
    @end-comment    

    @update
        @update-target-select "$Parameter:_locals/Field_Name"
        @update-source-select "execute-select-context()/Field_Name"

    @update
        @update-target-select "$Parameter:_locals/Sentence"
        @update-source-select "if(execute-select-context()/Use_Token = 'true', call('Core.EvalTokenInCurrentContext', execute-select-context()/Text_Expression/Text_Token, $Parameter:ContextStoreId), eval(execute-select-context()/Plain_Text_Expression))"

    @update
        @update-target-select "$Sentences"
        @update-source-select "$Parameter:_locals/Field_Name | $Parameter:_locals/Sentence"
        @update-source-type "Item"        

@end-execute


@execute "StorePreviousValues"

    @comment
    When the Token Mapping requires, capture the contents of the context store prior to publishing and evaluating the Token Mapping expressions
    These prior contents are captured to the global variable $Previous_Store
    The Token Mapping expressions can refer to these previous contents using that global variable like this: $Previous_Store/SomeNode
    @end-comment    

    @update
        @update-target-select "$Previous_Store"
        @update-source-select "''"

    @update
        @update-target-select "$Previous_Store"
        @update-source-select "store($Parameter:ContextStoreId)/*[1]"
        @update-source-type "Node"

@end-execute


@execute "SaveContextStore"

    @comment
    When the Token Mapping requires, publish the context store prior to evaluating the Token Mapping expressions
    @end-comment    

    @update
        @update-store-id "store-id()"
        @update-target-select "/store/Touch"
        @update-source-type "Node"
        @update-publish "Yes"
        
@end-execute




@execute "GenerateSentencesInCurrentContext"
@comment
Used to evaluate a series of expressions against the current store.

invoke-parameter "Mapping_ID"
    invoke-parameter-expression "Mapping ID in Core_Token_Mapping"
invoke-parameter "SentenceList"
    invoke-parameter-expression "Xpath to record Sentence List"
    invoke-parameter-direction "OUT"
@end-comment

    @comment
    Capture the Core_Token_Mapping information to local variable
    @end-comment    
    @update
        @update-target-select "$Parameter:_locals/Mapping"
        @update-source-select "store(queue('Core_Token_Mapping.Site_Admin_Q', concat('Mapping_ID = ', char(39), $Parameter:Mapping_ID, char(39)))/item[1]/StoreId)/Core_Token_Mapping"
        @update-source-type "Node"

    @comment
    Capture the pre-save value of the current store to a global variable $Previous_Store when required by the Token Mapping
    @end-comment    
    @invoke
        @invoke-execute "StorePreviousValues"
        @invoke-when "$Parameter:_locals/Mapping/Access_Previous_Version = 'true'"
        @invoke-parameter "ContextStoreId"
            @invoke-parameter-expression "store-id()"

    @comment
    Publish the current store when required by the Token Mapping
    @end-comment    
    @invoke
        @invoke-execute "SaveContextStore"
        @invoke-store-id "store-id()"
        @invoke-when "$Parameter:_locals/Mapping/Run_Workbook_For_Context = 'true'"

    @comment
    Prepare a global variable $Sentences to be used in the execute-select loop to store all Token Mapping sentences as list items as they are created
    @end-comment    
    @update
        @update-target-select "$Sentences"
        @update-source-select "''"

    @comment
    Invoke the sentence generation execute-select loop for each expression in the Token Mapping.  Record to the global variable $Sentences
    @end-comment    
    @invoke
        @invoke-execute "Core_Token_Mapping.GenerateSentenceTextsInCurrentContext"
        @invoke-parameter "ContextStoreId"
            @invoke-parameter-expression "store-id()"
        @invoke-parameter "TextExpressions"
            @invoke-parameter-expression "$Parameter:_locals/Mapping/Text_Expressions"

    @comment
    Record the full list of sentences generated to the OUT parameter SentenceList
    @end-comment    
    @update
        @update-target-select "$Parameter:SentenceList"
        @update-source-select "$Sentences"
        @update-source-type "Node"

    @comment
    Ensure the global variable $Previous_Store is cleared when finished
    @end-comment    
    @update
        @update-target-select "$Previous_Store"
        @update-source-type "Node"

    @comment
    Ensure the global variable $Sentences is cleared when finished
    @end-comment    
    @update
        @update-target-select "$Sentences"
        @update-source-type "Node"

@end-execute