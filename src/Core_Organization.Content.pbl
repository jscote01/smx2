﻿@mode "Main, Shared"
@title " "

[[Core_Organization.Content.Buttons]]

{label(x-large primary margin-lr-mini): "{{localize-token(Business_Type/Business_Type_Name)}} ^^Organization_Title|Information^^"}

@newline "Large"

@block
    @block-visible-when "call('Core.IsITUser') and is-not-blank(Business_Type) and is-blank(Organization_ID)"
    
    @block
        @block-visible-when "call('Core_Organization.DoesSequenceDefinitionExist')"
        
        @button "Generate_ID"
            @button-value "Generate_ID"
            @button-text "Auto-generate next Organization_ID from {{Business_Type/Business_Type_Name}} sequence"
            @button-action "Save,Validate"    
            @button-optional "Yes"
            @button-inline "Yes"
    @end-block
    
    @block
        @block-visible-when "not(call('Core_Organization.DoesSequenceDefinitionExist'))"
        
        @button "Generate_Default_ID"
            @button-value "Generate_Default_ID"
            @button-text "^^Auto-generate generic Organization_ID from default sequence|Auto-generate generic Organization_ID from default sequence^^"
            @button-action "Save,Validate"    
            @button-optional "Yes"
            @button-inline "Yes"
    @end-block
@end-block

@block
    @block-visible-when "is-not-blank(Business_Type)"
    
    @text "Organization_ID"
        @text-header "^^Organization ID|Organization ID^^:"
        @text-footer "^^Organization_ID_Message|Must be unique^^"
    
    @text "Organization_Name"
        @text-header "^^Organization Name|Organization Name^^:"
@end-block

@choice "Business_Type"
    @choice-style "DropDownLookup"
    @choice-header "^^Business Type|Business Type^^:"
    @choice-queue "Core_Admin_Business_Type.Organizations_Lookup_Q"
    @choice-select "Core_Admin_Business_Type/Business_Type_Id | Core_Admin_Business_Type/Business_Type_Name | Core_Admin_Business_Type/Organization_Status_Group"
    @choice-field "Business_Type_Name"
    @choice-updatable-when "true()"
    @choice-translate "Yes"    
@end-choice

@block
    @block-visible-when "is-not-blank(Business_Type)"
    
    @choice "Status"
        @choice-style "DropDownLookup"
        @choice-header "^^Status|Status^^:"
        @choice-queue "Core_Status.Master_Q"
        @choice-filter "Status_Group = {{Business_Type/Organization_Status_Group/Status_Group}}"
        @choice-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group | Core_Status/Status_Group_Sequence"
        @choice-field "Status_Name"
        @choice-updatable-when "true()"
        @choice-translate "Yes"        
    @end-choice

    @lookup "Parent_Organization"
        @lookup-header "^^Parent Organization|Parent Organization^^:"
        @lookup-allow-new-when "false()"
        @lookup-allow-navigate-when "call('Core.AllowLookupNavigation', 'Core_Organization')"
        @lookup-queue "Core_Organization.Active_Lookup_Q"
        @lookup-filter "StoreId <> {{store-id()}}"
        @lookup-select "Core_Organization/Organization_Name"
        @lookup-field "Organization_Name"
        @lookup-updatable-when "true()"
        @lookup-optional "Yes"
    
    @text "Trading_Name"
        @text-header "^^Trading Name|Trading Name^^:"
        @text-footer "^^Trading_Name_Message|Only required if different from Registered Name^^"
        @text-optional "Yes"
    
    @text "Phone_Number"
        @text-header "^^Phone|Phone^^:"
        @text-optional "Yes"
    
    @text "Fax_Number"
        @text-header "^^Fax|Fax^^:"
        @text-optional "Yes"
        
    @text "Latitude"
        @text-header "^^Latitude|Latitude^^:"
        @text-optional-when "true()"
        @text-type "Number"
        @text-mask "-99.999999"
        @text-validation-test "is-blank(Latitude) or (Latitude >= -90 and Latitude <= 90)"
        @text-validation-error "^^Latitude_Validation|Please enter a Latitude between -90 and 90 degrees.^^"

    @text "Longitude"
        @text-header "^^Longitude|Longitude^^:"
        @text-optional-when "true()"
        @text-type "Number"
        @text-mask "-999.999999"
        @text-validation-test "is-blank(Longitude) or (Longitude >= -180 and Longitude <= 180)"
        @text-validation-error "^^Longitude_Validation|Please enter a Longitude between -180 and 180 degrees.^^"
@end-block

@block
    @block-visible-when "is-not-blank(Business_Type)"  
    
    %Custom_Attribute_List%^^Org_Additional_Details|Additional Details^^||Details||%Custom_Attribute_Details_Section_UI% %Custom_Attribute_Details_Section_UI%%Custom_Attribute_List%
@end-block

@newline "Large"

@block "Addresses"
    @block-visible-when "is-not-blank(Business_Type)"
    
    
    @block
        @block-ui-style "clear span6"
        [[Shared_Core_Addresses.Physical_Address_With_Custom_Att]]
    @end-block

    @block
        @block-ui-style "span6"
        @block-visible-when "Mail_Add = 'true'"
        [[Shared_Core_Addresses.Mailing_Address_With_Custom_Att]]
    @end-block

    @block
        @block-ui-style "clear form-horizontal"
        [[Shared_Core_Addresses.Mailing_Handler]]
    @end-block
@end-block

@newline "Small"

@block
    @block-visible-when "is-not-blank(Business_Type)"
    @block-ui-style "clear span12"
    
    @block
        @block-ui-style "span6"
        
        %Custom_Attribute_List%^^Org_Additional_Info_Zone_1_Header|Additional Information^^||Zone 1||%Custom_Attribute_Zone_1_Section_UI% %Custom_Attribute_Zone_1_Section_UI%%Custom_Attribute_List%

        %Custom_Attribute_List%^^Org_Additional_Info_Zone_3_Header|Additional Information^^||Zone 3||%Custom_Attribute_Zone_3_Section_UI% %Custom_Attribute_Zone_3_Section_UI%%Custom_Attribute_List%
    @end-block
    
    @block
        @block-ui-style "span6"
        
        %Custom_Attribute_List%^^Org_Additional_Info_Zone_2_Header|Additional Information^^||Zone 2||%Custom_Attribute_Zone_2_Section_UI% %Custom_Attribute_Zone_2_Section_UI%%Custom_Attribute_List%

        %Custom_Attribute_List%^^Org_Additional_Info_Zone_4_Header|Additional Information^^||Zone 4||%Custom_Attribute_Zone_4_Section_UI% %Custom_Attribute_Zone_4_Section_UI%%Custom_Attribute_List%
    @end-block
@end-block


@block
    @block-visible-when "call('Core.IsITUser')"
    [[Core_Organization.Content.Calc_Fields]]
@end-block

[[Shared_Core_Store_Tracking_IT]]
[[Shared_Core_Account]]