﻿@display "No"

@comment
This function expects the Commodity Type Code as a param and returns True if Lab Results is required for the commodity type, False if it is optional, and NA if it is not applicable. Default is optional.
@end-comment
@function "SCQM.APLLabResults.IsLabResultsRequired"
@parameter "$CommodityType"
case(
    $CommodityType = '1', 'True',
    $CommodityType = '2', 'NA',
    'False'
)
@end-function

@comment
This function will return the Category Code of the specification for this APL. This function will only work in the scope of an APL Subprocess
@end-comment
@function "SCQM.APLLabResults.GetCategoryCode"
store(store(call('SCQM.APL.Subprocess.SpecificationStoreId', store-id()))/SCQM_Specification/Specification_Workbook/Product_Hierarchy_1/@refId)/SCQM_Admin_Product_Categories/Code
@end-function

@comment
Is the current user the given Approver
@end-comment
@function "SCQM.APLLabResults.IsApprover"
@parameter "$StoreId"
@parameter "$RoleId"
@parameter "$UserId"
boolean-from-string($UserId = call('Core.GetTaskAssignment', $StoreId, call('Core.GetRoleIdByID', $RoleId))/Assignee)
@end-function