﻿@display "No"

[[Shared_Core_Author_Collaboration.Default_Overrides]]

[[Shared_Core_Author_Collaboration.Content]]

[[Shared_Core_Author_Collaboration.Executes]]


@comment
    This is a shared Author Collaboration Process
...

    In order to impement this, you should copy the entire shared wiki set [Shared_Core_Approval_Process] into your content page where you would like the buttons.
    Block styling can be applied around them.
    You should also create an override page ABOVE the content wiki pages with a copy of the page [Shared_Core_Approval_Process.Default_Overrides].  NOTE: Make a copy, don't
    use this exact page as it is SHARED.  On the copy of the override page, you can customize all the settings (number of approvers, approval options, naming of buttons, etc.)
    for the application you are inserting it into.
    
    To begin the approval process, invoke the process as follows (in the current context):
        @invoke
            @invoke-execute "SAP_StartApprovalProcess"
    (NOTE: SAP = 'Shared Approval Process')
    
    When the process is complete, now(0) will be placed in the node /store/*[1]/End_Approval_Process.
    At the time the process is complete, the result of the approval process will be placed in node /store/*[1]/Approval_Process_Result.  This will be 'Approved', 'Returned', or 'Rejected' unless
    you change the text on the copied override page.  There will also be the value of 'Approved', 'Returned', or 'Rejected' (regardless of translations and overrides) 
    in /store/*[1]/Approval_Process_Result_Code.
    If the process is returned or rejected, the reason will be placed in the node /store/*[1]/Approval_Process_Return_Reject_Reason and the role that returned or rejected it will be placed in
    /store/*[1]/Approval_Process_Return_Reject_Role.
    No other nodes will be left in the store after the approval process is complete.
    
    To pick up your workflow on your app following the approval process, use the flags End_Approval_Process and/or Approval_Process_Result.
    
    To restart the approval process (for example, after the return or reject is dealt with) then you should simply invoke the execute StartApprovalProcess again.
    Upon restart, the nodes Start_Approval_Process, End_Approval_Process, Approval_Process_Result, and Approval_Process_Return_Reject_Reason will be cleared.
@end-comment