﻿@title "^^Microbiological|Microbiological^^"
    @title-ui-style "primary large margin-tb-mini"

@mode "Other"

{label(medium italic margin-tb-mini):'^^Spec_FPS_Microbiological_Note|If there are no Microbiological tests required for this specification type, please enter a note in the notes section below as to why, before submitting the specification^^.'}

@newline "Small"

@list "FPS_Microbiological"
    @list-style "Grid"
    @list-allow-order "No"
    @list-options "HeaderActions[ShowAdd:false, ShowDelete:false]"       
    @list-optional "Yes"
    @list-track "Yes"    
    
    @layout
    {
        "header": {
        	"columns": [
    			"<th>^^Standard & Test Method|Standard & Test Method^^</th>",
                "<th style='width:40px'>^^Min|Min^^</th>",
				"<th style='width:40px'>^^Target|Target^^</th>",
                "<th style='width:40px'>^^Max|Max^^</th>",
                "<th>^^Sample Size & Frequency|Sample Size & Frequency^^</th>",
				"<th style='min-width:270px;max-width:400px;'>^^Notes|Notes^^</th>"
			]
		},
		"body": {
			"itemRow": { 
				"columns": [
					"<td>{SCQM_Product_Standard} {SCQM_Test_Method} {Data_Type_Label}</td>", 
                    "<td>{Input_Label_1} {Min_1} {Input_Label_2} {Min_2} {Input_Label_3} {Min_3}</td>",
					"<td>{Input_Label_1_1} {Target_1} {Input_Label_2_1} {Target_2} {Input_Label_3_1} {Target_3}</td>",
                    "<td>{Input_Label_1_2} {Max_1} {Input_Label_2_2} {Max_2} {Input_Label_3_2} {Max_3}</td>",
                    "<td>{Sample_Size} {Frequency}</td>",
					"<td>{Notes}</td>"
				]
			}
		}
	}
	@end-layout    

    @choice "SCQM_Product_Standard"
        @choice-style "DropDownLookup"
        @choice-queue "SCQM_Product_Standard_Categories_Analyze.Active_Lookup_Q"
        @choice-select "SCQM_Product_Standard/Standard_Name"
        @choice-field "Standard_Name"
        @choice-filter "Standard_Type = 'Microbiological' AND Product_Category_StoreId = {{/store/*[1]/Specification_Workbook/Product_Hierarchy_1/@refId}}"
    @end-choice
    
    @block
    @block-visible-when "is-not-blank(SCQM_Product_Standard)"
        @choice "SCQM_Test_Method"
            @choice-style "DropDownLookup"
            @choice-queue "SCQM_Test_Method.Active_Lookup_Q"        
            @choice-select "SCQM_Test_Method/Test_Method_Name|SCQM_Test_Method/Number_Of_Inputs|SCQM_Test_Method/Input_1_Label|SCQM_Test_Method/Input_2_Label|SCQM_Test_Method/Input_3_Label|SCQM_Test_Method/Data_Validation|SCQM_Test_Method/Number_Of_Decimals"
            @choice-field "Test_Method_Name"
            @choice-filter "{{filter:concat('StoreId in ', char(40), char(39), join(queue('SCQM_Product_Standard_Test_Method.Master_Q', concat('SCQM_Product_Standard= ', char(39), SCQM_Product_Standard/@refId, char(39)))/item/SCQM_Test_Method, concat(char(39),char(44),char(39))), char(39), char(41))}}"
            @choice-translate "Yes"
        @end-choice 
    @end-block
    
    @label "Data_Type_Label"
        @label-template "{{case(SCQM_Test_Method/Data_Validation = 'Number', concat('Provide Min|Target|Max values as a number to ', SCQM_Test_Method/Number_Of_Decimals, ' decimals'), SCQM_Test_Method/Data_Validation = 'Integer', 'Provide Min|Target|Max values as a whole number (no decimals)', '')}}"
        @label-ui-style "{{if(is-blank(SCQM_Test_Method) or SCQM_Test_Method/Data_Validation = 'String', 'hide', 'italic')}}"
    
    @label "Input_Label_1"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, SCQM_Test_Method/Input_1_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, '', 'hide')}}"
            
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 0"
        @text "Min_1"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Min_1, SCQM_Test_Method/Data_Validation) and is-not-blank(Min_1)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block      
    
    @label "Input_Label_2"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, SCQM_Test_Method/Input_2_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, '', 'hide')}}"

    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 1"
        @text "Min_2"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Min_2, SCQM_Test_Method/Data_Validation) and is-not-blank(Min_2)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_3"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, SCQM_Test_Method/Input_3_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, '', 'hide')}}"
        
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 2"
        @text "Min_3"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Min_3, SCQM_Test_Method/Data_Validation) and is-not-blank(Min_3)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_1_1"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, SCQM_Test_Method/Input_1_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, '', 'hide')}}"
    
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 0"
        @text "Target_1"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Target_1, SCQM_Test_Method/Data_Validation) and is-not-blank(Target_1)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block    
    
    
    @label "Input_Label_2_1"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, SCQM_Test_Method/Input_2_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, '', 'hide')}}"
        
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 1"     
        @text "Target_2"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Target_2, SCQM_Test_Method/Data_Validation) and is-not-blank(Target_2)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_3_1"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, SCQM_Test_Method/Input_3_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, '', 'hide')}}"
    
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 2"
        @text "Target_3"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Target_3, SCQM_Test_Method/Data_Validation) and is-not-blank(Target_3)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_1_2"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, SCQM_Test_Method/Input_1_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 0, '', 'hide')}}"
        
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 0"
        @text "Max_1"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Max_1, SCQM_Test_Method/Data_Validation) and is-not-blank(Max_1)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_2_2"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, SCQM_Test_Method/Input_2_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 1, '', 'hide')}}"
    
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 1"     
        @text "Max_2"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Max_2, SCQM_Test_Method/Data_Validation) and is-not-blank(Max_2)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
    
    @label "Input_Label_3_2"
        @label-template "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, SCQM_Test_Method/Input_3_Label, '')}}"
        @label-ui-style "{{if(SCQM_Test_Method/Number_Of_Inputs > 2, '', 'hide')}}"
        
    @block
    @block-visible-when "SCQM_Test_Method/Number_Of_Inputs > 2"
        @text "Max_3"
            @text-columns "10"
            @text-validation-test "is-value-of-type(Max_3, SCQM_Test_Method/Data_Validation) and is-not-blank(Max_3)"
            @text-validation-error "^^Required field|Required field^^."
    @end-block
        
    @text "Sample_Size"
        @text-columns "20"

    @choice "Frequency"
        @choice-style "DropDownLookup"
        @choice-queue "Core_Custom_Dropdown_Item.Level_1_Lookup_Q"
        @choice-filter "Dropdown_Group_ID = 'Frequency'"        
        @choice-select "Core_Custom_Dropdown_Item/Item_Name"
        @choice-field "Item_Name"
        @choice-translate "Yes"
    @end-choice
    
    @text "Notes"
        @text-columns "80"        
        @text-rows "3"
        @text-ui-style "block"
        @text-optional "Yes"        

@end-list

@newline "Small"

@block
    @block-ui-style "form-horizontal"
    
    @text "Microbiological_General_Notes"
        @text-header "^^Notes|Notes^^:"
        @text-optional "Yes"
        @text-ui-style "clear span6"
        @text-rows "5"

@end-block

@comment
On Product Standard change, clear Test Method
On Test Method change, clear Min|Target|Max values
On entry of Min|Target|Max, format to correct number of decimals when approriate
@end-comment
@execute
    @execute-when "is-changed(FPS_Microbiological)"
    @execute-select "FPS_Microbiological/item"
    @execute-select-key "@id"
    
    @if "date-greater-than(execute-select-context()/SCQM_Product_Standard/@refTime, execute-select-context()/@modifiedOn) or is-blank(execute-select-context()/SCQM_Product_Standard)"

        @update
            @update-target-select "SCQM_Test_Method"
            @update-source-type "Node"

    @end-if
    
    @if "date-greater-than(execute-select-context()/SCQM_Test_Method/@refTime, execute-select-context()/@modifiedOn) or is-blank(execute-select-context()/SCQM_Test_Method)"
    
        @update
            @update-target-select "Min_1"
            @update-source-type "Node"
        @update
            @update-target-select "Target_1"
            @update-source-type "Node"
        @update
            @update-target-select "Max_1"
            @update-source-type "Node"
        @update
            @update-target-select "Min_2"
            @update-source-type "Node"
        @update
            @update-target-select "Target_2"
            @update-source-type "Node"
        @update
            @update-target-select "Max_2"
            @update-source-type "Node"
        @update
            @update-target-select "Min_3"
            @update-source-type "Node"
        @update
            @update-target-select "Target_3"
            @update-source-type "Node"
        @update
            @update-target-select "Max_3"
            @update-source-type "Node"
    
    @end-if
    
    @if "execute-select-context()/SCQM_Test_Method/Data_Validation = 'Number' and execute-select-context()/SCQM_Test_Method/Number_Of_Inputs > 0"

        @update
            @update-target-select "Min_1"
            @update-source-select "if(is-not-blank(execute-select-context()/Min_1), format(execute-select-context()/Min_1, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Target_1"
            @update-source-select "if(is-not-blank(execute-select-context()/Target_1), format(execute-select-context()/Target_1, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Max_1"
            @update-source-select "if(is-not-blank(execute-select-context()/Max_1), format(execute-select-context()/Max_1, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

    @end-if

    @if "execute-select-context()/SCQM_Test_Method/Data_Validation = 'Number' and execute-select-context()/SCQM_Test_Method/Number_Of_Inputs > 1"

        @update
            @update-target-select "Min_2"
            @update-source-select "if(is-not-blank(execute-select-context()/Min_2), format(execute-select-context()/Min_2, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Target_2"
            @update-source-select "if(is-not-blank(execute-select-context()/Target_2), format(execute-select-context()/Target_2, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Max_2"
            @update-source-select "if(is-not-blank(execute-select-context()/Max_2), format(execute-select-context()/Max_2, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

    @end-if

    @if "execute-select-context()/SCQM_Test_Method/Data_Validation = 'Number' and execute-select-context()/SCQM_Test_Method/Number_Of_Inputs > 2"

        @update
            @update-target-select "Min_3"
            @update-source-select "if(is-not-blank(execute-select-context()/Min_3), format(execute-select-context()/Min_3, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Target_3"
            @update-source-select "if(is-not-blank(execute-select-context()/Target_3), format(execute-select-context()/Target_3, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

        @update
            @update-target-select "Max_3"
            @update-source-select "if(is-not-blank(execute-select-context()/Max_3), format(execute-select-context()/Max_3, 'number', concat('{0:0.', case(execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=1, '0', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=2, '00', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=3, '000', execute-select-context()/SCQM_Test_Method/Number_Of_Decimals=4, '0000'), '}')), '')"

    @end-if

@end-execute