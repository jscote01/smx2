﻿@display "No"


@execute
    @execute-when "is-not-blank(argument('Spec_Created_From'))"
        
    @update
        @update-target-select "Original_Spec"
        @update-source-select "argument('Spec_Created_From')"
        @update-source-type "Lookup"
        @update-source-lookup-select "SCQM_Specification/Specification_Name | SCQM_Specification/Specification_Number"        
 
    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'AR_New', 'Approval Request')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group_Sequence"        
    
    @invoke
        @invoke-execute "Core_Sequential_Identifier.GetNextIdentifier"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "'SCQM_Approval_Request'"
        @invoke-parameter "Identifier"
            @invoke-parameter-expression "Request_Number"
            @invoke-parameter-direction "Out"

    @update
        @update-target-select "Requester"
        @update-source-select "user-id()"
        @update-source-type "Lookup"
        @update-source-lookup-select "User/Informal_Full_Name"    

    @update
        @update-target-select "Requested_Date"
        @update-source-select "now(0)"
        
@end-execute

@execute
    @execute-when "Action = 'Save'"
    
    @update
        @update-target-select "/store/*[1]/Processing"
        @update-source-select "'SpecVar_Request'"
    
    @update
        @update-target-select "/store/*[1]/Processing_Timestamp"
        @update-source-select "now(0)"

    @update 
        @update-target-select "$Spec_Creation_Method"
        @update-source-select "'Variation'"

    @update 
        @update-target-select "$Spec_Creation_Method_Name"
        @update-source-select "'Variation'"

    @update
        @update-target-select "$Spec_Created_From"
        @update-source-select "Original_Spec/@refId"
    
    @update
        @update-target-select "$Variation_Requested_From"
        @update-source-select "store-id()"

    @invoke
        @invoke-execute "Core_Executes.CreateAppStore"
        @invoke-parameter "AppName"
            @invoke-parameter-expression "'SCQM_Specification'"
        @invoke-parameter "Nodes"
            @invoke-parameter-expression "/"
        @invoke-parameter "StoreId"
            @invoke-parameter-expression "Spec_Variation_Created"
        @invoke-parameter-direction "Out"

    @update
        @update-store-id "Spec_Variation_Created"
        @update-target-select "/store/*[1]/Touch"
        @update-source-type "Node"
        @update-publish "Yes"
        
    @update
        @update-target-select "Action"
        @update-source-type "Node"
        
@end-execute

@execute "Begin_Request"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'AR_In_Progress', 'Approval Request')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name | Core_Status/Status_Group_Sequence"        
        
    @update
        @update-target-select "Spec_Variation_Created"
        @update-source-select "Spec_Variation_Created"
        @update-source-type "Lookup"
        @update-source-lookup-select "SCQM_Specification/Specification_Name | SCQM_Specification/Specification_Number"  
        @update-publish "Yes"        
        
    @invoke
        @invoke-execute "Update_Org_Lists"
        
@end-execute

@execute "Update_Org_Lists"

    @update
        @update-target-select "OrgList"
        @update-source-select "call('SCQM.SpecVarReq.GetUniqueOrgList', store-id())"

    @update
        @update-target-select "OrgLocList"
        @update-source-select "call('SCQM.SpecVarReq.GetUniqueOrgLocList', store-id())"
        @update-publish "Yes"        
            
@end-execute

@comment
Invoked by Workflow when Variation ends up Discontinued
@end-comment
@execute "Record_Discontinue_Reason"
    
    @update
        @update-target-select "Discontinue_Reason"
        @update-source-select "eval('/store/SCQM_Specification/Specification_Workbook/Discontinue_Reason', store(Spec_Variation_Created/@refId)/*[1])"
        
@end-execute