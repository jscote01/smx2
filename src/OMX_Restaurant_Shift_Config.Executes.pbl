﻿@display "No"

@execute
    @execute-when "is-blank(Organization_Location)"

    @comment
        Little trick to make sure that the lookup to organization gets set correctly. 
        The store of the shift config is composed of the store id of the organization location
    @end-comment

    @update
        @update-target-select "Organization_Location"
        @update-source-select "split(store-id(), '_')[2]"
        @update-source-type "Lookup"        

@end-execute


@comment
If a new shift config is created, we need to create the first OMX_Management_Log for the location if it doesn't already have one.
The flag Create_Management_Log is set by a propagator running in OMX_Management_Log when the Shift Config record is first created.
@end-comment
@execute
    @execute-when "is-not-blank(Create_Management_Log) and is-blank(Management_Log_Created)"

    @update
        @update-target-select "Management_Log_Created"
        @update-source-select "now(0)"

    @if "queue-count('OMX_Management_Log.Master_Q', 'Core_Organization_Location = {{Organization_Location}}') = 0"
    
        @invoke
            @invoke-execute "OMX_Management_Log.Create"
            @invoke-parameter "Core_Organization_Location"
                @invoke-parameter-expression "Organization_Location/@refId"
            @invoke-parameter "Log_Date"
                @invoke-parameter-expression "date()"
    
    @end-if

@end-execute