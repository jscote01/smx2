﻿@title "Discontinue Relationship"
    @title-ui-style "hide"

@mode "DiscontinueGroup"

@block
    @block-visible-when "is-blank(DisAction)"
    
    @button "DisAction"
        @button-value "PreDiscontinue"
        @button-text "^^Discontinue|Discontinue^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-danger margin-lr-mini pull-right"

@end-block

@block
    @block-visible-when "is-not-blank(DisAction)"
    
    @link "False_Button"
        @link-text "^^Discontinue|Discontinue^^"
        @link-ui-style "btn btn-danger margin-lr-mini disabled white pull-right"
        
    @newline

    @block
        @block-visible-when "DisAction[.='PreDiscontinue' or .='GenerateLetter' or .='GenerateLetterFailed']"
    
        @text "Discontinue_Reason"
            @text-header "^^Reason for Discontinuation|Reason for Discontinuation^^:"
            @text-rows "3"
            @text-read-only-when "is-not-blank(Discontinuation_Letter)"
            @text-validation-test "is-not-blank(Discontinue_Reason) or DisAction = 'PreDiscontinue'"
            @text-validation-error "^^A discontinuation reason is required|A discontinuation reason is required^^"
    
        @text "Discontinue_Date"
            @text-header "^^Discontinuation Date|Discontinuation Date^^:"
            @text-type "Date"
            @text-read-only-when "is-not-blank(Discontinuation_Letter)"
            @text-validation-test "(is-not-blank(Discontinue_Date) and call('SCQM.APLGroup.DaysUntil', Discontinue_Date) >= 0) or DisAction = 'PreDiscontinue'"
            @text-validation-error "^^Discontinue_Date_Footer|A discontinuation date is required and must be today or later^^"
    
    @end-block    
    
    @button "DisAction"
        @button-value "Cancel"
        @button-text "^^Cancel|Cancel^^"
        @button-optional "Yes"
        @button-inline "Yes"
        @button-ui-style "btn-warning margin-lr-mini pull-right"
        @button-action "Save, Validate"

    @block
        @block-visible-when "is-blank(Discontinuation_Letter)"
    
    @button "DisAction"
        @button-value "GenerateLetter"
        @button-text "^^Generate Discontinuation Letter|Generate Discontinuation Letter^^"
        @button-optional "Yes"
        @button-inline "Yes"        
        @button-ui-style "btn-warning margin-lr-mini pull-right"
        @button-action "Save, Validate"

    @end-block
    
    @block
        @block-visible-when "is-not-blank(Discontinuation_Letter) and is-blank(Discontinue_Letter_Sent)"
        
        @button "DisAction"
            @button-value "SendLetter"
            @button-text "^^Send Discontinuation Letter|Send Discontinuation Letter^^"
            @button-optional "Yes"
            @button-inline "Yes"        
            @button-ui-style "btn-success margin-lr-mini pull-right"
            @button-action "Save, Validate"
    
        @block
            @block-ui-style "clear"
            
            @newline "Small"
        
            @link "{{artifact-url(Discontinuation_Letter/@id, domain(), '', if(contains(domain(), '.local'), '8080', '0'))}}"
                @link-text "^^Download PDF|Download PDF^^"
                @link-href "Yes"
                @link-ui-style "btn"
                @link-icon "icon-download-alt medium"
                @link-align "Left"
            
            @button "DisAction"
                @button-value "RemoveLetter"
                @button-text " "
                @button-text-ui-style "hide"
                @button-icon "icon-remove danger"
                @button-optional "Yes"
                @button-inline "Yes"
                @button-ui-style "btn-flat pointer"
                @button-action "Save, Validate"
    
            @block
                {label(block small margin-tb-mini): concat('^^Last Generated|Last Generated^^: ', localize-timestamp(Discontinuation_Letter/@date))}
            @end-block
    
        @end-block
    
    @end-block

@end-block

@block
    @block-visible-when "false()"

    @report "Discontinuation_Letter"
        @report-text "^^Discontinuation Letter|Discontinuation Letter^^"
        @report-label "^^Discontinuation Letter|Discontinuation Letter^^"
        @report-when "is-blank(Discontinuation_Letter) and is-not-blank(Produce_Discontinue_Letter)"
        @report-template-expression "call('SCQM.APL.GetDiscontinueTemplate')"
        @report-optional "Yes"

        @include
            @include-store-id "Org_Location/@refId"

        @include
            @include-store-id "Org/@refId"

        @include
            @include-store-id "call('Core.GetOrgPrimaryContact', Org/@refId)/StoreId"

        @include
            @include-store-id "Specification/@refId"

        @include
            @include-queue "SCQM_Approved_Product_List.Master_Q"
            @include-queue-filter "APL_Group = {{store-id()}}"

    @end-report

@end-block









@comment

@block
    @block-visible-when "is-blank(Discontinue_Letter_Sent)"
    @block-ui-style "well well-danger-light well-small inverse"

    @block
        @block-visible-when "is-blank(Discontinuation_Letter)"
        @icon "icon-warning-sign medium danger"
        {label(bold margin-lr-mini):'^^APL_Generate_Discontinuation_Instructions|To generate the Discontinuation Letter, select the "Generate Discontinuation Letter" button below.^^'}
    @end-block
    
    @block
        @block-visible-when "is-not-blank(Discontinuation_Letter) and is-blank(Discontinue_Letter_Sent)"
        @icon "icon-warning-sign medium danger"
        {label(bold margin-lr-mini):'^^APL_Send_Discontinuation_Instructions|After reviewing the Discontinuation Letter, select the "Send Discontinuation Letter" button to finish this step of the process.^^'}
    @end-block

@end-block

@end-comment