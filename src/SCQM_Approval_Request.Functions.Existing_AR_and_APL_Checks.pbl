﻿@comment
==============================================================================
FUNCTIONS USED TO GATHER INFORMATION ABOUT EXISTING APPROVAL REQUESTS
==============================================================================
@end-comment

@function "SCQM.CountryfromCountryRegion"
@parameter "$CountryRegion"
store($CountryRegion)/Core_Admin_Country_Region/Country/@refId
@end-function


@comment
Given a Specification, Facility, and Country, return the StoreId of the LAST created Approval Request (or null if there is not one) OTHER than the current Approval Request
  #param StoreId $Spec Store ID of a specification in SCQM_Specification
  #param StoreId $Fac Store ID of a facility in Core_Organization_Location
  #param StoreId $Country Store ID of a Country from Core_Admin_Country_Region
  #return StoreId of the LAST created Approval Request
@end-comment
@function "SCQM.AR.ExistingARStoreId"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
find-store-id('SCQM_Approval_Request.Master_Q', concat('Specification = ', char(39), $Spec, char(39), ' and Existing_OrgLoc = ', char(39), $Fac, char(39), ' and Country_of_Export = ', char(39), $Country, char(39), ' and StoreId != ', char(39), store-id(), char(39)), 'Submitted DESC')
@end-function

@comment
Given an Approval Request StoreId, return the status code
  #param StoreId $AR Store ID
  #return String Status of the Approval Request
@end-comment
@function "SCQM.AR.ExistingARStatus"
@parameter "$AR"
store($AR)/*[1]/Status/Status_Code
@end-function

@comment
Given an Approval Request StoreId, return the status name
  #param StoreId $AR Store ID
  #return String Status of the Approval Request
@end-comment
@function "SCQM.AR.ExistingARStatusName"
@parameter "$AR"
store($AR)/*[1]/Status/Status_Name
@end-function



@comment
==============================================================================
FUNCTIONS USED TO GATHER INFORMATION ABOUT EXISTING APLS
==============================================================================
@end-comment

@comment
Given a Specification, Facility, and Country, return the StoreId of the APL (or null if there is not one) OTHER than the current Approval Request
  #param StoreId $Spec Store ID of a specification in SCQM_Specification
  #param StoreId $Fac Store ID of a facility in Core_Organization_Location
  #param StoreId $Country Store ID of a Country from Core_Admin_Country_Region (needs to be converted to a store in Core_Admin_Country)
  #return StoreId of the APL
@end-comment
@function "SCQM.APL.ExistingAPLStoreId"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
find-store-id('SCQM_Approved_Product_List.Master_Q', concat('APL_Group = ', char(39), call("SCQM.APLGroup.ExistingAPLGroupStoreId", $Spec, $Fac), char(39), ' and Country = ', char(39), call('SCQM.CountryfromCountryRegion', $Country), char(39), ' and StoreId != ', char(39), store-id(), char(39)), 'Created_Date DESC')
@end-function

@comment
Given an APL StoreId, return the status code
  #param StoreId $APL Store ID
  #return String Status_Code of the APL
@end-comment
@function "SCQM.APL.ExistingAPLStatus"
@parameter "$APL"
store($APL)/*[1]/Status/Status_Code
@end-function

@comment
Given an APL StoreId, return the status name
  #param StoreId $APL Store ID
  #return String Status_Name of the APL
@end-comment
@function "SCQM.APL.ExistingAPLStatusName"
@parameter "$APL"
store($APL)/*[1]/Status/Status_Name
@end-function

@comment
Given an APL store id, return the APL Group store id
@end-comment
@function "SCQM.APL.APLGroup_from_APL"
@parameter "$APL"
store($APL)/*[1]/APL_Group/@refId
@end-function


@comment
Return the StoreId of the existing APL that contains the given Specification and Organization and the first country has not yet been approved.
This means there is an APL Group for the given Spec and Facility where the Status_Code is APL_Draft, APL_In_Process, or APL_Conditionally_Approved.
  #param StoreId $Spec Store ID of a specification
  #param StoreId $Fac Store ID of a facility
  #return StoreID of an APL Group that exists for the given Spec and Facility in status Draft, In Process, or Conditionally Approved.
  EX: call('SCQM.APL.InFlightAPLStoreId', Specification/@refId, Core_Organization_Location/@refId)
@end-comment
@function "SCQM.APL.InFlightAPLStoreId"
@parameter "$Spec"
@parameter "$Fac"
find-store-id('SCQM_Approved_Product_List_Group.Master_Q', concat('Org_Location = ', char(39), $Fac, char(39), ' and Specification = ', char(39), $Spec, char(39), ' and Status_Code in (', char(39), 'APL_Draft', char(39), char(44), char(39), 'APL_In_Process', char(39), char(44), char(39), 'APL_Conditionally_Approved', char(39), ')'), 'Created_Date ASC')
@end-function

@comment
Return the Country Name of the first country to be approved for the given APL Group.
    #param StoreId $APLGroup Store ID of an APL Group
    #return String Country_Name of the FIRST country in the APL Group
    EX: call('SCQM.APL.FirstCountryNameofAPLGroup', store-id())
@end-comment
@function "SCQM.APL.FirstCountryNameofAPLGroup"
@parameter "$APLGroup"
store(find-store-id('SCQM_Approved_Product_List.Master_Q', concat('APL_Group = ', char(39), $APLGroup, char(39), ' and Status_Code not in (', char(39), 'APL_Discarded', char(39), char(44), char(39), 'APL_Discontinued', char(39), ')'), 'Created_Date ASC'))/*[1]/Country/Country_Name
@end-function



@comment
==============================================================================
FUNCTIONS USED TO DETERMINE MESSAGING TO PRESENT IF AR OR APL ALREADY EXIST
==============================================================================
The following is the given scenarios when messaging should be presented in Stage 0 and Stage 1 of the Approval Request process:

Refer to APL and disallow Submit (Existing_APL_Approved):
If APL in APL_Conditionally_Approved, APL_Approved,  APL_Pending_Discontinuation, APL_Pending_Archive, APL_Pending_Archive_In_Process
"Attention: The Facility is already approved to produce this Product for <Country Name>.  Select the "Existing APL Record" link below to go to the record."

Refer to APL and disallow Submit (Existing_APL_In_Progress):
If APL in APL_Draft, APL_In_Process
"Attention: An Approval Request for the Facility to produce this Product for <Country Name> is already in progress.  Select the "Existing APL Record" link below to go to the record."

Refer to AR and disallow Submit (Existing_AR_In_Progress):
If AR in AR_New, AR_In_Progress
"Attention: An Approval Request for the Facility to produce this Product for <Country Name> is already in progress.  Select the "Existing Approval Request" link below to go to the record."

Warning and allow Submit (show LATEST Reason, APL first, then AR) (Existing_APL_AR_Discontinued):
If AR in AR_Approved and APL in APL_Discarded, APL_Discontinued
If AR in AR_Discarded, AR_Discontinued
"Please Note: A request for the Facility to produce this Product for <Country Name> was started and then discontinued for the following reason: <Reason>"
"You may continue with the request by selecting the "Submit" button."
@end-comment

@comment
Given a Specification, Facility, and Country, return a string to indicate the state of any existing AR or APLs
  #param StoreId $Spec Store ID of a specification in SCQM_Specification
  #param StoreId $Fac Store ID of a facility in Core_Organization_Location
  #param StoreId $Country Store ID of a Country from Core_Admin_Country_Region
  #return 'Existing_APL_Approved', 'Existing_APL_In_Progress', 'Existing_AR_In_Progress', 'Existing_APL_AR_Discontinued', or ''
@end-comment
@function "SCQM.Existing_AR_APL_Check"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
case(
    call('SCQM.APL.ExistingAPLStatus', call('SCQM.APL.ExistingAPLStoreId', $Spec, $Fac, $Country))[.='APL_Conditionally_Approved' or .='APL_Approved' or .='APL_Pending_Discontinuation' or .='APL_Pending_Archive' or .='APL_Pending_Archive_In_Process'], 'Existing_APL_Approved', 
    call('SCQM.APL.ExistingAPLStatus', call('SCQM.APL.ExistingAPLStoreId', $Spec, $Fac, $Country))[.='APL_Draft' or .='APL_In_Process'], 'Existing_APL_In_Progress', 
    call('SCQM.AR.ExistingARStatus', call('SCQM.AR.ExistingARStoreId', $Spec, $Fac, $Country))[.='AR_New' or .='AR_In_Progress'], 'Existing_AR_In_Progress', 
    (call('SCQM.AR.ExistingARStatus', call('SCQM.AR.ExistingARStoreId', $Spec, $Fac, $Country)) = 'AR_Approved' or is-blank(call('SCQM.AR.ExistingARStatus', call('SCQM.AR.ExistingARStoreId', $Spec, $Fac, $Country)))) and call('SCQM.APL.ExistingAPLStatus', call('SCQM.APL.ExistingAPLStoreId', $Spec, $Fac, $Country))[.='APL_Discarded' or .='APL_Discontinued'], 'Existing_APL_AR_Discontinued', 
    call('SCQM.AR.ExistingARStatus', call('SCQM.AR.ExistingARStoreId', $Spec, $Fac, $Country))[.='AR_Discarded' or .='AR_Discontinued'], 'Existing_APL_AR_Discontinued', 
    ''
)
@end-function

@comment
Return true if Stage 0 product selection is valid to approve
@end-comment
@function "SCQM.Stage0ProductApprovalAvailable"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
(call('SCQM.Existing_AR_APL_Check', $Spec, $Fac, $Country) = 'Existing_APL_AR_Discontinued' or call('SCQM.Existing_AR_APL_Check', $Spec, $Fac, $Country) = '') and call('SCQM.APLGroup.APLGroupIsValid', call('SCQM.APLGroup.ExistingAPLGroupStoreId', $Spec, $Fac))
@end-function

@comment
Return true if Stage 1 product selection is valid to approve
@end-comment
@function "SCQM.Stage1ProductApprovalAvailable"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
((call('SCQM.Existing_AR_APL_Check', $Spec, $Fac, $Country) = 'Existing_APL_AR_Discontinued' or call('SCQM.Existing_AR_APL_Check', $Spec, $Fac, $Country) = '') and is-blank(call('SCQM.APL.InFlightAPLStoreId', $Spec, $Fac)) and call('SCQM.Spec.SpecIsValid', $Spec) and call('SCQM.APLGroup.APLGroupIsValid', call('SCQM.APLGroup.ExistingAPLGroupStoreId', $Spec, $Fac)) and is-blank(/store/*[1]/Stage1ProductApprovalAvailableOverride) = 'True') or /store/*[1]/Stage1ProductApprovalAvailableOverride = 'Pass'
@end-function

@comment
Given a Specification, Facility, and Country, return the Discard/Discontinue reason from the APL if it exists or alternatively the AR.
@end-comment
@function "SCQM.Existing_AR_APL_Discontinue_Reason"
@parameter "$Spec"
@parameter "$Fac"
@parameter "$Country"
coalesce(
    store(call('SCQM.APL.ExistingAPLStoreId', Specification/@refId, Core_Organization_Location/@refId, Country/@refId))/*[1]/Discontinue_Reason, 
    store(call('SCQM.AR.ExistingARStoreId', Specification/@refId, Core_Organization_Location/@refId, Country/@refId))/*[1]/Return_Reject_Reasons/item[last()]/Reason
)
@end-function



@comment
==============================================================================
FUNCTIONS USED TO CHECK ON EXISTING SPECIFICATIONS
==============================================================================
@end-comment

@comment
Spec is invalid if it in is the following Statuses:
    SPC_Pending_Archive
    SPC_Archived
    SPC_Discarded
    SPC_Discontinued
@end-comment
@function "SCQM.Spec.SpecIsValid"
@parameter "$SpecStoreId"
((store($SpecStoreId)/*[1]/Status/Status_Code != 'SPC_Pending_Archive' and store($SpecStoreId)/*[1]/Status/Status_Code != 'SPC_Archived' and store($SpecStoreId)/*[1]/Status/Status_Code != 'SPC_Discarded' and store($SpecStoreId)/*[1]/Status/Status_Code != 'SPC_Discontinued') or is-blank($SpecStoreId))
@end-function



@comment
==============================================================================
FUNCTIONS USED TO GATHER INFORMATION ABOUT EXISTING APL GROUPS
==============================================================================
@end-comment

@comment
Given a Specification and Facility, return the StoreId of the LATEST APL Group (or null if it does not exist)
  #param StoreId $Spec Store ID of a specification
  #param StoreId $Fac Store ID of a facility
  #return StoreId of the APL Group
  EX: call('SCQM.APLGroup.ExistingAPLGroupStoreId', Specification/@refId, Core_Organization_Location/@refId)
@end-comment
@function "SCQM.APLGroup.ExistingAPLGroupStoreId"
@parameter "$Spec"
@parameter "$Fac"
find-store-id('SCQM_Approved_Product_List_Group.Master_Q', concat('Org_Location = ', char(39), $Fac, char(39), ' and Specification = ', char(39), $Spec, char(39)), 'Created_Date DESC')
@end-function

@comment
Given an APL Group StoreId, return the status code
  #param StoreId $APLGroup Store ID
  #return String Status Code of the APL Group
  EX: call('SCQM.APLGroup.ExistingAPLGroupStatus', APL_Group/@refId)
@end-comment
@function "SCQM.APLGroup.ExistingAPLGroupStatus"
@parameter "$APLGroupStoreId"
store($APLGroupStoreId)/*[1]/Status/Status_Code
@end-function

@comment
Given an APL Group StoreId, return the status name
  #param StoreId $APLGroup Store ID
  #return String Status Name of the APL Group
  EX: call('SCQM.APLGroup.ExistingAPLGroupStatusName', APL_Group/@refId)
@end-comment
@function "SCQM.APLGroup.ExistingAPLGroupStatusName"
@parameter "$APLGroupStoreId"
store($APLGroupStoreId)/*[1]/Status/Status_Name
@end-function

@comment
APL Group is invalid if it in is the following Statuses:
    APL_Pending_Archive
    APL_Archived
    APL_Pending_Discontinuation
@end-comment
@function "SCQM.APLGroup.APLGroupIsValid"
@parameter "$APLGroupStoreId"
((store($APLGroupStoreId)/*[1]/Status/Status_Code != 'APL_Pending_Archive' and store($APLGroupStoreId)/*[1]/Status/Status_Code != 'APL_Archived' and store($APLGroupStoreId)/*[1]/Status/Status_Code != 'APL_Pending_Discontinuation') or is-blank($APLGroupStoreId))
@end-function