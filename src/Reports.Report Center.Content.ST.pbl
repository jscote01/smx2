﻿@title " "
@title-ui-style "hide"
@mode "ST"

@block "Filters"
    @block-ui-style "clear span4 well well-mini bcolor-white"
 
     @block
        @block-ui-style "pull-right margin-tb-mini form-inline"
        
            @button "Apply_Changes"
                @button-value "Apply"
                @button-text "^^Apply|Apply^^"
                @button-optional "Yes"
                @button-show-value "No"
                @button-accept "Yes"
                @button-action "Save, Reload"
                @button-ui-style "btn-success pull-right" 
                @button-inline "Yes"   
                @button-accept "Yes"    
            
            @button "Reset_Filters"
                @button-value "Reset"
                @button-text "^^Clear|Clear^^"
                @button-optional "Yes"
                @button-show-value "No"
                @button-accept "No"
                @button-action "Save"
                @button-ui-style "btn pull-right margin-lr-sm"
                @button-inline "Yes"

    @end-block  
    
    @block
        @block-ui-style "form-horizontal"
        
        @choice "Chart_Type"
            @choice-style "RadioButtonList"
            @choice-header "^^Use Default or Custom Chart|Use Default or Custom Chart^^:"
            @choice-default-expression "'Default'"
            @choice-optional "No"
            @choice-columns "2"
            @choice-postback "Yes"            
                        
                @option "Default"
                    @option-text "Default"
                    @option-value "Default"
                
                @option "Custom"
                    @option-text "Custom"
                    @option-value "Custom"
                                
        @end-choice        
        
        @choice "Region"
            @choice-style "DropDownList"
            @choice-header "^^Region|Region^^:"
            @choice-optional "Yes"
            @choice-ui-style "width-auto"  
            @choice-postback "Yes"            
            
            @options
                @options-queue "Core_Admin_Region.Active_Lookup_Q"
                @options-text "Region_Name"
                @options-value "StoreId"
        
        @end-choice 
        
        @lookup "Supplier"
            @lookup-header "^^Supplier|Supplier^^:"
            @lookup-optional "Yes"
            @lookup-queue "Reports_Supplier_Filter.Suppliers"
            @lookup-field "Organization_Name"
            @lookup-select "Core_Organization/Organization_Name"
            @lookup-allow-new-when "false()"
            @lookup-allow-navigate-when "false()"   
            @lookup-ui-style "width-auto"            
        
        @choice "Year"
            @choice-style "DropDownList"
            @choice-header "Year:"
            @choice-optional "Yes"            
            @choice-postback "Yes" 
            
            @options
                @options-select "queue('Reports_Periods_Filter.Periods')/item"
                @options-text "Period"
                @options-value "Period"
                
        @end-choice  
            
        @list "Chart_1_Configuration"
            @list-sort "Ascending"
            @list-style "List"
            @list-header "^^Chart 1 Configuration|Chart 1 Configuration^^:"
            @list-header-ui-style "bold"            
            @list-optional "Yes"
            @list-ui-style "no-item-background"
            @list-limit "1"    
            @list-item-summary "Specification"
            @list-allow-order "No"            
        
            @choice "Specification"
                @choice-style "DropDownList"
                @choice-header "^^Specification Chart One|Specification (Chart One)^^:"
                @choice-optional "No"
                @choice-postback "Yes"            
                @choice-ui-style "width-auto"            
                
                @options
                    @options-select "queue('Reports_Specifications_Filter.Specifications_Filter')/item"
                    @options-text "Specification_Name"
                    @options-value "Specification_Name"
                
            @end-choice
            
            @block
                @block-visible-when "false()"
            
                @text "Standards_Count"
                    @text-header "Standards Count:"
                    @text-optional "Yes"
                    @text-expression "queue-count('Reports_Standards_Filter.Standards', 'Specification_Name = {{Specification}}')"                
                
            @end-block            
        
            @block
                @block-visible-when "is-not-blank(Specification)"
            
                @list "Standards_List"
                    @list-sort "Ascending"
                    @list-style "List"
                    @list-header "^^Standards List|Standards - Select up to 10^^:"
                    @list-optional "No"
                    @list-validation-test "is-blank(Standards_List/item[11])"
                    @list-validation-error "^^Select up to 10|Select up to 10 Standards^^."        
                    @list-ui-style "no-item-background"        
                    @list-limit "10"                    
                    @list-item-summary "Standard"
                    @list-allow-order "No" 
                    
                    @choice "Standard"
                        @choice-style "DropDownList"
                        @choice-header "^^Standard|Standard^^:"
                        @choice-optional "No"
                        @choice-postback "Yes"            
                        @choice-ui-style "width-auto"  
                        
                        @options
                            @options-select "queue('Reports_Standards_Filter.Standards', 'Specification_Name = {{context()/../../Specification}}')/item[Standard_Item_Name = context()/Standard or not(Standard_Item_Name = store()/*/Chart_1_Configuration/item[1]/Standards_List/item/Standard)]"
                            @options-text "Standard_Item_Name"
                            @options-value "Standard_Item_Name"
                        
                    @end-choice     
                    
                @end-list
            
            @end-block
            
        @end-list

@comment
DISABLED

        @list "Chart_2_Configuration"
            @list-sort "Ascending"
            @list-style "List"
            @list-header "^^Chart 2 Configuration|Chart 2 Configuration^^:"
            @list-header-ui-style "bold"            
            @list-optional "Yes"
            @list-ui-style "no-item-background"
            @list-limit "1"            
            @list-item-summary "Specification" 
            @list-allow-order "No"            
            
            @choice "Specification"
                @choice-style "DropDownList"
                @choice-header "^^Specification Chart One|Specification (Chart Two)^^:"
                @choice-optional "No"
                @choice-postback "Yes"            
                @choice-ui-style "width-auto"            
                
                @options
                    @options-select "queue('Reports_Specifications_Filter.Specifications_Filter')/item"
                    @options-text "Specification_Name"
                    @options-value "Specification_Name"
                
            @end-choice
            
            @block
                @block-visible-when "false()"
            
                @text "Standards_Count"
                    @text-header "Standards Count:"
                    @text-optional "Yes"
                    @text-expression "queue-count('Reports_Standards_Filter.Standards', 'Specification_Name = {{Specification}}')"                
                
            @end-block  
 
            @block
                @block-visible-when "is-not-blank(Specification)"
        
                @list "Standards_List"
                    @list-sort "Ascending"
                    @list-style "List"
                    @list-header "^^Standards List|Standards - Select up to 10^^:"
                    @list-optional "No"
                    @list-validation-test "is-blank(Standards_List/item[11])"
                    @list-validation-error "^^Select up to 10|Select up to 10 Standards^^."        
                    @list-ui-style "no-item-background"   
                    @list-limit "10"                                                    
                    @list-item-summary "Standard"
                    @list-allow-order "No" 
                    
                    @choice "Standard"
                        @choice-style "DropDownList"
                        @choice-header "^^Standard|Standard^^:"
                        @choice-optional "No"
                        @choice-postback "Yes"            
                        @choice-ui-style "width-auto"            
                    
                        @options
                            @options-select "queue('Reports_Standards_Filter.Standards', 'Specification_Name = {{context()/../../Specification}}')/item[Standard_Item_Name = context()/Standard or not(Standard_Item_Name = store()/*/Chart_2_Configuration/item[1]/Standards_List/item/Standard)]"
                            @options-text "Standard_Item_Name"
                            @options-value "Standard_Item_Name"
                        
                    @end-choice          
        
                @end-list
            
            @end-block
            
        @end-list
        
        @list "Chart_3_Configuration"
            @list-sort "Ascending"
            @list-style "List"
            @list-header "^^Chart 3 Configuration|Chart 3 Configuration^^:"
            @list-header-ui-style "bold"            
            @list-optional "Yes"
            @list-ui-style "no-item-background"
            @list-limit "1"            
            @list-item-summary "Specification"  
            @list-allow-order "No"            
            
            @choice "Specification"
                @choice-style "DropDownList"
                @choice-header "^^Specification Chart One|Specification (Chart Three)^^:"
                @choice-optional "No"
                @choice-postback "Yes"            
                @choice-ui-style "width-auto"            
                
                @options
                    @options-select "queue('Reports_Specifications_Filter.Specifications_Filter')/item"
                    @options-text "Specification_Name"
                    @options-value "Specification_Name"
                
            @end-choice

            @block
                @block-visible-when "false()"
            
                @text "Standards_Count"
                    @text-header "Standards Count:"
                    @text-optional "Yes"
                    @text-expression "queue-count('Reports_Standards_Filter.Standards', 'Specification_Name = {{Specification}}')"                
                
            @end-block  
        
            @block
                @block-visible-when "is-not-blank(Specification)"
            
                @list "Standards_List"
                    @list-sort "Ascending"
                    @list-style "List"
                    @list-header "^^Standards List|Standards - Select up to 10^^:"
                    @list-optional "No"
                    @list-validation-test "is-blank(Standards_List/item[11])"
                    @list-validation-error "^^Select up to 10|Select up to 10 Standards^^."        
                    @list-ui-style "no-item-background"  
                    @list-limit "10"                               
                    @list-item-summary "Standard"
                    @list-allow-order "No" 
                    
                    @choice "Standard"
                        @choice-style "DropDownList"
                        @choice-header "^^Standard|Standard^^:"
                        @choice-optional "No"
                        @choice-postback "Yes"            
                        @choice-ui-style "width-auto"            
                    
                        @options
                            @options-select "queue('Reports_Standards_Filter.Standards', 'Specification_Name = {{context()/../../Specification}}')/item[Standard_Item_Name = context()/Standard or not(Standard_Item_Name = store()/*/Chart_3_Configuration/item[1]/Standards_List/item/Standard)]"
                            @options-text "Standard_Item_Name"
                            @options-value "Standard_Item_Name"
                        
                    @end-choice          
        
                @end-list
            
            @end-block 
               
        @end-list
        
        @list "Chart_4_Configuration"
            @list-sort "Ascending"
            @list-style "List"
            @list-header "^^Chart 4 Configuration|Chart 4 Configuration^^:"
            @list-header-ui-style "bold"            
            @list-optional "Yes"
            @list-ui-style "no-item-background"
            @list-limit "1"            
            @list-item-summary "Specification" 
            @list-allow-order "No"            
            
            @choice "Specification"
                @choice-style "DropDownList"
                @choice-header "^^Specification Chart One|Specification (Chart Four)^^:"
                @choice-optional "No"
                @choice-postback "Yes"            
                @choice-ui-style "width-auto"            
                
                @options
                    @options-select "queue('Reports_Specifications_Filter.Specifications_Filter')/item"
                    @options-text "Specification_Name"
                    @options-value "Specification_Name"
                
            @end-choice

            @block
                @block-visible-when "false()"
            
                @text "Standards_Count"
                    @text-header "Standards Count:"
                    @text-optional "Yes"
                    @text-expression "queue-count('Reports_Standards_Filter.Standards', 'Specification_Name = {{Specification}}')"                
                
            @end-block  
       
            @block
                @block-visible-when "is-not-blank(Specification)"
            
                @list "Standards_List"
                    @list-sort "Ascending"
                    @list-style "List"
                    @list-header "^^Standards List|Standards - Select up to 10^^:"
                    @list-optional "No"
                    @list-validation-test "is-blank(Standards_List/item[11])"
                    @list-validation-error "^^Select up to 10|Select up to 10 Standards^^."        
                    @list-ui-style "no-item-background"  
                    @list-limit "10"                                
                    @list-item-summary "Standard"
                    @list-allow-order "No" 
                    
                    @choice "Standard"
                        @choice-style "DropDownList"
                        @choice-header "^^Standard|Standard^^:"
                        @choice-optional "No"
                        @choice-postback "Yes"            
                        @choice-ui-style "width-auto"            
                    
                        @options
                            @options-select "queue('Reports_Standards_Filter.Standards', 'Specification_Name = {{context()/../../Specification}}')/item[Standard_Item_Name = context()/Standard or not(Standard_Item_Name = store()/*/Chart_4_Configuration/item[1]/Standards_List/item/Standard)]"
                            @options-text "Standard_Item_Name"
                            @options-value "Standard_Item_Name"
                        
                    @end-choice          
        
                @end-list
    
            @end-block
                
        @end-list

@end-comment
            
    @end-block        

@end-block

@block
    @block-visible-when "is-not-blank(Show_Chart_Timestamp) and is-not-blank(Chart_1_Configuration/item[1]) and Chart_Type = 'Default'"    
    @block-ui-style "span8"

<div style="text-align:center;">

@comment
display:inline-block;
@end-comment

<div class="padding-tb-mini padding-lr-mini margin-lr-mini margin-tb-mini" style="border:3px solid #e8e8e8;border-radius:20px;padding:6px;">

@embed "Reports_Sigma_Tracking_Alpha.Sigma_Tracking_Alpha_Chart"
    @embed-store-id "{{store-id()}}"
    @embed-postback-refresh "No"
    @embed-view-group "Charts"    
    
</div>

@newline

@embed "Reports_Sigma_Tracking_Alpha.Sigma_Tracking_Alpha_Default"
    @embed-store-id "{{store-id()}}"
    @embed-postback-refresh "No"
    @embed-view-group "Charts"    
    
</div>

@end-block

@block
    @block-visible-when "is-not-blank(Show_Chart_Timestamp) and is-not-blank(Chart_1_Configuration/item[1]) and Chart_Type = 'Custom'"    
    @block-ui-style "span8"

<div style="text-align:center;">

@embed "Reports_Sigma_Tracking_Alpha.Sigma_Tracking_Alpha_Queue"
    @embed-store-id "{{store-id()}}"
    @embed-postback-refresh "No"
    @embed-view-group "Charts"    
    
</div>

@end-block
