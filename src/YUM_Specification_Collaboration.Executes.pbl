﻿@display "No"

@comment
INITIAL CREATION OF COLLABORATION REQUEST
@end-comment
@execute
    @execute-when "is-not-blank(Request_Save)"

    @update
        @update-target-select "Request_Save"
        @update-source-type "Node"

    @update
        @update-target-select "YUM_Specification_Workbook"
        @update-source-select "''"

    @update
        @update-target-select "Author"
        @update-source-select "user-id()"
        @update-source-type "Lookup"
        @update-source-lookup-select "User/User_Name | User/Informal_Full_Name"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Requested', 'Approval States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name"

    @invoke
        @invoke-execute "Core_Task_Assignment.StartTaskAssignment"
        @invoke-parameter "Context"
            @invoke-parameter-expression "store-id()"
        @invoke-parameter "Summary"
            @invoke-parameter-expression "concat(Author/Informal_Full_Name, ' has requested your help completing the specification and provided the following notes: ', Collaboration_Note)"
        @invoke-parameter "Action_Item_Text"
            @invoke-parameter-expression "concat('A Specification requires your collaboration: ', SCQM_Specification/Specification_Name, ' (', SCQM_Specification/Specification_Number, ')')"
        @invoke-parameter "Role"
            @invoke-parameter-expression "call('Core.GetRoleIdByID', 'Spec_Collaboration_Submitter')"
		@invoke-parameter "OrganizationId"
			@invoke-parameter-expression "Core_Organization/@refId"
        @invoke-parameter "Use_Current_Context"
            @invoke-parameter-expression "'true'"
		@invoke-parameter "AssignmentId"
		    @invoke-parameter-expression "Submitter_Task_Assignment"
			@invoke-parameter-direction "Out"

    @update
        @update-target-select "Collaboration_Author"
        @update-source-select "store(Submitter_Task_Assignment)/*[1]/Assignee"
        @update-source-type "Lookup"
        @update-source-lookup-select "User/User_Name | User/Informal_Full_Name"

    @invoke
        @invoke-execute "Specification_Collaboration_Notification"
        @invoke-parameter "EmailtoSend"
            @invoke-parameter-expression "'Collaboration_Author'"

    @update
        @update-target-select "Requested_On"
        @update-source-select "now(0)"

    @invoke
        @invoke-execute "CopyTopics"    

@end-execute



@comment
COLLABORATION REQUEST SUBMITTED
@end-comment
@execute
    @execute-when "is-not-blank(Collaboration_Action) and Collaboration_Action = 'Submit'"

    @update
        @update-target-select "Collaboration_Action"
        @update-source-type "Node"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Submitted', 'Approval States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name"

    @invoke
        @invoke-execute "Core_Task_Assignment.MarkTaskCompleted"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "Submitter_Task_Assignment"

    @invoke
        @invoke-execute "Core_Task_Assignment.StartTaskAssignment"
        @invoke-parameter "Context"
            @invoke-parameter-expression "store-id()"
        @invoke-parameter "Summary"
            @invoke-parameter-expression "concat(Collaboration_Author/Informal_Full_Name, ' has submitted the specification collaboration and it is ready for your review')"
        @invoke-parameter "Action_Item_Text"
            @invoke-parameter-expression "concat('A Specification collaboration has been returned and is ready for your review: ', SCQM_Specification/Specification_Name, ' (', SCQM_Specification/Specification_Number, ')')"
        @invoke-parameter "Assignee"
            @invoke-parameter-expression "Author/@refId"
        @invoke-parameter "Use_Current_Context"
            @invoke-parameter-expression "'true'"
		@invoke-parameter "AssignmentId"
		    @invoke-parameter-expression "Author_Task_Assignment"
			@invoke-parameter-direction "Out"

    @invoke
        @invoke-execute "Specification_Collaboration_Notification"
        @invoke-parameter "EmailtoSend"
            @invoke-parameter-expression "'Collaboration_Return'"

    @update
        @update-target-select "Submitted_On"
        @update-source-select "now(0)"

@end-execute



@comment
COLLABORATION REQUEST PROCESSED
@end-comment
@execute
    @execute-when "is-not-blank(Collaboration_Action) and Collaboration_Action = 'Process'"

    @update
        @update-target-select "Collaboration_Action"
        @update-source-type "Node"

    @update
        @update-target-select "Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Approved', 'Approval States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name"

    @update
        @update-target-select "Status/Status_Name"
        @update-source-select "'Processed'"

    @invoke
        @invoke-execute "Core_Task_Assignment.MarkTaskCompleted"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "Author_Task_Assignment"

    @update
        @update-target-select "Processed_On"
        @update-source-select "now(0)"

@end-execute



@comment
MERGE COLLABORATION TOPICS INTO SPECIFICATION
@end-comment
@execute
    @execute-when "is-not-blank(Promote_Action)"

    @comment
    Clean up
    @end-comment

    @update
        @update-target-select "Promote_Action"
        @update-source-type "Node"

    @comment
    Record details to Collaboration record
    @end-comment    

    @update
        @update-target-select "Promoted_On"
        @update-source-select "now(0)"

    @update
        @update-target-select "Promoted_Topics"
        @update-source-select "join(Promote_Topics/*/@text, ', ')"

    @comment
    Construct a node containing all the topic nodes to be promoted to Specification
    @end-comment    

    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Topics_To_Promote"
        @update-source-type "Node"

    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Topics_To_Promote"
        @update-source-select "''"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'Formula_Ingredients'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Formula_Ingredients'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'Packaging'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Packaging'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'Storage'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Storage'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'FPS_Chemical'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Chemical'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'FPS_Microbiological'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Microbiological'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'FPS_Physical'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Physical'"
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"

    @invoke
        @invoke-execute "PrepareToPromoteTopics"
        @invoke-when "boolean-from-string(contains(/store/YUM_Specification_Collaboration/Promote_Topics, 'FPS_Sensory'))"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Sensory'"    
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"
            @invoke-parameter-direction "InOut"


    @comment
    Promote the constructed node to the Specification
    @end-comment    

    @invoke
        @invoke-execute "PromoteTopicsToSpecification"    
        @invoke-parameter "Topics_To_Promote"
            @invoke-parameter-expression "/store/YUM_Specification_Collaboration/Topics_To_Promote"


    @comment
    Create a promotion history for the Specification (list item) and copy to the Specification
    @end-comment    

    @update
        @update-target-select "$Local:Promotion_History/Promoted_Topics"
        @update-source-select "Promoted_Topics"
    @update
        @update-target-select "$Local:Promotion_History/Promoted_On"
        @update-source-select "Promoted_On"
    @update
        @update-target-select "$Local:Promotion_History/Promoted_From"
        @update-source-select "store-id()"
    @update
        @update-target-select "$Local:Promotion_History/Promoted_By"
        @update-source-select "user-id()"
        @update-source-type "Lookup"
        @update-source-lookup-select "User/User_Name | User/Informal_Full_Name"        
    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Promotion_History"
        @update-source-type "Node"
    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Promotion_History"
        @update-source-select "$Local:Promotion_History/*"
        @update-source-type "Item"        

    @if "is-blank(store(SCQM_Specification/@refId)/SCQM_Specification/Promotion_History)"
        @update
            @update-store-id "SCQM_Specification/@refId"
            @update-target-select "/store/SCQM_Specification/Promotion_History"
            @update-source-select "''"
    @end-if

    @update
        @update-store-id "SCQM_Specification/@refId"
        @update-target-select "/store/SCQM_Specification/Promotion_History"
        @update-source-select "/store/YUM_Specification_Collaboration/Promotion_History/*"
        @update-source-type "Subnodes[Append: Yes]"        


    @comment
    Clean up
    @end-comment
    
    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Topics_To_Promote"
        @update-source-type "Node"

    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Promotion_History"
        @update-source-type "Node"

    @update
        @update-target-select "/store/YUM_Specification_Collaboration/Promote_Topics"
        @update-source-type "Node"

@end-execute



@comment
CANCEL ALL COLLABORATIONS
@end-comment
@execute "CancelAllCollaboratios"
    @execute-select "queue('YUM_Specification_Collaboration.Master_Q', concat('SCQM_Specification = ', char(39), $Parameter:Spec_StoreId, char(39), ' AND Status_Name IN ', char(40), char(39), 'Requested', char(39), char(44), char(39), 'Submitted', char(39), char(41)))/item"
    @execute-select-key "StoreId"

    @update
        @update-target-select "$Local:Submitter_Task_Assignment"
        @update-source-select "store(execute-select-context()/StoreId)/YUM_Specification_Collaboration/Submitter_Task_Assignment"

    @update
        @update-target-select "$Local:Author_Task_Assignment"
        @update-source-select "store(execute-select-context()/StoreId)/YUM_Specification_Collaboration/Author_Task_Assignment"

    @update
        @update-target-select "$Local_Status_Change/Status"
        @update-source-select "call('Core.GetStatusStoreId', 'Discarded', 'Approval States')"
        @update-source-type "Lookup"
        @update-source-lookup-select "Core_Status/Status_Code | Core_Status/Status_Name"
        
    @update
        @update-target-select "$Local_Status_Change/Status/Status_Name"
        @update-source-select "'Discontinued'"

    @update
        @update-target-select "$Local_Status_Change/Discontinued_On"
        @update-source-select "now(0)"

    @update
        @update-store-id "execute-select-context()/StoreId"
        @update-target-select "/store/YUM_Specification_Collaboration"
        @update-source-select "$Local_Status_Change/*"
        @update-source-type "Subnodes"

    @invoke
        @invoke-execute "Core_Task_Assignment.MarkTaskCancelled"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "$Local:Submitter_Task_Assignment"    
    
    @invoke
        @invoke-execute "Core_Task_Assignment.MarkTaskCancelled"
        @invoke-parameter "AssignmentId"
            @invoke-parameter-expression "$Local:Author_Task_Assignment"    

@end-execute



@comment
COPY TOPICS FROM SPEC TO COLLABORATION WORKBOOK
@end-comment

@execute "CopyTopics"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Header'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'Formula_Ingredients')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Formula_Ingredients'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'Packaging')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Packaging'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'Storage')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'Storage'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'FPS_Chemical')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Chemical'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'FPS_Microbiological')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Microbiological'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'FPS_Physical')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Physical'"

    @invoke
        @invoke-execute "CopyTopic"
        @invoke-when "Copy_Or_Blank = 'Yes' and contains(Request_Topics, 'FPS_Sensory')"
        @invoke-parameter "Topic_Name"
            @invoke-parameter-expression "'FPS_Sensory'"

@end-execute

@execute "CopyTopic"
@comment
IN: Topic_Name
@end-comment

    @update
        @update-target-select "YUM_Specification_Workbook"
        @update-source-select "store(SCQM_Specification/@refId)/SCQM_Specification/Specification_Workbook/*[@meta-topic = $Parameter:Topic_Name]"
        @update-source-type "Subnodes"

@end-execute



@comment
PROMOTE TOPICS FROM COLLABORATION WORKBOOK TO SPECIFICATION
@end-comment

@execute "PrepareToPromoteTopics"
@comment
IN: Topic_Name
IN OUT: Topics_To_Promote
@end-comment

    @update
        @update-target-select "$Parameter:Topics_To_Promote"
        @update-source-select "/store/YUM_Specification_Collaboration/YUM_Specification_Workbook/*[@meta-topic = $Parameter:Topic_Name]"
        @update-source-type "Subnodes"

@end-execute

@execute "PromoteTopicsToSpecification"
@comment
IN: Topics_To_Promote
@end-comment

    @update
        @update-store-id "SCQM_Specification/@refId"
        @update-target-select "/store/SCQM_Specification/Specification_Workbook"
        @update-source-select "$Parameter:Topics_To_Promote/*"
        @update-source-type "Subnodes"

@end-execute


@comment
SEND NOTIFICATIONS
IN: EmailtoSend
@end-comment
@execute "Specification_Collaboration_Notification"

    @update
        @update-target-select "Email_to_Send"
        @update-source-select "$Parameter:EmailtoSend"
    
    @invoke
        @invoke-execute "Core_Notifications_Template.Standard_Notification_Execute_Workflow"    

    @update
        @update-target-select "Email_to_Send"
        @update-source-type "Node"        

@end-execute
